<?php

function krucial_custom_image_url($atts) {
	extract(shortcode_atts(array(
		'custom_image_url' => '',
		'custom_image_caption' => '',
		'custom_image_link_download'=>'',
		'custom_image_link' => '',
		'custom_image_link_text' => '',
		'custom_image_link_url' => '',
		'custom_image_link_target' => '',
	), $atts));

	ob_start();

	$uniqid = uniqid( 'custom_image_' );

	// Image URL
	$custom_image = wp_get_attachment_image_src( $custom_image_url, 'full' )[0];

	if(esc_html($custom_image_link) == true){
	?>
	<div class="custom-image" id="<?php echo esc_attr( $uniqid ) ?>">
		<a href="<?php echo esc_url($custom_image_link_url);?>" title="<?php echo esc_html($custom_image_link_text);?>" target="<?php echo esc_html($custom_image_link_target);?>">
			<img src="<?php echo esc_url($custom_image); ?>">
		</a>
		<div class="img-caption">
			<?php echo esc_html($custom_image_caption); ?>
			<?php
				if(esc_html($custom_image_link_download) == true){
			?>
			<div>
				<i class="fas fa-download"></i>
				<a href="<?php echo esc_url($custom_image); ?>" 
					title="<?php echo do_shortcode("[post_title]")." - ".esc_html($custom_image_caption); ?>" 
					target="_blank">
					<?php echo esc_html__('BAIXAR IMAGEM', 'oculis'); ?>
				</a>
			</div>
			<?php
				}else{}
			?>
		</div>
	</div>
	<?php
	}else{
	?>
	<div class="custom-image" id="<?php echo esc_attr( $uniqid ) ?>">
		<img src="<?php echo esc_url($custom_image); ?>">
		<div class="img-caption">
			<?php echo esc_html($custom_image_caption); ?>
			<?php
				if(esc_html($custom_image_link_download) == true){
			?>
			<div>
				<i class="fas fa-download"></i>
				<a href="<?php echo esc_url($custom_image); ?>" 
					title="<?php echo do_shortcode("[post_title]")." - ".esc_html($custom_image_caption); ?>" 
					target="_blank">
					<?php echo esc_html__('BAIXAR IMAGEM', 'oculis'); ?>
				</a>
			</div>
			<?php
				}else{}
			?>
		</div>
	</div>	
	<?php
	}
	
	/*
	<div class="custom-image" id="<?php echo esc_attr( $uniqid ) ?>">
		<a href="<?php echo esc_url($custom_image); ?>" title="<?php echo esc_url($custom_image_caption); ?>">
			<img src="<?php echo esc_url($custom_image); ?>">
		</a>
		<!--
			<div class="img-caption"><?php //echo esc_html($custom_image_caption); ?></div>
		!-->
	</div>
	*/

	$result = ob_get_contents();
	ob_end_clean();
	return $result;
}
add_shortcode('krucial-custom-image', 'krucial_custom_image_url');