<?php

/**
 * Get the sidebar
 */

function krucial_get_sidebar() {
    global $krucial_options;

    // Blog post sidebar
    if (is_single() && (get_post_type() == 'post') && is_active_sidebar('blog-sidebar')) {
        if ($krucial_options['blog_layout'] !== 'no-sidebar') {
            echo '<div id="secondary" class="widget-area sidebar">';
                dynamic_sidebar('blog-sidebar');
            echo '</div>';
        }
    }

    // Blog page sidebar
    if (is_home() && is_active_sidebar('blogpage-sidebar')) {
        if ($krucial_options['blogpage_layout'] !== 'no-sidebar') {
            echo '<div id="secondary" class="widget-area sidebar">';
                dynamic_sidebar('blogpage-sidebar');
            echo '</div>';
        }
    }

    // Archives sidebar
    if (is_archive() && is_active_sidebar('archive-sidebar')) {
        if ($krucial_options['archive_layout'] !== 'no-sidebar') {
            echo '<div id="secondary" class="widget-area sidebar">';
            dynamic_sidebar('archive-sidebar');
            echo '</div>';
        }
    }
}


/**
 * Add the correct bodyclass
 */

// Blog post class
function krucial_sidebar_layout_blogpost() {
	global $krucial_options;

	if (is_active_sidebar('blog-sidebar')) {
		switch ( $krucial_options['blog_layout'] ) {
			case 'left-sidebar':
				return 'left-sidebar';
			case 'no-sidebar':
				return 'no-sidebar';
			default:
				return 'right-sidebar';
		}
	} else {
		return 'no-sidebar';
	}
}

// Blog page class
function krucial_sidebar_layout_blogpage() {
    global $krucial_options;

    if (is_active_sidebar('blogpage-sidebar')) {
	    switch ( $krucial_options['blogpage_layout'] ) {
		    case 'left-sidebar':
			    return 'left-sidebar';
		    case 'no-sidebar':
			    return 'no-sidebar';
		    default:
			    return 'right-sidebar';
	    }
    } else {
    	return 'no-sidebar';
    }
}

// Archives class
function krucial_sidebar_layout_archives() {
	global $krucial_options;

	if (is_active_sidebar('archive-sidebar')) {
		switch ( $krucial_options['archive_layout'] ) {
			case 'left-sidebar':
				return 'left-sidebar';
			case 'no-sidebar':
				return 'no-sidebar';
			default:
				return 'right-sidebar';
		}
	} else {
		return 'no-sidebar';
	}
}

// WooCommerce archives class
function krucial_sidebar_wc_archives() {
	global $krucial_options;

	if (is_active_sidebar('woo-archive-sidebar')) {
		switch ( $krucial_options['shop_layout'] ) {
			case 'left-sidebar':
				return 'left-sidebar';
			case 'no-sidebar':
				return 'no-sidebar';
			default:
				return 'right-sidebar';
		}
	} else {
		return 'no-sidebar';
	}
}