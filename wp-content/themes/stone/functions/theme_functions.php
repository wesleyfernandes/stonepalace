<?php

/*--------------------------------------------------------------
# Add body classes
--------------------------------------------------------------*/
function krucial_add_body_classes( $classes ) {
    global $krucial_options;

	if (function_exists('get_field')) {
		// Check that it's not the blog
		if ( !is_home() && !is_singular('post') && !is_archive() && !is_search() && !is_404() ) {
			// Enable transparent header
			if ( get_field( 'enable_transparent_header' ) ) {
				$classes[] = 'transparent_header';
			}
			// Enable full-width header
			if ( get_field( 'enable_full-width_header' ) ) {
				$classes[] = 'fullwidth_header';
			}
			// Remove default page padding
			if ( get_field( 'remove_default_page_padding' ) ) {
				$classes[] = 'remove_page_padding';
			}
			// For custom post type archives
			if ( is_post_type_archive( array( 'testimonials', 'staff', 'projects', 'ambientes', 'services' ) ) ) {
				$classes[] = 'transparent_header';
				$classes[] = 'fullwidth_header';
			}

			if ( $krucial_options['enable_sticky_header'] === '1' ) {
				$classes[] = 'sticky-header';
			}
		} else {
			$classes[] = 'fullwidth_header remove_page_padding blog_header_tp';
		}
	}

	if ( $krucial_options['header_style'] === 'header-style-4' ) {
		$classes[] = 'sidebar-menu';
	}

	if ( $krucial_options['toolbar_switch'] === '1' ) {
		$classes[] = 'toolbar-active';
	}

	if (class_exists('Krucial_Theme_Functionality')) {
	    $classes[] = 'functionality-active';
	}

	return $classes;
}
add_filter( 'body_class', 'krucial_add_body_classes');


/*--------------------------------------------------------------
# Output default page header
--------------------------------------------------------------*/
function krucial_render_page_header() { ?>
    <div class="page-header">
        <div class="container">
			<?php krucial_render_breadcrumbs(); ?>
            <h1>
				<?php
				// If it's the blog page
				if ( is_home() && !is_front_page() ) {
					echo wp_title('');
				}

				// If it's the home page and blog page
                elseif ( is_home() && is_front_page() ) {
				    bloginfo('name');
				}

				// If it's a search
                elseif ( is_search() ) {
					$search_query = get_search_query();
					echo esc_html__( 'Search Results for: ', 'oculis' ) . esc_html( $search_query );
				}

				// If it's an archive
                elseif ( is_archive() ) {
					the_archive_title();
				}

				else {
					the_title();
				}
				?>
            </h1>
        </div>
    </div>
	<?php
}


/*--------------------------------------------------------------
# Output breadcrumbs
--------------------------------------------------------------*/
function krucial_render_breadcrumbs() {
	if ( function_exists( 'bcn_display' ) && !get_post_meta( get_the_ID(), 'disable_breadcrumbs', true ) ) { ?>
        <div class="breadcrumbs">
			<?php bcn_display(); ?>
        </div>
	<?php }
}