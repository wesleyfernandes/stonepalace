<?php

function krucial_register_sidebars() {
	// Blogpost sidebar
	register_sidebar(array(
			'name'          => esc_html__('BlogPost Sidebar', 'oculis'),
			'description'   => esc_html__('Sidebar for all blog posts.', 'oculis'),
			'id'            => 'blog-sidebar',
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget'  => '</aside>',
			'before_title'  => '<h5 class="widget_title">',
			'after_title'   => '</h5>',
		)
	);

	// Blogpage sidebar
	register_sidebar(array(
			'name'          => esc_html__('BlogPage Sidebar', 'oculis'),
			'description'   => esc_html__('Sidebar for the main blog page.', 'oculis'),
			'id'            => 'blogpage-sidebar',
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget'  => '</aside>',
			'before_title'  => '<h5 class="widget_title">',
			'after_title'   => '</h5>',
		)
	);

	// Archives sidebar
	register_sidebar(array(
			'name'          => esc_html__('Archives Sidebar', 'oculis'),
			'description'   => esc_html__('Sidebar for the archives.', 'oculis'),
			'id'            => 'archive-sidebar',
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget'  => '</aside>',
			'before_title'  => '<h5 class="widget_title">',
			'after_title'   => '</h5>',
		)
	);

	// WooCommerce archives sidebar
	register_sidebar(array(
			'name'          => esc_html__('Shop Sidebar', 'oculis'),
			'description'   => esc_html__('Sidebar for all WooCommerce archives.', 'oculis'),
			'id'            => 'woo-archive-sidebar',
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget'  => '</aside>',
			'before_title'  => '<h5 class="widget_title">',
			'after_title'   => '</h5>',
		)
	);

	// Footer column 1
	register_sidebar(array(
			'name'          => esc_html__('Footer Column 1', 'oculis'),
			'description'   => esc_html__('This is the first column for the footer.', 'oculis'),
			'id'            => 'footer-col-1',
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget'  => '</aside>',
			'before_title'  => '<h5 class="widget_title">',
			'after_title'   => '</h5>',
		)
	);

	// Footer column 2
	register_sidebar(array(
			'name'          => esc_html__('Footer Column 2', 'oculis'),
			'description'   => esc_html__('This is the second column for the footer.', 'oculis'),
			'id'            => 'footer-col-2',
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget'  => '</aside>',
			'before_title'  => '<h5 class="widget_title">',
			'after_title'   => '</h5>',
		)
	);

	// Footer column 3
	register_sidebar(array(
			'name'          => esc_html__('Footer Column 3', 'oculis'),
			'description'   => esc_html__('This is the third column for the footer.', 'oculis'),
			'id'            => 'footer-col-3',
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget'  => '</aside>',
			'before_title'  => '<h5 class="widget_title">',
			'after_title'   => '</h5>',
		)
	);

	// Footer column 4
	register_sidebar(array(
			'name'          => esc_html__('Footer Column 4', 'oculis'),
			'description'   => esc_html__('This is the fourth column for the footer.', 'oculis'),
			'id'            => 'footer-col-4',
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget'  => '</aside>',
			'before_title'  => '<h5 class="widget_title">',
			'after_title'   => '</h5>',
		)
	);

	if (class_exists('Krucial_Theme_Functionality')) {

		// Footer column 5
		register_sidebar( array(
				'name'          => esc_html__( 'Footer Column 5', 'oculis' ),
				'description'   => esc_html__( 'This is the fifth column for the footer.', 'oculis' ),
				'id'            => 'footer-col-5',
				'before_widget' => '<aside id="%1$s" class="widget %2$s">',
				'after_widget'  => '</aside>',
				'before_title'  => '<h5 class="widget_title">',
				'after_title'   => '</h5>',
			)
		);

		// Footer column 6
		register_sidebar( array(
				'name'          => esc_html__( 'Footer Column 6', 'oculis' ),
				'description'   => esc_html__( 'This is the sixth column for the footer.', 'oculis' ),
				'id'            => 'footer-col-6',
				'before_widget' => '<aside id="%1$s" class="widget %2$s">',
				'after_widget'  => '</aside>',
				'before_title'  => '<h5 class="widget_title">',
				'after_title'   => '</h5>',
			)
		);

	}
}
add_action('widgets_init', 'krucial_register_sidebars');

?>