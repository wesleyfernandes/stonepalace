<?php

/*-------------------------------------------------------------------
  Return theme options data
--------------------------------------------------------------------*/
if ( !function_exists( 'krucial_option' ) ) {
    function krucial_option( $id, $fallback = false, $param = false ) {
        if ( isset( $_GET['krucial_'.$id] ) ) {
            if ( '-1' == $_GET['krucial_'.$id] ) {
                return false;
            } else {
                return $_GET['krucial_'.$id];
            }
        } else {
            global $krucial_options;
            if ( $fallback == false ) $fallback = '';
            $output = ( isset($krucial_options[$id]) && $krucial_options[$id] !== '' ) ? $krucial_options[$id] : $fallback;
            if ( !empty($krucial_options[$id]) && $param ) {
                $output = $krucial_options[$id][$param];
            }
        }
        return $output;
    }
}


/*-------------------------------------------------------------------
  Check if post has (pingbacks, trackbacks, etc)
--------------------------------------------------------------------*/
function krucial_if_post_has( $type, $post_id ) {
	$comments = get_comments('status=approve&type=' . $type . '&post_id=' . $post_id );
	$comments = separate_comments( $comments );
	return 0 < count( $comments[ $type ] );
}


/*-------------------------------------------------------------------
  Get template link
--------------------------------------------------------------------*/
if ( !function_exists('krucial_get_template_link' ) ) {
    function krucial_get_template_link( $template ) {
        $args = array(
            'meta_key' => '_wp_page_template',
            'meta_value' => $template
        );
        $pages = get_pages($args);
        if( $pages ) {
            $add_link = get_permalink( $pages[0]->ID );
        } else {
            $add_link = esc_url( home_url('/') );
        }
        return $add_link;
    }
}


/*-------------------------------------------------------------------
  WooCommerce
--------------------------------------------------------------------*/

/**
 * Products Per Row
 */
if ( !function_exists( 'krucial_shop_num_columns' ) ) {
    function krucial_shop_num_columns() {
        return 3;
    }
}
add_filter( 'loop_shop_columns', 'krucial_shop_num_columns' );


/**
 * Number of products per page.
 */
function new_loop_shop_per_page($cols) {
  $cols = 9;
  return $cols;
}
add_filter( 'loop_shop_per_page', 'new_loop_shop_per_page', 20 );


/**
 * Remove upsells at cart hook.
 */
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_upsell_display', 15 );


/**
 * Remove WooCommerce stylesheets.
 */
add_filter( 'woocommerce_enqueue_styles', '__return_empty_array' );


/**
 * Manage WooCommerce styles and scripts.
 */
function krucial_woocommerce_script_cleaner() {
    if ( class_exists( 'WooCommerce' ) ) {

        // Remove the generator tag
        remove_action( 'get_the_generator_html', 'wc_generator_tag', 10 );
        remove_action( 'get_the_generator_xhtml', 'wc_generator_tag', 10 );

        // Dequeue pretty photo
        wp_dequeue_style( 'woocommerce_prettyPhoto_css' );
        wp_dequeue_script( 'prettyPhoto' );
        wp_dequeue_script( 'prettyPhoto-init' );
    }
}
add_action( 'wp_enqueue_scripts', 'krucial_woocommerce_script_cleaner', 99 );


/*-------------------------------------------------------------------
  Edit Excerpts Dots
--------------------------------------------------------------------*/
function krucial_excerpt_dots() {
    return '...';
}
add_filter( 'excerpt_more', 'krucial_excerpt_dots' );

// Edit the excerpt length
function krucial_excerpt_length() {
	return 15;
}
add_filter( 'excerpt_length', 'krucial_excerpt_length', 999 );


/*-------------------------------------------------------------------
  Load contact form 7 only on pages where the shortcode exists
--------------------------------------------------------------------*/
add_filter( 'wpcf7_load_js', '__return_false' );
add_filter( 'wpcf7_load_css', '__return_false' );

function krucial_load_contact_form( $content ) {
	if ( has_shortcode( $content, 'contact-form-7' ) ) {
		if ( function_exists( 'wpcf7_enqueue_scripts' ) ) {
			wpcf7_enqueue_scripts();
		}
		if ( function_exists( 'wpcf7_enqueue_styles' ) ) {
			wpcf7_enqueue_styles();
		}
	}

	return $content;
}
add_filter( 'the_content', 'krucial_load_contact_form' );


/*-------------------------------------------------------------------
  Show products for WooCommerce search, and posts for normal search
--------------------------------------------------------------------*/
function krucial_search_woocommerce_only( $query ) {
	if (function_exists('is_woocommerce')) {
		// Show products only for WooCommerce search
		if ( !is_admin() && is_search() && $query->is_search ) {
			$query->set( 'post_type', 'product' );
		}
		// Show posts only for blog search
		if ( !is_admin() && $query->is_search && !is_woocommerce() ) {
			$query->set( 'post_type', 'post' );
		}
	} elseif (!is_admin() && $query->is_search) {
		// Show posts by default for search if WC is not active
		$query->set( 'post_type', 'post' );
	}
}
add_action( 'pre_get_posts', 'krucial_search_woocommerce_only' );


/*-------------------------------------------------------------------
  Checks if on a woocommerce page
--------------------------------------------------------------------*/
function krucial_on_woocommerce_page() {
	if (class_exists('WooCommerce')) {
		if (!is_woocommerce() && !is_shop() && !is_cart() && !is_checkout() && !is_account_page()) {
			return 'false';
		} else {
			return 'true';
		}
	} else {
		return 'false';
	}
}


/*-------------------------------------------------------------------
  Put WooCommerce login page inside container
--------------------------------------------------------------------*/
add_action('woocommerce_before_customer_login_form', function() {
	echo '<div class="content-area">';
}, 10, 1);

add_action('woocommerce_after_customer_login_form', function() {
	echo '</div>';
});
