<?php

// Localize ajax
function krucial_localize_ajax() {
	wp_localize_script('bundle', 'krucial_ajax', array(
		'ajax_url' => admin_url( 'admin-ajax.php' ),
		'uploads_path' => wp_upload_dir()
	));
}
add_action('wp_enqueue_scripts', 'krucial_localize_ajax');

?>