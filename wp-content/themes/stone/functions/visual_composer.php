<?php
/**
 * Visual Composer Extensions
 */

if ( !defined( 'ABSPATH' ) ) {
	die( '-1' );
}

if (class_exists('WPBakeryShortCodesContainer') && class_exists('Krucial_Theme_Functionality')) {

	// Activate VC on custom post types
	if (function_exists('vc_set_default_editor_post_types')) {
		vc_set_default_editor_post_types(array('page', 'post', 'services', 'projects', 'testimonials', 'ambientes', 'staff'));
	}

	// Remove "Edit with WPBakery Page Build" from admin bar
	function krucial_vc_remove_frontend_links() {
		vc_disable_frontend();
	}
	add_action( 'vc_after_init', 'krucial_vc_remove_frontend_links' );


	// Load custom icon fonts for iconpicker
	function krucial_vc_iconpicker_load_fonts(){
		wp_enqueue_style( 'font-awesome-5', get_template_directory_uri() . '/assets/css/font-awesome.min.css', '3.5.1' );
		wp_enqueue_style( 'essentials', get_template_directory_uri() . '/assets/css/essential-icons.css', '1.0' );
	}
	add_action( 'vc_backend_editor_enqueue_js_css', 'krucial_vc_iconpicker_load_fonts' );

	// Add array of fonts to be displayed in the iconpicker select field
	function krucial_vc_iconpicker_icons() {
		return krucial_fa5_array();
	}
	add_filter( 'vc_iconpicker-type-font-awesome-5', 'krucial_vc_iconpicker_icons' );

	// Services categories
	$services_terms = get_terms( 'services_categories' );
	$services       = array( esc_html__( 'All Categories', 'oculis' ) => 'all' );
	foreach ( $services_terms as $service ) {
		$services[] = $service->name;
	}

	// Projects categories
	$projects_terms = get_terms('project_categories');
	$projects = array( esc_html__('All Categories', 'oculis') => 'all' );
	foreach ($projects_terms as $project) {
		$projects[] = $project->name;
	}
	
	// Testimonials categories
	$testimonials_terms = get_terms('testimonial_categories');
	$testimonials = array( esc_html__('All Categories', 'oculis') => 'all' );
	foreach ($testimonials_terms as $testimonial) {
		$testimonials[] = $testimonial->name;
	}

	// Staff member categories
	$members_terms = get_terms('staff_categories');
	$members = array( esc_html__('All Categories', 'oculis') => 'all' );
	foreach ($members_terms as $member) {
		$members[] = $member->name;
	}
	
	// Ambientes categories
	$ambientes_terms = get_terms('ambientes_categories');
	$ambientes = array( esc_html__('Total categorias', 'oculis') => 'all' );
	foreach ($ambientes_terms as $ambiente) {
		$ambientes[] = $ambiente->name;
	}



	/*---------------------------------------------------------------------------------
		GOOGLE MAPS
	-----------------------------------------------------------------------------------*/
	class WPBakeryShortCode_Krucial_GoogleMaps extends WPBakeryShortCodesContainer {}

	vc_map( array(
		'name'            => esc_html__( 'Custom Google Maps', 'oculis' ),
		'description'     => esc_html__( 'Google Maps with multiple locations', 'oculis' ),
		'as_parent'       => array( 'only' => 'krucial_googlemaps_marker' ),
		'base'            => 'krucial_googlemaps',
		'content_element' => true,
		'icon'            => 'map',
		'show_settings_on_create' => true,
		'js_view'         => 'VcColumnView',
		'category'        => esc_html__( 'Krucial', 'oculis' ),
		'params'          => array(

			/**
			 * Map Styling
			 */
			array(
				'type'        => 'textfield',
				'heading'     => esc_html__( 'Map Name', 'oculis' ),
				'description' => esc_html__( 'The title for your custom Google Map.', 'oculis' ),
				'param_name'  => 'map_name',
				'admin_label' => true,
				'group'       => esc_html__( 'Map Styling', 'oculis' ),
			),

			array(
				'type'        => 'dropdown',
				'heading'     => esc_html__( 'Auto Zoom', 'oculis' ),
				'description' => esc_html__( 'Auto center the map based on the markers.', 'oculis' ),
				'param_name'  => 'disable_auto_zoom',
				'admin_label' => false,
				'value'       => array(
					esc_html__( 'Yes', 'oculis' ) => 'true',
					esc_html__( 'No', 'oculis' ) => 'false'
				),
				'group'       => esc_html__( 'Map Styling', 'oculis' ),
				'std'         => 'true',
			),

			array(
				'type'        => 'textfield',
				'heading'     => esc_html__( 'Zoom', 'oculis' ),
				'description' => esc_html__( 'Zoom level', 'oculis' ),
				'param_name'  => 'zoom',
				'admin_label' => false,
				'value'       => esc_html('17'),
				'group'       => esc_html__( 'Map Styling', 'oculis' ),
				'dependency'  => array( 'element' => 'disable_auto_zoom', 'value' => 'false' ),
			),

			array(
				'type'        => 'textfield',
				'heading'     => esc_html__( 'Width', 'oculis' ),
				'param_name'  => 'width',
				'admin_label' => false,
				'value'       => esc_html('100'),
				'group'       => esc_html__( 'Map Styling', 'oculis' ),
				'edit_field_class' => esc_attr( 'vc_col-sm-6 vc_column' ),
			),
			array(
				'type'        => 'dropdown',
				'heading'     => esc_html__('Width Type', 'oculis'),
				'param_name'  => 'width_type',
				'admin_label' => false,
				'value'       => array(
					esc_html__('Pixels', 'oculis') => 'px',
					esc_html__('Percentage', 'oculis') => '%'
				),
				'group'       => esc_html__( 'Map Styling', 'oculis' ),
				'edit_field_class' => esc_attr( 'vc_col-sm-6 vc_column' ),
				'std'         => '%'
			),
			array(
				'type'        => 'textfield',
				'heading'     => esc_html__('Height', 'oculis'),
				'param_name'  => 'height',
				'admin_label' => false,
				'value'       => esc_html( '500' ),
				'group'       => esc_html__( 'Map Styling', 'oculis' ),
				'edit_field_class' => esc_attr( 'vc_col-sm-6 vc_column' ),
			),
			array(
				'type'        => 'dropdown',
				'heading'     => esc_html__('Height Type', 'oculis'),
				'param_name'  => 'height_type',
				'admin_label' => false,
				'value'       => array(
					esc_html__('Pixels', 'oculis') => 'px',
					esc_html__('Percentage', 'oculis') => '%'
				),
				'group'       => esc_html__( 'Map Styling', 'oculis' ),
				'edit_field_class' => esc_attr( 'vc_col-sm-6 vc_column' ),
				'std'         => 'px'
			),

			array(
				'type'        => 'dropdown',
				'heading'     => esc_html__('Custom Map Style', 'oculis'),
				'description' => esc_html__('Use the default style or enter your own custom styles.', 'oculis'),
				'param_name'  => 'custom_map_style',
				'admin_label' => false,
				'value'       => array(
					esc_html__('Default', 'oculis')      => 'default',
					esc_html__('Custom', 'oculis')       => 'custom',
				),
				'group'       => esc_html__( 'Map Styling', 'oculis' ),
				'std'         => 0,
			),

			array(
				'type'        => 'textarea_raw_html',
				'heading'     => esc_html__('Custom Map Style', 'oculis'),
				'description' => esc_html__('Take a look at the Snazzy Maps website.', 'oculis'),
				'param_name'  => 'styles',
				'admin_label' => false,
				'group'       => esc_html__( 'Map Styling', 'oculis' ),
				'dependency'  => array( 'element' => 'custom_map_style', 'value' => 'custom' ),
			),

			/**
			 * Map Options
			 */
			array(
				'type'        => 'dropdown',
				'heading'     => esc_html__('Disable All Map Controls', 'oculis'),
				'param_name'  => 'disable_map_controls',
				'admin_label' => false,
				'value'       => array(
					esc_html__('Yes', 'oculis') => 'true',
					esc_html__('No', 'oculis') => 'false'
				),
				'group'       => esc_html__( 'Map Options', 'oculis' ),
				'std'         => 'false',
			),
			array(
				'type'        => 'dropdown',
				'heading'     => esc_html__('Enable Different Map Types', 'oculis'),
				'param_name'  => 'map_type_control',
				'admin_label' => false,
				'value'       => array(
					esc_html__('Enable', 'oculis') => 'true',
					esc_html__('Disable', 'oculis') => 'false'
				),
				'group'       => esc_html__( 'Map Options', 'oculis' ),
				'dependency'  => array( 'element' => 'disable_map_controls', 'value' => 'false' ),
			),
			array(
				'type'        => 'checkbox',
				'heading'     => esc_html__('Map Type Controls', 'oculis'),
				'param_name'  => 'map_type_control_options',
				'admin_label' => false,
				'value'       => array(
					esc_html__('Roadmap', 'oculis')   => 'ROADMAP',
					esc_html__('Terrain', 'oculis')   => 'TERRAIN',
					esc_html__('Satellite', 'oculis') => 'SATELLITE',
					esc_html__('Hybrid', 'oculis')    => 'HYBRID'
				),
				'group'       => esc_html__( 'Map Options', 'oculis' ),
				'dependency'  => array( 'element' => 'map_type_control', 'value' => 'true' ),
				'std'         => 'ROADMAP'
			),
			array(
				'type'        => 'dropdown',
				'heading'     => esc_html__('Streetview Controls', 'oculis'),
				'param_name'  => 'streetview_control',
				'admin_label' => false,
				'value'       => array(
					esc_html__('Enable', 'oculis') => 'true',
					esc_html__('Disable', 'oculis') => 'false'
				),
				'group'       => esc_html__( 'Map Options', 'oculis' ),
				'std'         => 'false',
				'dependency'  => array( 'element' => 'disable_map_controls', 'value' => 'false' ),
			),
			array(
				'type'        => 'dropdown',
				'heading'     => esc_html__('Zoom Controls', 'oculis'),
				'param_name'  => 'zoom_control',
				'admin_label' => false,
				'value'       => array(
					esc_html__('Enable', 'oculis') => 'true',
					esc_html__('Disable', 'oculis') => 'false'
				),
				'group'       => esc_html__( 'Map Options', 'oculis' ),
				'dependency'  => array( 'element' => 'disable_map_controls', 'value' => 'false' ),
				'std'         => 'true'
			),
			array(
				'type'        => 'dropdown',
				'heading'     => esc_html__('Fullscreen Controls', 'oculis'),
				'param_name'  => 'fullscreen_control',
				'admin_label' => false,
				'value'       => array(
					esc_html__('Enable', 'oculis') => 'true',
					esc_html__('Disable', 'oculis') => 'false'
				),
				'group'       => esc_html__( 'Map Options', 'oculis' ),
				'std'         => 'false',
				'dependency'  => array( 'element' => 'disable_map_controls', 'value' => 'false' ),
			),

			// Mobile Dragging
			array(
				'type'        => 'dropdown',
				'heading'     => esc_html__('Dragging on Mobile', 'oculis'),
				'param_name'  => 'dragging_mobile',
				'value'       => array(
					esc_html__('Enable', 'oculis') => 'true',
					esc_html__('Disable', 'oculis') => 'false'
				),
				'group'       => esc_html__( 'Map Options', 'oculis' )
			),

			// Desktop Dragging
			array(
				'type'        => 'dropdown',
				'heading'     => esc_html__('Dragging on Desktop', 'oculis'),
				'param_name'  => 'dragging_desktop',
				'value'       => array(
					esc_html__('Enable', 'oculis') => 'true',
					esc_html__('Disable', 'oculis') => 'false'
				),
				'group'       => esc_html__( 'Map Options', 'oculis' )
			),

			/**
			 * Traffic Layers
			 */
			array(
				'type'        => 'dropdown',
				'heading'     => esc_html__('Traffic', 'oculis'),
				'description' => esc_html__('Adds real-time traffic information (where supported).', 'oculis'),
				'param_name'  => 'traffic_layer',
				'admin_label' => false,
				'value'       => array(
					esc_html__('Yes', 'oculis') => 'true',
					esc_html__('No', 'oculis') => 'false'),
				'group'       => esc_html__( 'Map Layers', 'oculis' ),
				'std'         => 'false',
			),
			array(
				'type'        => 'dropdown',
				'heading'     => esc_html__('Transit', 'oculis'),
				'description' => esc_html__('Adds a layer of transit paths, showing major transit lines as thick, colored lines.', 'oculis'),
				'param_name'  => 'transit_layer',
				'admin_label' => false,
				'value'       => array(
					esc_html__('Yes', 'oculis') => 'true',
					esc_html__('No', 'oculis') => 'false'),
				'group'       => esc_html__( 'Map Layers', 'oculis' ),
				'std'         => 'false',
			),
			array(
				'type'        => 'dropdown',
				'heading'     => esc_html__('Bicycling', 'oculis'),
				'description' => esc_html__('Adds a layer of bike paths, suggested bike routes and other overlays specific to bicycling usage.', 'oculis'),
				'param_name'  => 'bicycling_layer',
				'admin_label' => false,
				'value'       => array(
					esc_html__('Yes', 'oculis') => 'true',
					esc_html__('No', 'oculis') => 'false'
				),
				'group'       => esc_html__( 'Map Layers', 'oculis' ),
				'std'         => 'false',
			),

		)
	));


	/*---------------------------------------------------------------------------------
		GOOGLE MAPS MARKER
	-----------------------------------------------------------------------------------*/
	vc_map(array(
		'name'            => esc_html__('Google Maps Marker', 'oculis'),
		'description'     => esc_html__('Add a custom marker to your custom Google Map', 'oculis'),
		'as_child'        => array( 'only' => 'krucial_googlemaps' ),
		'base'            => 'krucial_googlemaps_marker',
		'content_element' => true,
		'icon'            => 'map',
		'show_settings_on_create' => true,
		'category'        => esc_html__( 'Krucial', 'oculis' ),
		'params'          => array(
			array(
				'type'        => 'dropdown',
				'heading'     => esc_html__('Marker Icon', 'oculis'),
				'param_name'  => 'marker_icon_option',
				'admin_label' => false,
				'value'       => array(
					esc_html__('Default', 'oculis') => 'default',
					esc_html__('Custom', 'oculis') => 'custom'
				),
				'std'         => 'default',
			),
			array(
				'type'        => 'attach_image',
				'heading'     => esc_html__('Custom Marker Icon', 'oculis'),
				'param_name'  => 'pin_icon',
				'admin_label' => false,
				'dependency'  => array( 'element' => 'marker_icon_option', 'value' => 'custom' ),
			),
			array(
				'type'             => 'textfield',
				'heading'          => esc_html__('Latitude', 'oculis'),
				'param_name'       => 'lat',
			),
			array(
				'type'             => 'textfield',
				'heading'          => esc_html__('Longitude', 'oculis'),
				'param_name'       => 'lng',
			),
			array(
				'type'        => 'textarea',
				'heading'     => esc_html__('Marker Description', 'oculis'),
				'description' => esc_html__('The description when the marker is clicked.', 'oculis'),
				'param_name'  => 'marker_description',
				'admin_label' => false,
			),

			array(
				'type'        => 'dropdown',
				'heading'     => esc_html__('Marker Animation', 'oculis'),
				'param_name'  => 'marker_animation',
				'admin_label' => false,
				'value'       => array(
					esc_html__('Drop', 'oculis') => 'DROP',
					esc_html__('Bounce', 'oculis') => 'BOUNCE'
				),
				'std'         => 'DROP',
			),
		)
	));



	/*---------------------------------------------------------------------------------
		BLOG CAROUSEL
	-----------------------------------------------------------------------------------*/
	vc_map( array(

		'name' => esc_html__( 'Blog Carousel', 'oculis' ),
		'description' => esc_html__( 'List your blog posts in a carousel', 'oculis' ),
		'base' => 'krucial-blog-carousel',
		'category' => esc_html( 'Krucial' ),
		'params' => array(

			// Number of total posts
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Total Number of Posts', 'oculis' ),
				'description' => esc_html__( 'How many posts would you like to display in the carousel? (-1 means all)', 'oculis' ),
				'param_name' => 'blog_carousel_number',
				'std' => '-1',
				'save_always' => true,
			),

			// Number to show in row
			array(
				'type' => 'dropdown',
				'heading' => esc_html( 'Posts per row', 'oculis' ),
				'description' => esc_html__( 'How many posts would you like to display per row?', 'oculis' ),
				'param_name' => 'blog_carousel_per_row',
				'value' => array(
					esc_html__( 'Two', 'oculis' ) => '2',
					esc_html__( 'Three', 'oculis' ) => '3',
					esc_html__( 'Four', 'oculis' ) => '4',
				),
				'std' => '3',
				'save_always' => true,

			),

			// Show navigation
			array(
				'type' => 'dropdown',
				'heading' => esc_html( 'Show Navigation Arrows', 'oculis' ),
				'description' => esc_html__( 'Would you like to display the navigation arrows?', 'oculis' ),
				'param_name' => 'blog_carousel_show_nav',
				'value' => array(
					esc_html__( 'Yes', 'oculis' ) => 'true',
					esc_html__( 'No', 'oculis' ) => 'false',
				),
				'std' => 'true',
				'save_always' => true,

			),

			// Show Dots
			array(
				'type' => 'dropdown',
				'heading' => esc_html( 'Show Navigation Dots', 'oculis' ),
				'description' => esc_html__( 'Would you like to display navigation dots under the slider?', 'oculis' ),
				'param_name' => 'blog_carousel_show_dots',
				'value' => array(
					esc_html__( 'Yes', 'oculis' ) => 'true',
					esc_html__( 'No', 'oculis' ) => 'false',
				),
				'std' => 'false',
				'save_always' => true,

			),

			// Slider speed
			array(
				'type' => 'textfield',
				'heading' => esc_html( 'Slider Speed', 'oculis' ),
				'description' => esc_html__( 'How fast should the slider switch slides? (Enter in milliseconds e.g. 500) ', 'oculis' ),
				'param_name' => 'blog_carousel_slide_speed',
				'std' => '500',
				'save_always' => true,
			),

			// Enable AutoPlay
			array(
				'type' => 'dropdown',
				'heading' => esc_html( 'Enable AutoPlay', 'oculis' ),
				'param_name' => 'blog_carousel_autoplay',
				'value' => array(
					esc_html__( 'No', 'oculis' ) => 'false',
					esc_html__( 'Yes', 'oculis' ) => 'true',
				),
				'std' => 'false',
				'save_always' => true,

			),

			// Autoplay speed
			array(
				'type' => 'textfield',
				'heading' => esc_html( 'AutoPlay Speed', 'oculis' ),
				'description' => esc_html__( 'How fast should the slider autoplay? (Enter in milliseconds e.g. 3000) ', 'oculis' ),
				'param_name' => 'blog_carousel_autoplay_speed',
				'std' => '3000',
				'save_always' => true,
			),

		)
	));

	/*---------------------------------------------------------------------------------
		BLOG GRID
	-----------------------------------------------------------------------------------*/
	vc_map( array(
		'name' => esc_html__( 'Blog Grid', 'oculis' ),
		'description' => esc_html__( 'List your blog posts in a grid', 'oculis' ),
		'base' => 'krucial-blog-grid',
		'category' => esc_html( 'Krucial' ),
		'params' => array(

			// Number of posts
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Total Number of Posts', 'oculis' ),
				'description' => esc_html__( 'How many posts would you like to display? (-1 means all)', 'oculis' ),
				'param_name' => 'blog_grid_number',
				'std' => '3',
				'save_always' => true,
			),

			// Posts per row
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Blogposts per row', 'oculis' ),
				'description' => esc_html__( 'How many blogposts would you like to display in each row?', 'oculis' ),
				'param_name' => 'blog_grid_layout',
				'value' => array(
					esc_html__( 'One', 'oculis' ) => 'col-12',
					esc_html__( 'Two', 'oculis' ) => 'col-12 col-md-6',
					esc_html__( 'Three', 'oculis' ) => 'col-12 col-md-4',
					esc_html__( 'Four', 'oculis' ) => 'col-12 col-md-3',
				),
				'std' => 'col-12 col-md-4',
				'save_always' => true,
			),
		)
	) );


	/*---------------------------------------------------------------------------------
		VIDEO PLAYER
	-----------------------------------------------------------------------------------*/
	vc_map( array(
		'name' => esc_html__( 'Custom Video Player', 'oculis' ),
		'description' => esc_html__( 'Add custom video player that will pop up.', 'oculis' ),
		'base' => 'krucial-video-player',
		'category' => esc_html( 'Krucial' ),
		'params' => array(

			// YouTube URL
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'YouTube URL', 'oculis' ),
				'description' => esc_html__( 'Enter in a YouTube embed link', 'oculis' ),
				'param_name' => 'youtube_url',
				'value' => esc_html( 'https://www.youtube.com/embed/jOY3gVV_KRE' ),
				'save_always' => true
			),

			// Show in modal?
			array(
				'type'        => 'dropdown',
				'heading'     => esc_html__('Show the video player in a popup modal?', 'oculis'),
				'param_name'  => 'in_modal',
				'admin_label' => false,
				'value'       => array(
					esc_html__('No', 'oculis') => 'no',
					esc_html__('Yes', 'oculis') => 'yes'
				),
				'std' => 'no',
				'save_always' => true
			),

			// Alignment
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Alignment', 'oculis' ),
				'description' => esc_html__( 'If you wish to align the video player icon in the center, check this box.', 'oculis' ),
				'param_name' => 'player_icon_align',
				'value' => array(
					esc_html__( 'Left Align', 'oculis' ) => '0',
					esc_html__( 'Center Align', 'oculis' ) => '0 auto',
					esc_html__( 'Right Align', 'oculis' ) => '0 0 0 auto',
				),
				'save_always' => true,
				'std' => 'left',
				'dependency' => array( 'element' => 'in_modal', 'value' => 'yes' ),
			),

		)
	));


	/*---------------------------------------------------------------------------------
		ACCORDION WRAPPER
	-----------------------------------------------------------------------------------*/
	class WPBakeryShortCode_Krucial_Accordion_wrapper extends WPBakeryShortCodesContainer {}

	vc_map( array(
		'name' => esc_html__( 'Accordion', 'oculis' ),
		'description' => esc_html__( 'Add collapsible content panels to your page', 'oculis' ),
		'base' => 'krucial_accordion_wrapper',
		'category' => esc_html( 'Krucial' ),
		'as_parent' => array( 'only' => 'krucial-accordion-item' ),
		'content_element' => true,
		'js_view' => 'VcColumnView',
		'show_settings_on_create' => false,
		'params' => array(

			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Title', 'oculis' ),
				'param_name' => 'accordion_wrapper_title',
			),

		)
	));


	/*---------------------------------------------------------------------------------
		ACCORDION ITEM
	-----------------------------------------------------------------------------------*/
	vc_map(array(
		'name'            => esc_html__('Accordion Item', 'oculis'),
		'description'     => esc_html__('Add an item to your accordion', 'oculis'),
		'as_child'        => array( 'only' => 'krucial-accordion-wrapper' ),
		'base'            => 'krucial-accordion-item',
		'content_element' => true,
		'show_settings_on_create' => true,
		'params'          => array(
			array(
				'heading'    => esc_html__( 'Accordion Item Icon', 'oculis' ),
				'type'       => 'iconpicker',
				'param_name' => 'accordion_item_icon',
				'settings' => array(
					'type' => 'font-awesome-5',
					'iconsPerPage' => 100,
				),
				'value'      => '',
				'weight'     => 1,
				'save_always' => true,
			),
			array(
				'type'        => 'textfield',
				'heading'     => esc_html__('Accordion Item Title', 'oculis'),
				'param_name'  => 'accordion_item_title',
			),
			array(
				'type'        => 'textarea',
				'heading'     => esc_html__('Body Content', 'oculis'),
				'param_name'  => 'accordion_item_content',
			),
		)
	));


	/*---------------------------------------------------------------------------------
		CUSTOM IMAGE
	-----------------------------------------------------------------------------------*/
	vc_map( array(
		'name' => esc_html__( 'Custom Image', 'oculis' ),
		'description' => esc_html__( 'Add a custom image to your page', 'oculis' ),
		'base' => 'krucial-custom-image',
		'category' => esc_html( 'Krucial' ),
		'params' => array(

			// Image
			array(
				'type' => 'attach_image',
				'heading' => esc_html__( 'Upload/Select an Image', 'oculis' ),
				'param_name' => 'custom_image_url'
			),

			// Image caption
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Image Caption', 'oculis' ),
				'param_name' => 'custom_image_caption'
			),
			
			// Active Download
			array( 
				'type' => 'checkbox',
				'name' => 'enable_image_link_download',
				'heading' => esc_html__( 'Enable Custom Download Image Link', 'oculis' ),
				'param_name' => 'custom_image_link_download',
			),

			// Download Link Text 
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Download Link Image', 'oculis' ),
				'param_name' => 'custom_download_image_link_text',
				'dependency' => array( 'element' => 'custom_image_link_download', 'value' => 'true' ),
			),

			// Download Image
			array(
				'type' => 'attach_image',
				'heading' => esc_html__( 'Upload/Select an Image', 'oculis' ),
				'param_name' => 'custom_download_image_url',
				'dependency' => array( 'element' => 'custom_image_link_download', 'value' => 'true' ),
			),

			// Active Link
			array(
				'type' => 'checkbox',
				'name' => 'enable_image_link',
				'heading' => esc_html__( 'Enable Custom Image Link', 'oculis' ),
				'param_name' => 'custom_image_link',
			),

			// Link Text
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Link Image', 'oculis' ),
				'param_name' => 'custom_image_link_text',
				'dependency' => array( 'element' => 'custom_image_link', 'value' => 'true' ),
			),

			// Link URL
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Image Link URL', 'oculis' ),
				'param_name' => 'custom_image_link_url',
				'dependency' => array( 'element' => 'custom_image_link', 'value' => 'true' ),
			),
			
			// Target Link URL
			array(
				'type'        => 'dropdown',
				'heading'     => esc_html__('Image Link Target', 'oculis'),
				'param_name'  => 'custom_image_link_target',
				'dependency' => array( 'element' => 'custom_image_link', 'value' => 'true' ),
				'admin_label' => false,
				'value'       => array(
					esc_html__('new window', 'oculis') => '_blank',
					esc_html__('current window', 'oculis') => '_self',
				),
				'std' => '_blank',
				'save_always' => true
			),

		)
	));


	/*---------------------------------------------------------------------------------
		FEATURED BOX
	-----------------------------------------------------------------------------------*/
	vc_map( array(
		'name' => esc_html__( 'Featured Box', 'oculis' ),
		'description' => esc_html__( 'Add a stylish featured box', 'oculis' ),
		'base' => 'krucial-featured-box',
		'category' => esc_html( 'Krucial' ),
		'params' => array(

			array(
				'type'        => 'dropdown',
				'heading'     => esc_html__('Choose Featured Box Style', 'oculis'),
				'param_name'  => 'featured_box_style',
				'admin_label' => false,
				'value'       => array(
					esc_html__('Style 1', 'oculis') => 'style_1',
					esc_html__('Style 2', 'oculis') => 'style_2',
					esc_html__('Style 3', 'oculis') => 'style_3',
					esc_html__('Style 4', 'oculis') => 'style_4'
				),
				'std' => 'style_1',
				'save_always' => true
			),

			/**
			 * Style 1
			 */

			// Image
			array(
				'type' => 'attach_image',
				'heading' => esc_html__( 'Upload an Image', 'oculis' ),
				'param_name' => 'featured_box_image'
			),

			// Title
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Featured Box Title', 'oculis' ),
				'param_name' => 'featured_box_title',
			),

			// Description
			array(
				'type' => 'textarea',
				'heading' => esc_html__( 'Featured Box Text', 'oculis' ),
				'param_name' => 'featured_box_text',
			),

			// Link
			array(
				'type' => 'checkbox',
				'name' => 'enable_link',
				'heading' => esc_html__( 'Enable Custom Link', 'oculis' ),
				'param_name' => 'featured_box_enable_link',
			),

			// Link Text
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Link Text', 'oculis' ),
				'param_name' => 'featured_box_link_text',
				'dependency' => array( 'element' => 'featured_box_enable_link', 'value' => 'true' ),
			),

			// Link URL
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Link URL', 'oculis' ),
				'param_name' => 'featured_box_link_url',
				'dependency' => array( 'element' => 'featured_box_enable_link', 'value' => 'true' ),
			),
			
			// Target Link URL
			array(
				'type'        => 'dropdown',
				'heading'     => esc_html__('Featured Box Link Target', 'oculis'),
				'param_name'  => 'featured_box_link_target',
				'dependency' => array( 'element' => 'featured_box_enable_link', 'value' => 'true' ),
				'admin_label' => false,
				'value'       => array(
					esc_html__('new window', 'oculis') => '_blank',
					esc_html__('current window', 'oculis') => '_self',
				),
				'std' => '_blank',
				'save_always' => true
			),
			
			// Extra Link
			array(
				'type' => 'checkbox',
				'name' => 'extra_enable_link',
				'heading' => esc_html__( 'Enable Extra Custom Link', 'oculis' ),
				'param_name' => 'featured_box_extra_enable_link',
			),
			
			// Extra Link Text
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Extra Link Text', 'oculis' ),
				'param_name' => 'featured_box_extra_link_text',
				'dependency' => array( 'element' => 'featured_box_extra_enable_link', 'value' => 'true' ),
			),

			// Extra Link URL
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Extra Link URL', 'oculis' ),
				'param_name' => 'featured_box_extra_link_url',
				'dependency' => array( 'element' => 'featured_box_extra_enable_link', 'value' => 'true' ),
			),
			
			// Extra Target Link URL
			array(
				'type'        => 'dropdown',
				'heading'     => esc_html__('Extra Featured Box Link Target', 'oculis'),
				'param_name'  => 'featured_box_extra_link_target',
				'dependency' => array( 'element' => 'featured_box_extra_enable_link', 'value' => 'true' ),
				'admin_label' => false,
				'value'       => array(
					esc_html__('new window', 'oculis') => '_blank',
					esc_html__('current window', 'oculis') => '_self',
				),
				'std' => 'target',
				'save_always' => true
			),

			/**
			 * Style 2
			 */


			/**
			 * Icon options
			 */

			// enable icon
			array(
				'type' => 'checkbox',
				'heading' => esc_html__( 'Enable Icon', 'oculis' ),
				'param_name' => 'featured_box_enable_icon',
				'group' => esc_html__( 'Icon', 'oculis' ),
			),

			// choose icon
			array(
				'heading'    => esc_html__( 'Icon', 'oculis' ),
				'type'       => 'iconpicker',
				'param_name' => 'featured_box_icon',
				'settings' => array(
					'type' => 'font-awesome-5',
					'iconsPerPage' => 100,
				),
				'value'      => '',
				'weight'     => 1,
				'group' => esc_html__( 'Icon', 'oculis' ),
				'save_always' => true,
				'dependency' => array( 'element' => 'featured_box_enable_icon', 'value' => 'true' ),
			),

		)
	) );


	/*---------------------------------------------------------------------------------
		CUSTOM BUTTON
	-----------------------------------------------------------------------------------*/
	vc_map( array(

		'name' => esc_html__( 'Custom Button', 'oculis' ),
		'description' => esc_html__( 'Custom button with multiple styles', 'oculis' ),
		'category' => esc_html( 'Krucial' ),
		'base' => 'krucial-custom-button',
		'params' => array(

			// Button text
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Button Text', 'oculis' ),
				'param_name' => 'custom_button_text',
			),

			// Button link
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Button Link', 'oculis' ),
				'param_name' => 'custom_button_link'
			),

			// Button target
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Link Target', 'oculis' ),
				'param_name' => 'custom_button_target',
				'value' => array(
					esc_html__( 'Open in Same Window', 'oculis' ) => '_self',
					esc_html__( 'Open in New Window', 'oculis' ) => '_blank',
				),
			),

			// Button size
			array(
				'type'=> 'dropdown',
				'heading' => esc_html__( 'Button Size', 'oculis' ),
				'param_name' => 'custom_button_size',
				'value' => array(
					esc_html__( 'Small', 'oculis' ) => 'size_small',
					esc_html__( 'Medium', 'oculis' ) => 'size_medium',
					esc_html__( 'Large', 'oculis' ) => 'size_large',
				),
				'save_always' => true,
				'group' => esc_html__( 'Button Style', 'oculis' ),
			),

			// Button corners
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Button Corner Style', 'oculis' ),
				'param_name' => 'custom_button_corners',
				'value' => array(
					esc_html__( 'Round', 'oculis' ) => 'corners_round',
					esc_html__( 'Rounded', 'oculis' ) => 'corners_rounded',
					esc_html__( 'Square', 'oculis' ) => 'corners_square',
				),
				'save_always' => true,
				'group' => esc_html__( 'Button Style', 'oculis' ),
			),

			/**
			 * Buttons alignment
			 */

			// Button aligment
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Button Alignment - Large Devices', 'oculis' ),
				'param_name' => 'custom_button_alignment_lg',
				'category' => esc_html__( 'Position', 'oculis' ),
				'value' => array(
					esc_html__( 'Left', 'oculis' ) => 'flex-start',
					esc_html__( 'Center', 'oculis' ) => 'center',
					esc_html__( 'Right', 'oculis' ) => 'flex-end',
				),
				'save_always' => true,
				'group' => esc_html__( 'Alignment', 'oculis' ),
			),

			// Button aligment
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Button Alignment - Medium Devices', 'oculis' ),
				'param_name' => 'custom_button_alignment_med',
				'category' => esc_html__( 'Position', 'oculis' ),
				'value' => array(
					esc_html__( 'Left', 'oculis' ) => 'flex-start',
					esc_html__( 'Center', 'oculis' ) => 'center',
					esc_html__( 'Right', 'oculis' ) => 'flex-end',
				),
				'save_always' => true,
				'group' => esc_html__( 'Alignment', 'oculis' ),
			),

			// Button aligment
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Button Alignment - Small Devices', 'oculis' ),
				'param_name' => 'custom_button_alignment_sml',
				'category' => esc_html__( 'Position', 'oculis' ),
				'value' => array(
					esc_html__( 'Left', 'oculis' ) => 'flex-start',
					esc_html__( 'Center', 'oculis' ) => 'center',
					esc_html__( 'Right', 'oculis' ) => 'flex-end',
				),
				'save_always' => true,
				'group' => esc_html__( 'Alignment', 'oculis' ),
			),


			/**
			 * Button Colors
			 */

			// Background color
			array(
				'type' => 'colorpicker',
				'heading' => esc_html__( 'Background Color', 'oculis' ),
				'param_name' => 'button_background_color',
				'group' => esc_html__( 'Button Colors', 'oculis' ),
			),

			// Text color
			array(
				'type' => 'colorpicker',
				'heading' => esc_html__( 'Text Color', 'oculis' ),
				'param_name' => 'button_text_color',
				'group' => esc_html__( 'Button Colors', 'oculis' ),
			),

			// Background hover color
			array(
				'type' => 'colorpicker',
				'heading' => esc_html__( 'Background Hover Color', 'oculis' ),
				'param_name' => 'button_background_color_hov',
				'group' => esc_html__( 'Button Colors', 'oculis' ),
			),

			// Text hover color
			array(
				'type' => 'colorpicker',
				'heading' => esc_html__( 'Text Hover Color', 'oculis' ),
				'param_name' => 'button_text_color_hov',
				'group' => esc_html__( 'Button Colors', 'oculis' ),
			),


			/**
			 * Icon Options
			 */

			// choose icon
			array(
				'heading'    => esc_html__( 'Icon', 'oculis' ),
				'type'       => 'iconpicker',
				'param_name' => 'button_icon',
				'settings' => array(
					'type' => 'font-awesome-5',
					'iconsPerPage' => 100,
				),
				'value'      => '',
				'weight'     => 1,
				'group' => esc_html__( 'Button Icon', 'oculis' ),
				'save_always' => true,
			),

			// icon color
			array(
				'type' => 'colorpicker',
				'heading' => esc_html__( 'Icon Color', 'oculis' ),
				'param_name' => 'button_icon_color',
				'group' => esc_html__( 'Button Icon', 'oculis' ),
			),

			// icon hover color
			array(
				'type' => 'colorpicker',
				'heading' => esc_html__( 'Icon Hover Color', 'oculis' ),
				'param_name' => 'button_icon_color_hov',
				'group' => esc_html__( 'Button Icon', 'oculis' ),
			),

			// custom class
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Custom CSS Class', 'oculis' ),
				'param_name' => 'button_css_class',
				'save_always' => true,
				'group' => esc_html__( 'Advanced', 'oculis' ),
			),

			// Use div instead of a tag
			array(
				'type' => 'dropdown',
				'heading' => esc_html( 'Use "div" instead of "a" tag', 'oculis' ),
				'description' => esc_html__( 'Use this if you would not like to attach a link to the button, but for example you may attach an event handler to this button through use of a custom class.', 'oculis' ),
				'param_name' => 'button_use_div',
				'value' => array(
					esc_html__( 'No', 'oculis' ) => 'false',
					esc_html__( 'Yes', 'oculis' ) => 'true',
				),
				'std' => 'false',
				'save_always' => true,
				'group' => esc_html__( 'Advanced', 'oculis' ),
			),
		)

	));


	/*---------------------------------------------------------------------------------
		SERVICES GRID
	-----------------------------------------------------------------------------------*/
	vc_map( array(
		'name' => esc_html__( 'Services Grid', 'oculis' ),
		'description' => esc_html__( 'Display your services in a grid', 'oculis' ),
		'base' => 'krucial-services-grid',
		'category' => esc_html( 'Krucial' ),
		'params' => array(

			// Number of services
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Total Number of Services', 'oculis' ),
				'description' => esc_html__( 'How many services would you like to display. (-1 means all)', 'oculis' ),
				'param_name' => 'services_grid_number',
				'std' => esc_html('-1'),
				'save_always' => true,
			),

			// Posts per row
			array(
				'type' => 'dropdown',
				'heading' => esc_html( 'Services per row', 'oculis' ),
				'description' => esc_html__( 'How many services would you like to display in each row?', 'oculis' ),
				'param_name' => 'services_grid_layout',
				'value' => array(
					esc_html__( 'Two', 'oculis' ) => 'col-12 col-md-6',
					esc_html__( 'Three', 'oculis' ) => 'col-12 col-md-4',
					esc_html__( 'Four', 'oculis' ) => 'col-12 col-md-3',
				),
				'std' => esc_html('col-12 col-md-4'),
				'save_always' => true,

			),

		)
	) );


	/*---------------------------------------------------------------------------------
		SERVICES CAROUSEL
	-----------------------------------------------------------------------------------*/
	vc_map( array(
		'name' => esc_html__( 'Services Carousel', 'oculis' ),
		'description' => esc_html__( 'Display your services in a carousel', 'oculis' ),
		'base' => 'krucial-services-carousel',
		'category' => esc_html( 'Krucial' ),
		'params' => array(

			// Number of total services
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Number of Total Posts', 'oculis' ),
				'description' => esc_html__( 'How many services would you like to display in the carousel. (-1 means all)', 'oculis' ),
				'param_name' => 'services_carousel_number',
				'std' => '-1'
			),

			// Number to show
			array(
				'type' => 'dropdown',
				'heading' => esc_html( 'Services to Show', 'oculis' ),
				'description' => esc_html__( 'How many services would you like to display at once?', 'oculis' ),
				'param_name' => 'services_carousel_per_row',
				'value' => array(
					esc_html__( 'Two', 'oculis' ) => '2',
					esc_html__( 'Three', 'oculis' ) => '3',
					esc_html__( 'Four', 'oculis' ) => '4',
				),
				'std' => '3',
				'save_always' => true,

			),

			// Show Dots
			array(
				'type' => 'dropdown',
				'heading' => esc_html( 'Show Navigation Dots', 'oculis' ),
				'description' => esc_html__( 'Would you like to display navigation dots under the slider?', 'oculis' ),
				'param_name' => 'services_carousel_show_dots',
				'value' => array(
					esc_html__( 'Yes', 'oculis' ) => 'true',
					esc_html__( 'No', 'oculis' ) => 'false',
				),
				'std' => 'true',
				'save_always' => true,

			),

			// Show navigation
			array(
				'type' => 'dropdown',
				'heading' => esc_html( 'Show Navigation Arrows', 'oculis' ),
				'description' => esc_html__( 'Would you like to display the navigation arrows?', 'oculis' ),
				'param_name' => 'services_carousel_show_nav',
				'value' => array(
					esc_html__( 'Yes', 'oculis' ) => 'true',
					esc_html__( 'No', 'oculis' ) => 'false',
				),
				'std' => 'true',
				'save_always' => true,

			),

			// Slider speed
			array(
				'type' => 'textfield',
				'heading' => esc_html( 'Slider Speed', 'oculis' ),
				'description' => esc_html__( 'How fast should the slider switch slides? (Enter in milliseconds e.g. 500) ', 'oculis' ),
				'param_name' => 'services_carousel_slide_speed',
				'std' => '500',
				'save_always' => true,
			),

			// Enable AutoPlay
			array(
				'type' => 'dropdown',
				'heading' => esc_html( 'Enable AutoPlay', 'oculis' ),
				'param_name' => 'services_carousel_autoplay',
				'value' => array(
					esc_html__( 'No', 'oculis' ) => 'false',
					esc_html__( 'Yes', 'oculis' ) => 'true',
				),
				'std' => 'false',
				'save_always' => true,

			),

			// Autoplay speed
			array(
				'type' => 'textfield',
				'heading' => esc_html( 'AutoPlay Speed', 'oculis' ),
				'description' => esc_html__( 'How fast should the slider autoplay? (Enter in milliseconds e.g. 3000) ', 'oculis' ),
				'param_name' => 'services_carousel_autoplay_speed',
				'std' => '3000',
				'save_always' => true,
			),
		)
	) );


	/*---------------------------------------------------------------------------------
		PROJECTS CAROUSEL
	-----------------------------------------------------------------------------------*/
	vc_map( array(
		'name' => esc_html__( 'Projects Carousel', 'oculis' ),
		'description' => esc_html__( 'Display a carousel full of projects.', 'oculis' ),
		'base' => 'krucial-projects-carousel',
		'category' => esc_html( 'Krucial' ),
		'params' => array(

			// Number of Projects
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Total Number of Projects', 'oculis' ),
				'description' => esc_html__( 'How many projects would you like to display? (-1 means all)', 'oculis' ),
				'param_name' => 'projects_carousel_number',
				'std' => '-1',
				'save_always' => true,
			),

			// Number to show
			array(
				'type' => 'dropdown',
				'heading' => esc_html( 'Projects to Show', 'oculis' ),
				'description' => esc_html__( 'How many services would you like to display at once?', 'oculis' ),
				'param_name' => 'projects_carousel_per_row',
				'value' => array(
					esc_html__( 'Two', 'oculis' ) => '2',
					esc_html__( 'Three', 'oculis' ) => '3',
					esc_html__( 'Four', 'oculis' ) => '4',
				),
				'std' => '3',
				'save_always' => true,
			),

			// Show Dots
			array(
				'type' => 'dropdown',
				'heading' => esc_html( 'Show Navigation Dots', 'oculis' ),
				'description' => esc_html__( 'Would you like to display navigation dots under the slider?', 'oculis' ),
				'param_name' => 'projects_carousel_show_dots',
				'value' => array(
					esc_html__( 'Yes', 'oculis' ) => 'true',
					esc_html__( 'No', 'oculis' ) => 'false',
				),
				'std' => 'true',
				'save_always' => true,
			),

			// Show navigation
			array(
				'type' => 'dropdown',
				'heading' => esc_html( 'Show Navigation Arrows', 'oculis' ),
				'description' => esc_html__( 'Would you like to display the navigation arrows?', 'oculis' ),
				'param_name' => 'projects_carousel_show_nav',
				'value' => array(
					esc_html__( 'Yes', 'oculis' ) => 'true',
					esc_html__( 'No', 'oculis' ) => 'false',
				),
				'std' => 'true',
				'save_always' => true,

			),

			// Slider speed
			array(
				'type' => 'textfield',
				'heading' => esc_html( 'Slider Speed', 'oculis' ),
				'description' => esc_html__( 'How fast should the slider switch slides? (Enter in milliseconds e.g. 500) ', 'oculis' ),
				'param_name' => 'projects_carousel_slide_speed',
				'std' => '500',
				'save_always' => true,
			),

			// Enable AutoPlay
			array(
				'type' => 'dropdown',
				'heading' => esc_html( 'Enable AutoPlay', 'oculis' ),
				'param_name' => 'projects_carousel_autoplay',
				'value' => array(
					esc_html__( 'No', 'oculis' ) => 'false',
					esc_html__( 'Yes', 'oculis' ) => 'true',
				),
				'std' => 'false',
				'save_always' => true,

			),

			// Autoplay speed
			array(
				'type' => 'textfield',
				'heading' => esc_html( 'AutoPlay Speed', 'oculis' ),
				'description' => esc_html__( 'How fast should the slider autoplay? (Enter in milliseconds e.g. 3000) ', 'oculis' ),
				'param_name' => 'projects_carousel_autoplay_speed',
				'std' => '3000',
				'save_always' => true,
			),

			// Categories
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Project Categories', 'oculis' ),
				'description' => esc_html__( 'Which project categories would you like to display?', 'oculis' ),
				'param_name' => 'projects_carousel_categories',
				'value' => $projects,
				'std' => 'all',
				'save_always' => true,
			),
		)
	));

	/*---------------------------------------------------------------------------------
		PROJECTS FILTER
	-----------------------------------------------------------------------------------*/
	vc_map( array(
		'name' => esc_html__( 'Projects Filter', 'oculis' ),
		'description' => esc_html__( 'Display a grid of projects with a filter.', 'oculis' ),
		'base' => 'krucial-projects-filter',
		'category' => esc_html( 'Krucial' ),
		'params' => array(

			// Projects Per Row
			array(
				'type' => 'dropdown',
				'heading' => esc_html( 'Projects Per Row', 'oculis' ),
				'description' => esc_html__( 'How many projects in a row?', 'oculis' ),
				'param_name' => 'projects_filter_per_row',
				'value' => array(
					esc_html__( 'Two', 'oculis' ) => '2',
					esc_html__( 'Three', 'oculis' ) => '3',
					esc_html__( 'Four', 'oculis' ) => '4',
				),
				'std' => '3',
				'save_always' => true,
			),

		),

	) );	
	
	/*---------------------------------------------------------------------------------
		AMBIENTES GRID
	-----------------------------------------------------------------------------------*/
	vc_map( array(
		'name' => esc_html__( 'Ambientes Grid', 'oculis' ),
		'description' => esc_html__( 'List your Ambientes posts in a grid', 'oculis' ),
		'base' => 'krucial-ambientes-grid',
		'category' => esc_html( 'Krucial' ),
		'params' => array(

			// Number of Ambientes
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Total Number of Ambientes', 'oculis' ),
				'description' => esc_html__( 'How many Ambientes would you like to display? (-1 means all)', 'oculis' ),
				'param_name' => 'ambientes_grid_number',
				'std' => '3',
				'save_always' => true,
			),

			// Posts per row
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Ambientes per row', 'oculis' ),
				'description' => esc_html__( 'How many Ambientes would you like to display in each row?', 'oculis' ),
				'param_name' => 'ambientes_grid_layout',
				'value' => array(
					esc_html__( 'One', 'oculis' ) => 'col-12',
					esc_html__( 'Two', 'oculis' ) => 'col-12 col-md-6',
					esc_html__( 'Three', 'oculis' ) => 'col-12 col-md-4',
					esc_html__( 'Four', 'oculis' ) => 'col-12 col-md-3',
				),
				'std' => 'col-12 col-md-4',
				'save_always' => true,
			),
		)
	) );


	/*---------------------------------------------------------------------------------
		AMBIENTES CAROUSEL
	-----------------------------------------------------------------------------------*/
	vc_map( array(
		'name' => esc_html__( 'Ambientes Carousel', 'oculis' ),
		'description' => esc_html__( 'Display your ambientes in a carousel', 'oculis' ),
		'base' => 'krucial-ambientes-carousel',
		'category' => esc_html( 'Krucial' ),
		'params' => array(

			// Number of total Ambientes
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Number of Total Posts', 'oculis' ),
				'description' => esc_html__( 'How many ambientes would you like to display in the carousel. (-1 means all)', 'oculis' ),
				'param_name' => 'ambientes_carousel_number',
				'std' => '-1'
			),

			// Number to show
			array(
				'type' => 'dropdown',
				'heading' => esc_html( 'Ambientes to Show', 'oculis' ),
				'description' => esc_html__( 'How many ambientes would you like to display at once?', 'oculis' ),
				'param_name' => 'ambientes_carousel_per_row',
				'value' => array(
					esc_html__( 'Two', 'oculis' ) => '2',
					esc_html__( 'Three', 'oculis' ) => '3',
					esc_html__( 'Four', 'oculis' ) => '4',
				),
				'std' => '3',
				'save_always' => true,

			),

			// Show Dots
			array(
				'type' => 'dropdown',
				'heading' => esc_html( 'Show Navigation Dots', 'oculis' ),
				'description' => esc_html__( 'Would you like to display navigation dots under the slider?', 'oculis' ),
				'param_name' => 'ambientes_carousel_show_dots',
				'value' => array(
					esc_html__( 'Yes', 'oculis' ) => 'true',
					esc_html__( 'No', 'oculis' ) => 'false',
				),
				'std' => 'true',
				'save_always' => true,

			),

			// Show navigation
			array(
				'type' => 'dropdown',
				'heading' => esc_html( 'Show Navigation Arrows', 'oculis' ),
				'description' => esc_html__( 'Would you like to display the navigation arrows?', 'oculis' ),
				'param_name' => 'ambientes_carousel_show_nav',
				'value' => array(
					esc_html__( 'Yes', 'oculis' ) => 'true',
					esc_html__( 'No', 'oculis' ) => 'false',
				),
				'std' => 'true',
				'save_always' => true,

			),

			// Slider speed
			array(
				'type' => 'textfield',
				'heading' => esc_html( 'Slider Speed', 'oculis' ),
				'description' => esc_html__( 'How fast should the slider switch slides? (Enter in milliseconds e.g. 500) ', 'oculis' ),
				'param_name' => 'ambientes_carousel_slide_speed',
				'std' => '500',
				'save_always' => true,
			),

			// Enable AutoPlay
			array(
				'type' => 'dropdown',
				'heading' => esc_html( 'Enable AutoPlay', 'oculis' ),
				'param_name' => 'ambientes_carousel_autoplay',
				'value' => array(
					esc_html__( 'No', 'oculis' ) => 'false',
					esc_html__( 'Yes', 'oculis' ) => 'true',
				),
				'std' => 'false',
				'save_always' => true,

			),

			// Autoplay speed
			array(
				'type' => 'textfield',
				'heading' => esc_html( 'AutoPlay Speed', 'oculis' ),
				'description' => esc_html__( 'How fast should the slider autoplay? (Enter in milliseconds e.g. 3000) ', 'oculis' ),
				'param_name' => 'ambientes_carousel_autoplay_speed',
				'std' => '3000',
				'save_always' => true,
			),
		)
	) );


	/*---------------------------------------------------------------------------------
		AMBIENTES FILTER
	-----------------------------------------------------------------------------------*/
	vc_map( array(
		'name' => esc_html__( 'Ambientes Filter', 'oculis' ),
		'description' => esc_html__( 'Display a grid of ambientes with a filter.', 'oculis' ),
		'base' => 'krucial-ambientes-filter',
		'category' => esc_html( 'Krucial' ),
		'params' => array(

			// Ambientes Per Row
			array(
				'type' => 'dropdown',
				'heading' => esc_html( 'ambientes Per Row', 'oculis' ),
				'description' => esc_html__( 'How many ambientes in a row?', 'oculis' ),
				'param_name' => 'ambientes_filter_per_row',
				'value' => array(
					esc_html__( 'Two', 'oculis' ) => '2',
					esc_html__( 'Three', 'oculis' ) => '3',
					esc_html__( 'Four', 'oculis' ) => '4',
				),
				'std' => '3',
				'save_always' => true,
			),

		),

	) );


	/*---------------------------------------------------------------------------------
		TESTIMONIALS GRID
	-----------------------------------------------------------------------------------*/
	vc_map( array(
		'name' => esc_html__( 'Testimonials', 'oculis' ),
		'description' => esc_html__( 'Display a grid full of testimonials', 'oculis' ),
		'base' => 'krucial-testimonials',
		'category' => esc_html( 'Krucial' ),
		'params' => array(

			// Number of Total Testimonials
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Total Number of Testimonials', 'oculis' ),
				'description' => esc_html__( 'How many testimonials would you like to display? (-1 means all)', 'oculis' ),
				'param_name' => 'testimonials_number',
				'std' => '-1',
				'save_always' => true,
			),

			// Testimonials Per Row
			array(
				'type' => 'dropdown',
				'heading' => esc_html( 'Testimonials Per Row', 'oculis' ),
				'description' => esc_html__( 'How many testimonials in a row?', 'oculis' ),
				'param_name' => 'testimonials_per_row',
				'value' => array(
					esc_html__( 'One', 'oculis' ) => 'col-12',
					esc_html__( 'Two', 'oculis' ) => 'col-12 col-md-6',
					esc_html__( 'Three', 'oculis' ) => 'col-12 col-md-4',
					esc_html__( 'Four', 'oculis' ) => 'col-12 col-md-3',
				),
				'std' => 'col-12 col-md-6',
				'save_always' => true,
			),

			// Testimonial Categories
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Testimonial Categories', 'oculis' ),
				'description' => esc_html__( 'Which testimonial categories would you like to display?', 'oculis' ),
				'param_name' => 'testimonials_categories',
				'value' => $testimonials,
				'std' => 'all',
				'save_always' => true,
			),

			// Show "See More" Link
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Display Link', 'oculis' ),
				'description' => esc_html__( 'Would you like a link that goes to each testimonials page?', 'oculis' ),
				'param_name' => 'testimonials_link',
				'value' => array(
					esc_html__( 'No', 'oculis' ) => 'false',
					esc_html__( 'Yes', 'oculis' ) => 'true'
				),
				'std' => 'false',
				'save_always' => true,
			)
		)
	));


	/*---------------------------------------------------------------------------------
		Testimonials carousel
	-----------------------------------------------------------------------------------*/
	vc_map( array(
		'name' => esc_html__( 'Testimonials Carousel', 'oculis' ),
		'description' => esc_html__( 'Display a carousel full of testimonials', 'oculis' ),
		'base' => 'krucial-testimonials-carousel',
		'category' => esc_html( 'Krucial' ),
		'params' => array(

			// Number of Total Testimonials
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Total Number of Testimonials', 'oculis' ),
				'description' => esc_html__( 'How many testimonials would you like to display? (-1 means all)', 'oculis' ),
				'param_name' => 'testimonials_carousel_number',
				'std' => '-1',
				'save_always' => true,
			),

			// Testimonials
			array(
				'type' => 'dropdown',
				'heading' => esc_html( 'Testimonials to Show', 'oculis' ),
				'description' => esc_html__( 'How many testimonials would you like to display at once?', 'oculis' ),
				'param_name' => 'testimonials_carousel_per_row',
				'value' => array(
					esc_html__( 'One', 'oculis' ) => '1',
					esc_html__( 'Two', 'oculis' ) => '2',
					esc_html__( 'Three', 'oculis' ) => '3',
					esc_html__( 'Four', 'oculis' ) => '4',
				),
				'std' => '2',
				'save_always' => true,

			),

			// Testimonial Categories
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Testimonial Categories', 'oculis' ),
				'description' => esc_html__( 'Which testimonial categories would you like to display?', 'oculis' ),
				'param_name' => 'testimonials_carousel_categories',
				'value' => $testimonials,
				'std' => 'all',
				'save_always' => true,
			),

			// Show Dots
			array(
				'type' => 'dropdown',
				'heading' => esc_html( 'Show Navigation Dots', 'oculis' ),
				'description' => esc_html__( 'Would you like to display navigation dots under the slider?', 'oculis' ),
				'param_name' => 'testimonials_carousel_show_dots',
				'value' => array(
					esc_html__( 'Yes', 'oculis' ) => 'true',
					esc_html__( 'No', 'oculis' ) => 'false',
				),
				'std' => 'false',
				'save_always' => true,

			),

			// Show navigation
			array(
				'type' => 'dropdown',
				'heading' => esc_html( 'Show Navigation Arrows', 'oculis' ),
				'description' => esc_html__( 'Would you like to display the navigation arrows?', 'oculis' ),
				'param_name' => 'testimonials_carousel_show_nav',
				'value' => array(
					esc_html__( 'Yes', 'oculis' ) => 'true',
					esc_html__( 'No', 'oculis' ) => 'false',
				),
				'std' => 'true',
				'save_always' => true,

			),

			// Slider speed
			array(
				'type' => 'textfield',
				'heading' => esc_html( 'Slider Speed', 'oculis' ),
				'description' => esc_html__( 'How fast should the slider switch slides? (Enter in milliseconds e.g. 500) ', 'oculis' ),
				'param_name' => 'testimonials_carousel_slide_speed',
				'std' => '500',
				'save_always' => true,
			),

			// Enable AutoPlay
			array(
				'type' => 'dropdown',
				'heading' => esc_html( 'Enable AutoPlay', 'oculis' ),
				'param_name' => 'testimonials_carousel_autoplay',
				'value' => array(
					esc_html__( 'No', 'oculis' ) => 'false',
					esc_html__( 'Yes', 'oculis' ) => 'true',
				),
				'std' => 'false',
				'save_always' => true,

			),

			// Autoplay speed
			array(
				'type' => 'textfield',
				'heading' => esc_html( 'AutoPlay Speed', 'oculis' ),
				'description' => esc_html__( 'How fast should the slider autoplay? (Enter in milliseconds e.g. 3000) ', 'oculis' ),
				'param_name' => 'testimonials_carousel_autoplay_speed',
				'std' => '3000',
				'save_always' => true,
			),

			// Show "See More" Link
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Display Link', 'oculis' ),
				'description' => esc_html__( 'Would you like to display a link that goes to each testimonials page?', 'oculis' ),
				'param_name' => 'testimonials_carousel_link',
				'value' => array(
					esc_html__( 'No', 'oculis' ) => 'false',
					esc_html__( 'Yes', 'oculis' ) => 'true'
				),
				'std' => 'false',
				'save_always' => true,
			)
		)
	));


	/*---------------------------------------------------------------------------------
		Staff members
	-----------------------------------------------------------------------------------*/
	vc_map( array(
		'name' => esc_html__( 'Staff Members', 'oculis' ),
		'description' => esc_html__( 'Display a grid which shows your company staff members.', 'oculis' ),
		'base' => 'krucial-staff-members',
		'category' => esc_html( 'Krucial' ),
		'params' => array(

			// Number of Total Staff
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Total Number of Staff', 'oculis' ),
				'description' => esc_html__( 'How many staff would you like to display? (-1 means all)', 'oculis' ),
				'param_name' => 'staff_number',
				'std' => '-1',
				'save_always' => true,
			),

			// Staff Per Row
			array(
				'type' => 'dropdown',
				'heading' => esc_html( 'Staff Per Row', 'oculis' ),
				'description' => esc_html__( 'How many staff member\'s in a row?', 'oculis' ),
				'param_name' => 'staff_per_row',
				'value' => array(
					esc_html__( 'One', 'oculis' ) => 'col-12',
					esc_html__( 'Two', 'oculis' ) => 'col-12 col-md-6',
					esc_html__( 'Three', 'oculis' ) => 'col-12 col-md-4',
					esc_html__( 'Four', 'oculis' ) => 'col-12 col-md-3'
				),
				'std' => 'col-12 col-md-6',
				'save_always' => true,
			),

			// Staff Categories
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Staff Categories', 'oculis' ),
				'description' => esc_html__( 'Which staff categories would you like to display?', 'oculis' ),
				'param_name' => 'staff_categories',
				'value' => $members,
				'std' => 'all',
				'save_always' => true,
			),

			// Show "See More" Link
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Display Link', 'oculis' ),
				'description' => esc_html__( 'Would you like to display a link that goes to each staff members profile?', 'oculis' ),
				'param_name' => 'staff_link',
				'value' => array(
					esc_html__( 'No', 'oculis' ) => 'false',
					esc_html__( 'Yes', 'oculis' ) => 'true'
				),
				'std' => 'false',
				'save_always' => true,
			)
		)
	));



	/*---------------------------------------------------------------------------------
		NUMBER COUNTER
	-----------------------------------------------------------------------------------*/
	vc_map(array(
		'name' => esc_html__( 'Number Counter', 'oculis' ),
		'description' => esc_html__( 'Add a number counter to your page', 'oculis' ),
		'base' => 'krucial-number-counter',
		'category' => esc_html( 'Krucial' ),
		'params' => array(

			// Title
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'CountUp Title', 'oculis' ),
				'param_name' => 'number_counter_title',
				'group' => esc_html( 'General', 'oculis' ),
			),

			// Value
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'CountUp Value', 'oculis' ),
				'param_name' => 'number_counter_value',
				'group' => esc_html( 'General', 'oculis' ),
			),

			// Value prefix
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Value Prefix', 'oculis' ),
				'param_name' => 'number_counter_prefix',
				'group' => esc_html( 'General', 'oculis' ),
			),

			// Value suffix
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Value Suffix', 'oculis' ),
				'param_name' => 'number_counter_suffix',
				'group' => esc_html( 'General', 'oculis' ),
			),

			// Timer
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Timer', 'oculis' ),
				'description' => esc_html__( 'How long should the animation last? Enter in seconds, Example: 2.5', 'oculis' ),
				'param_name' => 'number_counter_timer',
				'save_always' => true,
				'value' => esc_html('2.5'),
				'group' => esc_html( 'General', 'oculis' ),
			),

			// Alignment
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Alignment', 'oculis' ),
				'description' => esc_html__( 'If you wish to align the countup in the center, check this box.', 'oculis' ),
				'param_name' => 'number_counter_align',
				'value' => array(
					esc_html__( 'Left Align', 'oculis' ) => 'left',
					esc_html__( 'Center Align', 'oculis' ) => 'center',
					esc_html__( 'Right Align', 'oculis' ) => 'right',
				),
				'save_always' => true,
				'std' => 'left',
				'group' => esc_html( 'Styling', 'oculis' ),
			),

			// Font size
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Font Size', 'oculis' ),
				'description' => esc_html__( 'Enter the font-size in px. Example: 30', 'oculis' ),
				'param_name' => 'number_counter_size',
				'group' => esc_html( 'Styling', 'oculis' ),
			),

			// Icon Color
			array(
				'type' => 'colorpicker',
				'heading' => esc_html__( 'Text Color', 'oculis' ),
				'param_name' => 'number_counter_color',
				'group' => esc_html( 'Styling', 'oculis' ),
			),

		)

	));



	/*---------------------------------------------------------------------------------
		CONTENT BOX
	-----------------------------------------------------------------------------------*/
	class WPBakeryShortCode_Krucial_Content_Box extends WPBakeryShortCodesContainer {}

	vc_map( array(
		'name' => esc_html__( 'Content Box', 'oculis' ),
		'description' => esc_html__( 'Wrap any elements inside of a content box', 'oculis' ),
		'base' => 'krucial_content_box',
		'category' => esc_html( 'Krucial' ),
		'as_parent' => array( 'except' => 'krucial-contentbox' ),
		'js_view' => 'VcColumnView',
		'show_settings_on_create' => false,
		'params' => array(

			// alignment
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Content Box Alignment', 'oculis' ),
				'description' => esc_html__( 'Should the content box be aligned left, right or in the center? Best to set a max-width with this option.', 'oculis' ),
				'param_name' => 'content_box_alignment',
				'value' => array(
					esc_html__( 'Left', 'oculis' ) => 'left',
					esc_html__( 'Center', 'oculis' ) => 'center',
					esc_html__( 'Right', 'oculis' ) => 'right',
				),
				'std' => 'left',
				'save_always' => true,
			),

			// max width
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Content Box Width', 'oculis' ),
				'description' => esc_html__( 'Add this value in px, for example: 600', 'oculis' ),
				'param_name' => 'content_box_width',
				'save_always' => true,
			),

			// custom class
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Custom CSS Class', 'oculis' ),
				'param_name' => 'content_box_css_class',
				'save_always' => true,
			),
		)
	));



	/*---------------------------------------------------------------------------------
		ANIMATE
	-----------------------------------------------------------------------------------*/
	class WPBakeryShortCode_Krucial_Animate extends WPBakeryShortCodesContainer {}

	vc_map( array(
		'name' => esc_html__( 'Animate Element', 'oculis' ),
		'description' => esc_html__( 'CSS animations for any element!', 'oculis' ),
		'base' => 'krucial_animate',
		'category' => esc_html( 'Krucial' ),
		'as_parent' => array( 'except' => 'krucial_animate' ),
		'js_view' => 'VcColumnView',
		'params' => array(

			// Choose animation
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Choose Animation', 'oculis' ),
				'description' => esc_html__( 'Select the type of animation you would like to use', 'oculis' ),
				'param_name' => 'animate_element_type',
				'value' => array(

					// Fading entrances
					'fadeIn', 'fadeInUp', 'fadeInDown', 'fadeInLeft', 'fadeInRight',

					// Growing entrances
					'growIn', 'growInTop', 'growInBottom', 'growInTopLeft', 'growInTopRight', 'growInBottomLeft', 'growInBottomRight'

				),
				'save_always' => true,
				'std' => 'fadeIn',
			),

			// Animation duration
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Duration', 'oculis' ),
				'description' => esc_html__( 'How long should the animation last? Enter in seconds (e.g. \'2\' equals 2 seconds).', 'oculis' ),
				'param_name' => 'animate_element_duration',
				'value' => esc_html('2'),
				'save_always' => true,
			),

			// Animation delay
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Delay', 'oculis' ),
				'description' => esc_html__( 'Would you like to delay the animation? Enter in seconds (e.g. \'2\' equals 2 seconds).', 'oculis' ),
				'param_name' => 'animate_element_delay',
				'value' => esc_html('0.5'),
				'save_always' => true,
			),

			// Animation offset
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Offset', 'oculis' ),
				'description' => esc_html__( 'Would you like to delay the animation until it is at a certain position on your screen? Enter in px (e.g. \'200\' equals 200px).', 'oculis' ),
				'param_name' => 'animate_element_offset',
				'value' => esc_html('0'),
				'save_always' => true,
			),
		)
	));


	/*---------------------------------------------------------------------------------
		CREATE SPACE
	-----------------------------------------------------------------------------------*/
	vc_map( array(
		'name' => esc_html__( 'Create Space', 'oculis' ),
		'description' => esc_html__( 'Create space for multiple devices', 'oculis' ),
		'base' => 'krucial-create-space',
		'category' => esc_html( 'Krucial' ),
		'params' => array(

			// Desktop Spacing
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Desktop Spacing', 'oculis' ),
				'description' => esc_html__( 'Enter the spacing in px for Desktop Sized Devices.', 'oculis' ),
				'param_name' => 'create_space_desktop',
			),

			// Tablet Spacing
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Tablet Spacing', 'oculis' ),
				'description' => esc_html__( 'Enter the spacing in px for Tablet Sized Devices.', 'oculis' ),
				'param_name' => 'create_space_tablet',
			),

			// Mobile Spacing
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Mobile Spacing', 'oculis' ),
				'description' => esc_html__( 'Enter the spacing in px for Mobile Sized Devices.', 'oculis' ),
				'param_name' => 'create_space_mobile'
			),

		)

	) );


	/*---------------------------------------------------------------------------------
		ICON BOX
	-----------------------------------------------------------------------------------*/
	vc_map( array(
		'name' => esc_html__( 'Icon Box', 'oculis'),
		'description' => esc_html__( 'Add in a custom icon box', 'oculis' ),
		'base' => 'krucial-icon-box',
		'category' => esc_html( 'Krucial' ),
		'params' => array(

			// The Icon
			array(
				'type' => 'iconpicker',
				'heading' => esc_html__( 'Choose Icon', 'oculis' ),
				'param_name' => 'icon_box_icon',
				'settings' => array(
					'emptyIcon' => false,
					'type' => 'font-awesome-5',
					'iconsPerPage' => 100,
				),
			),

			// Title
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Icon Box Title', 'oculis' ),
				'param_name' => 'icon_box_title',
			),

			// Text
			array(
				'type' => 'textarea',
				'heading' => esc_html__( 'Icon Box Text', 'oculis' ),
				'param_name' => 'icon_box_text',
			),

			// Size
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Icon Size', 'oculis' ),
				'param_name' => 'icon_box_size',
				'value' => array(
					esc_html__( 'Small', 'oculis' ) => 'small',
					esc_html__( 'Medium', 'oculis' ) => 'medium',
					esc_html__( 'Large', 'oculis' ) => 'large',
				),
				'save_always' => true,
				'std' => 'medium',
			),

			// Radius
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Radius', 'oculis' ),
				'param_name' => 'icon_box_radius',
				'value' => array(
					esc_html__( 'Square', 'oculis' ) => 'square',
					esc_html__( 'Rounded', 'oculis' ) => 'rounded',
				),
				'save_always' => true,
				'std' => 'rounded',
			),

			// Icon Margin
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Icon Margin', 'oculis' ),
				'description' => esc_html__( 'Adjust the icons top margin in pixels to fit it perfectly. Example - 5', 'oculis' ),
				'param_name' => 'icon_box_icon_margin',
				'std' => 5,
			),

			// Center Align
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Center Align', 'oculis' ),
				'description' => esc_html__( 'Would you like to align the icon, title and text all in the center?', 'oculis' ),
				'param_name' => 'icon_box_center',
				'value' => array(
					esc_html__( 'Yes', 'oculis' ) => 'center-align',
					esc_html__( 'No', 'oculis' ) => 'left-align',
				),
				'std' => 'left-align',
				'save_always' => true,
			),

			// Icon on Left
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Icon on Left', 'oculis' ),
				'description' => esc_html__( 'Would you like to place the icon on the left?', 'oculis' ),
				'param_name' => 'icon_box_icon_left',
				'value' => array(
					esc_html__( 'Yes', 'oculis' ) => 'icon-left',
					esc_html__( 'No', 'oculis' ) => 'icon-top',
				),
				'std' => 'icon-top',
				'save_always' => true,
			),

			// Is active
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Keep Active', 'oculis' ),
				'description' => esc_html__( 'This will make the icon box stand out, giving it the hover effect', 'oculis' ),
				'param_name' => 'icon_box_active',
				'value' => array(
					esc_html__( 'No', 'oculis' ) => 'false',
					esc_html__( 'Yes', 'oculis' ) => 'true'
				),
				'std' => false,
				'save_always' => true,
			),

			// No Background
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Disable Background', 'oculis' ),
				'description' => esc_html__( 'Would you like to disable the background so only the icon shows?', 'oculis' ),
				'param_name' => 'icon_box_background',
				'value' => array(
					esc_html__( 'No', 'oculis' ) => 'false',
					esc_html__( 'Yes', 'oculis' ) => 'true'
				),
				'std' => false,
				'save_always' => true,
			),

			// Icon Color
			array(
				'type' => 'colorpicker',
				'heading' => esc_html__( 'Icon Color', 'oculis' ),
				'param_name' => 'icon_box_color',
				'group' => esc_html__( 'Colors', 'oculis' ),
			),

			// Background Color
			array(
				'type' => 'colorpicker',
				'heading' => esc_html__( 'Background Color', 'oculis' ),
				'param_name' => 'icon_box_bg',
				'group' => esc_html__( 'Colors', 'oculis' ),
			),

		)

	) );



	/*---------------------------------------------------------------------------------
		CUSTOM HEADING
	-----------------------------------------------------------------------------------*/
	vc_map( array(
		'name' => esc_html__( 'Custom Heading', 'oculis'),
		'description' => esc_html__( 'Create a custom heading', 'oculis' ),
		'base' => 'krucial-custom-heading',
		'category' => esc_html( 'Krucial' ),
		'params' => array(

			// The Title
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Heading Title', 'oculis' ),
				'description' => esc_html__( 'Enter the title for your custom heading.', 'oculis' ),
				'param_name' => 'heading_title'
			),

			// Text Transform
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Text Transform', 'oculis' ),
				'description' => esc_html__( 'What case should the text of your title be?', 'oculis' ),
				'param_name' => 'heading_transform',
				'value' => array(
					esc_html__( 'None', 'oculis' ) => 'none',
					esc_html__( 'Capitalize', 'oculis' ) => 'capitalize',
					esc_html__( 'Uppercase', 'oculis' ) => 'uppercase',
					esc_html__( 'Lowercase', 'oculis' ) => 'lowercase',
				),
				'std' => 'left',
				'save_always' => true,
			),

			// Font Weight
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Font Weight', 'oculis' ),
				'description' => esc_html__( 'Select the font weight for your heading.', 'oculis' ),
				'param_name' => 'heading_weight',
				'value' => array(
					esc_html__( 'Normal', 'oculis' ) => '400',
					esc_html__( 'Bold', 'oculis' ) => '700',
				),
				'std' => '700',
				'save_always' => true,
			),

			// The heading tag (h1,h2,h3)
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Heading Tag', 'oculis' ),
				'description' => esc_html__( 'Would you like to use an h1, h2, h3, h4 tag?', 'oculis' ),
				'param_name' => 'heading_tag',
				'value' => array(
					esc_html__( 'H1 Tag', 'oculis' ) => 'h1',
					esc_html__( 'H2 Tag', 'oculis' ) => 'h2',
					esc_html__( 'H3 Tag', 'oculis' ) => 'h3',
					esc_html__( 'H4 Tag', 'oculis' ) => 'h4',
				),
				'std' => 'h2',
				'save_always' => true,
			),

			// Color
			array(
				'type' => 'colorpicker',
				'heading' => esc_html__( 'Heading Color', 'oculis' ),
				'description' => esc_html__( 'Choose your heading\'s color', 'oculis' ),
				'param_name' => 'heading_color',
				'group'       => esc_html__( 'Colors', 'oculis' ),
			),

			// Mark Color
			array(
				'type' => 'colorpicker',
				'heading' => esc_html__( 'Mark Color', 'oculis' ),
				'description' => esc_html__( 'Choose the mark tags color', 'oculis' ),
				'param_name' => 'heading_mark_color',
				'group'       => esc_html__( 'Colors', 'oculis' ),
			),

			// Separator Color
			array(
				'type' => 'colorpicker',
				'heading' => esc_html__( 'Separator Color', 'oculis' ),
				'description' => esc_html__( 'Choose your separator\'s color', 'oculis' ),
				'param_name' => 'heading_separator_color',
				'group'       => esc_html__( 'Colors', 'oculis' ),
			),

			// Add separator
			array(
				'type' => 'dropdown',
				'heading' => esc_html( 'Add Title Separator', 'oculis' ),
				'description' => esc_html__( 'Would you like to show a separator under the title?', 'oculis' ),
				'param_name' => 'heading_add_separator',
				'value' => array(
					esc_html__( 'No', 'oculis' ) => 'false',
					esc_html__( 'Yes', 'oculis' ) => 'true',
				),
				'std' => 'false',
				'save_always' => true,

			),


			/**
			 * Responsive Sizes
			 */

			// Font Size - Default
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Font Size', 'oculis' ),
				'description' => esc_html__( 'Enter the font-size in px. Example: 30', 'oculis' ),
				'param_name' => 'heading_size',
				'group' => esc_html( 'Font Size', 'oculis' ),
			),

			// Medium Sized Devices
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Font Size - Medium Devices', 'oculis' ),
				'description' => esc_html__( 'Enter the font-size in px. Example: 30 - for medium sized devices between 600-1000px in screen width.', 'oculis' ),
				'param_name' => 'heading_size_medium',
				'group' => esc_html( 'Font Size', 'oculis' ),
			),

			// Small Sized Devices
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Font Size - Small Devices', 'oculis' ),
				'description' => esc_html__( 'Enter the font-size in px. Example: 30 - for small sized devices between 0-600px in screen width.', 'oculis' ),
				'param_name' => 'heading_size_small',
				'group' => esc_html( 'Font Size', 'oculis' ),
			),

			// Letter spacing - Default
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Letter Spacing', 'oculis' ),
				'description' => esc_html__( 'Enter the letter-spacing in px. Example: -0.5', 'oculis' ),
				'param_name' => 'heading_spacing',
				'group' => esc_html( 'Letter Spacing', 'oculis' ),
			),

			// Medium Sized Devices
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Letter Spacing - Medium Sized Devices', 'oculis' ),
				'description' => esc_html__( 'Enter the letter-spacing in px. Example: -0.5 for medium sized devices.', 'oculis' ),
				'param_name' => 'heading_spacing_medium',
				'group' => esc_html( 'Letter Spacing', 'oculis' ),
			),

			// Small Sized Devices
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Letter Spacing - Small Sized Device', 'oculis' ),
				'description' => esc_html__( 'Enter the letter-spacing in px. Example: -0.5 for small sized devices.', 'oculis' ),
				'param_name' => 'heading_spacing_small',
				'group' => esc_html( 'Letter Spacing', 'oculis' ),
			),

			// Heading Alignment - Default
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Heading Alignment', 'oculis' ),
				'description' => esc_html__( 'Should the heading be aligned left, right or in the center?', 'oculis' ),
				'param_name' => 'heading_alignment',
				'value' => array(
					esc_html__( 'Left', 'oculis' ) => 'left',
					esc_html__( 'Center', 'oculis' ) => 'center',
					esc_html__( 'Right', 'oculis' ) => 'right',
				),
				'std' => 'left',
				'save_always' => true,
				'group' => esc_html( 'Alignment', 'oculis' ),
			),

			// Medium Sized Devices
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Heading Alignment - Medium Sized Devices', 'oculis' ),
				'description' => esc_html__( 'Choose the heading alignment for medium sized devices.', 'oculis' ),
				'param_name' => 'heading_alignment_medium',
				'value' => array(
					esc_html__( 'Left', 'oculis' ) => 'left',
					esc_html__( 'Center', 'oculis' ) => 'center',
					esc_html__( 'Right', 'oculis' ) => 'right',
				),
				'std' => 'left',
				'save_always' => true,
				'group' => esc_html( 'Alignment', 'oculis' ),
			),

			// Small Sized Devices
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Heading Alignment - Small Sized Devices', 'oculis' ),
				'description' => esc_html__( 'Choose the heading alignment for small sized devices.', 'oculis' ),
				'param_name' => 'heading_alignment_small',
				'value' => array(
					esc_html__( 'Left', 'oculis' ) => 'left',
					esc_html__( 'Center', 'oculis' ) => 'center',
					esc_html__( 'Right', 'oculis' ) => 'right',
				),
				'std' => 'left',
				'save_always' => true,
				'group' => esc_html( 'Alignment', 'oculis' ),
			),


			/**
			 * Spacing Options
			 */

			// Margin Top
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Margin Top', 'oculis' ),
				'description' => esc_html__( 'Enter the top margin in px.', 'oculis' ),
				'param_name' => 'heading_margin_top',
				'group' => esc_html( 'Spacing', 'oculis' ),
			),

			// Margin Bottom
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Margin Bottom', 'oculis' ),
				'description' => esc_html__( 'Enter the bottom margin in px.', 'oculis' ),
				'param_name' => 'heading_margin_bottom',
				'group' => esc_html( 'Spacing', 'oculis' ),
			),
		)
	));
}

?>