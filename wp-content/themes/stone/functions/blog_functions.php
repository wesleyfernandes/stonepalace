<?php

/**
 * Show posts only in search results
 */
function krucial_search_filter( $query ) {
    if ( $query->is_search ) {
        $query->set( 'post_type', 'post' );
    }
    return $query;
}
add_filter( 'pre_get_posts', 'krucial_search_filter' );


/**
 * Get the post categories
 */
function krucial_post_categories() {
    $categories = get_the_category();
    $separator  = ', ';
    $output     = '';
    $i          = 1;

    if ( !empty( $categories ) ):
        foreach ( $categories as $category ):
            if ( $i > 1 ): $output .= $separator; endif;
            $output .= '<a href="' . esc_url( get_category_link( $category->term_id ) ) . '">' . esc_html( $category->name ) .'</a>';
            $i++;
        endforeach;
    endif;

    return $output;
}


/**
 * Move comment field to bottom
 */

function krucial_move_comment_field_to_bottom( $fields ) {
    $comment_field = $fields['comment'];
    unset( $fields['comment'] );
    $fields['comment'] = $comment_field;
    return $fields;
}

add_filter( 'comment_form_fields', 'krucial_move_comment_field_to_bottom' );



/**
 * Render comment section
 */

function krucial_render_comments( $comment, $args, $depth ) { ?>
    <div id="comment-<?php comment_ID() ?>" class="comment-body">
        <div class="comment-head">
            <?php if ($comment->comment_type !== 'pingback') { ?>
                <div class="comment-avatar clearfix">
                    <?php echo get_avatar( $comment, 90 ); ?>
                </div>
            <?php } ?>
            <div class="comment-top">
                <div class="comment-author">
                    <span>
                        <?php echo get_comment_author_link(); ?>
                    </span>
                    <a class="the-date" href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ); ?>">
                        <i class="krucial-essential-light-86-clock essentials"></i>
		                <?php printf( esc_html__( '%1$s at %2$s', 'oculis' ), get_comment_date(), get_comment_time() ); ?>
                    </a>
                </div>
                <div class="comment-meta">
                    <?php

                    // Comment Reply Link
                    comment_reply_link( array_merge( $args, array(
                        'reply_text' => esc_html__( 'Reply', 'oculis' ),
                        'depth'      => $depth,
                        'max_depth'  => $args['max_depth']
                    ) ) );

                    // Comment Edit Link
                    edit_comment_link( esc_html__( 'Edit', 'oculis' ), '  ', '' );

                    ?>
                </div>
            </div>
        </div>
        <div class="comment-text">
            <?php comment_text(); ?>
        </div>
        <?php if ( $comment->comment_approved == '0' ) { ?>
            <div class="comment-waiting-approval">
                <?php esc_html__( 'Your comment is awaiting approval.', 'oculis' ); ?>
            </div>
        <?php } ?>
    </div>
    <?php
}