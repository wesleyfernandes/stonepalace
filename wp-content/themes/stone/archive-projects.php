<?php
/**
 * Template for projects archive
 */

get_header();

?>

<div class="post-type-page-header">
    <div class="container">
        <div class="page-header-inner">
            <h1 class="title"><?php echo esc_html__('All Projects', 'oculis'); ?></h1>
        </div>
    </div>
</div>
<div class="container">
    <div class="cpt-archive">
        <div class="content-area row">
            <?php

            if ( have_posts() ):
                while ( have_posts() ): the_post(); ?>
                    <div class="col-12 col-md-6">
                        <div id="post-<?php the_ID() ?>" <?php post_class() ?>>
                            <?php get_template_part( 'components/cpt/post-type-card' ); ?>
                        </div>
                    </div>
            <?php endwhile;
            endif;

            echo paginate_links( array(
                'type'      => 'list',
                'prev_text' => '<i class="icon-arrow-left"></i>',
                'next_text' => '<i class="icon-arrow-right"></i>',
            ) );

            ?>
        </div>
    </div>
</div>

<?php get_footer(); ?>