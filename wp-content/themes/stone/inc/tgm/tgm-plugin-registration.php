<?php

require_once get_template_directory() . '/inc/tgm/class-tgm-plugin-activation.php';

function krucial_register_plugins() {

    $plugins_path = get_template_directory() . '/inc/tgm/plugins';
    $plugins      = array(

	    // Theme Functionality
	    array(
		    'name'               => esc_html('Krucial Theme Functionality'),
		    'slug'               => 'krucial_theme_functionality',
		    'source'             => $plugins_path . '/krucial_theme_functionality.zip',
		    'required'           => true,
		    'force_activation'   => false,
		    'force_deactivation' => false,
	    ),

        // Visual Composer
        array(
            'name'               => esc_html('WPBakery Visual Composer'),
            'slug'               => 'js_composer',
            'source'             => $plugins_path . '/js_composer.zip',
            'required'           => true,
            'force_activation'   => false,
            'force_deactivation' => false,
        ),

	    // Slider Revolution
	    array(
		    'name'               => esc_html('Slider Revolution'),
		    'slug'               => 'revslider',
		    'source'             => $plugins_path . '/revslider.zip',
		    'required'           => false,
		    'force_activation'   => false,
		    'force_deactivation' => false,
	    ),

        // Contact Form 7
        array(
            'name'               => esc_html('Contact Form 7'),
            'slug'               => 'contact-form-7',
            'required'           => false,
            'force_activation'   => false,
            'force_deactivation' => false,
        ),

        // WooCommerce
        array(
            'name'               => esc_html('WooCommerce'),
            'slug'               => 'woocommerce',
            'required'           => false,
            'force_activation'   => false,
            'force_deactivation' => false,
        ),

    );

    // TGM Main Settings
    $config = array(
        'id'           => 'krucial',               // Unique ID for hashing notices for multiple instances of TGMPA
        'default_path' => '',                      // Default absolute path to bundled plugins
        'menu'         => 'tgmpa-install-plugins', // Menu slug
        'has_notices'  => true,                    // Show admin notices
        'dismissable'  => true,                    // If false, a user cannot dismiss the nag message
        'dismiss_msg'  => '',                      // If 'dismissable' is false, this message will be output at top of nag
        'is_automatic' => false,                   // Automatically activate plugins after installation or not
        'message'      => '',                      // Message to output right before the plugins table
    );

    tgmpa( $plugins, $config );
}
add_action( 'tgmpa_register', 'krucial_register_plugins' );

?>