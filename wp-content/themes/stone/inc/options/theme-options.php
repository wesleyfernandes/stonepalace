<?php

/**
 * For full documentation, please visit: http://docs.reduxframework.com/
 * For a more extensive sample-config file, you may look at:
 * https://github.com/reduxframework/redux-framework/blob/master/sample/sample-config.php
 */

if ( ! class_exists( 'Redux' ) ) {
	return;
}

$opt_name = 'krucial_options';
$theme = wp_get_theme();

$args = array(
	'opt_name'              => $opt_name,
	'dev_mode'              => FALSE,
	'use_cdn'               => FALSE,
	'display_name'          => $theme->get( 'Name' ),
	'display_version'       => $theme->get( 'Version' ),
	'page_slug'             => 'theme-options',
	'page_title'            => 'Theme Options',
	'update_notice'         => TRUE,
	'admin_bar'             => TRUE,
	'menu_type'             => 'submenu',
	'page_parent'           => 'krucial-welcome',
	'menu_title'            => 'Theme Options',
	'allow_sub_menu'        => FALSE,
	'page_parent_post_type' => 'your_post_type',
	'default_mark'          => '*',
	'class'                 => 'theme-options',
	'output'                => TRUE,
	'output_tag'            => TRUE,
	'settings_api'          => TRUE,
	'cdn_check_time'        => '1440',
	'compiler'              => TRUE,
	'page_permissions'      => 'manage_options',
	'save_defaults'         => TRUE,
	'show_import_export'    => TRUE,
	'database'              => 'options',
	'transient_time'        => '3600',
	'network_sites'         => TRUE,
);


Redux::setArgs( $opt_name, $args );


/*--------------------------------------------------------------
# General
--------------------------------------------------------------*/

Redux::setSection( $opt_name, array(
	'title' => esc_html__( 'General', 'oculis' ),
	'icon' => 'el-icon-home',
));

/**
 * Logos
 */
Redux::setSection( $opt_name, array(
	'title' => esc_html__( 'Logos', 'oculis' ),
	'subsection' => true,
	'fields' => array(

		// Logo
		array(
			'title'	=> esc_html__( 'Logo', 'oculis' ),
			'subtitle' => esc_html__( 'Upload your custom site logo.', 'oculis' ),
			'id' => 'custom_logo',
			'url' => true,
			'type' => 'media',
			'read-only'	=> false
		),

		// Transparent Logo
		array(
			'title'	=> esc_html__( 'Transparent Header Logo', 'oculis' ),
			'subtitle' => esc_html__( 'Upload your custom logo for the transparent header.', 'oculis' ),
			'id' => 'transparent_logo',
			'url' => true,
			'type' => 'media',
			'read-only' => false
		),

		// Logo dimensions
		array(
			'id'       => 'logo_dimensions',
			'type'     => 'dimensions',
			'units'    => false,
			'title'    => esc_html__('Logo Dimensions (Width/Height)', 'oculis'),
			'subtitle' => esc_html__('Set the width and height of your logo in px.', 'oculis')
		),

		// Logo position
		array(
			'title' => esc_html__('Logo Position', 'oculis'),
			'subtitle' => esc_html__('Move your logo around to fit it perfectly.', 'oculis'),
			'id' => 'logo_position',
			'type' => 'spacing',
			'output' => array('.logo img'),
			'mode' => 'margin',
			'units' => array('px'),
			'units_extended' => 'false',
			'default' => array(
				'margin-top' => '5px',
				'margin-right' => '0px',
				'margin-bottom' => '0px',
				'margin-left' => '0px',
				'units' => 'px',
			)
		),

	)
));

Redux::setSection( $opt_name, array(
	'title'      => esc_html__('Toolbar', 'oculis'),
	'subsection' => true,
	'fields'     => array(

		array(
			'title'            => esc_html__('Header Toolbar', 'oculis'),
			'subtitle'         => esc_html__('Enable or disable an optional toolbar for the header. Note that this only applies for header styles 2 and 3.', 'oculis'),
			'id'               => 'toolbar_switch',
			'type'             => 'switch',
			'default'          => false,
		),

		array(
			'title'            => esc_html__('Social Media', 'oculis'),
			'subtitle'         => esc_html__('Choose the social media icons to display within Theme Options > Social Media > Header Social Media.', 'oculis'),
			'id'               => 'toolbar_social',
			'type'             => 'switch',
			'default'          => true,
			'required'         => array( 'toolbar_switch', '=', true ),
		),

		array(
			'title'            => esc_html__('WooCommerce Icon', 'oculis'),
			'id'               => 'toolbar_shop',
			'type'             => 'switch',
			'default'          => true,
			'required'         => array( 'toolbar_switch', '=', true ),
		),

		array(
			'title'            => esc_html__('Enable Phone Number', 'oculis'),
			'subtitle'         => esc_html__('This will enable a phone number section within your toolbar.', 'oculis'),
			'id'               => 'toolbar_number_switch',
			'type'             => 'switch',
			'default'          => false,
			'required'         => array( 'toolbar_switch', '=', true ),
		),

		array(
			'title'            => esc_html__('Phone Number', 'oculis'),
			'id'               => 'toolbar_number',
			'type'             => 'text',
			'default'          => esc_html('08 1234 5678'),
			'required'         => array( 'toolbar_number_switch', '=', true ),
		),

		array(
			'title'            => esc_html__('Enable Address Location', 'oculis'),
			'subtitle'         => esc_html__('This will enable a business address section within your toolbar.', 'oculis'),
			'id'               => 'toolbar_location_switch',
			'type'             => 'switch',
			'default'          => false,
			'required'         => array( 'toolbar_switch', '=', true ),
		),

		array(
			'title'            => esc_html__('Business Location', 'oculis'),
			'id'               => 'toolbar_location',
			'type'             => 'text',
			'default'          => esc_html('24 Mayweather PDE, Mosman Park'),
			'required'         => array( 'toolbar_location_switch', '=', true ),
		)

	)));

Redux::setSection( $opt_name, array(
	'title'      => esc_html__( 'Header', 'oculis' ),
	'id'         => 'header_style_options',
	'subsection' => true,
	'fields'     => array(

		// Sticky header
		array(
			'title'            => esc_html__('Enable Sticky Header', 'oculis'),
			'id'               => 'enable_sticky_header',
			'type'             => 'switch',
			'default'          => false,
		),

		// Header style
		array(
			'title'            => esc_html__( 'Header Style', 'oculis' ),
			'subtitle'         => esc_html__( 'Default page layout.', 'oculis' ),
			'id'               => 'header_style',
			'type'             => 'button_set',
			'options'          => array(
				'header-style-1' => esc_html__('Style 1', 'oculis'),
				'header-style-2' => esc_html__('Style 2', 'oculis'),
				'header-style-3' => esc_html__('Style 3', 'oculis'),
				'header-style-4' => esc_html__('Style 4', 'oculis'),
			),
			'default'          => 'header-style-1'
		),


		/**
		 * Header style 1 options
		 */

		// Nothing / phone number / quote button
		array(
			'title' => esc_html__('Call To Action', 'oculis'),
			'subtitle' => esc_html__('Select your header\'s call to action', 'oculis'),
			'id' => 'hs1_header_action',
			'type' => 'select',
			'options' => array(
				'nothing' => esc_html__('Nothing', 'oculis' ),
				'number' => esc_html__('Phone Number', 'oculis' ),
				'button' => esc_html__('Quote Button', 'oculis' ),
			),
			'default' => 'nothing',
			'required' => array('header_style', '=', 'header-style-1'),
		),

		// Phone number
		array(
			'id'             => 'hs1_phone_number',
			'type'           => 'text',
			'title'          => esc_html__('Phone Number', 'oculis'),
			'default'        => esc_html('+71 7083 4526'),
			'required'       => array(
				array('header_style', '=', 'header-style-1'),
				array('hs1_header_action', '=', 'number')
			)
		),

		// Quote Button Text
		array(
			'id'             => 'hs1_button_text',
			'type'           => 'text',
			'title'          => esc_html__('Quote Button Text', 'oculis'),
			'default'        => esc_html('Free Quote'),
			'required'       => array(
				array('header_style', '=', 'header-style-1'),
				array('hs1_header_action', '=', 'button')
			)
		),


		/**
		 * Header style 2 options
		 */

		array(
			'title'            => esc_html__('Social Media', 'oculis'),
			'subtitle'         => esc_html__('Choose the social media icons to display within Theme Options > Social Media > Header Social Media.', 'oculis'),
			'id'               => 'hs2_social_switch',
			'type'             => 'switch',
			'default'          => true,
			'required'         => array('header_style', '=', 'header-style-2'),
		),

		// Phone Number
		array(
			'title'            => esc_html__('Enable the header\'s phone number box.', 'oculis'),
			'id'               => 'hs2_phone_switch',
			'type'             => 'switch',
			'default'          => true,
			'required'         => array('header_style', '=', 'header-style-2'),
		),

		array(
			'title'            => esc_html__('Phone Number - Top Text', 'oculis'),
			'id'               => 'hs2_phone_top',
			'type'             => 'text',
			'default'          => esc_html('Main Sales Office'),
			'required'         => array( 'hs2_phone_switch', '=', true ),
		),

		array(
			'title'            => esc_html__('Phone Number - Bottom Text', 'oculis'),
			'id'               => 'hs2_phone_bottom',
			'type'             => 'text',
			'default'          => esc_html('+71 7083 4526'),
			'required'         => array( 'hs2_phone_switch', '=', true ),
		),

		// Location
		array(
			'title'            => esc_html__('Enable the header\'s location box.', 'oculis'),
			'id'               => 'hs2_location_switch',
			'type'             => 'switch',
			'default'          => true,
			'required'         => array('header_style', '=', 'header-style-2'),
		),

		array(
			'title'            => esc_html__('Location - Top Text', 'oculis'),
			'id'               => 'hs2_location_top',
			'type'             => 'text',
			'default'          => esc_html('443 Applecross Lane'),
			'required'         => array( 'hs2_location_switch', '=', true ),
		),

		array(
			'title'            => esc_html__('Location - Bottom Text', 'oculis'),
			'id'               => 'hs2_location_bottom',
			'type'             => 'text',
			'default'          => esc_html('San Francisco 8665.'),
			'required'         => array( 'hs2_location_switch', '=', true ),
		),

		// Open Hours
		array(
			'title'            => esc_html__('Enable the header\'s open hours box.', 'oculis'),
			'id'               => 'hs2_hours_switch',
			'type'             => 'switch',
			'default'          => true,
			'required'         => array('header_style', '=', 'header-style-2'),
		),

		array(
			'title'            => esc_html__('Open Hours - Top Text', 'oculis'),
			'id'               => 'hs2_hours_top',
			'type'             => 'text',
			'default'          => esc_html('Mon - Sat 6.00 - 18.00'),
			'required'         => array( 'hs2_hours_switch', '=', true ),
		),

		array(
			'title'            => esc_html__('Open Hours - Bottom Text', 'oculis'),
			'id'               => 'hs2_hours_bottom',
			'type'             => 'text',
			'default'          => esc_html('Sunday CLOSED'),
			'required'         => array( 'hs2_hours_switch', '=', true ),
		),


		/**
		 * Header style 3 options
		 */


		/**
		 * Header style 4 options
		 */

		array(
			'title'            => esc_html__( 'Phone Number', 'oculis' ),
			'subtitle'         => esc_html__( 'Enter in an optional phone number for Header Style 4.', 'oculis' ),
			'id'               => 'hs4_phone_number',
			'type'             => 'text',
			'default'          => esc_html('+714 783 452 6'),
			'required'         => array('header_style', '=', 'header-style-4'),
		),
	)
));


/**
 * Slideout quote form
 */
Redux::setSection( $opt_name, array(
	'title' => esc_html__( 'Slideout Quote', 'oculis' ),
	'subsection' => true,
	'fields' => array(

		// Enable slideout quote
		array(
			'title' => esc_html__( 'Enable Slideout Quote', 'oculis' ),
			'id' => 'slideout_quote_switch',
			'type' => 'switch',
			'default' => true
		),

		// Content before
		array(
			'title' => esc_html__('Content before', 'oculis'),
			'subtitle' => esc_html__( 'Simple HTML is allowed.', 'oculis' ),
			'id' => 'slideout_content_before',
			'type' => 'textarea',
			'required' => array( 'slideout_quote_switch', '=', true )
		),

		// Contact form 7 shortcode
		array(
			'title' => esc_html__('Contact Form 7 Shortcode', 'oculis'),
			'id' => 'quote_slideout_shortcode',
			'type' => 'text',
			'required' => array( 'slideout_quote_switch', '=', true )
		),

		// Content after
		array(
			'title' => esc_html__('Content after', 'oculis'),
			'subtitle' => esc_html__( 'Simple HTML is allowed.', 'oculis' ),
			'id' => 'slideout_content_after',
			'type' => 'textarea',
			'required' => array( 'slideout_quote_switch', '=', true )
		),
	)
));


/**
 * Mobile menu
 */
Redux::setSection( $opt_name, array(
	'title' => esc_html__( 'Mobile Menu', 'oculis' ),
	'subsection' => true,
	'fields' => array(

		// Mobile header logo
		array(
			'title'	=> esc_html__( 'Mobile header logo', 'oculis' ),
			'subtitle' => esc_html__( 'Upload your logo for the mobile\'s header', 'oculis' ),
			'id' => 'mh_logo',
			'url' => true,
			'type' => 'media',
			'read-only' => false
		),

		// Logo dimensions
		array(
			'id'       => 'mh_logo_dimensions',
			'type'     => 'dimensions',
			'units'    => false,
			'title'    => esc_html__('Logo Dimensions (Width/Height)', 'oculis'),
			'subtitle' => esc_html__('Set the width and height of your mobile header logo in px', 'oculis')
		),

		// Mobile menu logo
		array(
			'title'	=> esc_html__( 'Mobile menu logo', 'oculis' ),
			'subtitle' => esc_html__( 'Upload your logo for the mobile menu', 'oculis' ),
			'id' => 'mm_logo',
			'url' => true,
			'type' => 'media',
			'read-only' => false
		),

		// Mobile menu logo dimensions
		array(
			'id'       => 'mm_logo_dimensions',
			'type'     => 'dimensions',
			'units'    => false,
			'title'    => esc_html__('Logo Dimensions (Width/Height)', 'oculis'),
			'subtitle' => esc_html__('Set the width and height of your mobile menu\'s logo in px', 'oculis')
		),

		// Mobile menu number switch
		array(
			'title' => esc_html__( 'Enable phone number', 'oculis' ),
			'id' => 'mm_enable_number',
			'type' => 'switch',
			'default' => true
		),

		// Mobile menu number
		array(
			'title'            => esc_html__( 'Phone number', 'oculis' ),
			'subtitle'         => esc_html__( 'This phone number will be displayed in your mobile menu when it\'s opened', 'oculis' ),
			'id'               => 'mm_number',
			'type'             => 'text',
			'default'          => esc_html('+714 783 452 6'),
			'required'         => array('mm_enable_number', '=', true),
		),

		// Mobile menu quote switch
		array(
			'title' => esc_html__( 'Enable quote button', 'oculis' ),
			'subtitle' => esc_html__( 'This button inside the mobile menu will trigger your slideout quote', 'oculis' ),
			'id' => 'mm_enable_button',
			'type' => 'switch',
			'default' => true
		),

		// Mobile menu quote button text
		array(
			'title'            => esc_html__( 'Quote button text', 'oculis' ),
			'id'               => 'mm_button',
			'type'             => 'text',
			'default'          => esc_html('Free Quote'),
			'required'         => array('mm_enable_button', '=', true),
		),

	)
));


/*--------------------------------------------------------------
# Layout
--------------------------------------------------------------*/

Redux::setSection( $opt_name, array(
	'title' => esc_html__( 'Layout', 'oculis' ),
	'icon' => 'el-icon-th',
));

/**
 * Sidebars
 */
Redux::setSection( $opt_name, array(
	'title' => esc_html__( 'Sidebars', 'oculis' ),
	'subsection' => true,
	'fields' => array(

		// BlogPost Layout
		array(
			'title' => esc_html__( 'BlogPost Layout', 'oculis' ),
			'subtitle' => esc_html__( 'Layout for all blogposts.', 'oculis' ),
			'id' => 'blog_layout',
			'type' => 'button_set',
			'options' => array(
				'left-sidebar' => esc_html__('Left Sidebar', 'oculis' ),
				'no-sidebar' => esc_html__('No Sidebar', 'oculis' ),
				'right-sidebar' => esc_html__('Right Sidebar', 'oculis' ),
			),
			'default'          => 'right-sidebar'
		),

		// BlogPage Layout
		array(
			'title' => esc_html__( 'BlogPage Layout', 'oculis' ),
			'subtitle' => esc_html__( 'Layout for the main blogpage.', 'oculis' ),
			'id' => 'blogpage_layout',
			'type' => 'button_set',
			'options' => array(
				'left-sidebar' => esc_html__('Left Sidebar', 'oculis' ),
				'no-sidebar' => esc_html__('No Sidebar', 'oculis' ),
				'right-sidebar' => esc_html__('Right Sidebar', 'oculis' ),
			),
			'default' => 'right-sidebar'
		),

		// Archives Layout
		array(
			'title' => esc_html__( 'Archives Layout', 'oculis' ),
			'subtitle' => esc_html__( 'Layout for all archives such as categories and tags.', 'oculis' ),
			'id' => 'archive_layout',
			'type' => 'button_set',
			'options' => array(
				'left-sidebar' => esc_html__('Left Sidebar', 'oculis' ),
				'no-sidebar' => esc_html__('No Sidebar', 'oculis' ),
				'right-sidebar' => esc_html__('Right Sidebar', 'oculis' ),
			),
			'default'          => 'right-sidebar'
		),

		// Shop Layout
		array(
			'title' => esc_html__( 'Shop Layout', 'oculis' ),
			'subtitle' => esc_html__( 'Layout for all WooCommerce archives.', 'oculis' ),
			'id' => 'shop_layout',
			'type' => 'button_set',
			'options' => array(
				'left-sidebar' => esc_html__('Left Sidebar', 'oculis' ),
				'no-sidebar' => esc_html__('No Sidebar', 'oculis' ),
				'right-sidebar' => esc_html__('Right Sidebar', 'oculis' ),
			),
			'default' => 'right-sidebar'
		),
	)
));

/**
 * Footer
 */
Redux::setSection( $opt_name, array(
	'title' => esc_html__( 'Footer', 'oculis' ),
	'subsection' => true,
	'fields' => array(

		// Footer Layout
		array(
			'title' => esc_html__('Footer Layout', 'oculis'),
			'subtitle' => esc_html__('Select your footer\'s layout.', 'oculis'),
			'id' => 'footer_layout',
			'type' => 'select',
			'options' => array(
				'1'  => esc_html__('1 Column', 'oculis' ),
				'2'  => esc_html__('2 Columns', 'oculis' ),
				'3'  => esc_html__('3 Columns', 'oculis' ),
				'4'  => esc_html__('4 Columns', 'oculis' ),
				'5'  => esc_html__('6 Columns', 'oculis' ),
				'6'  => esc_html('2/4 + 1/4 + 1/4'),
				'7'  => esc_html('1/4 + 2/4 + 2/4'),
				'8' => esc_html('1/4 + 1/4 + 2/4'),
			),
			'default' => '4',
		),

		// Footer Padding
		array(
			'title' => esc_html__('Footer Padding', 'oculis'),
			'subtitle' => esc_html__('Set the padding top and bottom for your footer.', 'oculis'),
			'id' => 'footer_spacing',
			'type' => 'spacing',
			'output' => array('.footer-wrap .footer'),
			'mode' => 'padding',
			'units' => 'px',
			'left' => false,
			'right' => false,
			'units_extended' => 'false',
			'default' => array(
				'padding-top' => '100',
				'padding-bottom' => '100',
			)
		),

	)
));


/**
 * Copyright layout
 */
Redux::setSection( $opt_name, array(
	'title' => esc_html__( 'Copyright', 'oculis' ),
	'subsection' => true,
	'fields' => array(

		// Footer Layout
		array(
			'title' => esc_html__('Copyright Layout', 'oculis'),
			'subtitle' => esc_html__('Select your copyright layout.', 'oculis'),
			'id' => 'copyright_layout',
			'type' => 'select',
			'options' => array(
				'none'  => esc_html__('None', 'oculis' ),
				'style-1'  => esc_html__('Style 1', 'oculis' ),
				'style-2'  => esc_html__('Style 2', 'oculis' ),
				'style-3'  => esc_html__('Style 3', 'oculis' ),
			),
			'default' => 'none',
		),

		// Copyright text
		array(
			'title'            => esc_html__('Footer Copyright Text', 'oculis'),
			'subtitle'         => esc_html__('Place your footer\'s copyright text in here (simple HTML allowed).', 'oculis'),
			'id'               => 'copyright_text',
			'type'             => 'textarea',
		),
	)
));


/*--------------------------------------------------------------
# Styling
--------------------------------------------------------------*/
Redux::setSection( $opt_name, array(
	'title' => esc_html__( 'Styling', 'oculis' ),
	'icon' => 'el-icon-tint'
));

Redux::setSection( $opt_name, array(
	'title' => esc_html__('Global', 'oculis'),
	'subsection' => true,
	'fields' => array(

		// Primary theme color
		array(
			'title'            => esc_html__('Primary Theme Color', 'oculis'),
			'description'      => esc_html__('This color is your main theme color and will effect various components throughout your website such as buttons, links, etc.', 'oculis'),
			'id'               => 'primary_theme_color',
			'type'             => 'color_rgba',
			'compiler'         => true,
			'output'           => array(
				'background' => '
				.blog-grid .blog-item .blog-item-inner .blog-bottom:after,
				.blog-card .blog-card-inner .continue:after,
				.testimonials-wrap .testimonial .testimonial-inner:before,
				.post-page .post-bottom .post-tags a,
				.accordion-wrapper .accordion-item .item-head,
				.blog-page-header-wrap .first-post .recent-post-tag,
				.blog-page-header-wrap .first-post .featured-content:after,
				.blog-page-header-wrap .first-post .featured-content .continue:after,
				.projects-filter .project-wrap a figure figcaption .title:before,
					.button,
					.comment-form input[type="submit"],
					.post-page .post-tags a,
					.content-area .page-numbers li .page-numbers.current,
					.post-page .link-pages span,
					.post-password-form input[type="submit"],
					.slick-slider .slick-dots li button:hover,
					.slick-slider .slick-dots li button:focus,
					.slick-slider .slick-dots li.slick-active button,
					.rev-btn.button,
					.custom-video-player:after,
					.animated-pulse .pulse,
					body .wpcf7 form input[type=submit],
					.header.style-1 .header-inner .right .button-wrap,
					.sidebar .widget .widget_title:after
				',
				'color' => '
					.not-found h2,
					a,
					mark,
					.blog-page-header-wrap .first-post .featured-content .continue,
					.post-page-header .page-header-inner .categories a,
					.services-grid .service-item .item-content .services-grid-link a,
					.filter-by-main span,
					.sort-projects-overlay .sort-projects a,
					.header.style-1 .header-inner .right .number-wrap i,
					.staff-wrap .staff-member .staff-inner .staff-link,
					.testimonials-wrap .testimonial .testimonial-inner .testimonial-link,
					.contact-page-num a,
					.blog-card .blog-card-inner .continue,
					.featured-box.style-2 .featured-box-inner .featured-box-link a span,
					.services-grid .service-item .item-content .services-grid-link a span,
					.header.style-2 .hs2-top .hs2-top-inner .hs2-top-inner-contact .hs2-icon-box .hs2-icon i,
					body.transparent_header .header.style-2 .hs2-top .hs2-top-inner-contact .hs2-icon-box .hs2-icon i,
					.header.style-4 .hs4-bottom .hs4-phone i,
					.featured-box.style-1 .featured-box-inner .featured-box-link a span
				',
				'border-color' => '
					blockquote,
					.content-area .page-numbers li .page-numbers.current,
					.content-area .page-numbers li .page-numbers:hover,
					.post-page .link-pages span,
					.post-page .link-pages a span:hover
				'
			),
			'default'   => array(
				'color'     => '#ffb600',
				'alpha'     => 1
			),
		),

	)
));

Redux::setSection( $opt_name, array(
	'title' => esc_html__('Header', 'oculis'),
	'subsection' => true,
	'fields' => array(

		// Header Background
		array(
			'title'            => esc_html__('Header Background', 'oculis'),
			'id'               => 'header_background_color',
			'type'             => 'color_rgba',
			'compiler'         => true,
			'output'           => array(
				'background' => '
					.header,
					.header.style-1,
					body.transparent_header.sticky-header .header.style-1.sticky-active,
					body.sticky-header .header.style-1.sticky-active,
					.header.style-2 .hs2-main .hs2-main-inner,
					.header.style-3 .hs3-main-inner,
					body.sticky-header .header.style-3.sticky-active .hs3-main .hs3-main-inner
				'
			),
			'default'          => array(
				'color' => '#fff',
				'alpha' => 1,
			),
		),

		// Header Menu Item Color
		array(
			'title'            => esc_html__('Menu Item Color', 'oculis'),
			'id'               => 'menu_item_color',
			'type'             => 'color_rgba',
			'compiler'         => true,
			'output'           => array(
				'color' => '
					.header .main-menu > ul > li > a,
					body.transparent_header .header.sticky-active .main-menu > ul > li > a,
					.header.style-4 .dropdown-main-menu ul li a
				'),
			'default'   => array(
				'color'     => '#000',
				'alpha'     => 1
			),
		),

		// Header Menu Item Color Hover & Active
		array(
			'title'            => esc_html__('Menu Item Color Hover & Active', 'oculis'),
			'id'               => 'menu_item_color_hover',
			'type'             => 'color_rgba',
			'compiler'         => true,
			'output'           => array(
				'color' => '
					.header .main-menu > ul > li > a:hover,
					.header .main-menu ul li.current-menu-item > a,
					body.transparent_header .header.sticky-active .main-menu > ul > li > a:hover,
					body.transparent_header .header.sticky-active .main-menu ul li.current-menu-item > a,
					.header.style-4 .dropdown-main-menu ul > li > a:hover,
					.header.style-4 .dropdown-main-menu ul li.current-menu-item > a
				'),
			'default'   => array(
				'color'     => '#ffb600',
				'alpha'     => 1
			),
		),

		// Submenu Background
		array(
			'title'            => esc_html__('Submenu Background', 'oculis'),
			'id'               => 'submenu_background_color',
			'type'             => 'color_rgba',
			'compiler'         => true,
			'output'           => array(
				'background' => '.header .main-menu > ul > li ul.sub-menu li, .header .main-menu > ul > li ul.sub-menu:after'
			),
			'default'          => array(
				'color' => '#fff',
				'alpha' => 1,
			),

		),

		// Header SubMenu Item Color
		array(
			'title'            => esc_html__('Submenu Item Color', 'oculis'),
			'id'               => 'submenu_item_color',
			'type'             => 'color_rgba',
			'compiler'         => true,
			'output'           => array(
				'color' => '
					.header .main-menu > ul > li ul.sub-menu li a,
					.header.style-4 .dropdown-main-menu ul li ul.sub-menu li a
				'
			),
			'default'   => array(
				'color'     => '#000',
				'alpha'     => 1
			),

		),

		// Header SubMenu Item Color Hover & Active
		array(
			'title'            => esc_html__('Submenu Item Color Hover & Active', 'oculis'),
			'id'               => 'submenu_item_color_hover',
			'type'             => 'color_rgba',
			'compiler'         => true,
			'output'           => array(
				'color' => '
					.header .main-menu > ul > li ul.sub-menu li > a:hover,
					.header .main-menu > ul > li ul.sub-menu li.current-menu-item > a,
					.header.style-4 .dropdown-main-menu ul li ul.sub-menu li > a:hover,
					.header.style-4 .dropdown-main-menu ul li ul.sub-menu li.current-menu-item > a
				'
			),
			'default'   => array(
				'color'     => '#ffb600',
				'alpha'     => 1
			),

		),

		/**
		 * Transparent header styling notice
		 */
		array(
			'id'   => 'transparent_header_styling_notice',
			'type'  => 'info',
			'style' => 'success',
			'title' => esc_html__('Transparent Header Menu Colors', 'oculis'),
			'icon'  => 'fa fa-info-circle',
			'desc' => esc_html__('These colors will be used for pages which have a transparent header enabled on them.', 'oculis')
		),

		// Header Menu - Transparent Menu Item Color
		array(
			'title'            => esc_html__('Transparent Menu Item Color', 'oculis'),
			'id'               => 'tp_menu_item_color',
			'type'             => 'color_rgba',
			'compiler'         => true,
			'output'           => array('body.transparent_header .header .main-menu > ul > li > a'),
			'default'   => array(
				'color'     => '#fff',
				'alpha'     => 0.8
			),

		),

		// Header Menu - Transparent Menu Item Color Hover & Active
		array(
			'title'            => esc_html__('Transparent Menu Item Color Hover & Active', 'oculis'),
			'id'               => 'tp_menu_item_color_hover',
			'type'             => 'color_rgba',
			'compiler'         => true,
			'output'           => array('body.transparent_header .header .main-menu > ul > li > a:hover, body.transparent_header .header .main-menu ul li.current-menu-item > a'),
			'default'   => array(
				'color'     => '#fff',
				'alpha'     => 1
			),

		),

	)
));

Redux::setSection( $opt_name, array(
	'title' => esc_html__('Header Mobile', 'oculis'),
	'subsection' => true,
	'fields' => array(

		// Mobile menu background
		array(
			'title' => esc_html__('Mobile Menu Background', 'oculis'),
			'id' => 'mobile_menu_background',
			'type' => 'background',
			'compiler' => true,
			'output' => array( '.header-mobile .mobile-menu-wrapper' ),
			'background-repeat' => false,
			'background-attachment' => false,
			'background-size' => false,
			'default' => array(
				'background-color' => 'rgba(0,0,0,0.95)',
			),
		),

		// Menu item color
		array(
			'title'            => esc_html__('Menu Item Color', 'oculis'),
			'id'               => 'mobile_menu_item_color',
			'type'             => 'color_rgba',
			'compiler'         => true,
			'output'           => array(
				'color' => '
					.header-mobile .mobile-menu-wrapper .mobile-menu li > a
				'
			),
			'default'   => array(
				'color'     => '#f3f3f3',
				'alpha'     => 1
			),
		),

		// Menu item hover color
		array(
			'title'            => esc_html__('Menu Item Hover Color', 'oculis'),
			'id'               => 'mobile_menu_item_hover_color',
			'type'             => 'color_rgba',
			'compiler'         => true,
			'output'           => array(
				'color' => '
					.header-mobile .mobile-menu-wrapper .mobile-menu li > a:hover
				'
			),
			'default'   => array(
				'color'     => '#fff',
				'alpha'     => 1
			),
		),

		// Menu icon color
		array(
			'title'            => esc_html__('Menu Icons Color', 'oculis'),
			'description' => esc_html__('This will effect various icons within the mobile menu such as the close button, submenu dropdown icons, etc.', 'oculis'),
			'id'               => 'mobile_menu_icons_color',
			'type'             => 'color_rgba',
			'compiler'         => true,
			'output'           => array(
				'color' => '
					.header-mobile .mobile-menu-wrapper .mobile-menu li .mobile-submenu-arrow i
				',
				'background' => '
					.header-mobile .mobile-menu-wrapper .toggler-close div span
				'
			),
			'default'   => array(
				'color'     => '#fff',
				'alpha'     => 0.8
			),
		),

	)
));

Redux::setSection( $opt_name, array(
	'title' => esc_html__('Footer', 'oculis'),
	'subsection' => true,
	'fields' => array(

		// Footer Background
		array(
			'title' => esc_html__('Footer Background', 'oculis'),
			'id' => 'footer_background',
			'type' => 'background',
			'compiler' => true,
			'output' => array( '.footer-wrap .footer' ),
			'background-repeat' => false,
			'background-attachment' => false,
			'default' => array(
				'background-color' => '#0a0a0a',
			),
		),

		// Widget theme color
		array(
			'title'            => esc_html__('Widget Theme Color', 'oculis'),
			'description'      => esc_html__('This color will effect various widgets within the footer such as tags, calendar, etc.', 'oculis'),
			'id'               => 'footer_widget_theme_color',
			'type'             => 'color_rgba',
			'compiler'         => true,
			'output'           => array(
				'background' => '
					.footer-wrap .footer .widget.widget_calendar .calendar_wrap table thead th,
					.footer-wrap .footer .widget.widget_search .search-form button
				',
				'border-color' => '
					.footer-wrap .footer .widget.widget_search .search-form button,
					.footer-wrap .footer .widget.widget_search .search-form input,
					.footer-wrap .footer .widget.widget_tag_cloud .tagcloud a:hover,
					body .wpcf7 form .email-optin input[type=email]
				',
				'color' => '
					.footer-wrap .footer .widget.widget_tag_cloud .tagcloud a:hover
				'
			),
			'default'   => array(
				'color'     => '#ffb600',
				'alpha'     => 1
			),
		),

		// Widget title color
		array(
			'title'            => esc_html__('Widget Title Color', 'oculis'),
			'id'               => 'footer_widget_title_color',
			'type'             => 'color_rgba',
			'compiler'         => true,
			'output'           => array(
				'color' => '
					.footer-wrap .footer .widget .widget_title
				'
			),
			'default'   => array(
				'color'     => '#fff',
				'alpha'     => 1
			),
		),

		// Widget text color
		array(
			'title'            => esc_html__('Widget Text Color', 'oculis'),
			'id'               => 'footer_widget_text_color',
			'type'             => 'color_rgba',
			'compiler'         => true,
			'output'           => array(
				'color' => '
					.footer-wrap .footer .widget p,
					.footer-wrap .footer .rssSummary,
					.footer-wrap .footer .widget.widget_recent_comments ul li,
					.footer-wrap .footer .widget.widget_categories label.screen-reader-text,
					.footer-wrap .footer .widget.widget_categories ul li,
					.footer-wrap .footer label,
					.footer-wrap .footer .widget.widget_calendar .calendar_wrap table caption,
					.footer-wrap .footer .widget.widget_calendar .calendar_wrap table td,
					.footer-wrap .footer .widget.widget_archive ul li
				'
			),
			'default'   => array(
				'color'     => '#fff',
				'alpha'     => 1
			),
		),

		// Widget link color
		array(
			'title'            => esc_html__('Widget Link Color', 'oculis'),
			'id'               => 'footer_widget_link_color',
			'type'             => 'color_rgba',
			'compiler'         => true,
			'output'           => array(
				'color' => '
					.footer-wrap .footer .widget.widget_tag_cloud .tagcloud a,
					.footer-wrap .footer .widget ul li a,
					.footer-wrap .footer .widget.widget_categories ul li .children li a,
					.footer-wrap .footer .widget.widget_pages ul li .children li a,
					.footer-wrap .footer .widget.widget_rss .rsswidget,
					.footer-wrap .footer .widget.widget_nav_menu ul li .sub-menu li a
				'
			),
			'default'   => array(
				'color'     => '#fff',
				'alpha'     => 0.8
			),
		),

		// Widget link hover color
		array(
			'title'            => esc_html__('Widget Link Hover Color', 'oculis'),
			'id'               => 'footer_widget_link_hover_color',
			'type'             => 'color_rgba',
			'compiler'         => true,
			'output'           => array(
				'color' => '
					.footer-wrap .footer .widget ul li a:hover,
					.footer-wrap .footer .widget.widget_categories ul li .children li a:hover,
					.footer-wrap .footer .widget.widget_pages ul li .children li a:hover,
					.footer-wrap .footer .widget.widget_rss .rsswidget:hover,
					.footer-wrap .footer .widget.widget_nav_menu ul li .sub-menu li a:hover
				'
			),
			'default'   => array(
				'color'     => '#fff',
				'alpha'     => 1
			),
		),

	)
));

// Copyright section styles
Redux::setSection( $opt_name, array(
	'title' => esc_html__('Copyright', 'oculis'),
	'subsection' => true,
	'fields' => array(

		// Copyright background
		array(
			'title' => esc_html__('Copyright Background', 'oculis'),
			'id' => 'copyright_background',
			'type' => 'background',
			'compiler' => true,
			'output' => array( '.copyright-wrapper.style-2, .copyright-wrapper.style-1, .copyright-wrapper.style-3' ),
			'background-repeat' => false,
			'background-attachment' => false,
			'default' => array(
				'background-color' => '#fff',
			),
		),

		// Copyright text color
		array(
			'title'            => esc_html__('Copyright Text Color', 'oculis'),
			'id'               => 'copyright_text_color',
			'type'             => 'color_rgba',
			'compiler'         => true,
			'output'           => array(
				'color' => '
					.copyright-wrapper.style-2 .copyright-inner .cr-text,
					.copyright-wrapper.style-1 .copyright-inner .left .cr-text,
					.copyright-wrapper.style-3 .copyright-inner .cr-text
				'
			),
			'default'   => array(
				'color'     => '#2e2e2e',
				'alpha'     => 1
			),
		),

		// Copyright links color
		array(
			'title'            => esc_html__('Copyright Links Color', 'oculis'),
			'id'               => 'copyright_links_color',
			'type'             => 'color_rgba',
			'compiler'         => true,
			'output'           => array(
				'color' => '
					.copyright-wrapper.style-2 .copyright-inner .cr-menu ul li a,
					.copyright-wrapper.style-2 .copyright-inner .cr-social a i,
					.copyright-wrapper.style-1 .copyright-inner .right .cr-menu ul li a,
					.copyright-wrapper.style-1 .copyright-inner .left .cr-social a i,
					.copyright-wrapper.style-3 .copyright-inner .cr-menu ul li a,
					.copyright-wrapper.style-3 .copyright-inner .cr-social a i
				',
				'border-color' => '
					.copyright-wrapper.style-2 .copyright-inner .cr-social a,
					.copyright-wrapper.style-1 .copyright-inner .left .cr-social a,
					.copyright-wrapper.style-3 .copyright-inner .cr-social a
				'
			),
			'default'   => array(
				'color'     => '#2e2e2e',
				'alpha'     => 1
			),
		),

		// Copyright links hover color
		array(
			'title'            => esc_html__('Copyright Links Hover Color', 'oculis'),
			'id'               => 'copyright_links_hov_color',
			'type'             => 'color_rgba',
			'compiler'         => true,
			'output'           => array(
				'color' => '
					.copyright-wrapper.style-2 .copyright-inner .cr-menu ul li a:hover,
					.copyright-wrapper.style-2 .copyright-inner .cr-social a:hover i,
					.copyright-wrapper.style-1 .copyright-inner .right .cr-menu ul li a:hover,
					.copyright-wrapper.style-1 .copyright-inner .left .cr-social a:hover i,
					.copyright-wrapper.style-3 .copyright-inner .cr-menu ul li a:hover,
					.copyright-wrapper.style-3 .copyright-inner .cr-social a:hover i
				',
				'border-color' => '
					.copyright-wrapper.style-2 .copyright-inner .cr-social a:hover,
					.copyright-wrapper.style-1 .copyright-inner .left .cr-social a:hover,
					.copyright-wrapper.style-3 .copyright-inner .cr-social a:hover
				'
			),
			'default'   => array(
				'color'     => '#2e2e2e',
				'alpha'     => 0.8
			),
		),
	)
));

Redux::setSection( $opt_name, array(
	'title' => esc_html__('Sidebars', 'oculis'),
	'subsection' => true,
	'fields' => array(

		// Widget theme color
		array(
			'title'            => esc_html__('Widget Theme Color', 'oculis'),
			'description'      => esc_html__('This color will effect various widgets within the blog such as tags, calendar, etc.', 'oculis'),
			'id'               => 'sidebar_widget_theme_color',
			'type'             => 'color_rgba',
			'compiler'         => true,
			'output'           => array(
				'background' => '
					.sidebar .widget.widget_calendar .calendar_wrap table thead th,
					.sidebar .widget.widget_search .search-form button
				',
				'border-color' => '
					.sidebar .widget.widget_search .search-form button,
					.sidebar .widget.widget_search .search-form input,
					.sidebar .widget.widget_tag_cloud .tagcloud a:hover
				',
				'color' => '
					.sidebar .widget.widget_tag_cloud .tagcloud a:hover
				'
			),
			'default'   => array(
				'color'     => '#ffb600',
				'alpha'     => 1
			),
		),

		// Widget title color
		array(
			'title'            => esc_html__('Widget Title Color', 'oculis'),
			'id'               => 'sidebar_widget_title_color',
			'type'             => 'color_rgba',
			'compiler'         => true,
			'output'           => array(
				'color' => '
					.sidebar .widget .widget_title
				'
			),
			'default'   => array(
				'color'     => '#484848',
				'alpha'     => 1
			),
		),

		// Widget text color
		array(
			'title'            => esc_html__('Widget Text Color', 'oculis'),
			'id'               => 'sidebar_widget_text_color',
			'type'             => 'color_rgba',
			'compiler'         => true,
			'output'           => array(
				'color' => '
					.sidebar .widget p,
					.sidebar .rssSummary,
					.sidebar .widget.widget_recent_comments ul li,
					.sidebar .widget.widget_categories label.screen-reader-text,
					.sidebar .widget.widget_categories ul li,
					.sidebar label,
					.sidebar .widget.widget_calendar .calendar_wrap table caption,
					.sidebar .widget.widget_calendar .calendar_wrap table td,
					.sidebar .widget.widget_archive ul li
				'
			),
			'default'   => array(
				'color'     => '#484848',
				'alpha'     => 1
			),
		),

		// Widget link color
		array(
			'title'            => esc_html__('Widget Link Color', 'oculis'),
			'id'               => 'sidebar_widget_link_color',
			'type'             => 'color_rgba',
			'compiler'         => true,
			'output'           => array(
				'color' => '
					.sidebar .widget ul li a,
					.sidebar .widget.widget_categories ul li .children li a,
					.sidebar .widget.widget_pages ul li .children li a,
					.sidebar .widget.widget_rss .rsswidget,
					.sidebar .widget.widget_nav_menu ul li .sub-menu li a
				'
			),
			'default'   => array(
				'color'     => '#484848',
				'alpha'     => 1
			),
		),

		// Widget link hover color
		array(
			'title'            => esc_html__('Widget Link Hover Color', 'oculis'),
			'id'               => 'sidebar_widget_link_hover_color',
			'type'             => 'color_rgba',
			'compiler'         => true,
			'output'           => array(
				'color' => '
					.sidebar .widget ul li a:hover,
					.sidebar .widget.widget_categories ul li .children li a:hover,
					.sidebar .widget.widget_pages ul li .children li a:hover,
					.sidebar .widget.widget_rss .rsswidget:hover,
					.sidebar .widget.widget_nav_menu ul li .sub-menu li a:hover
				'
			),
			'default'   => array(
				'color'     => '#484848',
				'alpha'     => 0.8
			),
		),
	)
));

Redux::setSection( $opt_name, array(
	'title' => esc_html__('WooCommerce', 'oculis'),
	'subsection' => true,
	'fields' => array(

		// Shop page theme color
		array(
			'title'            => esc_html__('Shop Page Theme Color', 'oculis'),
			'id'               => 'shop_page_theme_color',
			'type'             => 'color_rgba',
			'compiler'         => true,
			'output'           => array(
				'color' => '
					body.woocommerce-page ul.products .product-item li.product a.woocommerce-LoopProduct-link .price
				',
				'background' => '
					body.woocommerce-page ul.products .product-item li.product .onsale,
					body.woocommerce.archive .woocommerce-pagination .page-numbers li .page-numbers.current
				',
				'border-color' => '
					body.woocommerce.archive .woocommerce-pagination .page-numbers li .page-numbers.current,
					body.woocommerce.archive .woocommerce-pagination .page-numbers li .page-numbers:hover
				'
			),
			'default'   => array(
				'color'     => '#ffb600',
				'alpha'     => 1
			),
		),

		// Shop page widget color
		array(
			'title'            => esc_html__('Shop Page Widget Color', 'oculis'),
			'id'               => 'shop_page_widget_color',
			'type'             => 'color_rgba',
			'compiler'         => true,
			'output'           => array(
				'color' => '
					body.woocommerce.archive .sidebar .star-rating:before,
					body.woocommerce.archive .sidebar .star-rating span:before
				',
				'background' => '
					.widget_shopping_cart .buttons a,
					.widget_shopping_cart .buttons a.checkout,
					.widget_price_filter .ui-slider .ui-slider-range,
					.widget_product_search .woocommerce-product-search button
				',
				'border-color' => '
					
				'
			),
			'default'   => array(
				'color'     => '#ffb600',
				'alpha'     => 1
			),
		),

		// Product page theme color
		array(
			'title'            => esc_html__('Product Page Theme Color', 'oculis'),
			'id'               => 'product_page_theme_color',
			'type'             => 'color_rgba',
			'compiler'         => true,
			'output'           => array(
				'color' => '
					body.single-product .star-rating span:before,
					body.single-product .star-rating:before,
					body.single-product .stars a,
					body.single-product .summary .posted_in a
				',
				'background' => '
					body.single-product .summary .button,
					body.single-product .comment-form input[type="submit"],
					body.single-product .woocommerce-message .button
				',
				'border-color' => '
					body.single-product .woocommerce-tabs ul.tabs li.active
				'
			),
			'default'   => array(
				'color'     => '#ffb600',
				'alpha'     => 1
			),
		),

		// Cart page theme color
		array(
			'title'            => esc_html__('Cart Page Theme Color', 'oculis'),
			'id'               => 'cart_page_theme_color',
			'type'             => 'color_rgba',
			'compiler'         => true,
			'output'           => array(
				'color' => '
					body.woocommerce-cart .shop_table tbody td.product-remove a,
					body.woocommerce-cart .shop_table tbody td.product-name a
				',
				'background' => '
					body.woocommerce-cart td.actions .button,
					body.woocommerce-cart .cart_totals .wc-proceed-to-checkout .checkout-button,
					.return-to-shop .button
				'
			),
			'default'   => array(
				'color'     => '#ffb600',
				'alpha'     => 1
			),
		),

		// Checkout page theme color
		array(
			'title'            => esc_html__('Checkout Page Theme Color', 'oculis'),
			'id'               => 'checkout_page_theme_color',
			'type'             => 'color_rgba',
			'compiler'         => true,
			'output'           => array(
				'color' => '
					body.woocommerce-checkout .woocommerce-form-coupon-toggle a,
					body.woocommerce-checkout .woocommerce-form-login-toggle a,
					body.woocommerce-checkout .woocommerce-form-login .lost_password a
				',
				'background' => '
					body.woocommerce-checkout .woocommerce-form-coupon button,
					body.woocommerce-checkout .woocommerce-form-login button,
					body.woocommerce-checkout .place-order .button
				'
			),
			'default'   => array(
				'color'     => '#ffb600',
				'alpha'     => 1
			),
		),

		// Account page theme color
		array(
			'title'            => esc_html__('Account Page Theme Color', 'oculis'),
			'id'               => 'account_page_theme_color',
			'type'             => 'color_rgba',
			'compiler'         => true,
			'output'           => array(
				'color' => '
					.woocommerce-MyAccount-content p a
				',
				'background' => '
					.woocommerce-MyAccount-navigation ul,
					.woocommerce-MyAccount-navigation ul li,
					.woocommerce-MyAccount-content .woocommerce-Message .woocommerce-Button,
					.woocommerce-Addresses .edit,
					.woocommerce-EditAccountForm .button,
					.woocommerce-address-fields .button
				'
			),
			'default'   => array(
				'color'     => '#ffb600',
				'alpha'     => 1
			),
		),

	)
));


/*--------------------------------------------------------------
# Typography
--------------------------------------------------------------*/
Redux::setSection( $opt_name, array(
	'title' => esc_html__( 'Typography', 'oculis' ),
	'icon' => 'el-fontsize el-icon-large',
));

Redux::setSection( $opt_name, array(
	'title' => esc_html__( 'Global', 'oculis' ),
	'id' => 'global_typography',
	'subsection' => true,
	'fields' => array(

		// Primary font
		array(
			'title'            => esc_html__('Primary Font', 'oculis'),
			'subtitle'         => esc_html__('Your primary font should be one that stands out, this will be used for various elements across your website such as titles and headings.', 'oculis'),
			'id'               => 'primary_font',
			'type'             => 'typography',
			'compiler'         => false,
			'google'           => true,
			'font-backup'      => false,
			'all_styles'       => true,
			'font-weight'      => false,
			'font-size'        => false,
			'font-style'       => false,
			'subsets'          => false,
			'line-height'      => false,
			'text-align'       => false,
			'word-spacing'     => false,
			'letter-spacing'   => false,
			'text-transform'   => false,
			'color'            => false,
			'preview'          => true,
			'output'           => array(
				'
					h1,h2,h3,h4,h5,h6,
					.h1,.h2,.h3,.h4,.h5,.h6,
					.header-mobile .mobile-menu-wrapper .number-wrap a,
					.post-page .post-bottom .post-author .author-content .name,
					.blog-page-header-wrap .first-post .featured-content .title,
					.blog-page-header-wrap .first-post .featured-content .continue,
					.header.style-1 .header-inner .right .number-wrap a,
					.blog-grid .blog-item .blog-item-inner .blog-bottom .blog-grid-title,
					.featured-box.style-2 .featured-box-inner .featured-box-title span,
					.featured-box.style-2 .featured-box-inner .featured-box-link a span,
					.rev-btn.button,
					.header .logo .site-title,
					.header-mobile .mobile-menu-wrapper .mobile-menu li>a,
					.header .main-menu>ul>li>a,
					.header .main-menu>ul>li ul.sub-menu li a,
					.toolbar .toolbar-inner .left .toolbar-number span,
					.toolbar .toolbar-inner .left .toolbar-location span,
					.toolbar .toolbar-inner .right .toolbar-shop a span,
					.header.style-4 .dropdown-main-menu ul li a,
					.header.style-4 .dropdown-main-menu ul li ul.sub-menu li a,
					.header.style-4 .hs4-bottom .hs4-phone a,
					.header-mobile .mobile-menu-wrapper .button-wrap .quote-button span,
					.header-mobile .mobile-menu-wrapper .number-wrap span,
					.post-page-header .page-header-inner .sub-wrap .post-date,
					.post-page-header .page-header-inner .sub-wrap .post-comments-count a,
					.post-page-header .page-header-inner .categories a,
					.post-comments .comments-list .comment-author,
					.page-comments .comments-list .comment-author,
					.post-comments .comments-list .comment-meta,
					.page-comments .comments-list .comment-meta,
					.comment-form input[type="submit"],
					.content-area .page-numbers li .page-numbers,
					body.woocommerce.archive .woocommerce-result-count,
					body.woocommerce-page ul.products .product-item li.product a.button,
					body.woocommerce-page ul.products .product-item li.product a.added_to_cart,
					.widget_shopping_cart .total,
					.widget_shopping_cart .buttons a,
					.widget_price_filter .price_slider_amount .button,
					.widget_product_search .woocommerce-product-search button,
					body.woocommerce.archive .woocommerce-pagination .page-numbers li .page-numbers,
					body.single-product .content-area .summary.entry-summary .price,
					.button,
					body.single-product .woocommerce-tabs ul.tabs li a,
					body.woocommerce-page ul.products .product-item li.product .onsale,
					body.single-product .woocommerce-tabs #comments ol li .comment_container .comment-text .meta,
					body.single-product .woocommerce-tabs #review_form #respond .comment-reply-title,
					.woocommerce-MyAccount-navigation ul li a,
					.woocommerce-Addresses .edit,
					.edit-account legend,
					.services-grid-title,
					.sr-desc,
					#slide-fancy-nav .number, #slide-fancy-nav .number:before,
					.tp-arr-titleholder,
					.services-grid .service-item .item-content .services-grid-title,
					.services-grid .service-item .item-content .services-grid-link a,
					.filter-by-main,
					.sort-projects-overlay .filter-by,
					.sort-projects-overlay .sort-projects a,
					.projects-filter .project-wrap a figure figcaption .title,
					.custom-button-wrapper .custom-button a, .custom-button-wrapper .custom-button .btn-link,
					.number-counter .counter-title,
					.blog-grid .blog-item .blog-bottom .blog-grid-title,
					body .wpcf7 form input[type=submit],
					.header .main-menu>ul>li>a,
					.number-counter .counter-value,
					.icon-box .icon-content .icon-box-title,
					.staff-wrap .staff-member .staff-inner .staff-name,
					.testimonials-wrap .testimonial .testimonial-bottom .left .testimonial-name,
					.projects-carousel-wrapper .projects-carousel .project-wrap a figure figcaption .title,
					.staff-wrap .staff-member .staff-inner .staff-link,
					.testimonials-wrap .testimonial .testimonial-inner .testimonial-link,
					.featured-box.style-2 .featured-box-inner .featured-box-title,
					body .wpcf7 form input[type=submit],
					h1, h2, h3, h4, h5, h6, .h1, .h2, .h3, .h4, .h5, .h6,
					.services-grid .service-item .item-content .services-grid-link a span,
					.widget.widget_archive ul li,
					.widget.widget_calendar .calendar_wrap table caption,
					.widget.widget_categories ul li,
					.widget.widget_pages ul li,
					.widget.widget_meta ul li,
					.widget.widget_recent_comments ul li,
					.widget.widget_recent_entries ul li,
					.widget.widget_rss ul li,
					.widget.widget_text b, .widget.widget_text strong,
					.widget.widget_tag_cloud .tagcloud a,
					.widget.widget_nav_menu ul li,
					.copyright-wrapper.style-2 .copyright-inner .cr-text,
					.copyright-wrapper.style-2 .copyright-inner .cr-menu ul li a,
					.copyright-wrapper.style-1 .copyright-inner .left .cr-text,
					.copyright-wrapper.style-1 .copyright-inner .right .cr-menu ul li a,
					.copyright-wrapper.style-3 .copyright-inner .cr-menu ul li a,
					.copyright-wrapper.style-3 .copyright-inner .cr-text,
					.blog-card .blog-card-inner .continue
					
				'
			),
			'units'            => 'px',
			'default'          => array(
				'font-family' => 'Poppins',
				'font-weight' => 700,
			)
		),

		// Body & Paragraph Text
		array(
			'title'            => esc_html__('Secondary Font', 'oculis'),
			'subtitle'         => esc_html__('Your secondary font should be easy to read, it will be used across your website for body and paragraph text.', 'oculis'),
			'id'               => 'secondary_font',
			'type'             => 'typography',
			'compiler'         => true,
			'google'           => true,
			'font-backup'      => false,
			'all_styles'       => true,
			'font-weight'      => false,
			'text-align'       => false,
			'font-size'        => false,
			'font-style'       => false,
			'subsets'          => false,
			'line-height'      => false,
			'word-spacing'     => false,
			'letter-spacing'   => false,
			'color'            => false,
			'preview'          => true,
			'output'           => array( 'p, body' ),
			'units'            => 'px',
			'default'          => array(
				'font-family' => 'Open Sans',
				'font-weight' => 400,
			)
		),

		// Offset font
		array(
			'title'            => esc_html__('Offset Font', 'oculis'),
			'subtitle'         => esc_html__('This font will be used for the mark element within custom headings', 'oculis'),
			'id'               => 'offset_font',
			'type'             => 'typography',
			'compiler'         => true,
			'google'           => true,
			'font-backup'      => false,
			'all_styles'       => true,
			'font-weight'      => false,
			'text-align'       => false,
			'font-size'        => false,
			'font-style'       => false,
			'subsets'          => false,
			'line-height'      => false,
			'word-spacing'     => false,
			'letter-spacing'   => false,
			'color'            => false,
			'preview'          => true,
			'output'           => array( 'mark' ),
			'units'            => 'px',
			'default'          => array(
				'font-family' => 'Playfair Display',
				'font-weight' => 400,
			)
		),

	)
));


/*--------------------------------------------------------------
# Blog
--------------------------------------------------------------*/
Redux::setSection( $opt_name, array(
	'title' => esc_html__( 'Blog', 'oculis' ),
	'icon' => 'el-icon-list',
));

Redux::setSection( $opt_name, array(
	'title' => esc_html__('Blog Page', 'oculis'),
	'subsection' => true,
	'fields' => array(
		array(
			'title' => esc_html__( 'Page Header Title', 'oculis' ),
			'id' => 'blog_page_header_title',
			'type' => 'text',
			'default' => esc_html( 'Blog' ),
		),
		array(
			'title' => esc_html__( 'Page Header Description', 'oculis' ),
			'id' => 'blog_page_header_description',
			'type' => 'text',
			'default' => esc_html( 'Welcome to our blog, read our latest posts.' ),
		)
	)
));


/*--------------------------------------------------------------
# Shop
--------------------------------------------------------------*/
Redux::setSection( $opt_name, array(
	'title' => esc_html__( 'WooCommerce', 'oculis' ),
	'icon' => 'el-icon-shopping-cart',
));

Redux::setSection( $opt_name, array(
	'title' => esc_html__('Shop Page', 'oculis'),
	'subsection' => true,
	'fields' => array(
		array(
			'title' => esc_html__('Page Header Background', 'oculis'),
			'id' => 'shop_page_head_background',
			'type' => 'background',
			'output' => '.shop-page-header',
			'background-repeat' => false,
			'background-attachment' => false,
			'background-size' => false,
			'default' => array(
				'background-color' => '#0d0d0d',
			),
		),
		array(
			'title' => esc_html__( 'Page Header Title', 'oculis' ),
			'id' => 'shop_page_header_title',
			'type' => 'text',
			'default' => esc_html( 'Shop' ),
		),
		array(
			'title' => esc_html__( 'Page Header Description', 'oculis' ),
			'id' => 'shop_page_header_description',
			'type' => 'text',
			'default' => esc_html( 'Welcome to our shop.' ),
		)
	)
));

Redux::setSection( $opt_name, array(
	'title' => esc_html__('Cart Page', 'oculis'),
	'subsection' => true,
	'fields' => array(
		array(
			'title' => esc_html__('Page Header Background', 'oculis'),
			'id' => 'cart_page_head_background',
			'type' => 'background',
			'output' => '.cart-page-header',
			'background-repeat' => false,
			'background-attachment' => false,
			'background-size' => false,
			'default' => array(
				'background-color' => '#0d0d0d',
			),
		),
		array(
			'title' => esc_html__( 'Page Header Title', 'oculis' ),
			'id' => 'cart_page_header_title',
			'type' => 'text',
			'default' => esc_html( 'Cart' ),
		),
		array(
			'title' => esc_html__( 'Page Header Description', 'oculis' ),
			'id' => 'cart_page_header_description',
			'type' => 'text',
			'default' => esc_html( 'View all the items in your cart.' ),
		)
	)
));

Redux::setSection( $opt_name, array(
	'title' => esc_html__('Checkout Page', 'oculis'),
	'subsection' => true,
	'fields' => array(
		array(
			'title' => esc_html__('Page Header Background', 'oculis'),
			'id' => 'checkout_page_head_background',
			'type' => 'background',
			'output' => '.checkout-page-header',
			'background-repeat' => false,
			'background-attachment' => false,
			'background-size' => false,
			'default' => array(
				'background-color' => '#0d0d0d',
			),
		),
		array(
			'title' => esc_html__( 'Page Header Title', 'oculis' ),
			'id' => 'checkout_page_header_title',
			'type' => 'text',
			'default' => esc_html( 'Checkout' ),
		),
		array(
			'title' => esc_html__( 'Page Header Description', 'oculis' ),
			'id' => 'checkout_page_header_description',
			'type' => 'text',
			'default' => esc_html( 'Make your payment and check out.' ),
		)
	)
));

Redux::setSection( $opt_name, array(
	'title' => esc_html__('Account Page', 'oculis'),
	'subsection' => true,
	'fields' => array(
		array(
			'title' => esc_html__('Page Header Background', 'oculis'),
			'id' => 'account_page_head_background',
			'type' => 'background',
			'output' => '.account-page-header',
			'background-repeat' => false,
			'background-attachment' => false,
			'background-size' => false,
			'default' => array(
				'background-color' => '#0d0d0d',
			),
		),
		array(
			'title' => esc_html__( 'Page Header Title', 'oculis' ),
			'id' => 'account_page_header_title',
			'type' => 'text',
			'default' => esc_html( 'My Account' ),
		),
		array(
			'title' => esc_html__( 'Page Header Description', 'oculis' ),
			'id' => 'account_page_header_description',
			'type' => 'text',
			'default' => esc_html( 'View your most recent orders and payment methods.' ),
		)
	)
));


/*--------------------------------------------------------------
# Social media
--------------------------------------------------------------*/
Redux::setSection( $opt_name, array(
	'title'   => esc_html__( 'Social', 'oculis' ),
	'submenu' => true,
	'icon'    => 'el-twitter el-icon-large',
));

Redux::setSection( $opt_name, array(
	'title'      => esc_html__( 'Social Media URL\'s', 'oculis' ),
	'desc'       => esc_html__('Enter your social media urls here and then activate them within the Header/Copyright social media tabs below. Make sure your social media URLs include http://', 'oculis' ),
	'submenu'    => true,
	'subsection' => true,
	'fields'     => array(
		array(
			'title'            => esc_html__('Twitter', 'oculis'),
			'desc'             => esc_html__('Enter your Twitter URL.', 'oculis'),
			'id'               => 'twitter',
			'type'             => 'text',
		),
		array(
			'title'            => esc_html__('Facebook', 'oculis'),
			'desc'             => esc_html__('Enter your Facebook URL.', 'oculis'),
			'id'               =>'facebook',
			'type'             => 'text',
		),
		array(
			'title'            => esc_html__('Linkedin', 'oculis'),
			'desc'             => esc_html__('Enter your Linkedin URL.', 'oculis'),
			'id'               =>'linkedin',
			'type'             => 'text',
		),
		array(
			'title'            => esc_html__('Pinterest', 'oculis'),
			'desc'             => esc_html__('Enter your Pinterest URL.', 'oculis'),
			'id'               => 'pinterest',
			'type'             => 'text',
		),
		array(
			'title'            => esc_html__('Google Plus', 'oculis'),
			'desc'             => esc_html__('Enter your Google Plus URL.', 'oculis'),
			'id'               => 'google',
			'type'             => 'text',
		),
		array(
			'title'            => esc_html__('Instagram', 'oculis'),
			'desc'             => esc_html__('Enter your Instagram URL.', 'oculis'),
			'id'               => 'instagram',
			'type'             => 'text',
		),
		array(
			'title'            => esc_html__('VK', 'oculis'),
			'desc'             => esc_html__('Enter your VK URL.', 'oculis'),
			'id'               => 'vk',
			'type'             => 'text',
		),
		array(
			'title'            => esc_html__('Yelp', 'oculis'),
			'desc'             => esc_html__('Enter your Yelp URL.', 'oculis'),
			'id'               => 'yelp',
			'type'             => 'text',
		),
		array(
			'title'            => esc_html__('Foursquare', 'oculis'),
			'desc'             => esc_html__('Enter your foursquare URL.', 'oculis'),
			'id'               => 'foursquare',
			'type'             => 'text',
		),
		array(
			'title'            => esc_html__('Flickr', 'oculis'),
			'desc'             => esc_html__('Enter your Flickr URL.', 'oculis'),
			'id'               => 'flickr',
			'type'             => 'text',
		),
		array(
			'title'            => esc_html__('Youtube', 'oculis'),
			'desc'             => esc_html__('Enter your Youtube URL.', 'oculis'),
			'id'               => 'youtube',
			'type'             => 'text',
		),
		array(
			'title'            => esc_html__('Email', 'oculis'),
			'desc'             => esc_html__('Enter your Email URL.', 'oculis'),
			'id'               => 'email',
			'type'             => 'text',
		),

	)
));

// Social Media Header
Redux::setSection( $opt_name, array(
	'title'      => esc_html__( 'Header Social Media', 'oculis' ),
	'submenu'    => true,
	'subsection' => true,
	'fields'     => array(
		array(
			'title'            => esc_html__('Header Social Icons', 'oculis'),
			'subtitle'         => esc_html__('Choose which social media icons should display, the URL will be taken from the Social Media URL\'s tab. Note that Header Social Media is only available for various header styles.', 'oculis'),
			'id'               => 'header_social_icons',
			'type'             => 'checkbox',
			'options'          => array(
				'twitter'    => esc_html__('Twitter', 'oculis'),
				'facebook'   => esc_html__('Facebook', 'oculis'),
				'linkedin'   => esc_html__('Linkedin', 'oculis'),
				'pinterest'  => esc_html__('Pinterest', 'oculis'),
				'google'     => esc_html__('Google', 'oculis'),
				'instagram'  => esc_html__('Instagram', 'oculis'),
				'flickr'     => esc_html__('Flickr', 'oculis'),
				'youtube'    => esc_html__('Youtube', 'oculis'),
				'instagram'  => esc_html__('Instagram', 'oculis'),
				'vk'         => esc_html__('VK', 'oculis'),
				'yelp'       => esc_html__('Yelp', 'oculis'),
				'foursquare' => esc_html__('Foursquare', 'oculis'),
				'email'      => esc_html__('Email', 'oculis'),
			),
			'default'          => array(
				'twitter'    => '1',
				'facebook'   => '1',
				'google'     => '1',
			),
		),

	)));


// Social Media Footer
Redux::setSection( $opt_name, array(
	'title'      => esc_html__( 'Copyright Social Media', 'oculis'),
	'submenu'    => true,
	'subsection' => true,
	'fields'     => array(
		array(
			'title'            => esc_html__('Copyright Social Icons', 'oculis'),
			'subtitle'         => esc_html__('Choose which social media icons should display in the copyright section, the URL will be taken from the Social Media URL\'s tab.', 'oculis'),
			'id'               => 'footer_social_icons',
			'type'             => 'checkbox',
			'options'          => array(
				'twitter'    => esc_html__('Twitter', 'oculis' ),
				'facebook'   => esc_html__('Facebook', 'oculis' ),
				'linkedin'   => esc_html__('Linkedin', 'oculis' ),
				'pinterest'  => esc_html__('Pinterest', 'oculis' ),
				'google'     => esc_html__('Google', 'oculis' ),
				'instagram'  => esc_html__('Instagram', 'oculis' ),
				'flickr'     => esc_html__('Flickr', 'oculis' ),
				'youtube'    => esc_html__('Youtube', 'oculis' ),
				'instagram'  => esc_html__('Instagram', 'oculis' ),
				'vk'         => esc_html__('VK', 'oculis' ),
				'yelp'       => esc_html__('Yelp', 'oculis' ),
				'foursquare' => esc_html__('Foursquare', 'oculis' ),
				'email'      => esc_html__('Email', 'oculis' ),
			),
			'default'          => array(
				'twitter'    => '1',
				'facebook'   => '1',
				'google'     => '1',
				'instagram'  => '1',
				'vk'         => '1',
			),
		),
	)
));


/*--------------------------------------------------------------
# Advanced
--------------------------------------------------------------*/
Redux::setSection( $opt_name, array(
	'title' => esc_html__( 'Enviroment', 'oculis' ),
	'icon' => 'el-icon-wordpress',
));

/**
 * GoogleMaps API
 */
Redux::setSection( $opt_name, array(
	'title' => esc_html__('Google Maps API', 'oculis'),
	'subsection' => true,
	'fields' => array(

		// Info
		array(
			'title' => esc_html__( 'GoogleMaps API Key', 'oculis' ),
			'desc' => esc_html__( 'Place your Google Maps API Key in here to use our custom Google Maps Module within visual composer. You can create a GoogleMaps API Key for free, to find out how to do this please read our theme documentation.', 'oculis' ),
			'id' => 'info_maps_api',
			'type' => 'info',
			'style' => 'success',
		),

		// API Key
		array(
			'title' => esc_html__( 'Google Maps Secret API Key', 'oculis' ),
			'id' => 'googlemaps_api_key',
			'type' => 'text',
			'default' => esc_html( '' ),
		),

	)
));

/**
 * Google Analytics
 */
Redux::setSection( $opt_name, array(
	'title' => esc_html__('Google Analytics', 'oculis'),
	'subsection' => true,
	'fields' => array(

		// Google Analytics Script
		array(
			'title' => esc_html__('Google Analytics Code', 'oculis'),
			'subtitle' => esc_html__('Place your Google Analytics code in here, make sure to include the whole script.', 'oculis'),
			'id' => 'google_analytics',
			'type' => 'textarea',
			'theme' => 'chrome',
			'default' => esc_html( '' ),
		),

	)
));