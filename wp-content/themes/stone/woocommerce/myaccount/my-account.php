<?php
/**
 * My Account page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/my-account.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.5.0
 */
defined( 'ABSPATH' ) || exit;

global $krucial_options;

$page_header_title = $krucial_options['account_page_header_title'];

?>

<div class="account-page-header">
    <div class="page-header-inner">
        <h1 class="title"><?php echo esc_html($page_header_title) === null ? esc_html__('Account', 'oculis') : esc_html($page_header_title); ?></h1>
        <?php if ($page_header_title !== null) {  ?>
            <div class="description"><?php echo esc_html($krucial_options['account_page_header_description']) ?></div>
        <?php } ?>
    </div>
</div>

<div class="content-area">
    <?php

    /**
     * My Account navigation.
     * @since 2.6.0
     */
    do_action( 'woocommerce_account_navigation' ); ?>

    <div class="woocommerce-MyAccount-content">
        <?php
            /**
             * My Account content.
             * @since 2.6.0
             */
            do_action( 'woocommerce_account_content' );
        ?>
    </div>
</div>