<?php

// Load slick slider
wp_enqueue_script('slick');
wp_enqueue_style('slick');

?>

<div class="product-image-carousel-wrapper">
    <div class="product-image-carousel">

        <?php

        global $post, $product, $woocommerce;

        $attachment_ids = $product->get_gallery_image_ids();

        // Get the featured image
        echo '<div class="product-image-carousel-item">';
        if (has_post_thumbnail()) {
	        echo the_post_thumbnail();
        } else {
            echo wc_placeholder_img('full');
        }
        echo '</div>';

        // Get the gallery images
        foreach ($attachment_ids as $attachment_id) {

            $props = wc_get_product_attachment_props($attachment_id, $post);

            if (!$props['url']) {
                continue;
            }

            echo '<div class="product-image-carousel-item">';
            echo wp_get_attachment_image($attachment_id, apply_filters('single_product_small_thumbnail_size', 'shop_thumbnail'), 0, $props);
            echo '</div>';
        }
        ?>

    </div>

    <?php

    // Generate IDs for slick prev/next
    $slick_prev = uniqid( 'prev_' );
    $slick_next = uniqid( 'next_' );

    /*

     <!-- Slick Pagination -->
     <span class="<?php echo esc_attr( $slick_next ) ?> slick-arrow-right"><i class="icon-arrow-right"></i></span>
     <span class="<?php echo esc_attr( $slick_prev ) ?> slick-arrow-left"><i class="icon-arrow-left"></i></span>

     */

    ?>

</div>

<script type="text/javascript">
    jQuery(document).ready(function($) {
        'use strict';

        $('.product-image-carousel').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            rows: 1,
            dots: true,
            arrows: true,
            infinite: true,
            speed: 500,
            prevArrow: $('.<?php echo esc_js( $slick_prev ) ?>'),
            nextArrow: $('.<?php echo esc_js( $slick_next ) ?>'),
            responsive: [
                {
                    breakpoint: 1100,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        rows: 1,
                    }
                },
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 1,
                        slidestToScroll: 1,
                        rows: 1,
                    }
                }
            ]
        });

    });
</script>