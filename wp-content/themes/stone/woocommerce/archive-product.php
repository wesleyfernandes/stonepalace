<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

global $krucial_options;

get_header( 'shop' );

$page_header_title = $krucial_options['shop_page_header_title'];

?>

    <div class="shop-page-header">
        <div class="page-header-inner">
			<?php if (is_product_category()) { ?>
                <h1 class="title"><?php echo esc_html__('Category: ', 'oculis') . esc_html(get_the_title()); ?></h1>
                <div class="description"><?php echo esc_html__('Displaying results for products within the category ', 'oculis') . esc_html(get_the_title()); ?></div>
			<?php } else { ?>
                <h1 class="title"><?php echo esc_html($page_header_title) === null || empty($page_header_title) ? esc_html__('Shop', 'oculis') : esc_html($page_header_title); ?></h1>
	            <?php if ($page_header_title !== null) {  ?>
                    <div class="description"><?php echo esc_html($krucial_options['shop_page_header_description']) ?></div>
                <?php }
			} ?>
        </div>
    </div>



    <div class="shop-archive <?php echo krucial_sidebar_wc_archives(); ?>">
		<?php

		/**
		 * Hook: woocommerce_before_main_content.
		 *
		 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
		 * @hooked woocommerce_breadcrumb - 20
		 * @hooked WC_Structured_Data::generate_website_data() - 30
		 */
		do_action( 'woocommerce_before_main_content' );


		if ( woocommerce_product_loop() ) {

			/**
			 * Hook: woocommerce_before_shop_loop.
			 *
			 * @hooked wc_print_notices - 10
			 * @hooked woocommerce_result_count - 20
			 * @hooked woocommerce_catalog_ordering - 30
			 */
			do_action( 'woocommerce_before_shop_loop' );

			woocommerce_product_loop_start();

			if ( wc_get_loop_prop( 'total' ) ) {
				while ( have_posts() ) {
					the_post(); ?>

                    <div class="product-item">
						<?php
						/**
						 * Hook: woocommerce_shop_loop.
						 *
						 * @hooked WC_Structured_Data::generate_product_data() - 10
						 */
						do_action( 'woocommerce_shop_loop' );

						wc_get_template_part( 'content', 'product' );
						?>
                    </div>
					<?php
				}
			}

			woocommerce_product_loop_end();

			/**
			 * Hook: woocommerce_after_shop_loop.
			 *
			 * @hooked woocommerce_pagination - 10
			 */
			do_action( 'woocommerce_after_shop_loop' );
		} else {
			/**
			 * Hook: woocommerce_no_products_found.
			 *
			 * @hooked wc_no_products_found - 10
			 */
			do_action( 'woocommerce_no_products_found' );
		}

		/**
		 * Hook: woocommerce_after_main_content.
		 *
		 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
		 */
		do_action( 'woocommerce_after_main_content' );

		/**
		 * Hook: woocommerce_sidebar.
		 *
		 * @hooked woocommerce_get_sidebar - 10
		 */
		do_action( 'woocommerce_sidebar' );

		/**
		 * Get the sidebar
		 */
		if (is_active_sidebar('woo-archive-sidebar') && $krucial_options['shop_layout'] !== 'no-sidebar') {
            echo '<div id="secondary" class="widget-area sidebar">';
                dynamic_sidebar('woo-archive-sidebar');
            echo '</div>';
		}

		?>
    </div>

<?php get_footer( 'shop' ); ?>