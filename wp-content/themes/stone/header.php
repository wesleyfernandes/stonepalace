<?php
global $krucial_options;
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<?php

// Header
switch ($krucial_options['header_style']) {
    case 'header-style-2':
	    get_template_part('components/header/headerstyle-2');
	    break;
	case 'header-style-3':
		get_template_part('components/header/headerstyle-3');
		break;
	case 'header-style-4':
		get_template_part('components/header/headerstyle-4');
		break;
    default:
		get_template_part('components/header/headerstyle-1');
		break;
}

// Mobile menu
get_template_part('components/header/mobile-menu');

// Modal slideout quote
if ($krucial_options['slideout_quote_switch']) {
    get_template_part('components/header/slideout-quote');
}

?>

<div id="wrapper">