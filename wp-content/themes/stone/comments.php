<?php
/**
 * Template for Post or Page Comments
 */

// Check if password required
if ( post_password_required() ) {
    return;
}

if ( have_comments() ) {
	$comments_number = get_comments(array(
		'status' => 'approve',
		'post_id' => get_the_ID(),
		'type'=> 'comment',
		'count' => true
	));
	$plural = ($comments_number === 1 ? '' : 's');
	?>
    <h4 class="comments-title"><?php echo esc_html($comments_number) . esc_html__(' Comment', 'oculis') . esc_html($plural); ?></h4>
    <ul class="comments-list">
        <?php
        wp_list_comments( array(
            'style'       => 'ul',
            'short_ping'  => true,
            'type'        => 'comment',
            'avatar_size' => 80,
            'callback'    => 'krucial_render_comments'
        ) );
        ?>
    </ul>
    <div class="comment-navigation">
        <?php
        echo paginate_comments_links( array(
	        'type' => 'list',
	        'prev_text' => '<i class="essentials krucial-essential-light-01-chevron-left"></i>',
	        'next_text' => '<i class="essentials krucial-essential-light-02-chevron-right"></i>',
        ));
        ?>
    </div>
    <?php
}

// Comments are closed
if ( !comments_open() && get_comments_number() && post_type_supports( get_post_type(), 'comments' ) ) { ?>
    <p class="no-comments"><?php esc_html__('Comments are closed.', 'oculis'); ?></p>
    <?php
}

// Load comment form
comment_form();

?>