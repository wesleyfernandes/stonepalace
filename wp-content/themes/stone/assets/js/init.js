(function($) {
    'use strict';

    /*--------------------------------------------------------------
     # Enable masonry for blog
     --------------------------------------------------------------*/
    const $container = $('.masonry-grid');

    $container.imagesLoaded(function() {
        $container.masonry({
            itemSelector: '.grid-item'
        });
    });


    /*--------------------------------------------------------------
     # Check if element is in viewport
     --------------------------------------------------------------*/
    $.fn.isOnScreen = function(){
        const win = $(window);
        const viewport = {
            top : win.scrollTop(),
            left : win.scrollLeft()
        };
        viewport.right = viewport.left + win.width();
        viewport.bottom = viewport.top + win.height();

        const bounds = this.offset();
        bounds.right = bounds.left + this.outerWidth();
        bounds.bottom = bounds.top + this.outerHeight();

        return (!(viewport.right < bounds.left || viewport.left > bounds.right || viewport.bottom < bounds.top || viewport.top > bounds.bottom));
    };


    /*--------------------------------------------------------------
     # HS1 sticky header
     --------------------------------------------------------------*/
    if ($('.header').hasClass('style-1')) {
        $(window).on('load scroll', () => {
            if ($('body').hasClass('sticky-header')) {
                const scrollTop = $(window).scrollTop();
                const header = $('.header.style-1');
                scrollTop > 200
                    ? header.addClass('sticky-active')
                    : header.removeClass('sticky-active');
            }
        });
    }


    /*--------------------------------------------------------------
     # HS2 sticky header
     --------------------------------------------------------------*/
    if ($('.header').hasClass('style-2')) {
        $(window).on('load scroll', () => {
            if ($('body').hasClass('sticky-header')) {
                const scrollPos = $(window).scrollTop();
                const header = $('.header.style-2');
                const navBar = $('.hs2-main-inner');
                const navBarHeight = navBar.height();
                const navBarWidth = navBar.outerWidth();
                const stickySpacer = $('.hs2-main-sticky-spacer');
                const posFromTop = navBar.offset().top;
                const spacerPosFromTop = stickySpacer.offset().top;
                if (scrollPos > posFromTop) {
                    header.addClass('sticky-active');
                    navBar.css('width', navBarWidth);
                    stickySpacer.css('height', navBarHeight);
                } else if (scrollPos < (spacerPosFromTop + 30)) {
                    header.removeClass('sticky-active');
                    stickySpacer.css('height', '0');
                }
            }
        });
    }


    /*--------------------------------------------------------------
     # HS3 sticky header
     --------------------------------------------------------------*/
    if ($('.header').hasClass('style-3')) {
        $(window).on('load scroll', () => {
            if ($('body').hasClass('sticky-header')) {
                const scrollPos = $(window).scrollTop();
                const header = $('.header.style-3');
                const navBar = $('.hs3-main-inner');
                const navBarHeight = navBar.height();
                const navBarWidth = navBar.outerWidth();
                const stickySpacer = $('.hs3-main-sticky-spacer');
                const posFromTop = navBar.offset().top;
                const spacerPosFromTop = stickySpacer.offset().top;
                if (scrollPos > posFromTop) {
                    header.addClass('sticky-active');
                    navBar.css('width', navBarWidth);
                    stickySpacer.css('height', navBarHeight);
                } else if (scrollPos < spacerPosFromTop) {
                    header.removeClass('sticky-active');
                    stickySpacer.css('height', '0');
                }
            }
        });
    }


    /*--------------------------------------------------------------
     # Mobile menu
     --------------------------------------------------------------*/

    // Toggle menu
    $('.toggler, .toggler-close').on('click', () => {
        $('.mobile-menu-wrapper').toggleClass('open');
    });

    // Add arrow to submenus
    $('.mobile-menu-wrapper ul li ul').before($('<span class="mobile-submenu-arrow"><i class="essentials krucial-essential-light-02-chevron-right"></i></span>'));

    // Submenu slide up/down
    $('.mobile-menu > li > span, .mobile-menu .sub-menu > li > span').on('touchstart click', function(e) {
        e.preventDefault();
        if (false === $(this).next().is(':visible')) {
            // If another submenu is open, slide this up
            $(this).parent().siblings().find('.sub-menu').slideUp(300);
            // Spin the dropdown icon back
            $(this).parent().siblings().find('.mobile-submenu-arrow').removeClass('mobile-submenu-active');
        }
        $(this).next().slideToggle(300);
        $(this).toggleClass('mobile-submenu-active');
    });


    /*--------------------------------------------------------------
     # HS4 Sidemenu
     --------------------------------------------------------------*/

    // Add arrow to submenus
    $('.header.style-4 ul li ul.sub-menu').before($('<span class="submenu-arrow"><i class="essentials krucial-essential-light-02-chevron-right"></i></span>'));

    // Submenu slide up/down
    $('.header.style-4 ul li span').on('touchstart click', function(e) {
        e.preventDefault();
        if (false === $(this).next().is(':visible')) {
            // If another submenu is open, slide this up
            $(this).parent().siblings().find('.sub-menu').slideUp(300);
            // Spin the dropdown icon back
            $(this).parent().siblings().find('.submenu-arrow').removeClass('submenu-active');
        }
        $(this).next().slideToggle(300);
        $(this).toggleClass('submenu-active');
    });



    /*--------------------------------------------------------------
     # Initialize Select 2
     --------------------------------------------------------------*/
    $('select').select2({
        width: '100%',
        minimumResultsForSearch: '-1'
    });


    /*--------------------------------------------------------------
     # Open/close slideout quote form
     --------------------------------------------------------------*/

    // Open
    $('.open-slideout-quote').on('click', function() {
        var modal = $('.slideout-wrap');
        modal.css('display', 'flex');
        setTimeout(function(){
            modal.addClass('show');
            $('body').css('overflow', 'hidden');
        }, 100);
    });

    // Close
    $('.slideout-close').on('click', function(){
        var modal = $('.slideout-wrap');
        modal.removeClass('show');
        setTimeout(function(){
            modal.css('display', 'none');
            $('body').css('overflow', 'initial');
        }, 400);
    });

})(jQuery); // end document