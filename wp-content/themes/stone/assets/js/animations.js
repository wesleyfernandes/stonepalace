(function($) {

    'use strict';

    /*--------------------------------------------------------------
     # Archive page animations
     --------------------------------------------------------------*/
    $('.animateFadeInUp').viewportChecker({
        classToAdd: 'animation_visible animated fadeInUp',
        classToRemove: 'animation_hidden',
        offset: 0
    });

})(jQuery); // end document