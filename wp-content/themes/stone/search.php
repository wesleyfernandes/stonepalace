<?php
/**
 * Template for Searches
 */

get_header();

?>

<div class="search-page-header">
    <div class="container">
        <div class="hero">
            <h1 class="title animation_hidden animateFadeInUp delay-0 duration-25">
	            <?php
	            echo (have_posts())
		            ? esc_html__( 'Search Results for: ', 'oculis' ) . esc_html( get_search_query() )
		            : esc_html__( 'Sorry, no posts found.', 'oculis' );
	            ?>
            </h1>
            <div class="description animation_hidden animateFadeInUp delay-25 duration-25">
		        <?php echo esc_html__( 'We couldn\'t find any posts matching your search', 'oculis' ) ?>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="search-page">
        <div class="content-area">
            <?php if ( have_posts() ) { ?>
                <div class="row">
                    <?php while ( have_posts() ): the_post(); ?>
                        <div class="col-12 col-md-6">
                            <div id="post-<?php the_ID() ?>" <?php post_class() ?>>
                                <?php get_template_part( 'components/blog/blog-card' ); ?>
                            </div>
                        </div>
                    <?php endwhile; ?>
                </div>
                <?php echo paginate_links(array(
                    'type' => 'list',
                    'prev_text' => '<i class="icon-arrow-left"></i>',
                    'next_text' => '<i class="icon-arrow-right"></i>',
                ));
            } else { ?>
                <div class="no-posts">
                    <div class="no-posts-text">
                        <?php echo esc_html__( 'Try searching for something else...', 'oculis' ) ?>
                    </div>
                    <?php get_search_form(); ?>
                </div>
            <?php } ?>
        </div>
    </div>
</div>

<?php get_footer(); ?>