<?php

/**
 * Theme setup
 */
if ( !isset( $content_width ) ) {
    $content_width = 1140;
}

function krucial_theme_setup() {
    // Image sizes
    add_image_size( 'krucial-image-150x150-cropped', 150, 150, true );
    add_image_size( 'krucial-image-250x250-cropped', 250, 250, true );
    add_image_size( 'krucial-image-350x250-cropped', 350, 250, true );
    add_image_size( 'krucial-image-500x300-cropped', 500, 300, true );
    add_image_size( 'krucial-image-500x500-cropped', 500, 500, true );
    add_image_size( 'krucial-image-530x265-cropped', 530, 265, true );/*padrão exibe fotos*/
    add_image_size( 'krucial-image-600x500-cropped', 600, 500, true );
    add_image_size( 'krucial-image-650x300-cropped', 650, 300, true );
    add_image_size( 'krucial-image-700x500-cropped', 700, 500, true );
    add_image_size( 'krucial-image-750x400-cropped', 750, 400, true );
	add_image_size( 'krucial-image-700x900-cropped', 700, 900, true );
    add_image_size( 'krucial-image-1200x500-cropped', 1200, 500, true );
    add_image_size( 'krucial-image-1200x500-cropped', 1200, 600, true );

    // Register menus
    register_nav_menus(array(
            'krucial-main-menu' => esc_html( 'Main Menu'),
            'krucial-copyright-menu' => esc_html( 'Copyright Menu'),
        )
    );

    // Theme Support
    add_theme_support( 'post-thumbnails' );
    add_theme_support( 'title-tag' );
    add_theme_support( 'woocommerce' );
    add_theme_support( 'automatic-feed-links' );
    add_theme_support( 'html5', array('search-form', 'comment-form', 'comment-list', 'gallery', 'caption') );
}
add_action( 'after_setup_theme', 'krucial_theme_setup' );


/**
 * Enqueue scripts & styles
 */
function krucial_enqueue_scripts() {
    $font_url = add_query_arg( 'family', urlencode( 'Poppins:400,500,600,700|Quicksand:400,700&subset=latin,latin-ext' ), "//fonts.googleapis.com/css" );

    // Register CSS
	wp_register_style( 'slick', get_template_directory_uri() . '/assets/css/slick.css', array(), '1.6.0' );

    // Load CSS
    wp_enqueue_style( 'select2', get_template_directory_uri() . '/assets/css/select2.min.css', array(), '4.0.3' );
    wp_enqueue_style( 'essential-icons', get_template_directory_uri() . '/assets/css/essential-icons.css', array(), null );
    wp_enqueue_style( 'font-awesome-5', get_template_directory_uri() . '/assets/css/font-awesome.min.css', array(), '3.5.1' );
	wp_enqueue_style('krucial-styles', get_template_directory_uri() . '/assets/bundle/main.min.css', array(), null);

	if (krucial_on_woocommerce_page() === 'true') {
		wp_enqueue_style( 'krucial-woocommerce', get_template_directory_uri() . '/assets/bundle/woocommerce.min.css', array(), null );
	}

	// RTL support
	wp_style_add_data( 'krucial-styles', 'rtl', 'replace' );

	if (!class_exists('Krucial_Theme_Functionality')) {
		wp_enqueue_style( 'krucial-fonts', $font_url, array(), '1.0' );
	}

	// Register JS
	wp_register_script( 'slick', get_template_directory_uri() . '/assets/js/slick.min.js', array( 'jquery' ), null, true );
	wp_register_script( 'isotope', get_template_directory_uri() . '/assets/js/isotope.pkgd.min.js', array( 'jquery' ), null, true );
	wp_register_script( 'plyr', get_template_directory_uri() . '/assets/js/plyr.min.js', array( 'jquery' ), null, true );
	wp_register_script( 'countup', get_template_directory_uri() . '/assets/js/countUp.min.js', array( 'jquery' ), null, true );

    // Load JS
	wp_enqueue_script( 'viewportchecker', get_template_directory_uri() . '/assets/js/viewportchecker.min.js', array( 'jquery' ), null, true );
	wp_enqueue_script( 'masonry' );
	wp_enqueue_script( 'imagesloaded' );
    wp_enqueue_script( 'select2', get_template_directory_uri() . '/assets/js/select2.min.js', array( 'jquery' ), null, true );
	wp_enqueue_script( 'krucial-scripts', get_template_directory_uri() . '/assets/bundle/app.min.js', array( 'jquery' ), null, true );

    if ( is_singular() && comments_open() ) {
        wp_enqueue_script( 'comment-reply' );
    }
}
add_action( 'wp_enqueue_scripts', 'krucial_enqueue_scripts' );


/**
 * Load files
 */

// Includes
require_once( get_template_directory() . '/inc/tgm/tgm-plugin-registration.php' );
require_once( get_template_directory() . '/inc/options/theme-options.php' );
require_once( get_template_directory() . '/functions/visual_composer_icons.php' );

// Init
require_once( get_template_directory() . '/sidebar.php' );
require_once( get_template_directory() . '/functions/init/register_sidebars.php' );
require_once( get_template_directory() . '/functions/init/register_composer.php' );

// Global
require_once( get_template_directory() . '/functions/blog_functions.php' );
require_once( get_template_directory() . '/functions/theme_functions.php' );
require_once( get_template_directory() . '/functions/helper_functions.php' );
require_once( get_template_directory() . '/functions/ajax/ajax_setup.php' );


/**
 * Load visual composer
 */
function krucial_load_composer() {
	require_once(get_template_directory() . '/functions/visual_composer.php');
}
add_action('init', 'krucial_load_composer', 999);


/**
 * Enqueue admin scripts
 */
function krucial_admin_scripts() {
    wp_enqueue_style( 'krucial-admin-styles', get_template_directory_uri() . '/admin/admin.css', array(), null );
}
add_action('admin_enqueue_scripts', 'krucial_admin_scripts');


/**
 * Load text domain
 */
function krucial_load_textdomain(){
    load_theme_textdomain( 'oculis', get_template_directory() . '/languages' );
}
add_action( 'after_setup_theme', 'krucial_load_textdomain' );

	// Shortcode to output custom PHP in Visual Composer
	function wpc_vc_shortcode( $atts ) {
		echo the_date()."This is my custom PHP output in Visual Composer!";
		$image = get_field('image');
		$size = 'full'; // (thumbnail, medium, large, full or custom size)

		if( $image ) {

			echo wp_get_attachment_image( $image, $size );

		}
	}
	add_shortcode( 'my_vc_php_output', 'wpc_vc_shortcode');
	
	function post_title_shortcode(){
		return get_the_title();
	}
	add_shortcode('post_title','post_title_shortcode');
	
/* CUSTOM POST TYPE */
require_once( get_template_directory() . '/custom/custom_functions.php' );

	
?>