<?php
/**
 * Template for Post or Page Pingbacks & Trackbacks
 */

if ( have_comments() ) {
	$comments_number = get_comments(array(
        'status' => 'approve',
        'post_id' => get_the_ID(),
        'count' => true
    ));
	$comments_text = ($comments_number === 1)
		? esc_html__('1 Comment', 'oculis')
		: $comments_number . esc_html__(' Comments', 'oculis');
    ?>
    <div class="post-comments">
        <h4 class="comments-title"><?php echo esc_html($comments_text); ?></h4>
        <ul class="comments-list">
            <?php
            wp_list_comments( array(
                'style'       => 'ul',
                'short_ping'  => true,
                'avatar_size' => 80,
                'callback'    => 'krucial_render_comments'
            ) );
            ?>
        </ul>
    </div>
	<?php
}

?>