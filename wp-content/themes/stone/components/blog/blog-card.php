<?php
/**
 * Blog Card component used by the post loop
 */
?>

<div class="blog-card">
	<?php if( has_post_thumbnail() ) { ?>
        <a class="card-thumbnail" href="<?php the_permalink() ?>">
			<?php the_post_thumbnail( 'krucial-image-1200x600-cropped' ); ?>
        </a>
	<?php } ?>
    <div class="blog-card-inner">
        <div class="card-date">
            <?php echo esc_html(get_the_date()); ?>
        </div>
        <h2 class="card-title">
            <?php the_title( '<a href="' . esc_url( get_permalink() ) . '">', '</a>'); ?>
        </h2>
        <div class="card-excerpt">
            <?php the_excerpt(); ?>
        </div>
        <a class="continue" href="<?php the_permalink(); ?>">
            <span><?php echo esc_html__('Continue Reading', 'oculis'); ?></span>
            <i class="essentials krucial-essential-light-07-arrow-right"></i>
        </a>
    </div>
</div>