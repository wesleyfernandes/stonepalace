<?php
/**
 * Template Part for Single BlogPosts
 */

$permalink = get_permalink();
$twitter   = 'https://twitter.com/intent/tweet?url=' . esc_url($permalink);
$facebook  = 'https://www.facebook.com/sharer/sharer.php?u=' . esc_url($permalink);
$google    = 'https://plus.google.com/share?url=' . esc_url($permalink);
$background = (has_post_thumbnail()) ? 'url(' . esc_url(get_the_post_thumbnail_url(get_the_ID())) . ')' : '#0d0d0d';
$comments_text = (get_comments_number() === '1')
    ? esc_html__('1 Comment', 'oculis')
    : get_comments_number() . esc_html__(' Comments', 'oculis');



if (class_exists('Krucial_Theme_Functionality')) { ?>
    <div class="post-page-header">
        <div class="post-date">
            <i class="essentials krucial-calendar"></i>
            <span><?php echo esc_html(get_the_date('d . m . Y')); ?></span>
        </div>
        <div class="post-sharing">
            <span><?php echo esc_html__( 'Share', 'oculis' ) ?></span>
            <a href="<?php echo esc_url( $twitter ) ?>" target="_blank" rel="nofollow"><i class="essentials krucial-twitter"></i></a>
            <a href="<?php echo esc_url( $facebook ) ?>" target="_blank" rel="nofollow"><i class="essentials krucial-facebook"></i></a>
            <a href="<?php echo esc_url( $google ) ?>" target="_blank" rel="nofollow"><i class="essentials krucial-googleplus"></i></a>
        </div>
        <div class="page-header-inner">
            <div class="categories animation_hidden animateFadeInUp delay-0 duration-25">
			    <?php echo krucial_post_categories(); ?>
            </div>
            <h1 class="title animation_hidden animateFadeInUp delay-25 duration-25"><?php the_title(); ?></h1>
        </div>
    </div>
<?php } else {
	krucial_render_page_header();
} ?>

<div class="container">
    <div class="post-page <?php echo krucial_sidebar_layout_blogpost(); ?>">
        <?php if (has_post_thumbnail()) { ?>
            <div class="post-thumbnail animation_hidden animateFadeInUp delay-50 duration-25">
                <?php the_post_thumbnail('krucial-image-750x400-cropped' ); ?>
            </div>
        <?php } ?>
        <div class="content-area">
            <div class="blog-post">
                <div class="post-content">
					<?php the_content(); ?>
                </div>
                <div class="post-bottom">
                    <div class="post-comments-count">
                        <i class="essentials krucial-chat"></i>
                        <?php echo '<a href="' . get_comments_link() . '">' . esc_html($comments_text) . '</a>'; ?>
                    </div>

                    <?php

                    // Post tags
                    if (has_tag()) { ?>
                        <div class="post-tags">
                            <?php echo get_the_tag_list(); ?>
                        </div>
                        <?php
                    }

                    // Post author
                    if ( !empty( get_the_author_meta( 'description' ) ) ) { ?>
                        <div class="post-author">
                            <div class="picture">
                                <?php echo get_avatar( get_the_author_meta( 'email' ), 100 ); ?>
                            </div>
                            <div class="author-content">
                                <div class="name">
                                    <?php echo esc_html(get_the_author()); ?>
                                </div>
                                <div class="bio">
                                    <?php echo esc_html( get_the_author_meta( 'description' ) ); ?>
                                </div>
                            </div>
                        </div>
                    <?php } ?>

                    <?php

                    wp_link_pages(array(
                        'before' => '<div class="link-pages">',
                        'after' => '</div>',
                        'separator' => '',
                        'pagelink' => '<span>%</span>'
                    ));

                    // Show pingbacks & trackbacks
                    if ( krucial_if_post_has( 'pings', get_the_ID() ) ) { ?>
                        <div class="pingbacks-trackbacks">
	                        <?php comments_template( '/components/blog/pingbacks.php' ); ?>
                        </div>
                    <?php } else if (comments_open()) {
	                    // Show comments ?>
                        <div class="post-comments" id="comments">
                            <?php the_comments_pagination(); comments_template(); ?>
                        </div>
                        <?php
                    }

                    ?>
                </div>
            </div>
        </div>
		<?php krucial_get_sidebar(); ?>
    </div>
</div>
<?php

$tags = wp_get_post_tags($post->ID);
if ($tags) { ?>
    <div class="related-posts">
        <div class="container">
            <div class="small-title center"><?php echo esc_html__('Related Posts', 'oculis'); ?></div>
            <h3 class="related-title"><?php echo esc_html__('You may also like to read...', 'oculis'); ?></h3>
            <div class="row">
                <?php
                $first_tag = $tags[0]->term_id;
                $related_query = new WP_Query(array(
                    'post_type' => 'post',
                    'post_status' => 'publish',
                    'tag__in' => array($first_tag),
                    'post__not_in' => array($post->ID),
                    'posts_per_page' => 3,
                    'orderby' => 'rand',
                ));

                $counter = 1;
                if ( $related_query->have_posts() ) {
                    while ($related_query->have_posts()) : $related_query->the_post();
	                    $unique_id = uniqid( 'animation-' );
	                    $delay = 'delay-' . ($counter * 25);
	                    ?>
                        <div class="col-12 col-md-4">
                            <div class="<?php echo esc_attr( $unique_id ) . ' ' . esc_attr($delay) ?> animation_hidden animateFadeInUp duration-25">
                                <?php get_template_part('components/blog/blog-card'); ?>
                            </div>
                        </div>
                        <?php
	                    $counter++;
                        endwhile;
                }
                wp_reset_query();
                ?>
            </div>
        </div>
    </div>
<?php } ?>
