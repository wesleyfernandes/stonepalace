<?php

global $krucial_options;
$year = date('Y');

?>

<div class="copyright-wrapper style-1">
    <div class="container">
        <div class="copyright-inner">
            <div class="left">
                <div class="cr-text">
                    <?php
                    echo (empty($krucial_options['copyright_text']))
                            ? esc_html__( 'Copyright ', 'oculis' ) . esc_html($year)
                            : wp_kses_post( $krucial_options['copyright_text'] );

                    ?>
                </div>
                <div class="cr-social">
                    <?php get_template_part('components/copyright/social-copyright'); ?>
                </div>
            </div>
            <div class="right">
                <div class="cr-menu">
                    <?php
                    wp_nav_menu(array(
                        'theme_location' => 'krucial-copyright-menu',
                        'fallback_cb' => false,
                        'depth' => 1
                    ));
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>