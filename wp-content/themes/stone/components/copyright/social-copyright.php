<?php
global $krucial_options;

if (isset($krucial_options['footer_social_icons']['facebook']) && isset($krucial_options['facebook']) && $krucial_options['footer_social_icons']['facebook'] == 1 && $krucial_options['facebook'] !== '' && $krucial_options['facebook'] !== '#') { ?>
    <a target="_blank" href="<?php echo esc_url($krucial_options['facebook']); ?>" title="Facebook">
        <i class="fab fa-facebook"></i>
    </a>
	<?php
}

if (isset($krucial_options['footer_social_icons']['twitter']) && isset($krucial_options['twitter']) && $krucial_options['footer_social_icons']['twitter'] == 1 && $krucial_options['twitter'] !== '' && $krucial_options['twitter'] !== '#') { ?>
    <a target="_blank" href="<?php echo esc_url($krucial_options['twitter']); ?>" title="Twitter">
        <i class="fab fa-twitter"></i>
    </a>
	<?php
}

if (isset($krucial_options['footer_social_icons']['linkedin']) && isset($krucial_options['linkedin']) && $krucial_options['footer_social_icons']['linkedin'] == 1 && $krucial_options['linkedin'] !== '' && $krucial_options['linkedin'] !== '#') { ?>
    <a target="_blank" href="<?php echo esc_url($krucial_options['linkedin']); ?>" title="Linkedin">
        <i class="fab fa-linkedin"></i>
    </a>
	<?php
}

if (isset($krucial_options['footer_social_icons']['pinterest']) && isset($krucial_options['pinterest']) && $krucial_options['footer_social_icons']['pinterest'] == 1 && $krucial_options['pinterest'] !== '' && $krucial_options['pinterest'] !== '#') { ?>
    <a target="_blank" href="<?php echo esc_url($krucial_options['pinterest']); ?>" title="Pinterest">
        <i class="fab fa-pinterest"></i>
    </a>
	<?php
}

if (isset($krucial_options['footer_social_icons']['google']) && isset($krucial_options['google']) && $krucial_options['footer_social_icons']['google'] == 1 && $krucial_options['google'] !== '' && $krucial_options['google'] !== '#') { ?>
    <a target="_blank" href="<?php echo esc_url($krucial_options['google']); ?>" title="Google Plus">
        <i class="fab fa-google-plus"></i>
    </a>
	<?php
}

if (isset($krucial_options['footer_social_icons']['instagram']) && isset($krucial_options['instagram']) && $krucial_options['footer_social_icons']['instagram'] == 1 && $krucial_options['instagram'] !== '' && $krucial_options['instagram'] !== '#') { ?>
    <a target="_blank" href="<?php echo esc_url($krucial_options['instagram']); ?>" title="Instagram">
        <i class="fab fa-instagram"></i>
    </a>
	<?php
}

if (isset($krucial_options['footer_social_icons']['vk']) && isset($krucial_options['vk']) && $krucial_options['footer_social_icons']['vk'] == 1 && $krucial_options['vk'] !== '' && $krucial_options['vk'] !== '#') { ?>
    <a target="_blank" href="<?php echo esc_url($krucial_options['vk']); ?>" title="VK">
        <i class="fab fa-vk"></i>
    </a>
	<?php
}

if (isset($krucial_options['footer_social_icons']['yelp']) && isset($krucial_options['yelp']) && $krucial_options['footer_social_icons']['yelp'] == 1 && $krucial_options['yelp'] !== '' && $krucial_options['yelp'] !== '#') { ?>
    <a target="_blank" href="<?php echo esc_url($krucial_options['yelp']); ?>" title="Yelp">
        <i class="fab fa-yelp"></i>
    </a>
	<?php
}

if (isset($krucial_options['footer_social_icons']['foursquare']) && isset($krucial_options['foursquare']) && $krucial_options['footer_social_icons']['foursquare'] == 1 && $krucial_options['foursquare'] !== '' && $krucial_options['foursquare'] !== '#') { ?>
    <a target="_blank" href="<?php echo esc_url($krucial_options['foursquare']); ?>" title="Foursquare">
        <i class="fab fa-foursquare"></i>
    </a>
	<?php
}

if (isset($krucial_options['footer_social_icons']['flickr']) && isset($krucial_options['flickr']) && $krucial_options['footer_social_icons']['flickr'] == 1 && $krucial_options['flickr'] !== '' && $krucial_options['flickr'] !== '#') { ?>
    <a target="_blank" href="<?php echo esc_url($krucial_options['flickr']); ?>" title="Flickr">
        <i class="fab fa-flickr"></i>
    </a>
	<?php
}

if (isset($krucial_options['footer_social_icons']['youtube']) && isset($krucial_options['youtube']) && $krucial_options['footer_social_icons']['youtube'] == 1 && $krucial_options['youtube'] !== '' && $krucial_options['youtube'] !== '#') { ?>
    <a target="_blank" href="<?php echo esc_url($krucial_options['youtube']); ?>" title="Youtube">
        <i class="fab fa-youtube"></i>
    </a>
	<?php
}

if (isset($krucial_options['footer_social_icons']['email']) && isset($krucial_options['email']) && $krucial_options['footer_social_icons']['email'] == 1 && $krucial_options['email'] !== '' && $krucial_options['email'] !== '#') { ?>
    <a href="<?php echo wp_kses_post($krucial_options['email']); ?>" title="Email">
        <i class="fas fa-envelope-square"></i>
    </a>
	<?php
}

?>