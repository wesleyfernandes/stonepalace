<?php
/**
 * Card component used for custom post type archives (e.g: archive-services.php, etc)
 */

$thumbnail_url = wp_get_attachment_image_src(get_post_thumbnail_id(), 'krucial-image-1200x500-cropped')[0];

?>

<div class="cpt-card">
    <?php if ( has_post_thumbnail() ) { ?>
        <a class="thumbnail-image" href="<?php the_permalink() ?>">
            <?php the_post_thumbnail('krucial-image-500x300-cropped'); ?>
        </a>
    <?php } ?>
    <div class="card-content">
        <a class="card-title-wrap" href="<?php the_permalink() ?>">
            <h2 class="card-title">
                <?php the_title(); ?>
            </h2>
        </a>
        <div class="card-excerpt">
            <?php the_excerpt(); ?>
        </div>
    </div>
</div>