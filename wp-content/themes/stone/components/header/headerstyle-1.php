<?php global $krucial_options; ?>

<div class="header style-1">
    <div class="container">
        <div class="header-inner">
            <a class="logo" href="<?php echo esc_url( home_url( '/' ) ) ?>">
				<?php get_template_part('components/header/render-logo'); ?>
            </a>
            <div class="right">
                <div class="main-menu">
					<?php wp_nav_menu(array(
							'theme_location' => 'krucial-main-menu',
							'container'      => false,
							'fallback_cb'    => false,
							'depth'          => 3,
							'menu_class'     => 'main-menu'
                    )); ?>
                </div>
				<?php if ($krucial_options['hs1_header_action'] === 'number') { ?>
                    <div class="number-wrap">
                        <i class="essentials krucial-phone-actions-ring"></i>
                        <a href="tel:<?php echo esc_html(preg_replace('/\s+/', '', $krucial_options['hs1_phone_number'])); ?>">
                            <?php echo esc_html($krucial_options['hs1_phone_number']); ?>
                        </a>
                    </div>
                <?php } ?>
	            <?php if ($krucial_options['hs1_header_action'] === 'button') { ?>
                    <div class="button-wrap open-slideout-quote">
                        <span><?php echo esc_html($krucial_options['hs1_button_text']); ?></span>
                        <i class="essentials krucial-essential-light-02-chevron-right"></i>
                    </div>
	            <?php } ?>
            </div>
        </div>
    </div>
</div>
<div class="hs1-fixed-spacer"></div>