<?php global $krucial_options; ?>

<div class="toolbar">
    <div class="container">
        <div class="toolbar-inner">
            <div class="left">
                <?php if ($krucial_options['toolbar_number_switch'] === '1'): ?>
                    <div class="toolbar-number">
                        <i class="fas fa-phone-volume"></i>
                        <span><?php echo esc_html ($krucial_options['toolbar_number']); ?></span>
                    </div>
                <?php endif; ?>

                <?php if ($krucial_options['toolbar_location_switch'] === '1'): ?>
                    <div class="toolbar-location">
                        <i class="far fa-map"></i>
                        <span><?php echo esc_html($krucial_options['toolbar_location']); ?></span>
                    </div>
               <?php endif; ?>
            </div>
            <div class="right">
                <?php if (class_exists('WooCommerce') && $krucial_options['toolbar_shop'] === '1'): ?>
                    <div class="toolbar-shop">
                        <a href="<?php echo esc_url(wc_get_cart_url()); ?>" class="cart-count">
                            <i class="fas fa-shopping-basket"></i>
                            <span class="item-count"><?php echo esc_html(WC()->cart->get_cart_contents_count()) . esc_html__(' Items in Cart', 'oculis'); ?></span>
                        </a>
                    </div>
                <?php endif; ?>

                <?php if ($krucial_options['toolbar_social'] === '1'): ?>
                    <div class="toolbar-social">
                       <?php get_template_part('components/header/social-header'); ?>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>