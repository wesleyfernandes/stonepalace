<?php

global $krucial_options;

// Toolbar
if ($krucial_options['toolbar_switch'] === '1') {
	get_template_part('components/header/toolbar');
}

?>

<div class="header style-3">
    <div class="hs3-top">
        <a class="logo" href="<?php echo esc_url( home_url( '/' ) ) ?>">
            <?php get_template_part('components/header/render-logo'); ?>
        </a>
    </div>
    <div class="hs3-main">
        <div class="container">
            <div class="hs3-main-sticky-spacer"></div>
            <div class="hs3-main-inner">
                <div class="main-menu">
                    <?php wp_nav_menu( array(
                            'theme_location' => 'krucial-main-menu',
                            'container'      => false,
                            'fallback_cb'    => false,
                            'depth'          => 3,
                            'menu_class'     => 'main-menu'
                        )
                    ); ?>
                </div>
            </div>
        </div>
    </div>
</div>