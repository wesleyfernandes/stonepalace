<?php

global $krucial_options;

// Toolbar
if ($krucial_options['toolbar_switch'] === '1') {
	get_template_part('components/header/toolbar');
}

?>

<div class="header style-2">
    <div class="hs2-top">
        <div class="container">
            <div class="hs2-top-inner">
                <a class="logo" href="<?php echo esc_url( home_url( '/' ) ) ?>">
		            <?php get_template_part('components/header/render-logo'); ?>
                </a>
                <div class="hs2-top-inner-contact">
                    <?php if ($krucial_options['hs2_phone_switch'] == 1) { ?>
                        <div class="hs2-icon-box">
                            <div class="hs2-icon">
                                <i class="essentials krucial-phone-actions-ring"></i>
                            </div>
                            <div class="hs2-text-wrap">
                                <span class="top-text"><?php echo esc_html($krucial_options['hs2_phone_top']); ?></span><br>
                                <span class="bottom-text"><?php echo esc_html($krucial_options['hs2_phone_bottom']); ?></span>
                            </div>
                        </div>
                    <?php } ?>
                    <?php if ($krucial_options['hs2_location_switch'] == 1) { ?>
                        <div class="hs2-icon-box">
                            <div class="hs2-icon">
                                <i class="essentials krucial-location"></i>
                            </div>
                            <div class="hs2-text-wrap">
                                <span class="top-text"><?php echo esc_html($krucial_options['hs2_location_top']); ?></span><br>
                                <span class="bottom-text"><?php echo esc_html($krucial_options['hs2_location_bottom']); ?></span>
                            </div>
                        </div>
                    <?php } ?>
                    <?php if ($krucial_options['hs2_hours_switch'] == 1) { ?>
                        <div class="hs2-icon-box">
                            <div class="hs2-icon">
                                <i class="essentials krucial-clock"></i>
                            </div>
                            <div class="hs2-text-wrap">
                                <span class="top-text"><?php echo esc_html($krucial_options['hs2_hours_top']); ?></span><br>
                                <span class="bottom-text"><?php echo esc_html($krucial_options['hs2_hours_bottom']); ?></span>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
    <div class="hs2-main">
        <div class="container">
            <div class="hs2-main-sticky-spacer"></div>
            <div class="hs2-main-inner">
                <div class="main-menu">
                    <?php wp_nav_menu( array(
                            'theme_location' => 'krucial-main-menu',
                            'container'      => false,
                            'fallback_cb'    => false,
                            'depth'          => 3,
                            'menu_class'     => 'main-menu'
                        )
                    ); ?>
                </div>
                <?php if ($krucial_options['hs2_social_switch'] == 1) { ?>
                    <div class="hs2-social-wrap">
                        <?php get_template_part('components/header/social-header'); ?>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>