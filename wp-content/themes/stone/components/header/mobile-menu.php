<?php global $krucial_options; ?>

<div class="header-mobile">
    <?php if (!empty(krucial_option('mh_logo', false, 'url'))) { ?>
        <a class="logo" href="<?php echo esc_url( home_url( '/' ) ) ?>">
	        <?php get_template_part('components/header/render-logo'); ?>
        </a>
    <?php } else { ?>
        <span class="site-title"><?php bloginfo( 'Name' ) ?></span>
    <?php } ?>
    <div class="toggler">
        <div class="bar-top"></div>
        <div class="bar-middle"></div>
        <div class="bar-bottom"></div>
    </div>
    <div class="mobile-menu-wrapper">
        <div class="toggler-close">
            <div>
                <span></span>
                <span></span>
            </div>
        </div>
	    <?php if (!empty(krucial_option('mm_logo', false, 'url'))) { ?>
            <div class="mm-logo">
                <img
                    src="<?php echo esc_url( krucial_option( 'mm_logo', false, 'url' ) ) ?>"
                    width="<?php echo esc_attr($krucial_options['mm_logo_dimensions']['width']); ?>"
                    height="<?php echo esc_attr($krucial_options['mm_logo_dimensions']['height']); ?>"
                    alt="<?php echo esc_attr__('logo', 'oculis'); ?>"
                >
            </div>
	    <?php } ?>
		<?php
		wp_nav_menu( array(
			'theme_location' => 'krucial-main-menu',
			'fallback_cb'    => false,
			'menu_class'     => 'mobile-menu',
		) );
		?>
        <?php if ($krucial_options['mm_enable_number'] === '1') { ?>
            <div class="number-wrap">
                <span><?php echo esc_html__('Call Today', 'oculis') ?></span>
                <a href="tel:<?php echo esc_html(preg_replace('/\s+/', '', $krucial_options['mm_number'])); ?>">
                    <?php echo esc_html($krucial_options['mm_number']); ?>
                </a>
            </div>
        <?php } ?>
	    <?php if ($krucial_options['mm_enable_button'] === '1') { ?>
            <div class="button-wrap">
                <div class="quote-button open-slideout-quote">
                    <span><?php echo esc_html($krucial_options['mm_button']); ?></span>
                    <i class="essentials krucial-essential-light-07-arrow-right"></i>
                </div>
            </div>
        <?php } ?>
    </div>
</div>