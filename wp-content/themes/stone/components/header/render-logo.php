<?php

global $krucial_options;
$transparent = (function_exists('get_field') ? get_field('enable_transparent_header') : 'false');

// Get transparent logo for custom post type archives
if (is_post_type_archive(array('testimonials', 'staff', 'projects', 'services'))) {
	$transparent = true;
}

if (!empty(krucial_option('custom_logo', false, 'url'))) { ?>
	<img
		src="<?php echo esc_url( krucial_option( 'custom_logo', false, 'url' ) ) ?>"
		width="<?php echo esc_attr($krucial_options['logo_dimensions']['width']); ?>"
		height="<?php echo esc_attr($krucial_options['logo_dimensions']['height']); ?>"
		alt="<?php echo esc_attr__('logo', 'oculis'); ?>"
        class="dark"
	>
<?php } else { ?>
    <span class="site-title"><?php bloginfo( 'Name' ) ?></span>
<?php } ?>

<?php if (!empty(krucial_option('transparent_logo', false, 'url'))) { ?>
	<img
		src="<?php echo esc_url( krucial_option( 'transparent_logo', false, 'url' )) ?>"
		width="<?php echo esc_attr($krucial_options['logo_dimensions']['width']); ?>"
		height="<?php echo esc_attr($krucial_options['logo_dimensions']['height']); ?>"
		alt="<?php echo esc_attr__('logo', 'oculis'); ?>"
        class="light"
	>
<?php } ?>