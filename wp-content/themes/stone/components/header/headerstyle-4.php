<?php global $krucial_options ?>

<div class="header style-4">
    <div class="side-menu-logo">
        <?php get_template_part('components/header/render-logo'); ?>
    </div>
    <div class="dropdown-main-menu">
	    <?php wp_nav_menu( array(
			    'theme_location' => 'krucial-main-menu',
			    'container'      => false,
			    'fallback_cb'    => false,
			    'depth'          => 3,
			    'menu_class'     => 'main-menu'
		    )
	    ); ?>
    </div>
    <div class="hs4-bottom">
        <?php if (!empty($krucial_options['hs4_phone_number'])) { ?>
            <div class="hs4-phone">
                <i class="fas fa-phone-volume"></i>
                <a href="tel:<?php echo esc_html(preg_replace('/\s+/', '', $krucial_options['hs4_phone_number'])); ?>">
                    <?php echo esc_html($krucial_options['hs4_phone_number']); ?>
                </a>
             </div>
        <?php }
        if ($krucial_options['toolbar_social'] == true) { ?>
            <div class="hs4-social">
                <?php get_template_part('components/header/social-header'); ?>
            </div>
        <?php } ?>
    </div>
</div>