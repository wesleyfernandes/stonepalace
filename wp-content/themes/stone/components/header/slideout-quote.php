<?php

global $krucial_options;

?>

<div class="slideout-wrap">
	<div class="slideout-close">
		<i class="essentials krucial-essential-light-10-close-big"></i>
	</div>
	<div class="slideout-content">
		<?php
		if ( !empty( $krucial_options['slideout_content_before'] ) ) {
			echo wp_kses_post( $krucial_options['slideout_content_before'] );
		}
		if ( !empty( $krucial_options['quote_slideout_shortcode'] ) ) {
			echo do_shortcode( $krucial_options['quote_slideout_shortcode'] );
		}
		if ( !empty( $krucial_options['slideout_content_after'] ) ) {
			echo do_shortcode( $krucial_options['slideout_content_after'] );
		}
		?>
	</div>
</div>