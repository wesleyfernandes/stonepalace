<div class="text-locations">
	<div class="item">
		<i class="essentials krucial-phone-actions-ring"></i>
		<span>+71 7083 4526</span>
	</div>
	<div class="item">
		<i class="essentials krucial-email-arrow-up"></i>
		<span><a href="mailto:hello@oculistheme.com">hello@oculistheme.com</a></span>
	</div>
	<div class="item">
		<i class="essentials krucial-location5"></i>
		<span><a href="#">728 Varcity Street,<br>San Francisco, USA 2783</a></span>
	</div>
</div>