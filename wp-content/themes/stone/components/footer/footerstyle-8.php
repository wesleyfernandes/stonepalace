<div class="row">
    <div class="cell small-12 medium-3">
		<?php dynamic_sidebar('footer-col-1'); ?>
    </div>
    <div class="cell small-12 medium-3">
		<?php dynamic_sidebar('footer-col-2'); ?>
    </div>
    <div class="cell small-12 medium-6">
		<?php dynamic_sidebar('footer-col-3'); ?>
    </div>
</div>