<div class="row">
    <div class="col-12 col-md-2">
		<?php dynamic_sidebar('footer-col-1'); ?>
    </div>
    <div class="col-12 col-md-2">
		<?php dynamic_sidebar('footer-col-2'); ?>
    </div>
    <div class="col-12 col-md-2">
		<?php dynamic_sidebar('footer-col-3'); ?>
    </div>
    <div class="col-12 col-md-2">
		<?php dynamic_sidebar('footer-col-4'); ?>
    </div>
    <div class="col-12 col-md-2">
		<?php dynamic_sidebar('footer-col-5'); ?>
    </div>
    <div class="col-12 col-md-2">
		<?php dynamic_sidebar('footer-col-6'); ?>
    </div>
</div>