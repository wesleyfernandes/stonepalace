<?php
/**
 * Page content template for page.php
 */

if (function_exists('get_field')) {
	if (get_field('remove_default_page_heading') !== true) {
		krucial_render_page_header();
	}
} else {
	krucial_render_page_header();
}

?>

<div class="container">
	<div class="content-area">
		<div class="page-single">
			<?php

			if ( have_posts() ):
				while ( have_posts() ): the_post();
					the_content();
				endwhile;
			endif;

			wp_link_pages(array(
				'before' => '<div class="link-pages">',
				'after' => '</div>',
				'separator' => '',
				'pagelink' => '<span>%</span>'
			));

			echo paginate_comments_links( array(
				'type' => 'list',
				'prev_text' => esc_html__( 'Next Comment', 'oculis' ),
				'next_text' => esc_html__( 'Previous Comment', 'oculis' ),
			));

			if ( comments_open() ) { ?>
				<div class="page-comments">
					<?php comments_template(); ?>
				</div>
				<?php
			}

			?>
		</div>
	</div>
</div>