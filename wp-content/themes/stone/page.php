<?php
/**
 * Template for Single Pages
 */

get_header();

if (krucial_on_woocommerce_page() === 'false') {
    get_template_part('components/page/page-content');
} else { ?>
    <div class="page-single-shop">
		<?php

		if ( have_posts() ):
			while ( have_posts() ): the_post();
				the_content();
			endwhile;
		endif;

		?>
    </div>
    <?php
}

get_footer(); ?>