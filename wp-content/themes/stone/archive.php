<?php
/**
 * Template for the Archives
 */

get_header();

global $krucial_options;

if (class_exists('Krucial_Theme_Functionality')) { ?>
    <div class="archive-page-header-wrap">
        <div class="read-more">
            <i class="essentials krucial-arrow-left2"></i>
            <span><?php echo esc_html__('View All Posts', 'oculis'); ?></span>
        </div>
        <div class="container">
            <div class="blog-intro">
                <h1 class="blog-title animation_hidden animateFadeInUp delay-0 duration-25"><?php the_archive_title(); ?></h1>
                <div class="blog-description animation_hidden animateFadeInUp delay-25 duration-25"><?php echo esc_html__('Showing all posts in ', 'oculis') . esc_html(get_the_archive_title()); ?></div>
            </div>
        </div>
    </div>
<?php } else {
	krucial_render_page_header();
} ?>
    <div class="container">
        <div class="archive-page <?php echo krucial_sidebar_layout_archives(); ?>">
            <div class="content-area">
                <div class="masonry-grid">
				<?php
				if ( have_posts() ):
					while ( have_posts() ): the_post(); ?>
                        <div class="grid-item">
                            <div id="post-<?php the_ID() ?>" <?php post_class() ?>>
								<?php get_template_part( 'components/blog/blog-card' ); ?>
                            </div>
                        </div>
					<?php endwhile;
				endif;
				?>
                </div>
                <?php
				echo paginate_links( array(
					'type'      => 'list',
					'prev_text' => '<i class="icon-arrow-left"></i>',
					'next_text' => '<i class="icon-arrow-right"></i>',
				) );
				?>
            </div>
			<?php krucial_get_sidebar(); ?>
        </div>
    </div>

<?php get_footer(); ?>