<?php
/**
 * Template for the Blog Page
 */

get_header();

global $krucial_options;

if (class_exists('Krucial_Theme_Functionality')) {

$first_post_count = 1;

if ( have_posts() ):
    while ( have_posts() ): the_post();
        if ($first_post_count === 1) { ?>
            <div class="blog-page-header-wrap">
                <div class="read-more">
                    <i class="essentials krucial-arrow-left2"></i>
                    <span><?php echo esc_html__('View All Posts', 'oculis'); ?></span>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-12 col-md-6 d-flex align-items-center">
                            <div class="blog-intro">
                                <h1 class="blog-title animation_hidden animateFadeInUp delay-0 duration-25"><?php echo esc_html($krucial_options['blog_page_header_title']); ?></h1>
                                <div class="blog-description animation_hidden animateFadeInUp delay-25 duration-25"><?php echo esc_html($krucial_options['blog_page_header_description']); ?></div>
                                <div class="last-post-author animation_hidden animateFadeInUp delay-50 duration-25">
                                    <?php if ( ! empty( get_the_author_meta( 'description' ) ) ) { ?>
                                        <div class="author-pic">
                                            <?php echo get_avatar( get_the_author_meta( 'email' ), 100 ); ?>
                                        </div>
                                        <div class="author-name">
                                            <?php echo esc_html__('Last Post by ', 'oculis'); ?>
                                            <span><?php echo esc_html(get_the_author()); ?></span>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-6">
                            <div class="first-post">
                                <a href="<?php the_permalink(); ?>">
                                    <div class="recent-post-tag"><?php echo esc_html__('Latest Post', 'oculis'); ?></div>
                                    <?php if (has_post_thumbnail()) { ?>
                                        <div class="featured" style="background: url(<?php echo esc_url(get_the_post_thumbnail_url()); ?>)"></div>
                                    <?php } ?>
                                </a>
                                <div class="featured-content">
                                    <div class="post-date"><?php echo esc_html(get_the_date('d . m . Y')); ?></div>
                                    <a href="<?php the_permalink(); ?>" class="title"><?php echo esc_html(get_the_title()); ?></a>
                                    <a class="continue" href="<?php the_permalink(); ?>">
                                        <span><?php echo esc_html__('Continue Reading', 'oculis'); ?></span>
                                        <i class="essentials krucial-essential-light-07-arrow-right"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php }
	    $first_post_count++;
    endwhile; wp_reset_postdata();
endif; ?>
<div class="container">
    <div class="blog-page <?php echo krucial_sidebar_layout_blogpage(); ?>">
        <div class="content-area">
            <div class="masonry-grid">
                <?php

                $post_count = 1;
                if ( have_posts() ):
                    while ( have_posts() ): the_post();
                        if ($post_count > 1) { ?>
                            <div class="grid-item">
                                <div id="post-<?php the_ID() ?>" <?php post_class() ?>>
                                    <?php get_template_part( 'components/blog/blog-card' ); ?>
                                </div>
                            </div>
                        <?php }
                        $post_count++;
                    endwhile;
                endif;

                ?>
            </div>
            <?php

            echo paginate_links( array(
                'type'      => 'list',
                'prev_text' => '<i class="fa fa-angle-left"></i>',
                'next_text' => '<i class="fa fa-angle-right"></i>',
            ) );

            ?>
        </div>
        <?php krucial_get_sidebar(); ?>
    </div>
</div>
<?php } else {
	krucial_render_page_header(); ?>

    <div class="container">
        <div class="blog-page <?php echo krucial_sidebar_layout_blogpage(); ?>">
            <div class="content-area">
                <div class="masonry-grid">
                    <?php
                    if ( have_posts() ):
                        while ( have_posts() ): the_post(); ?>
                            <div class="grid-item">
                                <div id="post-<?php the_ID() ?>" <?php post_class() ?>>
                                    <?php get_template_part( 'components/blog/blog-card' ); ?>
                                </div>
                            </div>
                            <?php
                        endwhile; wp_reset_postdata();
                    endif;
                    ?>
                </div>
                <?php
                echo paginate_links( array(
                    'type'      => 'list',
                    'prev_text' => '<i class="fa fa-angle-left"></i>',
                    'next_text' => '<i class="fa fa-angle-right"></i>',
                ) );
                ?>
            </div>
	        <?php krucial_get_sidebar(); ?>
        </div>
    </div>
<?php }

get_footer(); ?>