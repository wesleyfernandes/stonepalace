<form method="GET" class="search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
    <input type="search" placeholder="<?php esc_attr__( 'Search...', 'oculis' ); ?>" value="<?php echo esc_attr( get_search_query() ); ?>" name="s" />
    <button class="button" type="submit"><i class="essentials krucial-search"></i></button>
</form>