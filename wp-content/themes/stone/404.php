<?php
/**
 * Template for 404 Errors
 */

get_header(); ?>

    <div class="notfound-page-header">
        <div class="container">
            <div class="hero">
                <h1 class="title animation_hidden animateFadeInUp delay-0 duration-25">
                    <span><i class="krucial-tools essentials"></i></span>
					<?php echo esc_html__('Page not found.', 'oculis'); ?>
                </h1>
                <div class="description animation_hidden animateFadeInUp delay-25 duration-25"><?php echo esc_html__('404 error, the page you are looking for does not exist. Try searching for something else...', 'oculis') ?></div>
                <div class="not-found-content">
                    <div class="nf-form">
						<?php get_search_form(); ?>
                    </div>
                    <div class="nf-text"><?php echo esc_html__('or', 'oculis'); ?></div>
                    <div class="nf-button">
                        <a class="button" href="javascript: history.go(-1)">
							<?php echo esc_html__('Previous Page', 'oculis'); ?>
                        </a>
                        <a class="button" href="<?php echo esc_url(site_url()); ?>">
							<?php echo esc_html__('Go Back Home', 'oculis'); ?>
                        </a>
                    </div>
                </div>
                <div class="large-text">
					<?php echo esc_html__('404', 'oculis'); ?>
                </div>
            </div>
        </div>
    </div>

<?php get_footer(); ?>