<?php
/**
 * Template for Services PostType
 */

get_header();

if (function_exists('get_field')) {
	if (get_field('remove_default_page_heading') !== true) {
		krucial_render_page_header();
	}
} else {
	krucial_render_page_header();
}

?>

    <div class="services-page">
        <div class="container">
            <?php

            if ( have_posts() ):
                while ( have_posts() ): the_post();
                    the_content();
                endwhile;
            endif;

            if ( comments_open() ) { ?>
                <div class="page-comments"><?php comments_template(); ?></div>
                <?php
            }

            ?>
        </div>
    </div>

<?php get_footer(); ?>