<?php
/**
 * Template for Single Posts
 */

get_header();

if ( have_posts() ):
	while ( have_posts() ): the_post(); ?>
        <div id="post-<?php the_ID() ?>" <?php post_class() ?>>
			<?php get_template_part( 'components/blog/blog-post' ); ?>
        </div>
	<?php endwhile;
endif;

get_footer(); ?>