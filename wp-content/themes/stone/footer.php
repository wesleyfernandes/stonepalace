</div><!-- #wrapper (header.php) -->

<?php global $krucial_options; ?>

<div class="footer-wrap">
    <?php if (is_active_sidebar('footer-col-1') || is_active_sidebar('footer-col-2') || is_active_sidebar('footer-col-3') || is_active_sidebar('footer-col-4')) { ?>
        <div class="footer">
            <div class="container">
			    <?php

			    if ( $krucial_options['footer_layout'] == '1' ) {
				    get_template_part( 'components/footer/footerstyle-1' );
			    } elseif ( $krucial_options['footer_layout'] == '2' ) {
				    get_template_part( 'components/footer/footerstyle-2' );
			    } elseif ( $krucial_options['footer_layout'] == '3' ) {
				    get_template_part( 'components/footer/footerstyle-3' );
			    } elseif ( $krucial_options['footer_layout'] == '5' ) {
				    get_template_part( 'components/footer/footerstyle-5' );
			    } elseif ( $krucial_options['footer_layout'] == '6' ) {
				    get_template_part( 'components/footer/footerstyle-6' );
			    } elseif ( $krucial_options['footer_layout'] == '7' ) {
				    get_template_part( 'components/footer/footerstyle-7' );
			    } elseif ( $krucial_options['footer_layout'] == '8' ) {
				    get_template_part( 'components/footer/footerstyle-8' );
			    } elseif ( $krucial_options['footer_layout'] == '9' ) {
				    get_template_part( 'components/footer/footerstyle-9' );
			    } elseif ( $krucial_options['footer_layout'] == '10' ) {
				    get_template_part( 'components/footer/footerstyle-10' );
			    } elseif ( $krucial_options['footer_layout'] == '11' ) {
				    get_template_part( 'components/footer/footerstyle-11' );
			    } else {
				    get_template_part( 'components/footer/footerstyle-4' );
			    }

			    ?>
            </div>
        </div>
	    <?php
    }

    switch ($krucial_options['copyright_layout']) {
        case 'style-2':
            get_template_part('components/copyright/copyright2');
            break;
        case 'style-3':
            get_template_part('components/copyright/copyright3');
            break;
        default:
            get_template_part('components/copyright/copyright1');
            break;
    }

    ?>
</div>
<?php wp_footer(); ?>
</body>
</html>