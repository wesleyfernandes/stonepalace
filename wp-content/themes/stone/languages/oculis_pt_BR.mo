��    '      T  5   �      `  	   a     k  	   x  	   �     �     �  	   �     �     �     �     �     �     	                    "     /     6     C     F     R     V     e     v     �     �     �     �     �     �     �  w   	     �     �     �     �  "   �  �  �     �     �     �     �     �     �     �          !     2     J     a  
   w     �     �     �     �     �     �     �     �     �     �     �     	  	   	     )	     6	     C	     a	     �	     �	  }   �	     6
     <
     A
     I
  #   M
        #      %                !                &                                                                                         $      	             '      "          
        Comments %1$s at %2$s 1 Comment 404 Error All Projects All Services All Staff All Testimonials Archives Sidebar BlogPage Sidebar BlogPost Sidebar Comments are closed. Disable Edit Enable Four Go Back Home Height Next Comment No No Comments One Page Not Found Previous Comment Previous Page Reply Share Shop Sidebar Showing all posts in  Sidebar for all blog posts. Sidebar for the archives. Sidebar for the main blog page. The page you are looking for does not exist. You can either head back to the previous page or go back to the home page. Three Two Width Yes Your comment is awaiting approval. Project-Id-Version: Oculis WordPress Theme
POT-Creation-Date: 2018-10-18 14:06+0800
PO-Revision-Date: 2019-04-08 15:14-0300
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.2.1
X-Poedit-Basepath: ..
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-KeywordsList: __;_e;esc_attr_e;esc_html__;esc_html_e;esc_attr__
Last-Translator: 
Language: pt_BR
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: node_modules
 Comentários %1$s até %2$s 1 Comentário Erro 404 Todos projetos Todos serviços Todos funcionários Todos testemunhos Sidebar arquivos Sidebar página do blog Sidebar posts do blog  Comentários fechados Desativado Editar Ativado Quatro Voltar a Home Altura Próximo comentário Não Não há comentários Um Essa página não existe Comentário anterior Página anterior Responder Compartilhar Sidebar loja Mostrar todos as postagens em Sidebar todos os posts do blog Sidebar para os arquivos Sidebar página principal blog A página que você está procurando não existe. Você pode voltar para a página anterior ou voltar para a página inicial. Três Dois Largura Sim Seu comentário aguarda aprovação 