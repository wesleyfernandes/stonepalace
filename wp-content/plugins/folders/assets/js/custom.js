var defaultFolderHtml;
var folderID = 0;
var fileAddUpdateStatus = "add";
var fileFolderID = 0;
var folderNameDynamic = "";
var totalFolders = -1;
var isKeyActive = 0;
var folderLimitation = 10;
var nonce = "";
var folderId = 0;
var fID = 0;
var folderCurrentURL = wcp_settings.page_url;
var activeRecordID = "";
var folderIDs = "";

var listFolderString = "<li class='grid-view' data-id='__folder_id__' id='folder___folder_id__'>" +
	"<div class='folder-item is-folder' data-id='__folder_id__'>" +
	"<a title='__folder_name__' id='folder_view___folder_id__'" +
	"class='folder-view __append_class__ has-new-folder'" +
	"data-id='__folder_id__'>" +
	"<span class='folder item-name'><span id='wcp_folder_text___folder_id__'" +
	"class='folder-title'>__folder_name__</span></span>" +
	"</a>" +
	"</div>" +
	"</li>";

jQuery(document).ready(function(){
	jQuery(document).on("click", ".form-cancel-btn", function(){
		jQuery(".folder-popup-form").hide();
	});
	jQuery(document).on("click", ".folder-popup-form", function (e) {
		jQuery(".folder-popup-form").hide();
	});
	jQuery(document).on("click", ".popup-form-content", function (e) {
		e.stopPropagation();
	});
	jQuery(document).on("click", "#save-folder-data", function(e){
		e.stopPropagation();

		folderNameDynamic = jQuery("#add-update-folder-name").val();

		if(jQuery.trim(folderNameDynamic) == "") {
			jQuery(".folder-form-errors").addClass("active");
			jQuery("#add-update-folder-name").focus();
		} else {
			jQuery("#save-folder-data").html('<span class="dashicons dashicons-update"></span>');
			jQuery("#add-update-folder").addClass("disabled");

			jQuery.ajax({
				url: wcp_settings.ajax_url,
				data: "parent_id=" + fileFolderID + "&type=" + wcp_settings.post_type + "&action=wcp_add_new_folder&nonce=" + wcp_settings.nonce + "&term_id=" + fileFolderID + "&order=" + folderOrder + "&name=" + folderNameDynamic,
				method: 'post',
				success: function (res) {
					result = jQuery.parseJSON(res);
					if (result.status == '1') {
						jQuery("#space_" + result.parent_id).append(result.term_data);
						jQuery("#wcp_folder_" + result.parent_id).addClass("active has-sub-tree");
						isKeyActive = parseInt(result.is_key_active);
						totalFolders = parseInt(result.folders);
						jQuery("#current-folder").text(totalFolders);
						if (totalFolders > folderLimitation) {
							folderLimitation = totalFolders;
						}
						jQuery("#total-folder").text(folderLimitation);
						checkForExpandCollapse();
						add_menu_to_list();
						jQuery(".folder-popup-form").hide();
						jQuery(".folder-popup-form").removeClass("disabled");
                        ajaxAnimation();
						if(jQuery("#media-attachment-taxonomy-filter").length) {
							resetMediaData(0)
						}
					} else {
						jQuery(".folder-popup-form").hide();
						jQuery(".folder-popup-form").removeClass("disabled");
						jQuery("#error-folder-popup-message").html(result.message);
						jQuery("#error-folder-popup").show();
					}
				}
			});
		}
	});
	jQuery(document).on("click", "#update-folder-data", function(e){
		e.stopPropagation();

		folderNameDynamic = jQuery("#update-folder-item-name").val();

		if(jQuery.trim(folderNameDynamic) == "") {
			jQuery(".folder-form-errors").addClass("active");
			jQuery("#update-folder-item-name").focus();
		} else {
			jQuery("#update-folder-data").html('<span class="dashicons dashicons-update"></span>');
			jQuery("#update-folder-item").addClass("disabled");

			nonce = jQuery.trim(jQuery("#wcp_folder_" + fileFolderID).data("rename"));
			parentID = jQuery("#wcp_folder_" + fileFolderID).closest("li.route").data("folder-id");
			if (parentID == undefined) {
				parentID = 0;
			}
			jQuery.ajax({
				url: wcp_settings.ajax_url,
				data: "parent_id=" + parentID + "&nonce=" + nonce + "&type=" + wcp_settings.post_type + "&action=wcp_update_folder&term_id=" + fileFolderID + "&name=" + folderNameDynamic,
				method: 'post',
				success: function (res) {
					result = jQuery.parseJSON(res);
					if (result.status == '1') {
						jQuery("#wcp_folder_" + result.id + " > h3 > .title-text").text(result.term_title);
						jQuery("#wcp_folder_" + result.id + " > h3").attr("title", result.term_title);
						add_menu_to_list();
						jQuery(".folder-popup-form").hide();
						jQuery(".folder-popup-form").removeClass("disabled");
                        ajaxAnimation();
						if(jQuery("#media-attachment-taxonomy-filter").length) {
							resetMediaData(0)
						}
					} else {
						jQuery(".folder-popup-form").hide();
						jQuery(".folder-popup-form").removeClass("disabled");
						jQuery("#error-folder-popup-message").html(result.message);
						jQuery("#error-folder-popup").show();
					}
				}
			});
		}
	});
	jQuery(document).on("click", "#remove-folder-item", function (e){
		e.stopPropagation();
		jQuery(".folder-popup-form").addClass("disabled");
		jQuery("#remove-folder-item").html('<span class="dashicons dashicons-update"></span>');
		nonce = jQuery.trim(jQuery("#wcp_folder_"+fileFolderID).data("delete"));
		jQuery.ajax({
			url: wcp_settings.ajax_url,
			data: "type=" + wcp_settings.post_type + "&action=wcp_remove_folder&term_id=" + fileFolderID+"&nonce="+nonce,
			method: 'post',
			success: function (res) {
				res = jQuery.parseJSON(res);
				if (res.status == '1') {
					jQuery("#wcp_folder_"+fileFolderID).remove();
					jQuery("#folder_"+fileFolderID).remove();
					isKeyActive = parseInt(res.is_key_active);
					totalFolders = parseInt(res.folders);
					jQuery("#current-folder").text(totalFolders);
					if(totalFolders > folderLimitation) {
						folderLimitation = totalFolders;
					}
					jQuery("#total-folder").text(folderLimitation);
					add_menu_to_list();
                    ajaxAnimation();
					jQuery(".folder-popup-form").hide();
					jQuery(".folder-popup-form").removeClass("disabled");
					resetMediaAndPosts();

					if(activeRecordID == fileFolderID) {
						jQuery(".header-posts").trigger("click");
					}
				} else {
					jQuery(".folder-popup-form").hide();
					jQuery(".folder-popup-form").removeClass("disabled");
					jQuery("#error-folder-popup-message").html(res.message);
					jQuery("#error-folder-popup").show();
				}
			}
		});
	});
});

function triggerInlineUpdate() {
    jQuery(".form-loader-count").css("width", "0");
	if(typeof inlineEditPost == "object") {
        jQuery("#the-list").on("click",".editinline",function(){jQuery(this).attr("aria-expanded","true"),inlineEditPost.edit(this)});
		jQuery(document).on("click", ".inline-edit-save .save", function(){
			var thisID = jQuery(this).closest("tr").attr("id");
			thisID = thisID.replace("edit-","");
			inlineEditPost.save(thisID);
		});
		jQuery(document).on("click", ".inline-edit-save .cancel", function(){
			var thisID = jQuery(this).closest("tr").attr("id");
			thisID = thisID.replace("edit-","");
			inlineEditPost.revert(thisID);
		});
	}
}

function ajaxAnimation() {
    jQuery(".folder-loader-ajax").addClass("active");
    jQuery(".folder-loader-ajax img").removeClass("active");
    jQuery(".folder-loader-ajax svg#successAnimation").addClass("active").addClass("animated");
    setTimeout(function(){
        jQuery(".folder-loader-ajax").removeClass("active");
        jQuery(".folder-loader-ajax img").addClass("active");
        jQuery(".folder-loader-ajax svg#successAnimation").removeClass("active").removeClass("animated");
    }, 2000);
}

function addFolder() {
	if(isKeyActive == 0 && totalFolders >= folderLimitation) {
		jQuery("#folder-limitation-message").html("You've reached the "+totalFolders+" folder limitation!");
		jQuery("#no-more-folder-credit").show();
		return false;
	}
	folderOrder = jQuery("#space_"+fileFolderID+" > li").length+1;
	ajaxURL = wcp_settings.ajax_url+"?parent_id=" + fileFolderID + "&type=" + wcp_settings.post_type + "&action=wcp_add_new_folder&nonce=" + wcp_settings.nonce + "&term_id=" + fileFolderID + "&order=" + folderOrder+"&name=";

	jQuery("#add-update-folder-title").text("Add Folder");
	jQuery("#save-folder-data").text("Submit");
	jQuery(".folder-form-errors").removeClass("active");
	jQuery("#add-update-folder-name").val("");
	jQuery("#add-update-folder").removeClass("disabled");
	jQuery("#add-update-folder").show();
	jQuery("#add-update-folder-name").focus();
}

function updateFolder() {
	folderName = jQuery.trim(jQuery("#wcp_folder_"+fileFolderID+" > h3 > .title-text").text());
	parentID = jQuery("#wcp_folder_"+fileFolderID).closest("li.route").data("folder-id");
	if(parentID == undefined) {
		parentID = 0;
	}

	jQuery("#update-folder-data").text("Submit");
	jQuery(".folder-form-errors").removeClass("active");
	jQuery("#update-folder-item-name").val(folderName);
	jQuery("#update-folder-item").removeClass("disabled");
	jQuery("#update-folder-item").show();
	jQuery("#update-folder-item-name").focus();
}

function removeFolderFromID() {
	jQuery(".folder-popup-form").hide();
	jQuery(".folder-popup-form").removeClass("disabled");
	jQuery("#remove-folder-item").text("Yes, Delete it!");
	jQuery("#confirm-remove-folder").show();
}

function resetMediaAndPosts() {
	if(folderIDs != "" && (jQuery("#custom-menu li.active-item").length > 0 || activeRecordID == "-1")) {
		if(jQuery("#media-attachment-taxonomy-filter").length) {
			folderIDs = folderIDs.split(",");
			for (var i = 0; i < folderIDs.length; i++) {
				if(folderIDs[i] != "") {
					jQuery(".attachments-browser li[data-id='"+folderIDs[i]+"']").remove();
				}
			}
		}
		folderIDs = "";
	}
	if(jQuery("#media-attachment-taxonomy-filter").length) {
		resetMediaData(0);
	} else {
		jQuery.ajax({
			url: wcp_settings.ajax_url,
			data: "type=" + wcp_settings.post_type + "&action=get_folders_default_list",
			method: 'post',
			success: function (res) {
				res = jQuery.parseJSON(res);
				jQuery("#custom-menu > ul#space_0").html(res.data);
				jQuery(".header-posts .total-count").text(res.total_items);
				jQuery(".un-categorised-items .total-count").text(res.empty_items);
			}
		});
		jQuery("#wpbody").load(folderCurrentURL + " #wpbody-content", false, function (res) {
			if (!jQuery(".tree-structure").length) {
				jQuery(".wp-header-end").before('<div class="tree-structure"><ul></ul><div class="clear clearfix"></div></div>');
			}
			add_active_item_to_list();
			// triggerInlineUpdate();
		});
	}
}

function add_active_item_to_list() {
	folderId = 0;
	if(jQuery(".active-item").length) {
		folderId = jQuery(".active-item").data("folder-id");

	}
	jQuery(".tree-structure ul").html("");
	jQuery("#space_"+folderId).children().each(function(){
		fID = jQuery(this).data("folder-id");
		fName = jQuery(this).find("h3.title:first .title-text").text()
		liHtml = listFolderString.replace(/__folder_id__/g,fID);
		liHtml = liHtml.replace(/__folder_name__/g,fName);
		selectedClass = jQuery(this).hasClass("is-high")?"is-high":"";
		liHtml = liHtml.replace(/__append_class__/g,selectedClass);
		jQuery(".tree-structure ul").append(liHtml);
	});
}

function add_menu_to_list() {
	folderId = 0;
	if(jQuery(".active-term").length) {
		folderId = jQuery(".active-term").data("folder-id");
	}
	jQuery(".tree-structure ul").html("");
	jQuery("#space_"+folderId).children().each(function(){
		fID = jQuery(this).data("folder-id");
		fName = jQuery(this).find("h3.title:first .title-text").text()
		liHtml = listFolderString.replace(/__folder_id__/g,fID);
		liHtml = liHtml.replace(/__folder_name__/g,fName);
		selectedClass = jQuery(this).hasClass("is-high")?"is-high":"";
		liHtml = liHtml.replace(/__append_class__/g,selectedClass);
		jQuery(".tree-structure ul").append(liHtml);
	});
}

jQuery(document).ready(function(){

	wcp_settings.folder_width = parseInt(wcp_settings.folder_width);

	if(wcp_settings.can_manage_folder == "0") {
		jQuery(".wcp-custom-form a:not(.pink)").addClass("button-disabled");
	}

	isKeyActive = parseInt(wcp_settings.is_key_active);
	totalFolders = parseInt(wcp_settings.folders);

	if(wcp_settings.post_type == "attachment") {
		jQuery(".wp-header-end").before('<div class="tree-structure"><ul></ul><div class="clear clearfix"></div></div>');

		add_menu_to_list();
	}

	calcWidth(jQuery('#title_0'));

	jQuery("#cancel-button").click(function(){
		jQuery(".wcp-form-data").hide();
	});


	jQuery(document).on("click", "h3.title", function(e) {
		e.stopPropagation();
		jQuery(".un-categorised-items").removeClass("active-item");
		jQuery(".header-posts a").removeClass("active-item");
		activeRecordID = jQuery(this).closest("li.route").data("folder-id");
		if(!jQuery("#media-attachment-taxonomy-filter").length) {
			folderCurrentURL = wcp_settings.page_url + jQuery(this).closest("li.route").data("slug");
			jQuery(".form-loader-count").css("width", "100%");
			jQuery("#wpbody").load(folderCurrentURL + " #wpbody-content", function () {
				if (!jQuery(".tree-structure").length) {
					jQuery(".wp-header-end").before('<div class="tree-structure"><ul></ul><div class="clear clearfix"></div></div>');
				}
				add_active_item_to_list();
				triggerInlineUpdate();
			});
		} else {
			var thisIndex = jQuery(this).closest("li.route").data("folder-id");
			jQuery("#media-attachment-taxonomy-filter").val(thisIndex);
			jQuery("#media-attachment-taxonomy-filter").trigger("change");
			if(jQuery(this).hasClass("is-new-item")) {
				thisSlug = jQuery(this).closest("li.route").data("slug");
				alert("23412");
			}
		}
	});


	jQuery(".tree-structure a").livequery(function(){
		jQuery(this).click(function(){
			fID = jQuery(this).data("id");
			jQuery("#title_"+fID).trigger("click");
		});
	});

	jQuery(".wcp-parent > span").click(function(e){
		activeRecordID = "";
		jQuery(".wcp-container .route").removeClass("active-item");
		if(!jQuery("#media-attachment-taxonomy-filter").length) {
			folderCurrentURL = wcp_settings.page_url;
            jQuery(".form-loader-count").css("width", "100%");
			jQuery("#wpbody").load(folderCurrentURL + " #wpbody-content", function () {
				if (!jQuery(".tree-structure").length) {
					jQuery(".wp-header-end").before('<div class="tree-structure"><ul></ul><div class="clear clearfix"></div></div>');
				}
				add_active_item_to_list();
				triggerInlineUpdate();
			});
		} else {
			jQuery("#media-attachment-taxonomy-filter").val("all");
			jQuery("#media-attachment-taxonomy-filter").trigger("change");
		}
	});
	jQuery("h3.title").livequery(function(){
		jQuery(this).droppable({
			accept: ".wcp-move-file, .wcp-move-multiple, .attachments-browser li.attachment",
			hoverClass: 'wcp-drop-hover',
			classes: {
				"ui-droppable-active": "ui-state-highlight"
			},
			drop: function( event, ui ) {
				folderID = jQuery(this).closest("li.route").data('folder-id');
				if ( ui.draggable.hasClass( 'wcp-move-multiple' ) ) {
					if(jQuery(".wp-list-table input:checked").length) {
						chkStr = "";
						jQuery(".wp-list-table input:checked").each(function(){
							chkStr += jQuery(this).val()+",";
						});
						nonce = jQuery.trim(jQuery("#wcp_folder_"+folderID).data("nonce"));
						jQuery.ajax({
							url: wcp_settings.ajax_url,
							data: "post_ids=" + chkStr + "&type=" + wcp_settings.post_type + "&action=wcp_change_multiple_post_folder&folder_id=" + folderID+"&nonce="+nonce+"&status="+wcp_settings.taxonomy_status+"&taxonomy="+activeRecordID,
							method: 'post',
							success: function (res) {
								res = jQuery.parseJSON(res);
								if(res.status == "1") {
									// window.location.reload();
									resetMediaAndPosts();
                                    ajaxAnimation();
								} else {
									jQuery(".folder-popup-form").hide();
									jQuery(".folder-popup-form").removeClass("disabled");
									jQuery("#error-folder-popup-message").html(res.message);
									jQuery("#error-folder-popup").show()
								}
							}
						});
					}
				} else if( ui.draggable.hasClass( 'wcp-move-file' ) ){
					postID = ui.draggable[0].attributes['data-id'].nodeValue;
					nonce = jQuery.trim(jQuery("#wcp_folder_"+folderID).data("nonce"));
					jQuery.ajax({
						url: wcp_settings.ajax_url,
						data: "post_id=" + postID + "&type=" + wcp_settings.post_type + "&action=wcp_change_post_folder&folder_id=" + folderID+"&nonce="+nonce+"&status="+wcp_settings.taxonomy_status+"&taxonomy="+activeRecordID,
						method: 'post',
						success: function (res) {
							res = jQuery.parseJSON(res);
							if(res.status == "1") {
								// window.location.reload();
								resetMediaAndPosts();
                                ajaxAnimation();
							} else {
								jQuery(".folder-popup-form").hide();
								jQuery(".folder-popup-form").removeClass("disabled");
								jQuery("#error-folder-popup-message").html(res.message);
								jQuery("#error-folder-popup").show()
							}
						}
					});
				} else if (ui.draggable.hasClass('attachment')) {
					chkStr = ui.draggable[0].attributes['data-id'].nodeValue;
					nonce = jQuery.trim(jQuery("#wcp_folder_" + folderID).data("nonce"));
					if (jQuery(".attachments-browser li.attachment.selected").length > 1) {
						chkStr = "";
						jQuery(".attachments-browser li.attachment.selected").each(function () {
							chkStr += jQuery(this).data("id") + ",";
						});
					}
					folderIDs = chkStr;
					jQuery.ajax({
						url: wcp_settings.ajax_url,
						data: "post_ids=" + chkStr + "&type=" + wcp_settings.post_type + "&action=wcp_change_multiple_post_folder&folder_id=" + folderID + "&nonce="+nonce+"&status="+wcp_settings.taxonomy_status+"&taxonomy="+activeRecordID,
						method: 'post',
						success: function (res) {
							// window.location.reload();
							resetMediaAndPosts();
                            ajaxAnimation();
						}
					});
				}
			}
		});
	});

	jQuery(".attachments-browser li.attachment").livequery(function () {
		jQuery(this).draggable({
			revert: "invalid",
			containment: "document",
			helper: function (event, ui) {
				jQuery(".selected-items").remove();
				selectedItems = jQuery(".attachments-browser li.attachment.selected").length;
				selectedItems = (selectedItems == 0 || selectedItems == 1) ? "1 Item" : selectedItems + " Items";
				return jQuery("<div class='selected-items'><span class='total-post-count'>" + selectedItems + " Selected</span></div>");
			},
			start: function( event, ui){
				jQuery("body").addClass("no-hover-css");
			},
			cursor: "move",
			cursorAt: {
				left: 0,
				top: 0
			},
			stop: function( event, ui ) {
				jQuery(".selected-items").remove();
				jQuery("body").removeClass("no-hover-css");
			}
		});
	});

	jQuery(".media-button").livequery(function () {
		jQuery(this).click(function () {
			if (jQuery(".delete-selected-button").hasClass("hidden")) {
				//jQuery(".attachments-browser li.attachment").draggable("disable");
			} else {
				// jQuery(".attachments-browser li.attachment").draggable("enable");
			}
		});
	});

	jQuery(".header-posts").click(function(){
		activeRecordID = "";
		jQuery(".wcp-container .route").removeClass("active-item");
		jQuery(".un-categorised-items").removeClass("active-item");
		jQuery(".header-posts a").addClass("active-item");
		if(!jQuery("#media-attachment-taxonomy-filter").length) {
			folderCurrentURL = wcp_settings.page_url;
			 jQuery(".form-loader-count").css("width", "100%");
			jQuery("#wpbody").load(folderCurrentURL + " #wpbody-content", function () {

				if (!jQuery(".tree-structure").length) {
					jQuery(".wp-header-end").before('<div class="tree-structure"><ul></ul><div class="clear clearfix"></div></div>');
				}
				add_active_item_to_list();
				triggerInlineUpdate();
			});
		} else {
			activeRecordID = "";
			jQuery("#media-attachment-taxonomy-filter").val("all");
			jQuery("#media-attachment-taxonomy-filter").trigger("change");
		}
	});

	jQuery(".un-categorised-items").click(function(){
		activeRecordID = "-1";
		jQuery(".wcp-container .route").removeClass("active-item");
		jQuery(".header-posts a").removeClass("active-item");
		jQuery(".un-categorised-items").addClass("active-item");
		if(!jQuery("#media-attachment-taxonomy-filter").length) {
			folderCurrentURL = wcp_settings.page_url+"-1";
			 jQuery(".form-loader-count").css("width", "100%");
			jQuery("#wpbody").load(folderCurrentURL + " #wpbody-content", function () {

				if (!jQuery(".tree-structure").length) {
					jQuery(".wp-header-end").before('<div class="tree-structure"><ul></ul><div class="clear clearfix"></div></div>');
				}
				add_active_item_to_list();
				triggerInlineUpdate();
			});
		} else {
			jQuery("#media-attachment-taxonomy-filter").val("unassigned");
			jQuery("#media-attachment-taxonomy-filter").trigger("change");
		}
	});

	jQuery(".un-categorised-items").livequery(function () {
		jQuery(this).droppable({
			accept: ".wcp-move-file, .wcp-move-multiple, .attachments-browser li.attachment",
			hoverClass: 'wcp-hover-list',
			classes: {
				"ui-droppable-active": "ui-state-highlight"
			},
			drop: function (event, ui) {
				folderID = -1;
				nonce = wcp_settings.nonce;
				if (ui.draggable.hasClass('wcp-move-multiple')) {
					if (jQuery(".wp-list-table input:checked").length) {
						chkStr = "";
						jQuery(".wp-list-table input:checked").each(function () {
							chkStr += jQuery(this).val() + ",";
						});
						jQuery.ajax({
							url: wcp_settings.ajax_url,
							data: "post_id=" + chkStr + "&type=" + wcp_settings.post_type + "&action=wcp_remove_post_folder&folder_id=" + folderID + "&nonce=" + nonce+"&status="+wcp_settings.taxonomy_status+"&taxonomy="+activeRecordID,
							method: 'post',
							success: function (res) {
								//window.location.reload();
								resetMediaAndPosts();
                                ajaxAnimation();
							}
						});
					}
				} else if (ui.draggable.hasClass('wcp-move-file')) {
					postID = ui.draggable[0].attributes['data-id'].nodeValue;
					jQuery.ajax({
						url: wcp_settings.ajax_url,
						data: "post_id=" + postID + "&type=" + wcp_settings.post_type + "&action=wcp_remove_post_folder&folder_id=" + folderID + "&nonce=" + nonce+"&status="+wcp_settings.taxonomy_status+"&taxonomy="+activeRecordID,
						method: 'post',
						success: function (res) {
							//window.location.reload();
							resetMediaAndPosts();
                            ajaxAnimation();
						}
					});
				} else if (ui.draggable.hasClass('attachment')) {
					chkStr = ui.draggable[0].attributes['data-id'].nodeValue;
					if (jQuery(".attachments-browser li.attachment.selected").length > 1) {
						chkStr = "";
						jQuery(".attachments-browser li.attachment.selected").each(function () {
							chkStr += jQuery(this).data("id") + ",";
						});
					}
					folderIDs = chkStr;
					jQuery.ajax({
						url: wcp_settings.ajax_url,
						data: "post_id=" + chkStr + "&type=" + wcp_settings.post_type + "&action=wcp_remove_post_folder&folder_id=" + folderID + "&nonce=" + nonce+"&status="+wcp_settings.taxonomy_status+"&taxonomy="+activeRecordID,
						method: 'post',
						success: function (res) {
							// window.location.reload();
							resetMediaAndPosts();
                            ajaxAnimation();
						}
					});
				}
			}
		});
	});


	jQuery(".wcp-hide-show-buttons .toggle-buttons").click(function(){
		var folderStatus = "show";
		if(jQuery(this).hasClass("hide-folders")) {
			folderStatus = "hide";
		}
		jQuery(".wcp-hide-show-buttons .toggle-buttons").toggleClass("active");
		nonce = wcp_settings.nonce;
		if(folderStatus == "show") {
			jQuery("#wcp-content").addClass("no-transition");
			jQuery("#wcp-content").removeClass("hide-folders-area");
			if(wcp_settings.isRTL == "1") {
				jQuery("#wpcontent").css("padding-right", (wcp_settings.folder_width + 20) + "px");
				jQuery("#wpcontent").css("padding-left", "0px");
			} else {
				jQuery("#wpcontent").css("padding-left", (wcp_settings.folder_width + 20) + "px");
			}
			setTimeout(function(){
				jQuery("#wcp-content").removeClass("no-transition");
			}, 250);
		} else {
			jQuery("#wcp-content").addClass("hide-folders-area");
			if(wcp_settings.isRTL == "1") {
				jQuery("#wpcontent").css("padding-right", "20px");
				jQuery("#wpcontent").css("padding-left", "0px");
			} else {
				jQuery("#wpcontent").css("padding-left", "20px");
			}
		}

		jQuery.ajax({
			url: wcp_settings.ajax_url,
			data: "type=" + wcp_settings.post_type + "&action=wcp_change_folder_display_status&status=" + folderStatus +"&nonce="+nonce,
			method: 'post',
			success: function (res) {
				setStickyHeaderForMedia();
			}
		});
	});

	//if(wcp_settings.can_manage_folder == "1") {
	jQuery(".tree-structure .folder-item").livequery(function () {
		jQuery(this).droppable({
			accept: ".wcp-move-file, .wcp-move-multiple, .attachments-browser li.attachment",
			hoverClass: 'wcp-drop-hover-list',
			classes: {
				"ui-droppable-active": "ui-state-highlight"
			},
			drop: function (event, ui) {
				folderID = jQuery(this).data('id');
				if (ui.draggable.hasClass('wcp-move-multiple')) {
					nonce = jQuery.trim(jQuery("#wcp_folder_" + folderID).data("nonce"));
					if (jQuery(".wp-list-table input:checked").length) {
						chkStr = "";
						jQuery(".wp-list-table input:checked").each(function () {
							chkStr += jQuery(this).val() + ",";
						});
						jQuery.ajax({
							url: wcp_settings.ajax_url,
							data: "post_ids=" + chkStr + "&type=" + wcp_settings.post_type + "&action=wcp_change_multiple_post_folder&folder_id=" + folderID + "&nonce=" + nonce + "&status="+wcp_settings.taxonomy_status+"&taxonomy="+activeRecordID,
							method: 'post',
							success: function (res) {
								//window.location.reload();
								resetMediaAndPosts();
                                ajaxAnimation();
							}
						});
					}
				} else if (ui.draggable.hasClass('wcp-move-file')) {
					postID = ui.draggable[0].attributes['data-id'].nodeValue;
					nonce = jQuery.trim(jQuery("#wcp_folder_" + folderID).data("nonce"));
					jQuery.ajax({
						url: wcp_settings.ajax_url,
						data: "post_id=" + postID + "&type=" + wcp_settings.post_type + "&action=wcp_change_post_folder&folder_id=" + folderID + "&nonce=" + nonce +"&status="+wcp_settings.taxonomy_status+"&taxonomy="+activeRecordID,
						method: 'post',
						success: function (res) {
							//window.location.reload();
							resetMediaAndPosts();
                            ajaxAnimation();
						}
					});
				} else if (ui.draggable.hasClass('attachment')) {
					chkStr = ui.draggable[0].attributes['data-id'].nodeValue;
					nonce = jQuery.trim(jQuery("#wcp_folder_" + folderID).data("nonce"));
					if (jQuery(".attachments-browser li.attachment.selected").length > 1) {
						chkStr = "";
						jQuery(".attachments-browser li.attachment.selected").each(function () {
							chkStr += jQuery(this).data("id") + ",";
						});
					}
					jQuery.ajax({
						url: wcp_settings.ajax_url,
						data: "post_ids=" + chkStr + "&type=" + wcp_settings.post_type + "&action=wcp_change_multiple_post_folder&folder_id=" + folderID + "&nonce=" + nonce +"&status="+wcp_settings.taxonomy_status+"&taxonomy="+activeRecordID,
						method: 'post',
						success: function (res) {
							//window.location.reload();
							resetMediaAndPosts();
                            ajaxAnimation();
						}
					});
				}
			}
		});
	});
	//}

	jQuery("#expand-collapse-list").click(function(e){
		e.stopPropagation();
		statusType = 0;
		if(jQuery(this).hasClass("all-open")) {
			jQuery(this).removeClass("all-open");
			jQuery(".has-sub-tree").removeClass("active");
			statusType = 0;
		} else {
			jQuery(this).addClass("all-open");
			statusType = 1;
			jQuery(".has-sub-tree").addClass("active");
		}
		folderIDs = "";
		jQuery(".has-sub-tree").each(function(){
			folderIDs += jQuery(this).data("folder-id")+",";
		});
		if(folderIDs != "") {
			jQuery(".form-loader-count").css("width","100%");
			nonce = wcp_settings.nonce;
			jQuery.ajax({
				url: wcp_settings.ajax_url,
				data: "type=" + wcp_settings.post_type + "&action=wcp_change_all_status&status=" + statusType + "&folders="+folderIDs+"&nonce="+nonce,
				method: 'post',
				success: function (res) {
					jQuery(".form-loader-count").css("width","0");
					add_menu_to_list();
					res = jQuery.parseJSON(res);
					if(res.status == "0") {
						jQuery(".folder-popup-form").hide();
						jQuery(".folder-popup-form").removeClass("disabled");
						jQuery("#error-folder-popup-message").html(res.message);
						jQuery("#error-folder-popup").show();
						window.location.reload(true);
					}
				}
			});
		}
	});

	resizeDirection = (wcp_settings.isRTL == "1" || wcp_settings.isRTL == 1)?"w":"e";
	jQuery(".wcp-content").resizable( {
		resizeHeight:   false,
		handles:        resizeDirection,
		minWidth:       305,
		maxWidth: 		500,
		resize: function( e, ui ) {
			if(wcp_settings.isRTL == "1") {
				jQuery("#wpcontent").css("padding-right", (ui.size.width + 20) + "px");
				jQuery("#wpcontent").css("padding-left", "0px");
			} else {
				jQuery("#wpcontent").css("padding-left", (ui.size.width + 20) + "px");
			}
			newWidth = ui.size.width - 40;
			cssString = "";
			classString = "";
			for(i=0; i<=15; i++) {
				classString += " .space > .route >";
				currentWidth = newWidth - (13+(20*i));
				cssString += "#custom-menu > "+classString+" .title { width: "+currentWidth+"px !important; } ";
				setStickyHeaderForMedia();
			}
			jQuery("#wcp-custom-style").html("<style>"+cssString+"</style>");
		},
		stop: function( e, ui ) {
			nonce = wcp_settings.nonce;
			wcp_settings.folder_width = ui.size.width;
			jQuery.ajax({
				url: wcp_settings.ajax_url,
				data: "type=" + wcp_settings.post_type + "&action=wcp_change_post_width&width=" + ui.size.width+"&nonce="+nonce,
				method: 'post',
				success: function (res) {
					setStickyHeaderForMedia();
				}
			});
		}
	});

	jQuery(".wcp-move-file").livequery(function(){
		jQuery(this).draggable({
			revert: "invalid",
			containment: "document",
			helper: "clone",
			cursor: "move",
			start: function( event, ui){
				jQuery(this).closest("td").addClass("wcp-draggable");
				jQuery("body").addClass("no-hover-css");
			},
			stop: function( event, ui ) {
				jQuery(this).closest("td").removeClass("wcp-draggable");
				jQuery("body").removeClass("no-hover-css");
			}
		});
	});

	jQuery(".wcp-move-multiple").livequery(function(){
		jQuery(this).draggable({
			revert: "invalid",
			containment: "document",
			helper: function (event, ui) {
				jQuery(".selected-items").remove();
				selectedItems = jQuery("#the-list th input:checked").length;
				if(selectedItems > 0) {
					selectedItems = (selectedItems == 0 || selectedItems == 1) ? "1 Item" : selectedItems + " Items";
					return jQuery("<div class='selected-items'><span class='total-post-count'>" + selectedItems + " Selected</span></div>");
				} else {
					return  jQuery("<div class='selected-items'><span class='total-post-count'>Select Items to move</span></div>");
				}
			},
			start: function( event, ui){
				jQuery("body").addClass("no-hover-css");
			},
			cursor: "move",
			cursorAt: {
				left: 0,
				top: 0
			},
			stop: function( event, ui ) {
				jQuery(".selected-items").remove();
				jQuery("body").removeClass("no-hover-css");
			}
		});
	});

	jQuery("h3.title").livequery(function(){
		jQuery(this).on("contextmenu",function(e) {
			e.preventDefault();
			if(wcp_settings.can_manage_folder == 0) {
				return;
			}
			isHigh = jQuery(this).closest("li.route").hasClass("is-high");
			jQuery(".dynamic-menu").remove();
			jQuery(".active-menu").removeClass("active-menu");
			menuHtml = "<div class='dynamic-menu'><ul>" +
				"<li class='new-folder'><a href='javascript:;'><span class='folder-icon-create_new_folder'></span> New Folder</a></li>" +
				"<li class='rename-folder'><a href='javascript:;'><span class='folder-icon-border_color'><span class='path1'></span><span class='path2'></span></span> Rename</a></li>" +
				"<li class='mark-folder'><a href='javascript:;'><span class='folder-icon-star_rate'></span>" + ((isHigh) ? " Remove Star" : "Add a Star") + "</a></li>" +
				"<li class='remove-folder'><a href='javascript:;'><span class='folder-icon-delete'></span> Delete</a></li>" +
				"</ul></div>";
			jQuery(this).after(menuHtml);
			jQuery(this).parents("li.route").addClass("active-menu");
			return false;
		});
	});

	jQuery("body").click(function(){
		jQuery(".dynamic-menu").remove();
		jQuery(".active-menu").removeClass("active-menu");
	});

	jQuery(".dynamic-menu").livequery(function(){
		jQuery(this).click(function(e){
			e.stopPropagation();
		});
	});

	jQuery(".rename-folder").livequery(function(){
		jQuery(this).click(function(e){
			e.stopPropagation();
			fileFolderID = jQuery(this).closest("li.route").data("folder-id");
			updateFolder();
			add_menu_to_list();
		});
	});

	jQuery(".mark-folder").livequery(function(){
		jQuery(this).click(function(e){
			e.stopPropagation();
			folderID = jQuery(this).closest("li.route").data("folder-id");
			nonce = jQuery.trim(jQuery("#wcp_folder_"+folderID).data("star"));
			jQuery(".form-loader-count").css("width","100%");
			jQuery(".dynamic-menu").remove();
			jQuery(".active-menu").removeClass("active-menu");
			jQuery.ajax({
				url: wcp_settings.ajax_url,
				data: "term_id=" + folderID + "&type=" + wcp_settings.post_type + "&action=wcp_mark_un_mark_folder&nonce="+nonce,
				method: 'post',
				cache: false,
				success: function (res) {
					res = jQuery.parseJSON(res);
					jQuery(".form-loader-count").css("width","0%");
					if (res.status == '1') {
						if(res.marked == '1') {
							jQuery("#wcp_folder_"+res.id).addClass("is-high");
						} else {
							jQuery("#wcp_folder_"+res.id).removeClass("is-high");
						}
						add_menu_to_list();
                        ajaxAnimation();
					} else {
						jQuery(".folder-popup-form").hide();
						jQuery(".folder-popup-form").removeClass("disabled");
						jQuery("#error-folder-popup-message").html(res.message);
						jQuery("#error-folder-popup").show();
					}
				}
			});
		});
	});

	/* Add new folder */
	jQuery(".new-folder").livequery(function(){
		jQuery(this).click(function(e) {
			e.stopPropagation();
			jQuery(".active-menu").removeClass("active-menu");
			fileFolderID = jQuery(this).closest("li.route").data("folder-id");
			jQuery(".dynamic-menu").remove();
			jQuery(".active-menu").removeClass("active-menu");
			addFolder();
			add_menu_to_list();
		});
	});

	jQuery(".cancel-button").livequery(function(){
		jQuery(this).click(function(e){
			e.stopPropagation();
			jQuery(".form-li").remove();
		});
	});



	jQuery("#add-new-folder").livequery(function(){
		jQuery(this).click(function() {
			if(jQuery("#custom-menu li.active-item").length) {
				fileFolderID = jQuery("#custom-menu li.active-item").data("folder-id");
			} else {
				fileFolderID = 0;
			}
			addFolder();
			add_menu_to_list();
		});
	});

	jQuery("#inline-update").click(function(){
		if(jQuery("#custom-menu li.active-item").length) {
			fileFolderID = jQuery("#custom-menu li.active-item").data("folder-id");
			updateFolder();
			add_menu_to_list();
		}
	});

	jQuery("#inline-remove").click(function(){
		if(jQuery("#custom-menu li.active-item").length) {
			fileFolderID = jQuery("#custom-menu li.active-item").data("folder-id");
			removeFolderFromID()
			jQuery(".dynamic-menu").remove();
			jQuery(".active-menu").removeClass("active-menu");
		}
	});

	if(wcp_settings.can_manage_folder == "1") {
		jQuery('.space').livequery(function () {
			jQuery(this).sortable({
				placeholder: "ui-state-highlight",
				connectWith: '.space',
				tolerance: 'intersect',
				over: function (event, ui) {

				},
				update: function (event, ui) {
					thisId = ui.item.context.attributes['data-folder-id'].nodeValue;
					orderString = "";
					jQuery(this).children().each(function () {
						if (jQuery(this).hasClass("route")) {
							orderString += jQuery(this).data("folder-id") + ",";
						}
					});
					if (orderString != "") {
						jQuery(".form-loader-count").css("width", "100%");
						jQuery.ajax({
							url: wcp_settings.ajax_url,
							data: "term_ids=" + orderString + "&action=wcp_save_folder_order&type=" + wcp_settings.post_type + "&nonce=" + wcp_settings.nonce,
							method: 'post',
							success: function (res) {
								res = jQuery.parseJSON(res);
								if (res.status == '1') {
									jQuery("#wcp_folder_parent").html(res.options);
									jQuery(".form-loader-count").css("width", "0");
									add_menu_to_list();
									resetMediaAndPosts();
                                    ajaxAnimation();
								} else {
									jQuery(".folder-popup-form").hide();
									jQuery(".folder-popup-form").removeClass("disabled");
									jQuery("#error-folder-popup-message").html(res.message);
									jQuery("#error-folder-popup").show();
									window.location.reload(true);
								}
							}
						});
					}
				},
				receive: function (event, ui) {
					calcWidth(jQuery(this).siblings('.title'));
					check_for_sub_menu();
					jQuery(this).closest("li.route").addClass("active");
					jQuery(this).closest("li.route").find("ul.ui-sortable:first-child > li").slideDown();
					parentId = jQuery(this).closest("li.route").data("folder-id");
					thisId = ui.item.context.attributes['data-folder-id'].nodeValue;
					if(parentId == undefined) {
						parentId = 0;
					}
					orderString = "";
					if(jQuery("#wcp_folder_"+parentId+" .ui-sortable li").length) {
						jQuery("#wcp_folder_"+parentId+" .ui-sortable li").each(function(){
							orderString += jQuery(this).data("folder-id")+",";
						});
					} else if(parentId == 0) {
						jQuery("#custom-menu > ul.space > li").each(function(){
							orderString += jQuery(this).data("folder-id")+",";
						});
					}
					jQuery(".form-loader-count").css("width","100%");
					nonce = jQuery.trim(jQuery("#wcp_folder_"+thisId).data("nonce"));
					jQuery.ajax({
						url: wcp_settings.ajax_url,
						data: "term_id=" + thisId + "&action=wcp_update_parent_information&parent_id=" + parentId+"&type=" + wcp_settings.post_type+"&nonce="+nonce,
						method: 'post',
						success: function (res) {
							jQuery(".form-loader-count").css("width","0%");
							res = jQuery.parseJSON(res);
							if(res.status == 0) {
								jQuery(".folder-popup-form").hide();
								jQuery(".folder-popup-form").removeClass("disabled");
								jQuery("#error-folder-popup-message").html(res.message);
								jQuery("#error-folder-popup").show();
							} else {
								add_menu_to_list();
                                ajaxAnimation();
							}
						}
					});
				}
			});
			jQuery(this).disableSelection();
		});
	}
	jQuery(".update-inline-record").livequery(function(){
		jQuery(this).click(function(e){
			e.stopPropagation();
			isHigh = jQuery(this).closest("li.route").hasClass("is-high");
			jQuery(".dynamic-menu").remove();
			jQuery(".active-menu").removeClass("active-menu");
			menuHtml = "<div class='dynamic-menu'><ul>" +
				"<li class='new-folder'><a href='javascript:;'><span class='folder-icon-create_new_folder'></span> New Folder</a></li>" +
				"<li class='rename-folder'><a href='javascript:;'><span class='folder-icon-border_color'><span class='path1'></span><span class='path2'></span></span> Rename</a></li>" +
				"<li class='mark-folder'><a href='javascript:;'><span class='folder-icon-star_rate'></span>" + ((isHigh) ? " Remove Star" : "Add a Star") + "</a></li>" +
				"<li class='remove-folder'><a href='javascript:;'><span class='folder-icon-delete'></span> Delete</a></li>" +
				"</ul></div>";
			jQuery(this).closest("h3.title").after(menuHtml);
			jQuery(this).parents("li.route").addClass("active-menu");
		});
	});
	//check_for_sub_menu();
	//jQuery(".has-sub-tree:first").addClass("active");
	jQuery(".nav-icon").livequery(function(){
		jQuery(this).click(function(){
			folderID = jQuery(this).closest("li.route").data("folder-id");
			if(jQuery("#wcp_folder_"+folderID).hasClass("active")) {
				folderStatus = 0;
			} else {
				folderStatus = 1;
			}
			jQuery(".form-loader-count").css("width","100%");
			nonce = jQuery.trim(jQuery("#wcp_folder_"+folderID).data("nonce"));
			checkForExpandCollapse();
			jQuery.ajax({
				url: wcp_settings.ajax_url,
				data: "is_active=" + folderStatus + "&action=save_wcp_folder_state&term_id=" + folderID+"&nonce="+nonce,
				method: 'post',
				success: function (res) {
					jQuery(".form-loader-count").css("width","0");
					res = jQuery.parseJSON(res);
					if(res.status == "0") {
						jQuery(".folder-popup-form").hide();
						jQuery(".folder-popup-form").removeClass("disabled");
						jQuery("#error-folder-popup-message").html(res.message);
						jQuery("#error-folder-popup").show();
					} else {
						if(jQuery("#wcp_folder_"+folderID).hasClass("active")) {
							jQuery("#wcp_folder_"+folderID).removeClass("active");
							jQuery("#wcp_folder_"+folderID).find("ul.ui-sortable:first-child > li").slideUp();
							folderStatus = 0;
						} else {
							jQuery("#wcp_folder_"+folderID).addClass("active");
							jQuery("#wcp_folder_"+folderID).find("ul.ui-sortable:first-child > li").slideDown();
							folderStatus = 1;
						}
						add_menu_to_list();
                        ajaxAnimation();
					}
				}
			});
		});
	});
	jQuery("#custom-menu .ui-icon, #custom-menu h3").livequery(function(){
		jQuery(this).click(function(){
			jQuery("#custom-menu .active-item").removeClass("active-item");
			jQuery(this).closest(".route").addClass("active-item");
			add_menu_to_list();
		});
	});
	jQuery(".remove-folder").livequery(function(){
		jQuery(this).click(function() {
			folderID = jQuery(this).closest("li.route").data("folder-id");
			fileFolderID = folderID;
			removeFolderFromID();
			jQuery(".dynamic-menu").remove();
			jQuery(".active-menu").removeClass("active-menu");
		});
	});
	jQuery(".wcp-parent .fa-caret-right").livequery(function(){
		jQuery(this).click(function() {
			autoStatus = 1;
			if (jQuery(this).closest(".wcp-parent").hasClass("active")) {
				jQuery(this).closest(".wcp-parent").removeClass("active");
				jQuery("#custom-menu").removeClass("active");
				autoStatus = 0;
			} else {
				jQuery(this).closest(".wcp-parent").addClass("active");
				jQuery("#custom-menu").addClass("active");
			}
			jQuery(".form-loader-count").css("width","100%");
			add_menu_to_list();
			jQuery.ajax({
				url: wcp_settings.ajax_url,
				data: "type=" + wcp_settings.post_type + "&action=wcp_save_parent_data&is_active=" + autoStatus+"&nonce="+wcp_settings.nonce,
				method: 'post',
				success: function (res) {
					jQuery(".form-loader-count").css("width","0%");
					res = jQuery.parseJSON(res);
					if (res.status == '1') {
						jQuery(".folder-popup-form").hide();
						jQuery(".folder-popup-form").removeClass("disabled");
						jQuery("#error-folder-popup-message").html(res.message);
						jQuery("#error-folder-popup").show();
					}
				}
			});
		});
	});

	checkForExpandCollapse();
})

function checkForExpandCollapse() {
	add_menu_to_list();
	currentStatus = true;
	if((jQuery("#custom-menu .has-sub-tree").length == jQuery("#custom-menu .has-sub-tree.active").length) && jQuery("#custom-menu .has-sub-tree").length) {
		jQuery("#expand-collapse-list").addClass("all-open");
	} else {
		jQuery("#expand-collapse-list").removeClass("all-open");
	}
}

function check_for_sub_menu() {
	jQuery("#custom-menu li.route").removeClass("has-sub-tree");
	jQuery("#custom-menu li.route").each(function(){
		if(jQuery(this).find("ul.ui-sortable li").length) {
			jQuery(this).addClass("has-sub-tree");
			if(jQuery(this).find("ul.ui-sortable:first").is(":hidden")) {
				jQuery(this).removeClass("is-hidden");
			} else {
				jQuery(this).addClass("is-hidden")
			}
		} else {
			jQuery(this).removeClass("active");
		}
	});
}

//recursively calculate the Width all titles
function calcWidth(obj){
	var titles =
		jQuery(obj).siblings('.space').children('.route').children('.title');
	jQuery(titles).each(function(index, element){
		var pTitleWidth = parseInt(jQuery(obj).css('width'));
		var leftOffset = parseInt(jQuery(obj).siblings('.space').css('margin-left'));
		var newWidth = pTitleWidth - leftOffset;
		if (jQuery(obj).attr('id') == 'title_0'){
			newWidth = newWidth - 10;
		}
		jQuery(element).css({
			'width': newWidth
		});
		calcWidth(element);
	});

}

/* code for sticky menu for media screen*/

if(wcp_settings.post_type == "attachment") {
	jQuery(window).load(function() {
		jQuery("button.button.media-button.select-mode-toggle-button").after("<button class='button organize-button'>Bulk Organize</button>");
		jQuery(".media-toolbar-secondary").append("<span class='media-info-message'>Drag and drop your media files to the relevant folders</span>");
		if(jQuery(".wcp-custom-form").length) {
			if (wp.Uploader !== undefined) {
				wp.Uploader.queue.on('reset', function () {
					resetMediaData(1);
				});
			}
			jQuery(document).ajaxComplete(function(ev, jqXHR, settings) {
				var actionName = settings.data;
				if (typeof actionName != "undefined") {
					if (actionName.length && actionName.indexOf("action=delete-post&id=") == 0) {
						resetMediaData(0);
					}
				}
			});
		}
		setTimeout(function(){
			docReferrar = document.referrer;
			if(docReferrar.indexOf("wp-admin/upload.php") != -1) {
				mediaMode = getCookie("media-select-mode");
				if (mediaMode == "on") {
					jQuery("button.button.media-button.select-mode-toggle-button").trigger("click");
					jQuery(".attachments-browser li.attachment").draggable("enable");

					if (jQuery(".media-frame").hasClass("mode-select")) {
						jQuery(".media-info-message").addClass("active");
					} else {
						jQuery(".media-info-message").removeClass("active");
					}
				}
			} else {
				eraseCookie("media-select-mode");
			}

			if(jQuery("#media-attachment-taxonomy-filter").length) {
				resetMediaData(0);
			}
		}, 1000);
	});

	function resetMediaData(loadData) {
		jQuery.ajax({
			url: wcp_settings.ajax_url,
			data: "type=" + wcp_settings.post_type + "&action=wcp_get_default_list&active_id="+activeRecordID,
			method: 'post',
			success: function (res) {
				res = jQuery.parseJSON(res);
				jQuery("#custom-menu > ul#space_0").html(res.data);
				jQuery(".header-posts .total-count").text(res.total_items);
				jQuery(".un-categorised-items .total-count").text(res.empty_items);
				selectedVal = jQuery("#media-attachment-taxonomy-filter").val();
				if(selectedVal != "all" && loadData == 1) {
					var wp1 = parent.wp;
					wp1.media.frame.setState('insert');
					if (wp1.media.frame.content.get() !== null) {
						wp1.media.frame.content.get().collection.props.set({ignore: (+new Date())});
						wp1.media.frame.content.get().options.selection.reset();
					} else {
						wp1.media.frame.library.props.set({ignore: (+new Date())});
					}
				}
				if(res.taxonomies.length) {
					if(jQuery("#media-attachment-taxonomy-filter").length) {
						folders_media_options.terms = res.taxonomies;
						var selectedDD = jQuery("#media-attachment-taxonomy-filter");
						selectedDD.html("<option value='all'>All Folders</option><option value='unassigned'>(Unassigned)</option>");
						for (i = 0; i < res.taxonomies.length; i++) {
							selectedDD.append("<option value='" + res.taxonomies[i].term_id + "'>" + res.taxonomies[i].name + " (" + res.taxonomies[i].count + ")</option>");
						}
						selectedDD.val(selectedVal);
					}
				}
				if(activeRecordID != "") {
					jQuery("#wcp_folder_"+activeRecordID).addClass("active-item");
				}
			}
		});
	}

	function setMediaBoxWidth() {
		jQuery(".media-frame-content .media-toolbar").width(jQuery(".media-frame-content").width() - 20);
	}

	setMediaBoxWidth();

	jQuery(window).resize(function(){
		setMediaBoxWidth();
	});

	jQuery(document).ready(function(){

	});

	jQuery(document).on("click", ".button.organize-button", function(){
		if(!jQuery(".media-frame").hasClass("mode-select")) {
			setCookie("media-select-mode", "on", 7);
		} else {
			eraseCookie("media-select-mode");
		}
		jQuery("button.button.media-button.select-mode-toggle-button").trigger("click");
		if(jQuery(".media-frame").hasClass("mode-select")) {
			jQuery(".media-info-message").addClass("active");
		} else {
			jQuery(".media-info-message").removeClass("active");
		}
	});

	jQuery(document).on("click", ".select-mode-toggle-button", function(){
		setTimeout(function() {
			if(!jQuery(".media-frame").hasClass("mode-select")) {
				setCookie("media-select-mode", "off", -1);
			}
			if(jQuery(".media-frame").hasClass("mode-select")) {
				jQuery(".media-info-message").addClass("active");
			} else {
				jQuery(".media-info-message").removeClass("active");
			}
		}, 10);
	});

	function setCookie(name,value,days) {
		var expires = "";
		if (days) {
			var date = new Date();
			date.setTime(date.getTime() + (days*24*60*60*1000));
			expires = "; expires=" + date.toUTCString();
		}
		document.cookie = name + "=" + (value || "")  + expires + "; path=/";
	}
	function getCookie(name) {
		var nameEQ = name + "=";
		var ca = document.cookie.split(';');
		for(var i=0;i < ca.length;i++) {
			var c = ca[i];
			while (c.charAt(0)==' ') c = c.substring(1,c.length);
			if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
		}
		return null;
	}


	function eraseCookie(name) {
		document.cookie = name+'=; Max-Age=-99999999;';
	}

	function setStickyHeaderForMedia() {
		if(!jQuery(".media-position").length) {
			jQuery(".media-frame-content .media-toolbar").before("<div class='media-position'></div>")
		}

		if(jQuery(".media-position").length) {
			setMediaBoxWidth();

			thisPosition = jQuery(".media-position").offset().top - jQuery(window).scrollTop();
			if(thisPosition <= 32) {
				jQuery(".media-frame-content .media-toolbar").addClass("sticky-media");
				jQuery(".media-position").height(jQuery(".media-frame-content .media-toolbar").outerHeight());
			} else {
				jQuery(".media-frame-content .media-toolbar").removeClass("sticky-media");
				jQuery(".media-position").height(1);
			}
		}
	}

	jQuery(window).scroll(function(){
		setStickyHeaderForMedia()
	});
} else {
	function setStickyHeaderForMedia() {}
}