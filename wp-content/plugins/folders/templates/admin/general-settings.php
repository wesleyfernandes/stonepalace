<?php
defined('ABSPATH') or wp_die('Nope, not accessing this');
?>
<style>
    span.upgrade-message {
        padding: 5px 10px;
        background: #fff;
        display: inline-block;
        font-size: 14px;
        color: #000;
    }
    a.pink, span.pink {
        color: #FF5983;
        text-decoration: none;
        font-weight: bold;
    }
    #setting-form {
        float: left;
        width: 580px;
    }
    .form-table th.default-folder {
        width: 100px;
    }
    .premio-help {
        width: calc(100% - 600px);
        float: left;
        text-align: center;
    }
    .premio-help a {
        display: inline-block;
        width: 200px;
        margin-top: 15px;
    }
    .premio-help a img {
        width: 100%;
        height: auto;
    }
    .hide-option {
        display: none;
    }
    select.hide-show-option {
        width: 170px;
    }
    <?php if ( function_exists( 'is_rtl' ) && is_rtl() ) { ?>
    #setting-form {
        float: right;
    }
    <?php } ?>
</style>
<script>
    jQuery(document).ready(function(){
        jQuery(document).on("click",".folder-select",function(){
            if(jQuery(this).is(":checked")) {
                jQuery(this).closest("tr").find(".hide-show-option").removeClass("hide-option");
            } else {
                jQuery(this).closest("tr").find(".hide-show-option").addClass("hide-option");
            }
        });
    });
</script>
<div class="wrap">
    <h1><?php esc_attr_e( 'Folders Settings', WCP_FOLDER ); ?></h1>
    <form action="options.php" method="post" id="setting-form">
        <?php
        settings_fields('folders_settings');
        settings_fields('default_folders');
        $options = get_option('folders_settings');
        $default_folders = get_option('default_folders');
        $default_folders = (empty($default_folders) || !is_array($default_folders))?array():$default_folders;
        do_settings_sections( __FILE__ );
        ?>
        <table class="form-table">
            <?php
            $post_types = get_post_types( array( 'public' => true ), 'objects' );
            $post_array = array("page", "post", "attachment");
            foreach ( $post_types as $post_type ) : ?>
                <?php
                if ( ! $post_type->show_ui) continue;
                $is_checked = !in_array( $post_type->name, $options )?"hide-option":"";
                $selected_id = (isset($default_folders[$post_type->name]))?$default_folders[$post_type->name]:"all";
                if(in_array($post_type->name, $post_array)){
                    ?>
                    <tr>
                        <th>
                            <label for="folders_<?php echo esc_attr($post_type->name); ?>" ><?php esc_html_e( 'Use folders with: ', WCP_FOLDER )." ".esc_html_e($post_type->label); ?></label>
                        </th>
                        <td>
                            <input type="checkbox" class="folder-select" id="folders_<?php echo esc_attr($post_type->name); ?>" name="folders_settings[]" value="<?php echo esc_attr($post_type->name); ?>"<?php if ( in_array( $post_type->name, $options ) ) echo ' checked="checked"'; ?>/>
                        </td>
                        <th class="default-folder">
                            <label class="hide-show-option <?php echo esc_attr($is_checked) ?>" for="folders_for_<?php echo esc_attr($post_type->name); ?>" ><?php esc_html_e( 'Default folder: ', WCP_FOLDER ) ?></label>
                        </th>
                        <td>
                            <select class="hide-show-option <?php echo esc_attr($is_checked) ?>" id="folders_for_<?php echo esc_attr($post_type->name); ?>" name="default_folders[<?php echo esc_attr($post_type->name); ?>]" ?>">
                                <option value="">All <?php echo esc_attr($post_type->label) ?> Folder</option>
                                <option value="-1" <?php echo ($selected_id == -1)?"selected":"" ?>>Unassigned <?php echo esc_attr($post_type->label) ?></option>
                                <?php
                                if(isset($terms_data[$post_type->name]) && !empty($terms_data[$post_type->name])) {
                                    foreach ($terms_data[$post_type->name] as $term) {
                                        $selected = ($selected_id == $term->slug)?"selected":"";
                                        echo "<option ".esc_attr($selected)." value='".esc_attr($term->slug)."'>".esc_attr($term->name)."</option>";
                                    }
                                } ?>
                            </select>
                        </td>
                    </tr>
                <?php
                } else { ?>
                    <tr>
                        <th>
                            <label for="folders_<?php echo esc_attr($post_type->name); ?>" ><?php esc_html_e( 'Use folders with: ', WCP_FOLDER )." ".esc_html_e($post_type->label); ?></label>
                        </th>
                        <td>
                            <input type="checkbox" class="folder-select" id="folders_<?php echo esc_attr($post_type->name); ?>" name="folders_settings[]" value="<?php echo esc_attr($post_type->name); ?>"<?php if ( in_array( $post_type->name, $options ) ) echo ' checked="checked"'; ?>/>
                        </td>
                        <th class="default-folder">
                            <label class="hide-show-option <?php echo esc_attr($is_checked) ?>" for="folders_for_<?php echo esc_attr($post_type->name); ?>" ><?php esc_html_e( 'Default folder: ', WCP_FOLDER ) ?></label>
                        </th>
                        <td>
                            <select class="hide-show-option <?php echo esc_attr($is_checked) ?>" id="folders_for_<?php echo esc_attr($post_type->name); ?>" name="default_folders[<?php echo esc_attr($post_type->name); ?>]" ?>">
                                <option value="">All <?php echo esc_attr($post_type->label) ?> Folder</option>
                                <option value="-1" <?php echo ($selected_id == -1)?"selected":"" ?>>Unassigned <?php echo esc_attr($post_type->label) ?></option>
                                <?php
                                if(isset($terms_data[$post_type->name]) && !empty($terms_data[$post_type->name])) {
                                    foreach ($terms_data[$post_type->name] as $term) {
                                        $selected = ($selected_id == $term->slug)?"selected":"";
                                        echo "<option ".esc_attr($selected)." value='".esc_attr($term->slug)."'>".esc_attr($term->name)."</option>";
                                    }
                                } ?>
                            </select>
                        </td>
                    </tr>
                <?php } endforeach; ?>
            <tr>
                <th>
                    <label for="folders_<?php echo esc_attr($post_type->name); ?>" ><?php esc_html_e( 'Show Folders in Menu:', WCP_FOLDER ); ?></label>
                </th>
                <td>
                    <?php $val = get_option("folders_show_in_menu"); ?>
                    <input type="hidden" name="folders_show_in_menu" value="off" />
                    <input type="checkbox" name="folders_show_in_menu" value="on" <?php echo ($val == "on")?"checked='checked'":"" ?>/>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="padding: 10px 0">
                    <?php
                    $total_folders = get_option("folder_old_plugin_folder_status");
                    if($total_folders == false || $total_folders < 10) {
                        $total_folders = 10;
                    }
                    $total = WCP_Folders::get_total_term_folders();
                    if($total > $total_folders) {
                        $total_folders = $total;
                    }
                    ?>
                    <span class="upgrade-message">You have used <span class='pink'><?php echo esc_attr($total) ?></span>/<?php echo esc_attr($total_folders) ?> Folders. <a class="pink" href="<?php echo esc_url(admin_url("admin.php?page=wcp_folders_upgrade")) ?>"><?php esc_html_e("Upgrade", WCP_FOLDER) ?></a></span>
                </td>
            </tr>
        </table>
        <input type="hidden" name="folders_settings1[premio_folder_option]" value="yes" />
        <?php submit_button(); ?>
    </form>
    <div class="premio-help">
        <a href="https://premio.io/help/folders/?utm_source=pluginspage" target="_blank">
        <img src="<?php echo esc_url(WCP_FOLDER_URL."assets/images/premio-help.png") ?>" alt="Premio Help" class="Premio Help" />
        </a>
    </div>
</div>