<?php
/**
 * Administration class
 * 
 * @package   Media_Library_Organizer
 * @author    WP Media Library
 * @version   1.0.0
 */
class Media_Library_Organizer_Admin {

    /**
     * Holds the base class object.
     *
     * @since   1.0.0
     *
     * @var     object
     */
    public $base;

    /**
     * Constructor
     * 
     * @since   1.0.0
     *
     * @param   object $base    Base Plugin Class
     */
    public function __construct( $base ) {

        // Store base class
        $this->base = $base;

        // Actions
        add_action( 'admin_enqueue_scripts', array( $this, 'scripts_css' ) );
        add_action( 'admin_menu', array( $this, 'admin_menu' ) );
        add_action( 'media_library_organizer_admin_scripts_css_general', array( $this, 'enqueue_css_settings' ) );
        add_action( 'media_library_organizer_admin_scripts_css_import-export', array( $this, 'enqueue_css_settings' ) );

        // Screens
        add_filter( 'media_library_organizer_admin_get_current_screen_import-export', array( $this, 'get_current_screen_import_export' ), 10, 2 );

    }

    /**
     * Enqueues JS and CSS if we're on a plugin screen or the welcome screen.
     *
     * @since 1.0.0
     */
    public function scripts_css() {

        // Bail if we can't get the current admin screen, or we're not viewing a screen
        // belonging to this plugin.
        if ( ! function_exists( 'get_current_screen' ) ) {
            return;
        }

        // Get current screen, registered plugin screens and the media view (list or grid)
        $screen = get_current_screen();
        $screens = $this->get_screens();
        $mode = $this->base->get_class( 'common' )->get_media_view();

        // If we're on the Media screen, enqueue
        if ( $screen->id == 'upload' || $screen->id == 'media' ) {
            $this->enqueue_scripts_css( 'media', $screen, $screens, $mode );
            return;
        }

        // If we're on the top level screen, enqueue
        if ( $screen->base == 'toplevel_page_' . $this->base->plugin->name ) {
            $this->enqueue_scripts_css( 'general', $screen, $screens, $mode );
            return;
        }

        // Iterate through the registered screens, to see if we're viewing that screen
        foreach ( $screens as $registered_screen ) {
            if ( $screen->id == 'media-library-organizer_page_media-library-organizer-' . $registered_screen['name'] ) {
                // We're on a plugin screen
                $this->enqueue_scripts_css( $registered_screen['name'], $screen, $screens, $mode );
                return;
            }
        }

    }

    /**
     * Enqueues scripts and CSS
     *
     * @since   1.0.0
     *
     * @param   string      $plugin_screen_name     Plugin Screen Name (general|import-export|media)
     * @param   WP_Screen   $screen                 Current WordPress Screen object
     * @param   array       $screens                Registered Plugin Screens (optional)
     * @param   string      $mode                   Media View Mode (list|grid)
     */
    public function enqueue_scripts_css( $plugin_screen_name, $screen, $screens = '', $mode ) {

        global $post;

        // Enqueue JS
        // These scripts are registered in _modules/licensing/lum.php
        wp_enqueue_script( 'lum-admin-tabs' );
        wp_enqueue_script( 'lum-admin-conditional' );
        wp_enqueue_script( 'lum-admin' );

        
        /**
         * Enqueue Javascript for the given screen and Media View mode
         *
         * @since   1.0.7
         *
         * @param   WP_Screen   $screen                 Current WordPress Screen object
         * @param   array       $screens                Registered Plugin Screens (optional)
         * @param   string      $mode                   Media View Mode (list|grid)
         */
        do_action( 'media_library_organizer_admin_scripts_js', $screen, $screens, $mode );

        /**
         * Enqueue Javascript for the given screen and Media View mode by Plugin
         * Screen Name
         *
         * @since   1.0.7
         *
         * @param   WP_Screen   $screen                 Current WordPress Screen object
         * @param   array       $screens                Registered Plugin Screens (optional)
         * @param   string      $mode                   Media View Mode (list|grid)
         */
        do_action( 'media_library_organizer_admin_scripts_js_' . $plugin_screen_name, $screen, $screens, $mode );
        
        /**
         * Enqueue Stylesheets (CSS) for the given screen and Media View mode
         *
         * @since   1.0.7
         *
         * @param   WP_Screen   $screen                 Current WordPress Screen object
         * @param   array       $screens                Registered Plugin Screens (optional)
         * @param   string      $mode                   Media View Mode (list|grid)
         */
        do_action( 'media_library_organizer_admin_scripts_css', $screen, $screens, $mode );

        /**
         * Enqueue Stylesheets (CSS) for the given screen and Media View mode
         *
         * @since   1.0.7
         *
         * @param   WP_Screen   $screen                 Current WordPress Screen object
         * @param   array       $screens                Registered Plugin Screens (optional)
         * @param   string      $mode                   Media View Mode (list|grid)
         */
        do_action( 'media_library_organizer_admin_scripts_css_' . $plugin_screen_name, $screen, $screens, $mode );

    }

    /**
     * Enqueues CSS for the Settings screen
     *
     * @since   1.0.3
     */
    public function enqueue_css_settings() {

        // Enqueue CSS
        wp_enqueue_style( $this->base->plugin->name . '-admin', $this->base->plugin->url . '/assets/css/admin.css' );

    }
    
    /**
     * Adds menu and sub menu items to the WordPress Administration
     *
     * @since 1.0.0
     */
    public function admin_menu() {

        // Get the registered screens
        $screens = $this->get_screens();

        // Create the top level screen
        add_menu_page( $this->base->plugin->displayName, $this->base->plugin->displayName, 'manage_options', $this->base->plugin->name, array( $this, 'admin_screen' ), 'dashicons-admin-media' );
       
        // Iterate through screens, adding as submenu items
        foreach ( (array) $screens as $screen ) {
            // The settings screen doesn't need to append the page slug
            $slug = ( ( $screen['name'] == 'settings' ) ? $this->base->plugin->name : $this->base->plugin->name . '-' . $screen['name'] );

            // Add submenu page
            add_submenu_page( $this->base->plugin->name, $screen['label'], $screen['label'], 'manage_options', $slug, array( $this, 'admin_screen' ) );
        }

        /**
         * Register Menu or Submenu Pages relative to this Plugin
         *
         * @since   1.0.7
         */
        do_action( str_replace( '-', '_', $this->base->plugin->name ) . '_admin_menu' );

    }

    /**
     * Returns an array of screens for the plugin's admin
     *
     * @since   1.0.0
     *
     * @return  array Sections
     */
    private function get_screens() {

        // Define the settings screen
        $screens = array(
            'settings'   => array(
                'name'          => 'settings',
                'label'         => __( 'Settings', 'media-library-organizer' ),
                'description'   => __( 'Defines Plugin-wide settings for Media Library Organizer.', 'media-library-organizer' ),
                'view'          => $this->base->plugin->folder . 'views/admin/settings-general.php',
                'columns'       => 2,
                'data'          => array(),
                'documentation' => 'https://wpmedialibrary.com/documentation/settings',
            ),
        );

        /**
         * Define sections in the Plugin's Settings
         *
         * @since   1.0.7
         *
         * @param   array       $screens                Registered Plugin Screens
         */
        $screens = apply_filters( 'media_library_organizer_admin_get_screens', $screens );

        // Finally, add the Import & Export and Support Screens
        // This ensures they are always at the end of the admin menu
        $screens['import-export'] = array(
            'name'          => 'import-export',
            'label'         => __( 'Import &amp; Export', 'media-library-organizer' ),
            'description'   => 
                __( 'Import configuration data from another Media Library Organizer installation, or a third party Media Library plugin 
                    that has been previously used on this site.
                    <br />
                    Export Media Library Organizer configuration data to a JSON file.', 'media-library-organizer' ),
            'view'          => $this->base->plugin->folder . 'views/admin/settings-import-export.php',
            'columns'       => 1,
            'data'          => array(),
            'documentation' => 'https://wpmedialibrary.com/documentation',
        );
        $screens['support'] = array(
            'name'          => 'support',
            'label'         => __( 'Support', 'media-library-organizer' ),
        );

        // Return
        return $screens;

    }

    /**
     * Gets the current admin screen the user is on
     *
     * @since   1.0.0
     *
     * @return  array    Screen name and label
     */
    public function get_current_screen() {

        // Bail if no page given
        if ( ! isset( $_GET['page'] ) ) {
            return;
        }

        // Get current screen name
        $screen = sanitize_text_field( $_GET['page'] );

        // Get registered screens
        $screens = $this->get_screens();

        // Remove the plugin name from the screen
        $screen = str_replace( $this->base->plugin->name . '-', '', $screen );

        // If the screen is the plugin name, it's the settings screen
        if ( $screen == $this->base->plugin->name ) {
            $screen = 'settings';
        }

        // Check if the screen exists
        if ( ! isset( $screens[ $screen ] ) ) {
            return new WP_Error( 'screen_missing', __( 'The requested administration screen does not exist', 'media-library-organizer' ) );
        }

        /**
         * Adjust the screen data immediately before returning 
         *
         * @since   1.0.7
         *
         * @param   array   $screens[ $screen ] Screen Data
         * @param   string  $screen             Screen Name
         */
        $screens[ $screen ] = apply_filters( 'media_library_organizer_admin_get_current_screen_' . $screen, $screens[ $screen ], $screen );

        // Return the screen
        return $screens[ $screen ];

    }

    /**
     * Injects Import Sources information into the Import / Export screen data, for use by the view.
     *
     * @since   1.0.0
     *
     * @param   array   $screen         Screen
     * @param   string  $screen_name    Screen Name
     * @return  array                   Screen
     */
    public function get_current_screen_import_export( $screen, $screen_name ) {

        $screen['data'] = array(
            'import_sources' => $this->base->get_class( 'import' )->get_import_sources(),
        );

        return $screen;

    }

    /**
     * Gets the current admin screen name the user is on
     *
     * @since   1.0.0
     *
     * @return  mixed  false | Screen Name
     */
    private function get_current_screen_name() {

        // If no page name was given, we're not on a plugin screen.
        if ( ! isset( $_GET['page'] ) ) {
            return false;
        }

        // Get screen name
        $screen = sanitize_text_field( $_GET['page'] );

        // Return
        return $screen;

    }

    /**
     * Gets the current admin screen tab the user is on
     *
     * @since   1.0.0
     *
     * @param   array   $tabs   Screen Tabs
     * @return  array           Tab name and label
     */
    private function get_current_screen_tab( $tabs ) {

        // If the supplied tabs are an empty array, return false
        if ( empty( $tabs ) ) {
            return false;
        }

        // If no tab defined, get the first tab name from the tabs array
        if ( ! isset( $_REQUEST['tab'] ) ) {
            foreach ( $tabs as $tab ) {
                return $tab;
            }
        }

        // Return the requested tab, if it exists
        if ( isset( $tabs[ $_REQUEST['tab'] ] ) ) {
            $tab = $tabs[ $_REQUEST['tab'] ];
            return $tab;
        } else {
            foreach ( $tabs as $tab ) {
                return $tab;
            }
        }

    }

    /**
     * Returns an array of tabs for each plugin section
     *
     * @since   1.0.0
     *
     * @param   string  $screen     Screen
     * @return  array               Tabs
     */
    private function get_screen_tabs( $screen ) {

        // Define tabs array
        $tabs = array();

        // Define the tabs depending on which screen is specified
        switch ( $screen ) {

            /**
             * Settings
             */
            case 'settings':
                $tabs = array(
                    'settings' => array(
                        'name'          => 'general',
                        'label'         => __( 'General', 'media-library-organizer' ),
                        'documentation' => $this->base->plugin->support_url,
                        'menu_icon'     => 'filter',
                    ),
                    'user-options' => array(
                        'name'          => 'user-options',
                        'label'         => __( 'User Options', 'media-library-organizer' ),
                        'documentation' => $this->base->plugin->support_url,
                        'menu_icon'     => 'user',
                    ),
                );
                break;

            /**
             * Import & Export
             */
            case 'import-export':
                // Default tabs
                $tabs = array(
                    'import' => array(
                        'name'          => 'import',
                        'label'         => sprintf( __( 'Import from %s', 'media-library-organizer' ), $this->base->plugin->displayName ),
                        'documentation' => 'https://wpmedialibrary.com/documentation/import-wp-media-library-pro/',
                        'menu_icon'     => 'minimize-2',
                    ),
                );

                // Depending on whether any third party SEO plugin data is present in this install,
                // add additional tabs.
                $import_sources = $this->base->get_class( 'import' )->get_import_sources();
                if ( count( $import_sources ) > 0 ) {
                    foreach ( $import_sources as $import_source ) {
                        $tabs[ 'import-' . $import_source['name'] ] = array(
                            'name'          => 'import-' . $import_source['name'],
                            'label'         => sprintf( __( 'Import from %s', 'media-library-organizer' ), $import_source['label'] ),
                            'documentation' => $import_source['documentation'],
                            'menu_icon'     => 'minimize-2',
                        );
                    }
                }

                // Finally, add the export tab
                $tabs['export'] = array(
                    'name'          => 'export',
                    'label'         => __( 'Export', 'media-library-organizer' ),
                    'documentation' => 'https://wpmedialibrary.com/documentation/export-configuration/',
                    'menu_icon'     => 'maximize-2',
                );

                break;

        }

        /**
         * Define tabs in the Plugin Settings section.
         *
         * @since   1.0.7
         *
         * @param   array   $tabs       Settings Tabs
         * @param   string  $screen     Current Screen Name to define Tabs for
         */
        $tabs = apply_filters( 'media_library_organizer_admin_get_screen_tabs', $tabs, $screen );

        // Return
        return $tabs;

    }

    /**
     * Output the Settings screen
     * Save POSTed data from the Administration Panel into a WordPress option
     *
     * @since 1.0.0
     */
    public function admin_screen() {

        // Get the current screen
        $screen = $this->get_current_screen();
        if ( ! $screen || is_wp_error( $screen ) ) {
            require_once( $this->base->plugin->folder . '/views/admin/error.php' ); 
            return;
        }

        // Maybe save settings
        $this->save_settings( $screen['name'] );

        // Hacky; get the current screen again, so its data is refreshed post save and actions
        // @TODO optimize this
        $screen = $this->get_current_screen();
        if ( ! $screen || is_wp_error( $screen ) ) {
            require_once( $this->base->plugin->folder . '/views/admin/error.php' ); 
            return;
        }

        // Get the tabs for the given screen
        $tabs = $this->get_screen_tabs( $screen['name'] );

        // Get the current tab
        // If no tab specified, get the first tab
        $tab = $this->get_current_screen_tab( $tabs );

        // Define a string of conditional tabs
        // The tabs are only displayed if the General > Enabled option is checked
        $conditional_tabs = '';
        foreach ( $tabs as $tab_key => $data ) {
            if ( $tab_key == 'settings' ) {
                continue;
            }

            $conditional_tabs .= $tab_key . ',';
        }
        $conditional_tabs = trim( $conditional_tabs, ',' );

        // Load View
        require_once( $this->base->plugin->folder . '/views/admin/settings.php' ); 
    
    }

    /**
     * Save settings for the given screen
     *
     * @since 1.0
     *
     * @param string     $screen     Screen (settings|import-export)
     */
    public function save_settings( $screen = 'settings' ) {

        // Check that some data was submitted in the request
        if ( ! isset( $_REQUEST[ $this->base->plugin->name . '_nonce' ] ) ) { 
            return;
        }

        // Invalid nonce
        if ( ! wp_verify_nonce( $_REQUEST[ $this->base->plugin->name . '_nonce' ], 'media-library-organizer_' . $screen ) ) {
            $this->base->get_class( 'notices' )->add_error_notice( __( 'Invalid nonce specified. Settings NOT saved.', 'media-library-organizer' ) );
            return false;
        }

        // Depending on the screen we're on, save the data and perform some actions
        switch ( $screen ) {

            /**
             * Settings
             */
            case 'settings':
                // General
                $result = $this->base->get_class( 'settings' )->update_settings( 'general', $_POST['general'] );
                if ( is_wp_error( $result ) ) {
                    $this->base->get_class( 'notices' )->add_error_notice( $result->get_error_message() );
                    return;
                }

                // User Options
                $result = $this->base->get_class( 'settings' )->update_settings( 'user-options', $_POST['user-options'] );
                if ( is_wp_error( $result ) ) {
                    $this->base->get_class( 'notices' )->add_error_notice( $result->get_error_message() );
                    return;
                }
                
                /**
                 * Save POSTed data on a Settings Screen
                 *
                 * @since   1.0.7
                 *
                 * @param   mixed   $result    Result of saving data (true or WP_Error)
                 * @param   array   $_POST     Unfiltered $_POST data
                 */
                $result = apply_filters( 'media_library_organizer_admin_save_settings', true, $_POST );

                if ( is_wp_error( $result ) ) {
                    $this->base->get_class( 'notices' )->add_error_notice( $result->get_error_message() );
                    return;
                }

                // If here, OK
                $this->base->get_class( 'notices' )->add_success_notice( __( 'Settings saved.', 'media-library-organizer' ) );
                
                // Exit
                return;
                break;
  
            /**
             * Import
             */
            case 'import-export':
                // Determine which plugin we're importing settings from
                $import_sources = $this->base->get_class( 'import' )->get_import_sources();
                if ( is_array( $import_sources ) && count( $import_sources ) > 0 ) {
                    foreach ( $import_sources as $import_source => $label ) {
                        // If a POST variable is set, import from this Plugin
                        if ( isset( $_POST['import_' . $import_source ] ) ) {
                            /**
                             * Run the Import Routine for the given Import Source
                             *
                             * @since   1.0.7
                             *
                             * @param   mixed   $result    Result of importing data (true or WP_Error)
                             */
                            $result = apply_filters( 'media_library_organizer_import_' . $import_source, false );
                            break;
                        }
                    }

                    if ( isset( $result ) ) {
                        break;
                    }
                }

                // If here, we might be importing a JSON file
                // Check if a file was uploaded
                if ( ! is_array( $_FILES ) ) {
                    $result = new WP_Error( __( 'No JSON file uploaded.', 'media-library-organizer' ) );
                    break;
                }

                // Check if the uploaded file encountered any errors
                if ( $_FILES['import']['error'] != 0 ) {
                    $result = new WP_Error( __( 'Error when attempting to upload JSON file for import.', 'media-library-organizer' ) );
                    break;
                }

                // Read file
                $handle = fopen( $_FILES['import']['tmp_name'], 'r' );
                $json = fread( $handle, $_FILES['import']['size'] );
                fclose( $handle );
                $data = json_decode( $json, true );

                /**
                 * Import the given data
                 *
                 * @since   1.0.7
                 *
                 * @param   mixed   $result     Result of importing data (true or WP_Error)
                 * @param   array   $data       Data to Import
                 */
                $result = apply_filters( 'media_library_organizer_import', false, $data );
                break;

            /**
             * Addons
             */
            default:
                /**
                 * Saves Settings for an Addon
                 *
                 * @since   1.0.7
                 *
                 * @param   mixed   $result     Result of importing data (true or WP_Error)
                 * @param   array   $_POST      Unfiltered $_POST data to save
                 */
                $result = apply_filters( 'media_library_organizer_admin_save_settings_' . $screen, '', $_POST );
                break;
        }

        // Check the result
        if ( isset( $result ) && is_wp_error( $result ) ) {
            $this->base->get_class( 'notices' )->add_error_notice( $result->get_error_message() );
            return;
        }

        // OK
        $this->base->get_class( 'notices' )->add_success_notice( __( 'Settings saved.', 'media-library-organizer' ) );
        return true;

    }

    /**
     * Helper method to get the setting value from the Plugin settings
     *
     * @since 1.0.0
     *
     * @param   string    $screen   Screen
     * @param   string    $keys     Setting Key(s)
     * @return  mixed               Value
     */
    public function get_setting( $screen = '', $key = '' ) {

        return $this->base->get_class( 'settings' )->get_setting( $screen, $key );

    }

    /**
     * Returns the singleton instance of the class.
     *
     * @since       1.0.0
     * @deprecated  1.0.5
     *
     * @return      object Class.
     */
    public static function get_instance() {

        // Define class name
        $name = 'admin';

        // Warn the developer that they shouldn't use this function.
        _deprecated_function( __FUNCTION__, '1.0.5', 'Media_Library_Organizer()->get_class( \'' . $name . '\' )' );

        // Return the class
        return Media_Library_Organizer()->get_class( $name );

    }

}