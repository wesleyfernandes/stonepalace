<?php
/**
 * Export class
 * 
 * @package   Media_Library_Organizer
 * @author    WP Media Library
 * @version   1.0.0
 */
class Media_Library_Organizer_Export {

    /**
     * Holds the base class object.
     *
     * @since   1.0.0
     *
     * @var     object
     */
    public $base;

    /**
     * Constructor
     * 
     * @since   1.0.0
     *
     * @param   object $base    Base Plugin Class
     */
    public function __construct( $base ) {

        // Store base class
        $this->base = $base;

    }

    /**
     * Export data, forcing a browser download
     *
     * @since   1.0.0
     *
     * @return  array           Data
     */
    public function export() {

        // Get plugin settings
        $settings = array(
            'general'       => $this->base->get_class( 'settings' )->get_settings( 'general' ),
            'user-options'  => $this->base->get_class( 'settings' )->get_settings( 'user-options' ),
        );

        /**
         * Define settings data to include in the Export JSON file
         *
         * @since   1.0.7
         *
         * @param   array   $settings   Settings Data to include in Export JSON file
         */
        $settings = apply_filters( 'media_library_organizer_export', $settings );

        // Build JSON
        return json_encode( $settings );
        
    }

    /**
     * Force a browser download comprising of the given JSON data
     *
     * @since   1.0.0
     *
     * @param   string  $json   JSON Data for file
     */
    public function force_file_download( $json ) {

        // Output JSON, prompting the browser to auto download as a JSON file now
        header( "Content-type: application/x-msdownload" );
        header( "Content-Disposition: attachment; filename=export.json" );
        header( "Pragma: no-cache" );
        header( "Expires: 0" );
        echo $json;
        exit();

    }

    /**
     * Returns the singleton instance of the class.
     *
     * @since       1.0.0
     * @deprecated  1.0.5
     *
     * @return      object Class.
     */
    public static function get_instance() {

        // Define class name
        $name = 'export';

        // Warn the developer that they shouldn't use this function.
        _deprecated_function( __FUNCTION__, '1.0.5', 'Media_Library_Organizer()->get_class( \'' . $name . '\' )' );

        // Return the class
        return Media_Library_Organizer()->get_class( $name );

    }

}