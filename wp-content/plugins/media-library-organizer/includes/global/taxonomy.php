<?php
/**
 * Taxonomy class. Registers the Media Categories taxonomy,
 * and provides helper functions for creating or updating
 * Terms.
 * 
 * @package   Media_Library_Organizer
 * @author    WP Media Library
 * @version   1.0.0
 */
class Media_Library_Organizer_Taxonomy {

    /**
     * Holds the base class object.
     *
     * @since   1.0.5
     *
     * @var     object
     */
    public $base;

    /**
     * Holds the Post Type name.
     *
     * @since   1.0.0
     *
     * @var     string
     */
    public $taxonomy_name = 'mlo-category';

    /**
     * Constructor
     * 
     * @since   1.0.5
     *
     * @param   object $base    Base Plugin Class
     */
    public function __construct( $base ) {

        // Store base class
        $this->base = $base;

        // Actions
        add_action( 'init', array( $this, 'register_taxonomy' ), 20 );

    }

    /**
     * Registers the Taxonomy
     *
     * @since   1.0.0
     */
    public function register_taxonomy() {
        
        // Register taxonomy
        register_taxonomy( $this->taxonomy_name, array( 'attachment' ), array(
            'labels'                => array(
                'name'              => __( 'Media Categories', 'media-library-organizer' ),
                'singular_name'     => __( 'Media Category', 'media-library-organizer' ),
                'search_items'      => __( 'Search Media Categories', 'media-library-organizer' ),
                'all_items'         => __( 'All Media Categories', 'media-library-organizer' ),
                'parent_item'       => __( 'Parent Media Category', 'media-library-organizer' ),
                'parent_item_colon' => __( 'Parent Media Category:', 'media-library-organizer' ),
                'edit_item'         => __( 'Edit Media Category', 'media-library-organizer' ),
                'update_item'       => __( 'Update Media Category', 'media-library-organizer' ),
                'add_new_item'      => __( 'Add New Media Category', 'media-library-organizer' ),
                'new_item_name'     => __( 'New Media Category', 'media-library-organizer' ),
                'menu_name'         => __( 'Media Categories', 'media-library-organizer' ),
            ),
            'public'                => false,
            'publicly_queryable'    => false,
            'show_ui'               => true,
            'show_in_menu'          => true,
            'show_in_nav_menus'     => false,
            'show_in_rest'          => true,
            'show_tagcloud'         => false,
            'show_in_quick_edit'    => true,
            'show_admin_column'     => true,
            'hierarchical'          => true,
            'show_ui'               => true,

            // Force counts on Terms
            'update_count_callback' => '_update_generic_term_count',
        ) );

    }

    /**
     * Returns the Taxonomy object
     *
     * @since   1.0.5
     *
     * @return  WP_Taxonomy     Taxonomy
     */
    public function get_taxonomy() {

        return get_taxonomy( $this->taxonomy_name );

    }

    /**
     * Creates or Updates a Term for this Taxonomy
     *
     * @since   1.0.5
     *
     * @param   string  $name           Name
     * @param   string  $description    Description
     * @param   int     $parent         Parent Term
     * @return  mixed                   WP_Error | Term ID
     */
    public function create_or_update_term( $term, $parent = 0 ) {

        // Check to see if the Term already exists
        $existing_term_id = term_exists( $term, $this->taxonomy_name, $parent );

        if ( $existing_term_id ) {
            $result = wp_update_term( $existing_term_id['term_id'], $this->taxonomy_name, array(
                'name'          => $term,
                'parent'        => (int) $parent,
            ) );
        } else {
            $result = wp_insert_term( $term, $this->taxonomy_name, array(
                'parent'        => (int) $parent,
            ) );
        }

        // Bail if an error occured
        if ( is_wp_error( $result ) ) {
            return $result;
        }

        // Return Term ID
        return $result['term_id'];

    }

    /**
     * Returns the singleton instance of the class.
     *
     * @since       1.0.0
     * @deprecated  1.0.5
     *
     * @return      object Class.
     */
    public static function get_instance() {

        // Define class name
        $name = 'taxonomy';

        // Warn the developer that they shouldn't use this function.
        _deprecated_function( __FUNCTION__, '1.0.5', 'Media_Library_Organizer()->get_class( \'' . $name . '\' )' );

        // Return the class
        return Media_Library_Organizer()->get_class( $name );

    }

}