<?php
/**
 * MIME class. Defines various file types for easy identification,
 * such as images, video, audio and documents.
 * 
 * @package   Media_Library_Organizer
 * @author    WP Media Library
 * @version   1.0.5
 */
class Media_Library_Organizer_MIME {

    /**
     * Holds the base class object.
     *
     * @since   1.0.5
     *
     * @var     object
     */
    public $base;

    /**
     * Constructor
     * 
     * @since   1.0.5
     *
     * @param   object $base    Base Plugin Class
     */
    public function __construct( $base ) {

        // Store base class
        $this->base = $base;

    }

    /**
     * Returns the file type (image, video, audio, document)
     * of the given Attachment ID
     *
     * @since   1.0.5
     *
     * @param   int     $attachment_id  Attachment ID
     * @return  string                  File Type
     */
    public function get_file_type( $attachment_id ) {

        // Get all file types
        $file_types = $this->get_all_file_types();

        // Determine file type
        $filename = get_attached_file( $attachment_id );

        // Get MIME type
        $mime = mime_content_type( $filename );

        // Iterate through the file types until we match the mime
        foreach ( $file_types as $file_type => $mimes ) {
            if ( in_array( $mime, $mimes ) ) {
                return $file_type;
            }
        }

        // If here, we couldn't determine the file type
        return false;

    }

    /**
     * Defines top level file types, such as Images, Videos,
     * Audio and Documents.
     *
     * @since   1.0.5
     *
     * @return  array   Image File Types
     */
    public function get_file_types() {

        // Define File Types
        $file_types = array(
            'image'     => __( 'Images', 'media-library-organizer' ),
            'video'     => __( 'Videos', 'media-library-organizer' ),
            'audio'     => __( 'Audio', 'media-library-organizer' ),
            'document'  => __( 'Documents', 'media-library-organizer' ),
            'other'     => __( 'Other', 'media-library-organizer' ),
        );

        /**
         * Defines top level file types, such as Images, Videos,
         * Audio and Documents.
         *
         * @since   1.0.5
         *
         * @param   array   $file_types     Image File Types
         */
        $file_types = apply_filters( 'media_library_organizer_mime_get_file_types', $file_types );

        // Return
        return $file_types;

    }

    /**
     * Defines all supported file types, grouped by file type
     *
     * @since   1.0.5
     *
     * @return  array   Image File Types
     */
    public function get_all_file_types() {

        // Get all available file types
        $file_types = array(
            'image'     => $this->get_image_file_types(),
            'video'     => $this->get_video_file_types(),
            'audio'     => $this->get_audio_file_types(),
            'document'  => $this->get_document_file_types(),
        );

        /**
         * Defines all file types
         *
         * @since   1.0.5
         *
         * @param   array   $file_types     All File Types
         */
        $file_types = apply_filters( 'media_library_organizer_mime_get_all_file_types', $file_types );

        // Return
        return $file_types;

    }

    /**
     * Defines image file types
     *
     * @since   1.0.5
     *
     * @return  array   Image File Types
     */
    public function get_image_file_types() {

        // Define File Types
        $file_types = array(
            'image/jpg',
            'image/jpeg',
            'image/png',
            'image/gif',
            'image/ico',
            'image/bmp',
        );

        /**
         * Defines image file types
         *
         * @since   1.0.5
         *
         * @param   array   $file_types     Image File Types
         */
        $file_types = apply_filters( 'media_library_organizer_mime_get_image_file_types', $file_types );

        // Return
        return $file_types;

    }

    /**
     * Defines video file types
     *
     * @since   1.0.5
     *
     * @return  array   Video File Types
     */
    public function get_video_file_types() {

        // Define File Types
        $file_types = array(
            'video/mp4',
            'video/m4v',
            'video/mov',
            'video/wmv',
            'video/avi',
            'video/mpg',
            'video/ogv',
            'video/3gp',
            'video/3g2',
            'video/webm',
        );

        /**
         * Defines video file types
         *
         * @since   1.0.5
         *
         * @param   array   $file_types     Image File Types
         */
        $file_types = apply_filters( 'media_library_organizer_mime_get_video_file_types', $file_types );

        // Return
        return $file_types;

    }

    /**
     * Defines audio file types
     *
     * @since   1.0.5
     *
     * @return  array   Audio File Types
     */
    public function get_audio_file_types() {

        // Define File Types
        $file_types = array(
            'audio/mpeg',
            'audio/mp3',
            'audio/m4a',
            'audio/ogg',
            'audio/wav',
        );

        /**
         * Defines audio file types
         *
         * @since   1.0.5
         *
         * @param   array   $file_types     Audio File Types
         */
        $file_types = apply_filters( 'media_library_organizer_mime_get_audio_file_types', $file_types );

        // Return
        return $file_types;

    }

    /**
     * Defines document file types
     *
     * @since   1.0.5
     *
     * @return  array   Document File Types
     */
    public function get_document_file_types() {

        // Define File Types
        $file_types = array(
            'application/pdf',
            'application/doc',
            'application/docx',
            'application/xls',
            'application/xlsx',
            'application/vnd.ms-excel',
            'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            'application/ppt',
            'application/pptx',
            'application/pps',
            'application/ppsx',
            'application/odt',
            'application/psd',
        );

        /**
         * Defines image types
         *
         * @since   1.0.5
         *
         * @param   array   $file_types     Document File Types
         */
        $file_types = apply_filters( 'media_library_organizer_mime_get_document_file_types', $file_types );

        // Return
        return $file_types;

    }

    /**
     * Defines archive file types
     *
     * @since   1.0.5
     *
     * @return  array   Image File Types
     */
    public function get_archive_file_types() {

        // Define File Types
        $file_types = array(
            'application/zip',
        );

        /**
         * Defines image file types
         *
         * @since   1.0.5
         *
         * @param   array   $file_types     Image File Types
         */
        $file_types = apply_filters( 'media_library_organizer_mime_get_image_file_types', $file_types );

        // Return
        return $file_types;

    }

}