/**
 * Initializes selectize instances
 *
 * @since 	1.0.7
 */
function mediaLibraryOrganizerSelectizeInit() {

    ( function( $ ) {

	    /**
		 * Selectize Instances: WordPress AJAX Search
		 */
		$( 'select.media-library-organizer-selectize-search' ).each( function() {

			var action 			= $( this ).data( 'action' ),
				name 			= $( this ).attr( 'name' ),
				name_field 		= $( this ).data( 'name-field' ),
				value_field 	= $( this ).data( 'value-field' ),
				method 			= $( this ).data( 'method' ),
				output_fields 	= $( this ).data( 'output-fields' ).split( ',' ),
				plugins 		= $( this ).data( 'plugins' ).split( ',' );

			$( this ).selectize( {
				plugins: 		plugins,
			    delimiter: 		',',
			    valueField: 	value_field, // The value to store in the select when the form is submitted
			    labelField: 	name_field,  // What to display on the output?
			    searchField: 	name_field,  // For some reason, this has to be specified
			    options: 		[],
			    create: 		false,
			    render: {
			        option: function( item, escape ) {

			        	// Build string
			        	var output_string = [];
			        	for ( var i = 0; i < output_fields.length; i++ ) {
			        		output_string.push( item[ output_fields[ i ] ] );
			        	}

			        	// Return output
			        	return '<div>' + output_string.join( ', ' ) + '</div>';

			        }
			    },
			    load: function( query, callback ) {

			        // Bail if the query is too short
			        if ( ! query.length || query.length < 3 ) {
			        	return callback();
			        }

			       	// Send request to Plugin's AJAX endpoint to call Georocket
			       	$.ajax( {
				        url: 		ajaxurl,
				        type: 		method,
				        dataType: 	'json',
				        data: 	{
				            'action': 		action,
				            'query': 		query,
				        },
				        error: function() {

				            callback();

				        },
				        success: function( result ) {

				        	callback( result.data );

				        }
				    } );
			    }
			} );
		} );

	} )( jQuery );

}

/**
 * Destroys selectize instances
 *
 * @since 	1.0.7
 */
function mediaLibraryOrganizerSelectizeDestroy() {

	( function( $ ) {

		$( 'select.media-library-organizer-selectize-search' ).selectize().each( function() {
			this.selectize.destroy();
		} );

	} )( jQuery );

}