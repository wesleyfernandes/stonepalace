=== Media Library Organizer ===
Contributors: wpmedialibrary
Donate link: https://wpmedialibrary.com
Tags: media categories, media organizer, media search, media library
Requires at least: 4.5
Tested up to: 5.2.2
Requires PHP: 5.6
Stable tag: trunk
License: GPLv2 or later

Organize and Search your Media Library, quicker and easier.

== Description ==

= Media Library Organization Plugin =

Media Library Organizer is a simple, effective WordPress Plugin that allows you to categorize and search images, video and other media
in your WordPress Media Library.

= Features =

* Categorize images, video and other media
* Works with all Media Library views (Featured Image, Image Picker, Media Library List, Media Library Grid, Editing a Media Library item)
* Search Media by Category
* Sort Media by Category, Date or Name
* Intuitive Category picker, seamlessly integrated into WordPress' native UI
* Import Categories and Categorization Data from JSON or WordPress standards export file
* Import Categories and Categorization Data from Enhanced Media Library and Media Library Assistant
* Export Categories and Categorization Data to JSON or a WordPress standards export file

= Migrations =

Media Library Organizer has in built importers, allowing you to migrate from other WordPress Media Library Plugins:

* Enhanced Media Library
* Media Library Assistant

> #### Premium Features
> For additional features, <a href="https://wpmedialibrary.com/pricing" title="WordPress Media Library Organizer">purchase a license</a> to access Addons:<br />
>
> - **<a href="https://wpmedialibrary.com/addons/auto-categorization" title="WordPress Auto Categorize Media">Auto Categorization</a>:** Automatically categorize uploaded Images with automatic object detection (e.g. a boat, animal or person)<br />
> - **<a href="https://wpmedialibrary.com/addons/bulk-quick-edit" title="WordPress Bulk and Quick Edit Media">Bulk & Quick Edit Addon</a>:**  Add Bulk and Quick Edit capabilities for Media in <b>both</b> List and Grid views, and define metadata and Categories<b>before</b> uploading to the Media Library<br />
> - **<a href="https://wpmedialibrary.com/addons/defaults" title="WordPress Media Library Defaults">Defaults Addon</a>:** Define Default Metadata and Categories when uploading files, and default Attachment Display Settings (Alignment, Link To, Size) when inserting Media into Posts<br />
> - **<a href="https://wpmedialibrary.com/addons/dynamic-galleries" title="WordPress Dynamic Galleries">Dynamic Galleries</a>:** Extends WordPress' native [gallery] shortcode, displaying Dynamic Galleries by Category, Author, Search Terms and more, with Pagination.<br />
> - **<a href="https://wpmedialibrary.com/addons/auto-categorization" title="WordPress Unzip Archives to Media Library">ZIP Addon</a>:** Automatically extract files within a ZIP file when uploading it to the Media Library, and ZIP multiple existing files in the Media Library<br />
> - **Support:** Access to one on one priority email support<br />
>
> [Upgrade](https://wpmedialibrary.com/pricing)

= Fully Documented =

We understand that no WordPress Plugin is good if you don't know how to use it.  That's why we provide extensive, full documentation
covering all aspects of Media Library Organizer:

<a href="https://wpmedialibrary.com/documentation" title="Media Library Organizer Documentation">https://wpmedialibrary.com/documentation</a>

Best of all, you'll find contextual Documentation links from within Media Library Organizer's interface.

= Fully Supported =

> We truly want Media Library Organizer to be the best WordPress Media Library Organization Plugin.  If you have any questions, or something goes wrong, please reach out to us through the wordpress.org Support Forums. This not only helps fix your support issue, but improves Media Library Organizer for everyone.

== Installation ==

1. Install Media Library Organizer via the Plugins > Add New section of your WordPress Installation, or by uploading the downloaded
ZIP file via Plugins > Add New > Upload Plugin.
2. Active the Media Library Organizer plugin through the 'Plugins' menu in WordPress

== Frequently Asked Questions ==

= Where can I find Bulk Categorization? =

Bulk Editing existing files in the Media Library can be enabled by using the Bulk & Quick Edit Addon.

With the Bulk & Quick Edit Addon, you're able to Bulk Edit Titles, Captions, Alt Tags, Descriptions and Categories for any number of selected files.

The Quick Edit functionality provides the same Quick Edit option you're used to for your Posts and Pages, just applied to your Media Library. 

= Can I define metadata and categories before uploading to the Media Library? =

Yes! This can be enabled by using the Defaults Addon.

On the Media > Add New upload screen, you can define the Title, Caption, Alt Tag, Description and Categories to apply to the file(s) that you upload.

= How do I output Images by Media Category? =

The Dynamic Galleries Addon adds support to WordPress' [gallery] shortcode, where you can specify one or more Media Categories to display Images from.

Other options include displaying by uploader, search terms and paginating Galleries.

== Screenshots ==

1. Categorization Filters and Sort Filters on Media Screen
2. Edit Categories when editing Media
3. Manage Categories
4. Plugin-wide Settings

== Changelog ==

= 1.0.8 =
* Added: Integration with Divi Frontend Builder

= 1.0.7 =
* Added: Integration with Beaver Builder
* Added: Integration with Kallyas Theme's Zion Builder

= 1.0.6 =
* Fix: PHP error: Trying to get property term_id of non-object

= 1.0.5 =
* Added: Settings: Notices can be dismissed / closed
* Fix: Code changes to improve performance
* Fix: CSS / JS: Enqueue on all screens in the Media section of the WordPress Admin UI

= 1.0.4 =
* Fix: Multisite: Network Activation: Ensure installation routines are automatically run on all existing sites
* Fix: Multisite: Network Activation: Ensure installation routines are automatically run on new sites created after Network Activation of Plugin
* Fix: Multisite: Site Activation: Ensure installation routines are automatically run
* Fix: Multisite: Network Deactivation: Ensure uninstallation routines are automatically run on all existing sites
* Fix: Deactivation: PHP warnings
* Fix: Changing a media item's Categories in Grid View would result in console errors and not saving changes

= 1.0.3 =
* Added: Settings: UI Enhancements to allow for a larger number of setting tabs
* Added: Import & Export: UI Enhancements to allow for a larger number of setting tabs
* Fix: Gutenberg: Select or Upload Media: Sparodic issue where cached media queries would produce no results

= 1.0.2 =
* Fix: PHP Warning: call_user_func_array() expects parameter 1 to be a valid callback, class 'Media_Library_Organizer_Install' does not have a method 'install_shutdown' 
* Fix: PHP Notice: Undefined index: post_type in /wp-content/plugins/media-library-organizer/includes/admin/media.php on line 313
* Fix: PHP Notice: Trying to get property of non-object in /wp-content/plugins/media-library-organizer/includes/admin/media.php on line 414

= 1.0.1 =
* Added: Review Helper
* Added: Media View Mode (list or grid) is included in Admin Screen calls for Addons
* Fix: Removed unused upgrade() call
* Fix: Corrected Author information
* Fix: Settings > User Options were not always honored
* Fix: Settings > User Options > Sort Order description
* Fix: Media Library > Grid View > Order By value set to descending by default

= 1.0.0 =
* First release.

== Upgrade Notice ==
