<?php
/**
* Plugin Name: Media Library Organizer
* Plugin URI: https://wpmedialibrary.com
* Version: 1.0.8
* Author: WP Media Library
* Author URI: https://wpmedialibrary.com
* Description: Organize and Search your Media Library, quicker and easier.
*/

/**
 * Media Library Organizer Class
 * 
 * @package   Media_Library_Organizer
 * @author    WP Media Library
 * @version   1.0.0
 */
class Media_Library_Organizer {

    /**
     * Holds the class object.
     *
     * @since   1.0.0
     *
     * @var     object
     */
    public static $instance;

    /**
     * Holds the plugin information object.
     *
     * @since   1.0.0
     *
     * @var     object
     */
    public $plugin = '';

    /**
     * Holds the licensing class object.
     *
     * @since   1.0.0
     *
     * @var     object
     */
    public $licensing = '';

    /**
     * Classes
     *
     * @since   1.0.5
     *
     * @var     array
     */
    public $classes = '';

    /**
    * Constructor. Acts as a bootstrap to load the rest of the plugin
    *
    * @since    1.0.0
    */
    public function __construct() {

        // Plugin Details
        $this->plugin               = new stdClass;
        $this->plugin->name         = 'media-library-organizer';
        $this->plugin->displayName  = 'Media Library Organizer';
        $this->plugin->folder       = plugin_dir_path( __FILE__ );
        $this->plugin->url          = plugin_dir_url( __FILE__ );
        $this->plugin->version      = '1.0.8';
        $this->plugin->home_url     = 'https://wpmedialibrary.com';
        $this->plugin->support_url  = 'https://wpmedialibrary.com/documentation';
        $this->plugin->purchase_url = 'https://wpmedialibrary.com/pricing';
        $this->plugin->review_notice = sprintf( __( 'Thanks for using %s to organize your Media Library!', $this->plugin->name ), $this->plugin->displayName );

        // Licensing Submodule
        if ( ! class_exists( 'Licensing_Update_Manager' ) ) {
            require_once( $this->plugin->folder . '_modules/licensing/lum.php' );
        }
        $this->licensing = new Licensing_Update_Manager( $this->plugin, 'https://wpmedialibrary.com', $this->plugin->name );

        // Defer loading of Plugin Classes
        add_action( 'init', array( $this, 'initialize' ), 1 );
        add_action( 'init', array( $this, 'upgrade' ), 2 );
        add_action( 'init', array( $this, 'maybe_export' ), 3 );

    }

    /**
     * Initializes required and licensed classes
     *
     * @since   1.0.5
     */
    public function initialize() {

        $this->classes = new stdClass;

        // Admin - only load in wp-admin
        if ( is_admin() ) {
            $this->classes->admin           = new Media_Library_Organizer_Admin( self::$instance );
            $this->classes->export          = new Media_Library_Organizer_Export( self::$instance );
            $this->classes->import          = new Media_Library_Organizer_Import( self::$instance );
            $this->classes->install         = new Media_Library_Organizer_Install( self::$instance );
            $this->classes->notices         = new Media_Library_Organizer_Notices( self::$instance );
        }

        // Global - always load
        $this->classes->common              = new Media_Library_Organizer_Common( self::$instance );
        $this->classes->filesystem          = new Media_Library_Organizer_Filesystem( self::$instance );
        $this->classes->mime                = new Media_Library_Organizer_MIME( self::$instance );
        $this->classes->settings            = new Media_Library_Organizer_Settings( self::$instance );
        $this->classes->taxonomy            = new Media_Library_Organizer_Taxonomy( self::$instance );
        $this->classes->user_option         = new Media_Library_Organizer_User_Option( self::$instance );

        // Admin or Frontend Page Builder - only load in wp-admin or when editing a frontend page builder,
        // as these fire various action and filter hooks that visitors don't need
        if ( $this->is_admin_or_frontend_editor() ) {
            $this->classes->editor          = new Media_Library_Organizer_Editor( self::$instance );
            $this->classes->media           = new Media_Library_Organizer_Media( self::$instance );
            $this->classes->taxonomy_walker = new Media_Library_Organizer_Taxonomy_Walker( self::$instance );
            $this->classes->upload          = new Media_Library_Organizer_Upload( self::$instance );
        }
          
    }

    /**
     * Improved version of WordPress' is_admin(), which includes whether we're
     * editing on the frontend using a Page Builder, or a developer / Addon
     * wants to load Editor, Media Management and Upload classes on the frontend
     * of the site.
     *
     * @since   1.0.7
     *
     * @return  bool    Is Admin or Frontend Editor Request
     */
    public function is_admin_or_frontend_editor() {

        // If we're in the wp-admin, return true
        if ( is_admin() ) {
            return true;
        }

        // If the request global exists, check for specific request keys which tell us
        // that we're using a frontend editor
        if ( isset( $_REQUEST ) && ! empty( $_REQUEST ) ) {
            // Beaver Builder
            if ( array_key_exists( 'fl_builder', $_REQUEST ) ) {
                return true;
            }

            // Divi
            if ( array_key_exists( 'et_fb', $_REQUEST ) ) {
                return true;
            }

            // Elementor
            if ( array_key_exists( 'action', $_REQUEST ) && sanitize_text_field( $_REQUEST['action'] ) == 'elementor' ) {
                return true;
            }

            // Kallyas
            if ( array_key_exists( 'zn_pb_edit', $_REQUEST ) ) {
                return true;
            }
        }

        // Assume we're not in the Administration interface
        $is_admin_or_frontend_editor = false;

        /**
         * Filters whether the current request is a WordPress Administration / Frontend Editor request or not.
         *
         * Page Builders can set this to true to allow Media Library Organizer and its Addons to load its
         * functionality.
         *
         * @since   1.0.7
         *
         * @param   bool    $is_admin_or_frontend_editor    Is WordPress Administration / Frontend Editor request.
         * @param   array   $_REQUEST                       $_REQUEST data                
         */
        $is_admin_or_frontend_editor = apply_filters( 'media_library_organizer_is_admin_or_frontend_editor', $is_admin_or_frontend_editor, $_REQUEST );
       
        // Return filtered result 
        return $is_admin_or_frontend_editor;

    }

    /**
     * Runs the upgrade routine once the plugin has loaded
     *
     * @since   1.0.5
     */
    public function upgrade() {

        // Bail if we're not in the WordPress Admin
        if ( ! is_admin() ) {
            return;
        }

        // Run upgrade routine
        $this->get_class( 'install' )->upgrade();

    }

    /**
     * If the Export button was clicked, generate a JSON file and prompt its download now
     *
     * @since   1.0.0
     */
    public function maybe_export() {

        // Bail if we're not in the WordPress Admin interface
        if ( ! is_admin() ) {
            return;
        }

        // Get current screen
        $screen = $this->get_class( 'admin' )->get_current_screen();
        if ( ! $screen || is_wp_error( $screen ) ) {
            return;
        }

        // Check we're on the Import / Export screen
        if ( $screen['name'] != 'import-export' ) {
            return;
        }

        // Check we requested the export action
        if ( ! isset( $_GET['export'] ) ) {
            return;
        }
        if ( $_GET['export'] != 1 ) {
            return;
        }

        // Get export data
        $settings = $this->get_class( 'export' )->export();
        $this->get_class( 'export' )->force_file_download( $settings ); // This ends the PHP operation

    }

    /**
     * Returns the given class
     *
     * @since   1.0.5
     *
     * @param   string  $name   Class Name
     */
    public function get_class( $name ) {

        // If the class hasn't been loaded, throw a WordPress die screen
        // to avoid a PHP fatal error.
        if ( ! isset( $this->classes->{ $name } ) ) {
            // Define the error
            $error = new WP_Error( 'media_library_organizer_get_class', sprintf( __( 'Media Library Organizer: Error: Could not load Plugin class <strong>%s</strong>', 'media-library-organizer' ), $name ) );
             
            // Depending on the request, return or display an error
            // Admin UI
            if ( is_admin() ) {  
                wp_die(
                    $error,
                    __( 'Media Library Organizer: Error', 'media-library-organizer' ),
                    array(
                        'back_link' => true,
                    )
                );
            }

            // Cron / CLI
            return $error;
        }

        // Return the class object
        return $this->classes->{ $name };

    }

    /**
     * Returns the singleton instance of the class.
     *
     * @since   1.0.0
     *
     * @return  object Class.
     */
    public static function get_instance() {

        if ( ! isset( self::$instance ) && ! ( self::$instance instanceof self ) ) {
            self::$instance = new self;
        }

        return self::$instance;

    }

}

/**
 * Define the autoloader for this Plugin
 *
 * @since   1.0.0
 *
 * @param   string  $class_name     The class to load
 */
function Media_Library_Organizer_Autoloader( $class_name ) {

    // Define the required start of the class name
    $class_start_name = 'Media_Library_Organizer';

    // Get the number of parts the class start name has
    $class_parts_count = count( explode( '_', $class_start_name ) );

    // Break the class name into an array
    $class_path = explode( '_', $class_name );

    // Bail if it's not a minimum length (i.e. doesn't potentially have Media_Library_Organizer Autolinker)
    if ( count( $class_path ) < $class_parts_count ) {
        return;
    }

    // Build the base class path for this class
    $base_class_path = '';
    for ( $i = 0; $i < $class_parts_count; $i++ ) {
        $base_class_path .= $class_path[ $i ] . '_';
    }
    $base_class_path = trim( $base_class_path, '_' );

    // Bail if the first parts don't match what we expect
    if ( $base_class_path != $class_start_name ) {
        return;
    }

    // Define the file name we need to include
    $file_name = strtolower( implode( '-', array_slice( $class_path, $class_parts_count ) ) ) . '.php';

    // Define the paths with file name we need to include
    $include_paths = array(
        dirname( __FILE__ ) . '/includes/admin/' . $file_name,
        dirname( __FILE__ ) . '/includes/global/' . $file_name,
    );

    // Iterate through the include paths to find the file
    foreach ( $include_paths as $path_file ) {
        if ( file_exists( $path_file ) ) {
            require_once( $path_file );
            return;
        }
    }

    // If here, we couldn't find the file!

}
spl_autoload_register( 'Media_Library_Organizer_Autoloader' );

// Load Activation, Cron and Deactivation functions
include_once( dirname( __FILE__ ) . '/includes/admin/activation.php' );
include_once( dirname( __FILE__ ) . '/includes/admin/deactivation.php' );
register_activation_hook( __FILE__, 'media_library_organizer_activate' );
add_action( 'wpmu_new_blog', 'media_library_organizer_activate_new_site' );
add_action( 'activate_blog', 'media_library_organizer_activate_new_site' );
register_deactivation_hook( __FILE__, 'media_library_organizer_deactivate' );

/**
 * Main function to return Plugin instance.
 *
 * @since   1.0.5
 */
function Media_Library_Organizer() {
    
    return Media_Library_Organizer::get_instance();

}

// Finally, initialize the Plugin.
$media_library_organizer = Media_Library_Organizer();