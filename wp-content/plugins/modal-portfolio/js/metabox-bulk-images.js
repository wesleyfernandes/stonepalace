;(function($) {
    // Variable de la librairie des médias
    var portfolio_bulk_images_frame = null;

    // On lance la fonction au clic sur le bouton
    $('#portfolio_bulk_images_button').on("click", function(e){
        // Sécurité
        e.preventDefault();
 
        // On réouvre la frame si elle existe déjà...
        if(portfolio_bulk_images_frame) {
            portfolio_bulk_images_frame.open();
            return;
        }
 
        // Personnalisation de la librairie des médias (plein d'options possibles)
        portfolio_bulk_images_frame = wp.media.frames.portfolio_bulk_images_frame = wp.media({
            // title: portfolio_bulk_images.title,
            // button: {text: portfolio_bulk_images.button},
            // library: {type: 'image, video'},
            frame: "select",
            multiple: "add"
        });
 
        // Permet de sélectionner les médias
        portfolio_bulk_images_frame.on('select', function(){
            // Supprime le texte par défaut
            $('.bulk_images_selected').text('');

            // Créé le tableau de données par défaut
            $('.bulk_images_selected').append('<table id="bulk_images_tab"></table>');
            $('#bulk_images_tab').append('<tr><th>'+meta_bulk_images.title_col+'</th><th>'+meta_bulk_images.url_col+'</th><th>'+meta_bulk_images.cat_col+'</th><th>'+meta_bulk_images.desc_col+'</th></tr>');

            // Récupère les images
            var length = portfolio_bulk_images_frame.state().get("selection").length;
            var images = portfolio_bulk_images_frame.state().get("selection").models;

            // Ajoute les données pour chaque image
            for(var iii = 0; iii < length; iii++) {
                // Récupération des champs simples
                var image_url = images[iii].changed.url;
                var image_title = images[iii].changed.title;
                var image_id = images[iii].id;

                // Ajout de chaque <tr>
                $('#bulk_images_tab').append('<tr class="bulk_images_selected_block bulk_image_'+iii+'"></tr>');

                // Ajout des <td>
                $('.bulk_images_selected_block.bulk_image_'+iii).append('<td class="td_title"><input class="bluk_input" name="modal_portfolio_bulk_images_datas['+iii+'][title]" value="' + image_title + '"/></td>');
                $('.bulk_images_selected_block.bulk_image_'+iii).append('<td class="td_url"><i>' + image_url + '</i></td>');
                $('.bulk_images_selected_block.bulk_image_'+iii).append('<td class="td_cat"></td>');
                $('.mpbi_source.portfolio_bulk_images_categories').clone().removeClass("mpbi_source").attr('name', 'modal_portfolio_bulk_images_datas['+iii+'][cat][]').show().appendTo('.bulk_images_selected_block.bulk_image_'+iii+' td.td_cat').multipleSelect({
                    selectAll: false,
                    allSelected: meta_multiple_select.all_selected,
                    countSelected: meta_multiple_select.count_selected,
                    noMatchesFound: meta_multiple_select.no_match,
                });;
                $('.bulk_images_selected_block.bulk_image_'+iii).append('<td class="td_desc"><textarea name="modal_portfolio_bulk_images_datas['+iii+'][desc]" class="bulk_textarea"></textarea></td>');

                // Ajout des champs cachés (datas) dans les <tr>
                $('.bulk_images_selected_block.bulk_image_'+iii).append('<input type="hidden" name="modal_portfolio_bulk_images_datas['+iii+'][id]" value="' + image_id + '"/>');
                $('.bulk_images_selected_block.bulk_image_'+iii).append('<input type="hidden" name="modal_portfolio_bulk_images_datas['+iii+'][url]" value="' + image_url + '"/>');
            }
        });
 
        // Ouvre la fenêtre des médias si ce n'est toujours pas le cas.
        portfolio_bulk_images_frame.open();
    });
})(jQuery);