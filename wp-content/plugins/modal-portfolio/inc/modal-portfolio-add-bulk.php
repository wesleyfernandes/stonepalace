<?php
function modal_portfolio_bulk_images_update() {
	$modal_portfolio_bulk_images_datas = $_POST['modal_portfolio_bulk_images_datas'];

	// Créé les items du portfolio automatiquement
	if(!empty($modal_portfolio_bulk_images_datas)) {
		foreach($modal_portfolio_bulk_images_datas as $bulk_image) {
			$added_post = array(
				'post_type'		=> 'portfolio',
				'post_date'		=> gmdate("Y-m-d H:i:s", time()+rand(1,120)),
				'post_title'    => wp_strip_all_tags($bulk_image['title']),
				'post_content'  => wp_strip_all_tags($bulk_image['desc']),
				'post_status'   => 'publish',
				'meta_input'    => array(
					'portfolio_title' => wp_strip_all_tags($bulk_image['title'])
				),
				// 'tax_input' => array(
					// 'modal-portfolio-cats' => $bulk_image['category'],
				// )
			);

			// Insère dans la base de données
			$inserted_id = wp_insert_post($added_post);

			// Insère dans la bonne catégorie
			wp_set_object_terms($inserted_id, $bulk_image['cat'], 'modal-portfolio-cats', false);

			// Relie la post_thumbnail correspondante
			set_post_thumbnail($inserted_id, $bulk_image['id']);
		}

		// En cas de succès
		if($inserted_id) {
			echo '<div class="notice notice-success is-dismissible"><p>'.__('Data passed successfully. Consult the list of items to check the creation of the corresponding items.', 'sample-text-domain').'</p></div>';
		}
	}
}


function modal_portfolio_add_bulk() {
	// Déclenche la fonction de mise à jour (upload)
	if(isset($_POST['modal_portfolio_bulk_images_action']) && $_POST['modal_portfolio_bulk_images_action'] == __('Create items with added images', 'modal-portfolio')) {
		modal_portfolio_bulk_images_update();
	}

	/*---------------------------------------------------------------------*/
	/*----------------------- Affichage des options -----------------------*/
	/*---------------------------------------------------------------------*/
	echo '<div class="wrap modal-portfolio-admin">';
	echo '<h2 class="icon">'; _e('Add multiple images in bulk', 'modal-portfolio'); echo '</h2><br/>';
	echo '<div class="text">';
	_e('Add multiple images in bulk to save time. This option will automatically create portfolio items with featured images, with related information (category, description...)', 'modal-portfolio'); echo ".<br/>";
	echo '<br/>';
	echo '</div>';

	// Récupération des catégories du portfolio (à masquer par défaut)
	$modal_portfolio_cats = get_terms('modal-portfolio-cats', array('hide_empty' => false));
	if(!empty($modal_portfolio_cats)) {
		echo '<select class="mpbi_source portfolio_bulk_images_categories" name="portfolio_bulk_images_categories" multiple="multiple" style="display:none;">';
		foreach($modal_portfolio_cats as $cat) {
			/*if(!empty($cat->parent)) {
				$cat->name = "&nbsp;&nbsp;&nbsp;".$cat->name;
			}*/
			echo '<option value="'.$cat->slug.'">'.$cat->name.'</option>';
		}
		echo '</select>';
	}
?>
	<div class="block">
		<!-- Formulaire de mise à jour des données -->
	    <form method="post" action="">
	    <div class="">
	        <p class="tr">
	            <input type="button" id="portfolio_bulk_images_button" class="button bulk_button" value="<?php _e("Add multiple images...", "modal-portfolio"); ?>"/>
	            <label for="modal_portfolio_bulk_images_button"><strong><?php _e('Add multiple images in bulk', 'modal-portfolio'); ?></strong></label>
	            <br/><em><?php _e('This option will automatically create portfolio items with featured images and related informations (after saving).', 'modal-portfolio'); ?></em>
	        </p>
	    	<p class="submit">
	        	<input type="submit" name="modal_portfolio_bulk_images_action" class="button-primary" value="<?php _e('Create items with added images', 'modal-portfolio'); ?>" />
	        </p>
		</div>
		<div class="">
	        <h4><?php _e('Selected images', 'modal-portfolio'); ?></h4>
	        <p class="tr bulk_images_selected">
	        	<?php _e('No images added yet', 'modal-portfolio'); ?>
	        </p>
		</div>
	    </form>
	</div>
<?php
}

// Ajoute le Javascript pour la galerie des médias
function js_modal_portfolio_bulk_images_enqueue() {
    global $typenow;
	// Uniquement si le type est portfolio !
    if($typenow == 'portfolio') {
        wp_enqueue_media();

        // Ajoute les scripts JS utiles
		$args = array(
			'all_selected' => __('All selected', 'modal-portfolio'),
			'count_selected' => __('# of % selected', 'modal-portfolio'),
			'no_match' => __('No matches found', 'modal-portfolio'),
		);
        wp_register_script('metabox-multiple-select', plugins_url('js/multiple-select.min.js', dirname(__FILE__)), array('jquery'), false, true);
        wp_localize_script('metabox-multiple-select', 'meta_multiple_select', $args);
        wp_enqueue_script('metabox-multiple-select');
		wp_register_style('metabox-multiple-select-css', plugins_url('css/multiple-select.css', dirname(__FILE__)), false, '1.0.0');
        wp_enqueue_style('metabox-multiple-select-css');

        // Galerie des médias personnalisée
		$args2 = array(
			'title_col' => __('Image title', 'modal-portfolio'),
			'url_col' => __('Image URL', 'modal-portfolio'),
			'cat_col' => __('Image categories', 'modal-portfolio'),
			'desc_col' => __('Image description', 'modal-portfolio'),
		);
        wp_register_script('metabox-bulk-images', plugins_url('js/metabox-bulk-images.min.js', dirname(__FILE__)), array('jquery'), false, true);
        wp_localize_script('metabox-bulk-images', 'meta_bulk_images', $args2);
        wp_enqueue_script('metabox-bulk-images');
    }
}
add_action('admin_enqueue_scripts', 'js_modal_portfolio_bulk_images_enqueue');
?>