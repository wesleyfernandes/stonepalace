!function(o,e){var t=new acf.Model({id:"fieldGroupManager",events:{"submit #post":"onSubmit",'click a[href="#"]':"onClick","click .submitdelete":"onClickTrash"},filters:{find_fields_args:"filterFindFieldArgs"},onSubmit:function(e,t){var i=o("#titlewrap #title");i.val()||(e.preventDefault(),acf.unlockForm(t),alert(acf.__("Field group title is required")),i.focus())},onClick:function(e){e.preventDefault()},onClickTrash:function(e){var t;confirm(acf.__("Move to trash. Are you sure?"))||e.preventDefault()},filterFindFieldArgs:function(e){return e.visible=!0,e}}),i=new acf.Model({id:"screenOptionsManager",wait:"prepare",events:{change:"onChange"},initialize:function(){var e=o("#adv-settings"),t=o("#acf-append-show-on-screen");e.find(".metabox-prefs").append(t.html()),e.find(".metabox-prefs br").remove(),t.remove(),this.$el=o("#acf-field-key-hide"),this.render()},isChecked:function(){return this.$el.prop("checked")},onChange:function(e,t){var i=this.isChecked()?1:0;acf.updateUserSetting("show_field_keys",i),this.render()},render:function(){this.isChecked()?o("#acf-field-group-fields").addClass("show-field-keys"):o("#acf-field-group-fields").removeClass("show-field-keys")}}),n=new acf.Model({actions:{new_field:"onNewField"},onNewField:function(e){if(e.has("append")){var t=e.get("append"),i=e.$el.siblings('[data-name="'+t+'"]').first();if(i.length){var n=i.children(".acf-input"),a=n.children("ul");a.length||(n.wrapInner('<ul class="acf-hl"><li></li></ul>'),a=n.children("ul"));var c=e.$(".acf-input").html(),l=o("<li>"+c+"</li>");a.append(l),a.attr("data-cols",a.children().length),e.remove()}}}})}(jQuery),function(s,i){acf.FieldObject=acf.Model.extend({eventScope:".acf-field-object",events:{"click .edit-field":"onClickEdit","click .delete-field":"onClickDelete","click .duplicate-field":"duplicate","click .move-field":"move","change .field-type":"onChangeType","change .field-required":"onChangeRequired","blur .field-label":"onChangeLabel","blur .field-name":"onChangeName",change:"onChange",changed:"onChanged"},data:{id:0,key:"",type:""},setup:function(e){this.$el=e,this.inherit(e),this.prop("ID"),this.prop("parent"),this.prop("menu_order")},$input:function(e){return s("#"+this.getInputId()+"-"+e)},$meta:function(){return this.$(".meta:first")},$handle:function(){return this.$(".handle:first")},$settings:function(){return this.$(".settings:first")},$setting:function(e){return this.$(".acf-field-settings:first > .acf-field-setting-"+e)},getParent:function(){return acf.getFieldObjects({child:this.$el,limit:1}).pop()},getParents:function(){return acf.getFieldObjects({child:this.$el})},getFields:function(){return acf.getFieldObjects({parent:this.$el})},getInputName:function(){return"acf_fields["+this.get("id")+"]"},getInputId:function(){return"acf_fields-"+this.get("id")},newInput:function(e,t){var i=this.getInputId(),n=this.getInputName();e&&(i+="-"+e,n+="["+e+"]");var a=s("<input />").attr({id:i,name:n,value:t});return this.$("> .meta").append(a),a},getProp:function(e){if(this.has(e))return this.get(e);var t=this.$input(e),i=t.length?t.val():null;return this.set(e,i,!0),i},setProp:function(e,t){var i=this.$input(e),n=i.val();return i.length||(i=this.newInput(e,t)),null===t?i.remove():i.val(t),this.has(e)?this.set(e,t):this.set(e,t,!0),this},prop:function(e,t){return t!==i?this.setProp(e,t):this.getProp(e)},props:function(t){Object.keys(t).map(function(e){this.setProp(e,t[e])},this)},getLabel:function(){var e=this.prop("label");return""===e&&(e=acf.__("(no label)")),e},getName:function(){return this.prop("name")},getType:function(){return this.prop("type")},getTypeLabel:function(){var e=this.prop("type"),t=acf.get("fieldTypes");return t[e]?t[e].label:e},getKey:function(){return this.prop("key")},initialize:function(){},render:function(){var e=this.$(".handle:first"),t=this.prop("menu_order"),i=this.getLabel(),n=this.prop("name"),a=this.getTypeLabel(),c=this.prop("key"),l=this.$input("required").prop("checked");e.find(".acf-icon").html(parseInt(t)+1),l&&(i+=' <span class="acf-required">*</span>'),e.find(".li-field-label strong a").html(i),e.find(".li-field-name").text(n),e.find(".li-field-type").text(a),e.find(".li-field-key").text(c),acf.doAction("render_field_object",this)},refresh:function(){acf.doAction("refresh_field_object",this)},isOpen:function(){return this.$el.hasClass("open")},onClickEdit:function(e){this.isOpen()?this.close():this.open()},open:function(){var e=this.$el.children(".settings");e.slideDown(),this.$el.addClass("open"),acf.doAction("open_field_object",this),this.trigger("openFieldObject"),acf.doAction("show",e)},close:function(){var e=this.$el.children(".settings");e.slideUp(),this.$el.removeClass("open"),acf.doAction("close_field_object",this),this.trigger("closeFieldObject"),acf.doAction("hide",e)},serialize:function(){return acf.serialize(this.$el,this.getInputName())},save:function(e){var t;e=e||"settings","settings"!==this.getProp("save")&&(this.setProp("save",e),this.$el.attr("data-save",e),acf.doAction("save_field_object",this,e))},submit:function(){var e=this.getInputName(),t=this.get("save");this.isOpen()&&this.close(),"settings"==t||("meta"==t?this.$('> .settings [name^="'+e+'"]').remove():this.$('[name^="'+e+'"]').remove()),acf.doAction("submit_field_object",this)},onChange:function(e,t){this.save(),acf.doAction("change_field_object",this)},onChanged:function(e,t,i,n){"save"!=i&&(-1<["menu_order","parent"].indexOf(i)?this.save("meta"):this.save(),-1<["menu_order","label","required","name","type","key"].indexOf(i)&&this.render(),acf.doAction("change_field_object_"+i,this,n))},onChangeLabel:function(e,t){var i=t.val();if(this.set("label",i),""==this.prop("name")){var n=acf.applyFilters("generate_field_object_name",acf.strSanitize(i),this);this.prop("name",n)}},onChangeName:function(e,t){var i=t.val();this.set("name",i),"field_"===i.substr(0,6)&&alert(acf.__('The string "field_" may not be used at the start of a field name'))},onChangeRequired:function(e,t){var i=t.prop("checked")?1:0;this.set("required",i)},delete:function(e){e=acf.parseArgs(e,{animate:!0});var t=this.prop("ID");if(t){var i=s("#_acf_delete_fields"),n=i.val()+"|"+t;i.val(n)}acf.doAction("delete_field_object",this),e.animate?this.removeAnimate():this.remove()},onClickDelete:function(e,t){this.$el.addClass("-hover");var i=this,n=acf.newTooltip({confirmRemove:!0,target:t,confirm:function(){i.delete(!0)},cancel:function(){i.$el.removeClass("-hover")}})},removeAnimate:function(){var e=this,t=this.$el.parent(),i=acf.findFieldObjects({sibling:this.$el});acf.remove({target:this.$el,endHeight:i.length?0:50,complete:function(){e.remove(),acf.doAction("removed_field_object",e,t)}}),acf.doAction("remove_field_object",e,t)},duplicate:function(){var e=acf.uniqid("field_"),t=acf.duplicate({target:this.$el,search:this.get("id"),replace:e});t.attr("data-key",e);var i=acf.getFieldObject(t);this.isOpen()?this.close():i.open();var n=i.$setting("label input");setTimeout(function(){n.focus()},251);var a=i.prop("label"),c=i.prop("name"),l=c.split("_").pop(),o=acf.__("copy");if(s.isNumeric(l)){var r=1*l+1;a=a.replace(l,r),c=c.replace(l,r)}else if(0===l.indexOf(o)){var r;r=(r=1*l.replace(o,""))?r+1:2,a=a.replace(l,o+r),c=c.replace(l,o+r)}else a+=" ("+o+")",c+="_"+o;i.prop("ID",0),i.prop("label",a),i.prop("name",c),i.prop("key",e),acf.doAction("duplicate_field_object",this,i),acf.doAction("append_field_object",i)},wipe:function(){var e=this.get("id"),t=this.get("key"),i=acf.uniqid("field_");acf.rename({target:this.$el,search:e,replace:i}),this.set("id",i),this.set("prevId",e),this.set("prevKey",t),this.prop("key",i),this.prop("ID",0),this.$el.attr("data-key",i),this.$el.attr("data-id",i),acf.doAction("wipe_field_object",this)},move:function(){var t=function(e){return"settings"==e.get("save")},i=t(this);if(i||acf.getFieldObjects({parent:this.$el}).map(function(e){i=t(e)||e.changed}),i)alert(acf.__("This field cannot be moved until its changes have been saved"));else{var n=this.prop("ID"),a=this,c=!1,e=function(){c=acf.newPopup({title:acf.__("Move Custom Field"),loading:!0,width:"300px"});var e={action:"acf/field_group/move_field",field_id:n};s.ajax({url:acf.get("ajaxurl"),data:acf.prepareForAjax(e),type:"post",dataType:"html",success:l})},l=function(e){c.loading(!1),c.content(e),c.on("submit","form",o)},o=function(e,t){e.preventDefault(),acf.startButtonLoading(c.$(".button"));var i={action:"acf/field_group/move_field",field_id:n,field_group_id:c.$("select").val()};s.ajax({url:acf.get("ajaxurl"),data:acf.prepareForAjax(i),type:"post",dataType:"html",success:r})},r=function(e){c.content(e),a.removeAnimate()};e()}},onChangeType:function(e,t){this.changeTimeout&&clearTimeout(this.changeTimeout),this.changeTimeout=this.setTimeout(function(){this.changeType(t.val())},300)},changeType:function(e){var t=this.prop("type"),i=acf.strSlugify("acf-field-object-"+t),n=acf.strSlugify("acf-field-object-"+e);this.$el.removeClass(i).addClass(n),this.$el.attr("data-type",e),this.$el.data("type",e),this.has("xhr")&&this.get("xhr").abort();var a=this.$("> .settings > table > tbody"),c=a.children('[data-setting="'+t+'"]');if(this.set("settings-"+t,c),c.detach(),this.has("settings-"+e)){var l=this.get("settings-"+e);return this.$setting("conditional_logic").before(l),void this.set("type",e)}var o=s('<tr class="acf-field"><td class="acf-label"></td><td class="acf-input"><div class="acf-loading"></div></td></tr>');this.$setting("conditional_logic").before(o);var r={action:"acf/field_group/render_field_settings",field:this.serialize(),prefix:this.getInputName()},d=s.ajax({url:acf.get("ajaxurl"),data:acf.prepareForAjax(r),type:"post",dataType:"html",context:this,success:function(e){e&&(o.after(e),acf.doAction("append",a))},complete:function(){o.remove(),this.set("type",e)}});this.set("xhr",d)},updateParent:function(){var e=acf.get("post_id"),t=this.getParent();t&&(e=parseInt(t.prop("ID"))||t.prop("key")),this.prop("parent",e)}})}(jQuery),function(i,e){var l=function(e){return acf.strPascalCase(e||"")+"FieldSetting"};acf.registerFieldSetting=function(e){var t=e.prototype,i=l(t.type+" "+t.name);this.models[i]=e},acf.newFieldSetting=function(e){var t=e.get("setting")||"",i=e.get("name")||"",n=l(t+" "+i),a=acf.models[n]||null,c;return null!==a&&new a(e)},acf.getFieldSetting=function(e){return e instanceof jQuery&&(e=acf.getField(e)),e.setting};var t=new acf.Model({actions:{new_field:"onNewField"},onNewField:function(e){e.setting=acf.newFieldSetting(e)}});acf.FieldSetting=acf.Model.extend({field:!1,type:"",name:"",wait:"ready",eventScope:".acf-field",events:{change:"render"},setup:function(e){var t=e.$el;this.$el=t,this.field=e,this.$fieldObject=t.closest(".acf-field-object"),this.fieldObject=acf.getFieldObject(this.$fieldObject),i.extend(this.data,e.data)},initialize:function(){this.render()},render:function(){}});var n=acf.FieldSetting.extend({type:"",name:"",render:function(){var e=this.$('input[type="radio"]:checked');"other"!=e.val()&&this.$('input[type="text"]').val(e.val())}}),a=n.extend({type:"date_picker",name:"display_format"}),c=n.extend({type:"date_picker",name:"return_format"});acf.registerFieldSetting(a),acf.registerFieldSetting(c);var o=n.extend({type:"date_time_picker",name:"display_format"}),r=n.extend({type:"date_time_picker",name:"return_format"});acf.registerFieldSetting(o),acf.registerFieldSetting(r);var d=n.extend({type:"time_picker",name:"display_format"}),s=n.extend({name:"time_picker",name:"return_format"});acf.registerFieldSetting(d),acf.registerFieldSetting(s)}(jQuery),function(d,e){var t=acf.FieldSetting.extend({type:"",name:"conditional_logic",events:{"change .conditions-toggle":"onChangeToggle","click .add-conditional-group":"onClickAddGroup","focus .condition-rule-field":"onFocusField","change .condition-rule-field":"onChangeField","change .condition-rule-operator":"onChangeOperator","click .add-conditional-rule":"onClickAdd","click .remove-conditional-rule":"onClickRemove"},$rule:!1,scope:function(e){return this.$rule=e,this},ruleData:function(e,t){return this.$rule.data.apply(this.$rule,arguments)},$input:function(e){return this.$rule.find(".condition-rule-"+e)},$td:function(e){return this.$rule.find("td."+e)},$toggle:function(){return this.$(".conditions-toggle")},$control:function(){return this.$(".rule-groups")},$groups:function(){return this.$(".rule-group")},$rules:function(){return this.$(".rule")},open:function(){var e=this.$control();e.show(),acf.enable(e)},close:function(){var e=this.$control();e.hide(),acf.disable(e)},render:function(){this.$toggle().prop("checked")?(this.renderRules(),this.open()):this.close()},renderRules:function(){var e=this;this.$rules().each(function(){e.renderRule(d(this))})},renderRule:function(e){this.scope(e),this.renderField(),this.renderOperator(),this.renderValue()},renderField:function(){var a=[],e=[],c=this.fieldObject.cid,t=this.$input("field");acf.getFieldObjects().map(function(e){var t={id:e.getKey(),text:e.getLabel()},i;e.cid===c&&(t.text+=acf.__("(this field)"),t.disabled=!0),acf.getConditionTypes({fieldType:e.getType()}).length||(t.disabled=!0);var n=e.getParents().length;t.text="- ".repeat(n)+t.text,a.push(t)}),a.length||a.push({id:"",text:acf.__("No toggle fields available")}),acf.renderSelect(t,a),this.ruleData("field",t.val())},renderOperator:function(){if(this.ruleData("field")){var e=this.$input("operator"),t=e.val(),i=[];null===e.val()&&acf.renderSelect(e,[{id:this.ruleData("operator"),text:""}]);var n=acf.findFieldObject(this.ruleData("field")),a=acf.getFieldObject(n),c;acf.getConditionTypes({fieldType:a.getType()}).map(function(e){i.push({id:e.prototype.operator,text:acf.strEscape(e.prototype.label)})}),acf.renderSelect(e,i),this.ruleData("operator",e.val())}},renderValue:function(){if(this.ruleData("field")&&this.ruleData("operator")){var t=this.$input("value"),e=this.$td("value"),i=t.val(),n=acf.findFieldObject(this.ruleData("field")),a=acf.getFieldObject(n),c,l,o=acf.getConditionTypes({fieldType:a.getType(),operator:this.ruleData("operator")})[0].prototype.choices(a);if(o instanceof Array){var r=d("<select></select>");acf.renderSelect(r,o)}else var r=d(o);t.detach(),e.html(r),setTimeout(function(){["class","name","id"].map(function(e){r.attr(e,t.attr(e))})},0),r.prop("disabled")||acf.val(r,i,!0),this.ruleData("value",r.val())}},onChangeToggle:function(){this.render()},onClickAddGroup:function(e,t){this.addGroup()},addGroup:function(){var e=this.$(".rule-group:last"),t=acf.duplicate(e);t.find("h4").text(acf.__("or")),t.find("tr").not(":first").remove(),this.fieldObject.save()},onFocusField:function(e,t){this.renderField()},onChangeField:function(e,t){this.scope(t.closest(".rule")),this.ruleData("field",t.val()),this.renderOperator(),this.renderValue()},onChangeOperator:function(e,t){this.scope(t.closest(".rule")),this.ruleData("operator",t.val()),this.renderValue()},onClickAdd:function(e,t){var i=acf.duplicate(t.closest(".rule"));this.renderRule(i)},onClickRemove:function(e,t){var i=t.closest(".rule");this.fieldObject.save(),0==i.siblings(".rule").length&&i.closest(".rule-group").remove(),i.remove()}});acf.registerFieldSetting(t);var i=new acf.Model({actions:{duplicate_field_objects:"onDuplicateFieldObjects"},onDuplicateFieldObjects:function(e,t,i){var n={},a=d();e.map(function(e){n[e.get("prevKey")]=e.get("key"),a=a.add(e.$(".condition-rule-field"))}),a.each(function(){var e=d(this),t=e.val();t&&n[t]&&(e.find("option:selected").attr("value",n[t]),e.val(n[t]))})}})}(jQuery),function(r,e){acf.findFieldObject=function(e){return acf.findFieldObjects({key:e,limit:1})},acf.findFieldObjects=function(e){e=e||{};var t=".acf-field-object",i=!1;return(e=acf.parseArgs(e,{id:"",key:"",type:"",limit:!1,list:null,parent:!1,sibling:!1,child:!1})).id&&(t+='[data-id="'+e.id+'"]'),e.key&&(t+='[data-key="'+e.key+'"]'),e.type&&(t+='[data-type="'+e.type+'"]'),i=e.list?e.list.children(t):e.parent?e.parent.find(t):e.sibling?e.sibling.siblings(t):e.child?e.child.parents(t):r(t),e.limit&&(i=i.slice(0,e.limit)),i},acf.getFieldObject=function(e){"string"==typeof e&&(e=acf.findFieldObject(e));var t=e.data("acf");return t||(t=acf.newFieldObject(e)),t},acf.getFieldObjects=function(e){var t=acf.findFieldObjects(e),i=[];return t.each(function(){var e=acf.getFieldObject(r(this));i.push(e)}),i},acf.newFieldObject=function(e){var t=new acf.FieldObject(e);return acf.doAction("new_field_object",t),t};var t=new acf.Model({priority:5,initialize:function(){var e;["prepare","ready","append","remove"].map(function(e){this.addFieldActions(e)},this)},addFieldActions:function(e){var n=e+"_field_objects",a=e+"_field_object",c=e+"FieldObject",t=function(e){var t=acf.getFieldObjects({parent:e});if(t.length){var i=acf.arrayArgs(arguments);i.splice(0,1,n,t),acf.doAction.apply(null,i)}},i=function(e){var t=acf.arrayArgs(arguments);t.unshift(a),e.map(function(e){t[1]=e,acf.doAction.apply(null,t)})},l=function(t){var i=acf.arrayArgs(arguments),e;i.unshift(a),["type","name","key"].map(function(e){i[0]=a+"/"+e+"="+t.get(e),acf.doAction.apply(null,i)}),i.splice(0,2),t.trigger(c,i)};acf.addAction(e,t,5),acf.addAction(n,i,5),acf.addAction(a,l,5)}}),i=new acf.Model({id:"fieldManager",events:{"submit #post":"onSubmit","mouseenter .acf-field-list":"onHoverSortable","click .add-field":"onClickAdd"},actions:{removed_field_object:"onRemovedField",sortstop_field_object:"onReorderField",delete_field_object:"onDeleteField",change_field_object_type:"onChangeFieldType",duplicate_field_object:"onDuplicateField"},onSubmit:function(e,t){var i;acf.getFieldObjects().map(function(e){e.submit()})},setFieldMenuOrder:function(e){this.renderFields(e.$el.parent())},onHoverSortable:function(e,n){n.hasClass("ui-sortable")||n.sortable({handle:".acf-sortable-handle",connectWith:".acf-field-list",start:function(e,t){var i=acf.getFieldObject(t.item);t.placeholder.height(t.item.height()),acf.doAction("sortstart_field_object",i,n)},update:function(e,t){var i=acf.getFieldObject(t.item);acf.doAction("sortstop_field_object",i,n)}})},onRemovedField:function(e,t){this.renderFields(t)},onReorderField:function(e,t){e.updateParent(),this.renderFields(t)},onDeleteField:function(e){e.getFields().map(function(e){e.delete({animate:!1})})},onChangeFieldType:function(e){},onDuplicateField:function(e,t){var i=t.getFields();i.length&&(i.map(function(e){e.wipe(),e.updateParent()}),acf.doAction("duplicate_field_objects",i,t,e)),this.setFieldMenuOrder(t)},renderFields:function(e){var t=acf.getFieldObjects({list:e});t.length?(e.removeClass("-empty"),t.map(function(e,t){e.prop("menu_order",t)})):e.addClass("-empty")},onClickAdd:function(e,t){var i=t.closest(".acf-tfoot").siblings(".acf-field-list");this.addField(i)},addField:function(i){var e=r("#tmpl-acf-field").html(),t=r(e),n=t.data("id"),a=acf.uniqid("field_"),c=acf.duplicate({target:t,search:n,replace:a,append:function(e,t){i.append(t)}}),l=acf.getFieldObject(c);l.prop("key",a),l.prop("ID",0),l.prop("label",""),l.prop("name",""),c.attr("data-key",a),c.attr("data-id",a),l.updateParent();var o=l.$input("label");setTimeout(function(){o.focus()},251),l.open(),this.renderFields(i),acf.doAction("add_field_object",l),acf.doAction("append_field_object",l)}})}(jQuery),function(a,e){var t=new acf.Model({id:"locationManager",wait:"ready",events:{"click .add-location-rule":"onClickAddRule","click .add-location-group":"onClickAddGroup","click .remove-location-rule":"onClickRemoveRule","change .refresh-location-rule":"onChangeRemoveRule"},initialize:function(){this.$el=a("#acf-field-group-locations")},onClickAddRule:function(e,t){this.addRule(t.closest("tr"))},onClickRemoveRule:function(e,t){this.removeRule(t.closest("tr"))},onChangeRemoveRule:function(e,t){this.changeRule(t.closest("tr"))},onClickAddGroup:function(e,t){this.addGroup()},addRule:function(e){acf.duplicate(e)},removeRule:function(e){0==e.siblings("tr").length?e.closest(".rule-group").remove():e.remove()},changeRule:function(t){var e=t.closest(".rule-group"),i=t.find("td.param select").attr("name").replace("[param]",""),n={action:"acf/field_group/render_location_rule"};n.rule=acf.serialize(t,i),n.rule.id=t.data("id"),n.rule.group=e.data("id"),acf.disable(t.find("td.value")),a.ajax({url:acf.get("ajaxurl"),data:acf.prepareForAjax(n),type:"post",dataType:"html",success:function(e){e&&t.replaceWith(e)}})},addGroup:function(){var e=this.$(".rule-group:last");$group2=acf.duplicate(e),$group2.find("h4").text(acf.__("or")),$group2.find("tr").not(":first").remove()}})}(jQuery),function(l,i){var e=acf.getCompatibility(acf);e.field_group={save_field:function(e,t){t=t!==i?t:"settings",acf.getFieldObject(e).save(t)},delete_field:function(e,t){t=t===i||t,acf.getFieldObject(e).delete({animate:t})},update_field_meta:function(e,t,i){acf.getFieldObject(e).prop(t,i)},delete_field_meta:function(e,t){acf.getFieldObject(e).prop(t,null)}},e.field_group.field_object=acf.model.extend({type:"",o:{},$field:null,$settings:null,tag:function(e){var t=this.type,i=e.split("_");return i.splice(1,0,"field"),e=i.join("_"),t&&(e+="/type="+t),e},selector:function(){var e=".acf-field-object",t=this.type;return t&&(e+="-"+t,e=acf.str_replace("_","-",e)),e},_add_action:function(e,t){var i=this;acf.add_action(this.tag(e),function(e){i.set("$field",e),i[t].apply(i,arguments)})},_add_filter:function(e,t){var i=this;acf.add_filter(this.tag(e),function(e){i.set("$field",e),i[t].apply(i,arguments)})},_add_event:function(e,t){var i=this,n=e.substr(0,e.indexOf(" ")),a=e.substr(e.indexOf(" ")+1),c=this.selector();l(document).on(n,c+" "+a,function(e){e.$el=l(this),e.$field=e.$el.closest(".acf-field-object"),i.set("$field",e.$field),i[t].apply(i,[e])})},_set_$field:function(){this.o=this.$field.data(),this.$settings=this.$field.find("> .settings > table > tbody"),this.focus()},focus:function(){},setting:function(e){return this.$settings.find("> .acf-field-setting-"+e)}});var t=new acf.Model({actions:{open_field_object:"onOpenFieldObject",close_field_object:"onCloseFieldObject",add_field_object:"onAddFieldObject",duplicate_field_object:"onDuplicateFieldObject",delete_field_object:"onDeleteFieldObject",change_field_object_type:"onChangeFieldObjectType",change_field_object_label:"onChangeFieldObjectLabel",change_field_object_name:"onChangeFieldObjectName",change_field_object_parent:"onChangeFieldObjectParent",sortstop_field_object:"onChangeFieldObjectParent"},onOpenFieldObject:function(e){acf.doAction("open_field",e.$el),acf.doAction("open_field/type="+e.get("type"),e.$el),acf.doAction("render_field_settings",e.$el),acf.doAction("render_field_settings/type="+e.get("type"),e.$el)},onCloseFieldObject:function(e){acf.doAction("close_field",e.$el),acf.doAction("close_field/type="+e.get("type"),e.$el)},onAddFieldObject:function(e){acf.doAction("add_field",e.$el),acf.doAction("add_field/type="+e.get("type"),e.$el)},onDuplicateFieldObject:function(e){acf.doAction("duplicate_field",e.$el),acf.doAction("duplicate_field/type="+e.get("type"),e.$el)},onDeleteFieldObject:function(e){acf.doAction("delete_field",e.$el),acf.doAction("delete_field/type="+e.get("type"),e.$el)},onChangeFieldObjectType:function(e){acf.doAction("change_field_type",e.$el),acf.doAction("change_field_type/type="+e.get("type"),e.$el),acf.doAction("render_field_settings",e.$el),acf.doAction("render_field_settings/type="+e.get("type"),e.$el)},onChangeFieldObjectLabel:function(e){acf.doAction("change_field_label",e.$el),acf.doAction("change_field_label/type="+e.get("type"),e.$el)},onChangeFieldObjectName:function(e){acf.doAction("change_field_name",e.$el),acf.doAction("change_field_name/type="+e.get("type"),e.$el)},onChangeFieldObjectParent:function(e){acf.doAction("update_field_parent",e.$el)}})}(jQuery);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 (function($, undefined){
		
	/**
	*  acf
	*
	*  description
	*
	*  @date	14/12/17
	*  @since	5.6.5
	*
	*  @param	type $var Description. Default.
	*  @return	type Description.
	*/
		
	// The global acf object
	var acf = {};
	
	// Set as a browser global
	window.acf = acf;
	
	/** @var object Data sent from PHP */
	acf.data = {};
	
	
	/**
	*  get
	*
	*  Gets a specific data value
	*
	*  @date	14/12/17
	*  @since	5.6.5
	*
	*  @param	string name
	*  @return	mixed
	*/
	
	acf.get = function( name ){
		return this.data[name] || null;
	};
	
	
	/**
	*  has
	*
	*  Returns `true` if the data exists and is not null
	*
	*  @date	14/12/17
	*  @since	5.6.5
	*
	*  @param	string name
	*  @return	boolean
	*/
	
	acf.has = function( name ){
		return this.get(name) !== null;
	};
	
	
	/**
	*  set
	*
	*  Sets a specific data value
	*
	*  @date	14/12/17
	*  @since	5.6.5
	*
	*  @param	string name
	*  @param	mixed value
	*  @return	this
	*/
	
	acf.set = function( name, value ){
		this.data[ name ] = value;
		return this;
	};
	
	
	/**
	*  uniqueId
	*
	*  Returns a unique ID
	*
	*  @date	9/11/17
	*  @since	5.6.3
	*
	*  @param	string prefix Optional prefix.
	*  @return	string
	*/
	
	var idCounter = 0;
	acf.uniqueId = function(prefix){
		var id = ++idCounter + '';
		return prefix ? prefix + id : id;
	};
	
	/**
	*  acf.uniqueArray
	*
	*  Returns a new array with only unique values
	*  Credit: https://stackoverflow.com/questions/1960473/get-all-unique-values-in-an-array-remove-duplicates
	*
	*  @date	23/3/18
	*  @since	5.6.9
	*
	*  @param	type $var Description. Default.
	*  @return	type Description.
	*/
	
	acf.uniqueArray = function( array ){
		function onlyUnique(value, index, self) { 
		    return self.indexOf(value) === index;
		}
		return array.filter( onlyUnique );
	};
	
	/**
	*  uniqid
	*
	*  Returns a unique ID (PHP version)
	*
	*  @date	9/11/17
	*  @since	5.6.3
	*  @source	http://locutus.io/php/misc/uniqid/
	*
	*  @param	string prefix Optional prefix.
	*  @return	string
	*/
	
	var uniqidSeed = '';
	acf.uniqid = function(prefix, moreEntropy){
		//  discuss at: http://locutus.io/php/uniqid/
		// original by: Kevin van Zonneveld (http://kvz.io)
		//  revised by: Kankrelune (http://www.webfaktory.info/)
		//      note 1: Uses an internal counter (in locutus global) to avoid collision
		//   example 1: var $id = uniqid()
		//   example 1: var $result = $id.length === 13
		//   returns 1: true
		//   example 2: var $id = uniqid('foo')
		//   example 2: var $result = $id.length === (13 + 'foo'.length)
		//   returns 2: true
		//   example 3: var $id = uniqid('bar', true)
		//   example 3: var $result = $id.length === (23 + 'bar'.length)
		//   returns 3: true
		if (typeof prefix === 'undefined') {
			prefix = '';
		}
		
		var retId;
		var formatSeed = function(seed, reqWidth) {
			seed = parseInt(seed, 10).toString(16); // to hex str
			if (reqWidth < seed.length) { // so long we split
				return seed.slice(seed.length - reqWidth);
			}
			if (reqWidth > seed.length) { // so short we pad
				return Array(1 + (reqWidth - seed.length)).join('0') + seed;
			}
			return seed;
		};
		
		if (!uniqidSeed) { // init seed with big random int
			uniqidSeed = Math.floor(Math.random() * 0x75bcd15);
		}
		uniqidSeed++;
		
		retId = prefix; // start with prefix, add current milliseconds hex string
		retId += formatSeed(parseInt(new Date().getTime() / 1000, 10), 8);
		retId += formatSeed(uniqidSeed, 5); // add seed hex string
		if (moreEntropy) {
			// for more entropy we add a float lower to 10
			retId += (Math.random() * 10).toFixed(8).toString();
		}
		
		return retId;
	};
	
	
	/**
	*  strReplace
	*
	*  Performs a string replace
	*
	*  @date	14/12/17
	*  @since	5.6.5
	*
	*  @param	string search
	*  @param	string replace
	*  @param	string subject
	*  @return	string
	*/
	
	acf.strReplace = function( search, replace, subject ){
		return subject.split(search).join(replace);
	};
	
	
	/**
	*  strCamelCase
	*
	*  Converts a string into camelCase
	*  Thanks to https://stackoverflow.com/questions/2970525/converting-any-string-into-camel-case
	*
	*  @date	14/12/17
	*  @since	5.6.5
	*
	*  @param	string str
	*  @return	string
	*/
	
	acf.strCamelCase = function( str ){
		
		// replace [_-] characters with space
		str = str.replace(/[_-]/g, ' ');
		
		// camelCase
		str = str.replace(/(?:^\w|\b\w|\s+)/g, function(match, index) {
			if (+match === 0) return ""; // or if (/\s+/.test(match)) for white spaces
			return index == 0 ? match.toLowerCase() : match.toUpperCase();
		});
		
		// return
		return str;
	};
	
	/**
	*  strPascalCase
	*
	*  Converts a string into PascalCase
	*  Thanks to https://stackoverflow.com/questions/1026069/how-do-i-make-the-first-letter-of-a-string-uppercase-in-javascript
	*
	*  @date	14/12/17
	*  @since	5.6.5
	*
	*  @param	string str
	*  @return	string
	*/
	
	acf.strPascalCase = function( str ){
		var camel = acf.strCamelCase( str );
		return camel.charAt(0).toUpperCase() + camel.slice(1); 
	};
	
	/**
	*  acf.strSlugify
	*
	*  Converts a string into a HTML class friendly slug
	*
	*  @date	21/3/18
	*  @since	5.6.9
	*
	*  @param	string str
	*  @return	string
	*/
	
	acf.strSlugify = function( str ){
		return acf.strReplace( '_', '-', str.toLowerCase() );
	};
	
	
	acf.strSanitize = function( str ){
		
		// chars (https://jsperf.com/replace-foreign-characters)
		var map = {
            "À": "A",
            "Á": "A",
            "Â": "A",
            "Ã": "A",
            "Ä": "A",
            "Å": "A",
            "Æ": "AE",
            "Ç": "C",
            "È": "E",
            "É": "E",
            "Ê": "E",
            "Ë": "E",
            "Ì": "I",
            "Í": "I",
            "Î": "I",
            "Ï": "I",
            "Ð": "D",
            "Ñ": "N",
            "Ò": "O",
            "Ó": "O",
            "Ô": "O",
            "Õ": "O",
            "Ö": "O",
            "Ø": "O",
            "Ù": "U",
            "Ú": "U",
            "Û": "U",
            "Ü": "U",
            "Ý": "Y",
            "ß": "s",
            "à": "a",
            "á": "a",
            "â": "a",
            "ã": "a",
            "ä": "a",
            "å": "a",
            "æ": "ae",
            "ç": "c",
            "è": "e",
            "é": "e",
            "ê": "e",
            "ë": "e",
            "ì": "i",
            "í": "i",
            "î": "i",
            "ï": "i",
            "ñ": "n",
            "ò": "o",
            "ó": "o",
            "ô": "o",
            "õ": "o",
            "ö": "o",
            "ø": "o",
            "ù": "u",
            "ú": "u",
            "û": "u",
            "ü": "u",
            "ý": "y",
            "ÿ": "y",
            "Ā": "A",
            "ā": "a",
            "Ă": "A",
            "ă": "a",
            "Ą": "A",
            "ą": "a",
            "Ć": "C",
            "ć": "c",
            "Ĉ": "C",
            "ĉ": "c",
            "Ċ": "C",
            "ċ": "c",
            "Č": "C",
            "č": "c",
            "Ď": "D",
            "ď": "d",
            "Đ": "D",
            "đ": "d",
            "Ē": "E",
            "ē": "e",
            "Ĕ": "E",
            "ĕ": "e",
            "Ė": "E",
            "ė": "e",
            "Ę": "E",
            "ę": "e",
            "Ě": "E",
            "ě": "e",
            "Ĝ": "G",
            "ĝ": "g",
            "Ğ": "G",
            "ğ": "g",
            "Ġ": "G",
            "ġ": "g",
            "Ģ": "G",
            "ģ": "g",
            "Ĥ": "H",
            "ĥ": "h",
            "Ħ": "H",
            "ħ": "h",
            "Ĩ": "I",
            "ĩ": "i",
            "Ī": "I",
            "ī": "i",
            "Ĭ": "I",
            "ĭ": "i",
            "Į": "I",
            "į": "i",
            "İ": "I",
            "ı": "i",
            "Ĳ": "IJ",
            "ĳ": "ij",
            "Ĵ": "J",
            "ĵ": "j",
            "Ķ": "K",
            "ķ": "k",
            "Ĺ": "L",
            "ĺ": "l",
            "Ļ": "L",
            "ļ": "l",
            "Ľ": "L",
            "ľ": "l",
            "Ŀ": "L",
            "ŀ": "l",
            "Ł": "l",
            "ł": "l",
            "Ń": "N",
            "ń": "n",
            "Ņ": "N",
            "ņ": "n",
            "Ň": "N",
            "ň": "n",
            "ŉ": "n",
            "Ō": "O",
            "ō": "o",
            "Ŏ": "O",
            "ŏ": "o",
            "Ő": "O",
            "ő": "o",
            "Œ": "OE",
            "œ": "oe",
            "Ŕ": "R",
            "ŕ": "r",
            "Ŗ": "R",
            "ŗ": "r",
            "Ř": "R",
            "ř": "r",
            "Ś": "S",
            "ś": "s",
            "Ŝ": "S",
            "ŝ": "s",
            "Ş": "S",
            "ş": "s",
            "Š": "S",
            "š": "s",
            "Ţ": "T",
            "ţ": "t",
            "Ť": "T",
            "ť": "t",
            "Ŧ": "T",
            "ŧ": "t",
            "Ũ": "U",
            "ũ": "u",
            "Ū": "U",
            "ū": "u",
            "Ŭ": "U",
            "ŭ": "u",
            "Ů": "U",
            "ů": "u",
            "Ű": "U",
            "ű": "u",
            "Ų": "U",
            "ų": "u",
            "Ŵ": "W",
            "ŵ": "w",
            "Ŷ": "Y",
            "ŷ": "y",
            "Ÿ": "Y",
            "Ź": "Z",
            "ź": "z",
            "Ż": "Z",
            "ż": "z",
            "Ž": "Z",
            "ž": "z",
            "ſ": "s",
            "ƒ": "f",
            "Ơ": "O",
            "ơ": "o",
            "Ư": "U",
            "ư": "u",
            "Ǎ": "A",
            "ǎ": "a",
            "Ǐ": "I",
            "ǐ": "i",
            "Ǒ": "O",
            "ǒ": "o",
            "Ǔ": "U",
            "ǔ": "u",
            "Ǖ": "U",
            "ǖ": "u",
            "Ǘ": "U",
            "ǘ": "u",
            "Ǚ": "U",
            "ǚ": "u",
            "Ǜ": "U",
            "ǜ": "u",
            "Ǻ": "A",
            "ǻ": "a",
            "Ǽ": "AE",
            "ǽ": "ae",
            "Ǿ": "O",
            "ǿ": "o",
            
            // extra
            ' ': '_',
			"'": '',
			'?': '',
			'/': '',
			'\\': '',
			'.': '',
			',': '',
			'`': '',
			'>': '',
			'<': '',
			'"': '',
			'[': '',
			']': '',
			'|': '',
			'{': '',
			'}': '',
			'(': '',
			')': ''
        };
		
		// vars
		var nonWord = /\W/g;
        var mapping = function (c) {
            return (map[c] !== undefined) ? map[c] : c;
        };
        
        // replace
        str = str.replace(nonWord, mapping);
	    
	    // lowercase
	    str = str.toLowerCase();
	    
	    // return
	    return str;	
	};
	
	/**
	*  acf.strMatch
	*
	*  Returns the number of characters that match between two strings
	*
	*  @date	1/2/18
	*  @since	5.6.5
	*
	*  @param	type $var Description. Default.
	*  @return	type Description.
	*/
	
	acf.strMatch = function( s1, s2 ){
		
		// vars
		var val = 0;
		var min = Math.min( s1.length, s2.length );
		
		// loop
		for( var i = 0; i < min; i++ ) {
			if( s1[i] !== s2[i] ) {
				break;
			}
			val++;
		}
		
		// return
		return val;
	};
	
	/**
	*  acf.decode
	*
	*  description
	*
	*  @date	13/1/18
	*  @since	5.6.5
	*
	*  @param	type $var Description. Default.
	*  @return	type Description.
	*/
	
	acf.decode = function( string ){
		return $('<textarea/>').html( string ).text();
	};
	
	/**
	*  acf.strEscape
	*
	*  description
	*
	*  @date	3/2/18
	*  @since	5.6.5
	*
	*  @param	type $var Description. Default.
	*  @return	type Description.
	*/
	
	acf.strEscape = function( string ){
		
		var entityMap = {
		  '&': '&amp;',
		  '<': '&lt;',
		  '>': '&gt;',
		  '"': '&quot;',
		  "'": '&#39;',
		  '/': '&#x2F;',
		  '`': '&#x60;',
		  '=': '&#x3D;'
		};
		
		return String(string).replace(/[&<>"'`=\/]/g, function (s) {
			return entityMap[s];
		});
	};
	
	/**
	*  parseArgs
	*
	*  Merges together defaults and args much like the WP wp_parse_args function
	*
	*  @date	14/12/17
	*  @since	5.6.5
	*
	*  @param	object args
	*  @param	object defaults
	*  @return	object
	*/
	
	acf.parseArgs = function( args, defaults ){
		if( typeof args !== 'object' ) args = {};
		if( typeof defaults !== 'object' ) defaults = {};
		return $.extend({}, defaults, args);
	}
	
	/**
	*  __
	*
	*  Retrieve the translation of $text.
	*
	*  @date	16/4/18
	*  @since	5.6.9
	*
	*  @param	string text Text to translate.
	*  @return	string Translated text.
	*/
	
	if( window.acfL10n == undefined ) {
		acfL10n = {};
	}
	
	acf.__ = function( text ){
		return acfL10n[ text ] || text;
	};
	
	/**
	*  _x
	*
	*  Retrieve translated string with gettext context.
	*
	*  @date	16/4/18
	*  @since	5.6.9
	*
	*  @param	string text Text to translate.
	*  @param	string context Context information for the translators.
	*  @return	string Translated text.
	*/
	
	acf._x = function( text, context ){
		return acfL10n[ text + '.' + context ] || acfL10n[ text ] || text;
	};
	
	/**
	*  _n
	*
	*  Retrieve the plural or single form based on the amount. 
	*
	*  @date	16/4/18
	*  @since	5.6.9
	*
	*  @param	string single Single text to translate.
	*  @param	string plural Plural text to translate.
	*  @param	int number The number to compare against.
	*  @return	string Translated text.
	*/
	
	acf._n = function( single, plural, number ){
		if( number == 1 ) {
			return acf.__(single);
		} else {
			return acf.__(plural);
		}
	};
	
	acf.isArray = function( a ){
		return Array.isArray(a);
	};
	
	acf.isObject = function( a ){
		return ( typeof a === 'object' );
	}
	
	/**
	*  serialize
	*
	*  description
	*
	*  @date	24/12/17
	*  @since	5.6.5
	*
	*  @param	type $var Description. Default.
	*  @return	type Description.
	*/
	
	var buildObject = function( obj, name, value ){
		
		// replace [] with placeholder
		name = name.replace('[]', '[%%index%%]');
		
		// vars
		var keys = name.match(/([^\[\]])+/g);
		if( !keys ) return;
		var length = keys.length;
		var ref = obj;
		
		// loop
		for( var i = 0; i < length; i++ ) {
			
			// vars
			var key = String( keys[i] );
			
			// value
			if( i == length - 1 ) {
				
				// %%index%%
				if( key === '%%index%%' ) {
					ref.push( value );
				
				// default
				} else {
					ref[ key ] = value;
				}
				
			// path
			} else {
				
				// array
				if( keys[i+1] === '%%index%%' ) {
					if( !acf.isArray(ref[ key ]) ) {
						ref[ key ] = [];
					}
				
				// object	
				} else {
					if( !acf.isObject(ref[ key ]) ) {
						ref[ key ] = {};
					}
				}
				
				// crawl
				ref = ref[ key ];
			}
		}
	};
	
	acf.serialize = function( $el, prefix ){
			
		// vars
		var obj = {};
		var inputs = acf.serializeArray( $el );
		
		// prefix
		if( prefix !== undefined ) {
			
			// filter and modify
			inputs = inputs.filter(function( item ){
				return item.name.indexOf(prefix) === 0;
			}).map(function( item ){
				item.name = item.name.slice(prefix.length);
				return item;
			});
		}
		
		// loop
		for( var i = 0; i < inputs.length; i++ ) {
			buildObject( obj, inputs[i].name, inputs[i].value );
		}
		
		// return
		return obj;
	};
	
	/**
	*  acf.serializeArray
	*
	*  Similar to $.serializeArray() but works with a parent wrapping element.
	*
	*  @date	19/8/18
	*  @since	5.7.3
	*
	*  @param	jQuery $el The element or form to serialize.
	*  @return	array
	*/
	
	acf.serializeArray = function( $el ){
		return $el.find('select, textarea, input').serializeArray();
	}
	
	/**
	*  acf.serializeForAjax
	*
	*  Returns an object containing name => value data ready to be encoded for Ajax.
	*
	*  @date	17/12/18
	*  @since	5.8.0
	*
	*  @param	jQUery $el The element or form to serialize.
	*  @return	object
	*/
	acf.serializeForAjax = function( $el ){
			
		// vars
		var data = {};
		var index = {};
		
		// Serialize inputs.
		var inputs = acf.serializeArray( $el );
		
		// Loop over inputs and build data.
		inputs.map(function( item ){
			
			// Append to array.
			if( item.name.slice(-2) === '[]' ) {
				data[ item.name ] = data[ item.name ] || [];
				data[ item.name ].push( item.value );
			// Append	
			} else {
				data[ item.name ] = item.value;
			}
		});
		
		// return
		return data;
	};
	
	/**
	*  addAction
	*
	*  Wrapper for acf.hooks.addAction
	*
	*  @date	14/12/17
	*  @since	5.6.5
	*
	*  @param	n/a
	*  @return	this
	*/
	
/*
	var prefixAction = function( action ){
		return 'acf_' + action;
	}
*/
	
	acf.addAction = function( action, callback, priority, context ){
		//action = prefixAction(action);
		acf.hooks.addAction.apply(this, arguments);
		return this;
	};
	
	
	/**
	*  removeAction
	*
	*  Wrapper for acf.hooks.removeAction
	*
	*  @date	14/12/17
	*  @since	5.6.5
	*
	*  @param	n/a
	*  @return	this
	*/
	
	acf.removeAction = function( action, callback ){
		//action = prefixAction(action);
		acf.hooks.removeAction.apply(this, arguments);
		return this;
	};
	
	