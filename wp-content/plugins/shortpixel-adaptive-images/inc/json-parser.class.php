<?php
/**
 * User: simon
 * Date: 23.09.2019
 */

class ShortPixelJsonParser {

    protected $ctrl;
    private $logger;

    function __construct($ctrl) {
        $this->ctrl = $ctrl;
        $this->logger = ShortPixelAILogger::instance();
    }

    function parse($content) {
        //return $content;
        $this->logger->log("******** JSON PARSER *********");
        return $this->parseRecursive($content);
    }

    protected function parseRecursive($content) {
        if(is_array($content) || is_object($content) ) {
            foreach ($content as $key => $value) {
                if(is_array($content)) {
                    $this->logger->log("JSON: Key $key: " . json_encode($content[$key]));
                    $content[$key] = $this->parseRecursive($value);
                    //echo ("$key changed to: " . json_encode($content[$key]) . " \n\n");
                } else {
                    $this->logger->log("JSON: Key $key: " . json_encode($content->$key));
                    $content->$key = $this->parseRecursive($value);
                    //echo ("$key changed to: " . json_encode($content->$key) . " \n\n");
                }
            }
            return $content;
        } elseif(is_string($content)) {
            $this->logger->log("Leaf $content \n\n");
            $parsed = json_decode($content);
            if(json_last_error() !== JSON_ERROR_SYNTAX) {
                //echo ("IsJson \n\n");
                return json_encode($this->parseRecursive($parsed));
            } else {
                //here replace the urls
                return strlen($content) > 19 ? $this->replaceUrls($content) : $content;
            }
        } else {
            return $content;
        }
    }

    protected function replaceUrls($text) {
        $this->logger->log("try replace URLs in $text \n\n");
        $ret = preg_replace_callback("/(\bhttps?:|)\/\/(?:www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,4}\b(?:[-a-zA-Z0-9@:%_\+.~#?&\/\/=\(\)]*)\.(jpe?g|png|gif)\b/s",
            array($this, 'replaceUrl'),
            $text
        );
        return $ret;

    }

    protected function replaceUrl($match) {
        $this->logger->log('Matches' . json_encode($match) . "\n");
        if(strpos($match[0], $this->ctrl->settings['api_url']) === false) {
            $proto = '';
            if($match[1] == '') {
                $proto = strtoupper($_SERVER['SERVER_PROTOCOL']);
                $proto = strpos($proto, 'HTTPS') === false ? 'http:' : 'https:';
                $this->logger->log("Adding $proto.\n\n");
            }
            $ret = $this->ctrl->get_api_url(false) . '/' . $proto . $match[0];
            $this->logger->log("Changing to $ret.\n\n");
            return $ret;
        } else {
            return $match[0];
        }
    }
}