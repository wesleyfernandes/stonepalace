<?php
/*
 * Plugin Name: Krucial Theme Functionality
 * Plugin URI:  http://themeforest.net/user/krucialau
 * Description: Adds functionality to Krucial WordPress Themes.
 * Version:     1.0
 * Author:      KrucialAU
 * Author URI:  http://themeforest.net/user/krucialau
 * License:     GPL2
 */

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) {
	die;
}

define( 'KRUCIAL_THEME_NAME', 'Oculis');
define( 'KRUCIAL_PLUGIN_URL', plugin_dir_url( __FILE__ ));
define( 'KRUCIAL_PLUGIN_PATH', dirname( __FILE__ ));

// Plugin init
Krucial_Theme_Functionality::on_load();

class Krucial_Theme_Functionality {

	static function on_load() {

		// Load plugin textdomain
		function krucial_load_plugin_textdomain() {
			load_plugin_textdomain( 'krucial_theme_functionality', false, dirname( plugin_basename( __FILE__ ) ) . '/languages' );
		}
		add_action( 'plugins_loaded', 'krucial_load_plugin_textdomain' );

		// Check it's the correct theme
		$theme_name = ( wp_get_theme()->Name );

		if ( $theme_name == KRUCIAL_THEME_NAME ) {
			add_action('plugins_loaded', array(__CLASS__, 'krucial_include_files'));
		}
	}
	
	static function krucial_include_files() {

		// Load Redux Framework
		if ( ! class_exists( 'ReduxFramework' ) ) {
			require_once( KRUCIAL_PLUGIN_PATH . '/redux/framework.php' );
		}

		// One Click Demo Importer
		require_once( KRUCIAL_PLUGIN_PATH . '/redux/inc/extensions/demo-importer.php' );

		// WooCommerce Setup
		if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
			require_once( KRUCIAL_PLUGIN_PATH . '/init/woocommerce_setup.php');
		}

		// Load Extra Functions
		require_once( KRUCIAL_PLUGIN_PATH . '/init/additional_functions.php' );

		// Register Custom PostTypes
		require_once( KRUCIAL_PLUGIN_PATH . '/init/register_post_types.php' );

		// Register Custom Taxonomies
		require_once( KRUCIAL_PLUGIN_PATH . '/init/register_taxonomies.php' );

		// VC functions
		require_once(KRUCIAL_PLUGIN_PATH . '/init/vc/remove_elements.php');
		require_once(KRUCIAL_PLUGIN_PATH . '/init/vc/remove_fields.php');

		// VC Shortcodes
		require_once(KRUCIAL_PLUGIN_PATH . '/vc_shortcodes/accordion-wrapper.php');
		require_once(KRUCIAL_PLUGIN_PATH . '/vc_shortcodes/accordion-item.php');
		require_once(KRUCIAL_PLUGIN_PATH . '/vc_shortcodes/animate.php');
		require_once(KRUCIAL_PLUGIN_PATH . '/vc_shortcodes/ambientes-carousel.php');
		require_once(KRUCIAL_PLUGIN_PATH . '/vc_shortcodes/ambientes-filter.php');
		require_once(KRUCIAL_PLUGIN_PATH . '/vc_shortcodes/ambientes-grid.php');
		require_once(KRUCIAL_PLUGIN_PATH . '/vc_shortcodes/services-grid.php');
		require_once(KRUCIAL_PLUGIN_PATH . '/vc_shortcodes/blog-carousel.php');
		require_once(KRUCIAL_PLUGIN_PATH . '/vc_shortcodes/blog-grid.php');
		require_once(KRUCIAL_PLUGIN_PATH . '/vc_shortcodes/create-space.php');
		require_once(KRUCIAL_PLUGIN_PATH . '/vc_shortcodes/content-box.php');
		require_once(KRUCIAL_PLUGIN_PATH . '/vc_shortcodes/custom-button.php');
		require_once(KRUCIAL_PLUGIN_PATH . '/vc_shortcodes/custom-heading.php');
		require_once(KRUCIAL_PLUGIN_PATH . '/vc_shortcodes/custom-image.php');
		require_once(KRUCIAL_PLUGIN_PATH . '/vc_shortcodes/custom-image-url.php');
		require_once(KRUCIAL_PLUGIN_PATH . '/vc_shortcodes/featured-box.php');
		require_once(KRUCIAL_PLUGIN_PATH . '/vc_shortcodes/google-maps.php');
		require_once(KRUCIAL_PLUGIN_PATH . '/vc_shortcodes/icon-box.php');
		require_once(KRUCIAL_PLUGIN_PATH . '/vc_shortcodes/number-counter.php');
		require_once(KRUCIAL_PLUGIN_PATH . '/vc_shortcodes/projects-carousel.php');
		require_once(KRUCIAL_PLUGIN_PATH . '/vc_shortcodes/projects-filter.php');
		require_once(KRUCIAL_PLUGIN_PATH . '/vc_shortcodes/services-carousel.php');
		require_once(KRUCIAL_PLUGIN_PATH . '/vc_shortcodes/services-grid.php');
		require_once(KRUCIAL_PLUGIN_PATH . '/vc_shortcodes/staff-members.php');
		require_once(KRUCIAL_PLUGIN_PATH . '/vc_shortcodes/testimonials.php');
		require_once(KRUCIAL_PLUGIN_PATH . '/vc_shortcodes/testimonials-carousel.php');
		require_once(KRUCIAL_PLUGIN_PATH . '/vc_shortcodes/video-player.php');


		/**
		 * ACF
		 */

		// Hide ACF
		define( 'ACF_LITE', false );

		// Load ACF
		require_once(KRUCIAL_PLUGIN_PATH . '/acf/acf.php');

		// Load ACF options
		require_once(KRUCIAL_PLUGIN_PATH . '/options/acf_options.php');


		/**
		 * Load admin
		 */

		// Load admin welcome page
		function krucial_admin_welcome_page() {
			require_once(KRUCIAL_PLUGIN_PATH . '/admin/welcome.php');
		};

		// Load server info page
		require_once(KRUCIAL_PLUGIN_PATH . '/admin/server-info/server-info-functions.php');

		// Custom admin menu
		function krucial_admin_menu() {

			// Main menu item
			add_menu_page( wp_get_theme()->get('Name'), wp_get_theme()->get('Name'), 'manage_options', 'krucial-welcome', 'krucial_admin_welcome_page', '', 100  );

			// Welcome - submenu item
			add_submenu_page('krucial-welcome', 'Welcome', 'Welcome', 'manage_options', 'krucial-welcome', 'krucial_admin_welcome_page', '', 10 );

			// Server Info - submenu item
			add_submenu_page('krucial-welcome', 'Server Info', 'Server Info', 'manage_options', 'krucial-server-info', 'server_info', '', 20 );

		}

		add_action( 'admin_menu', 'krucial_admin_menu' );
	}
}