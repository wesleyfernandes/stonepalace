<?php
/*
 * @package     WBC_Importer - Extension for Importing demo content
 * @author      Webcreations907
 * @version     1.0
 */

if ( !function_exists( 'wbc_after_content_import' ) ) {

	function krucial_importer_info( $description ) {
		$message = '<p>'. esc_html__( 'Demos are best if used on a new install of WordPress. When a demo is importing please be patient and do no click anywhere else, this may take up to 5-10 minutes depending on your servers speed. You will be notified once the demo has completed importing. Also remember that all images used are only for demo purposes.', 'krucial_theme_functionality' ) .'</p>';
		$message .= '<h2 style="padding-top: 20px">' . esc_html__( 'What if the Import Fails or Stalls?', 'krucial_theme_functionality') . '</h2>

        <p>' . esc_html__( 'If the import stalls and fails to respond after a few minutes, you are suffering from PHP configuration limits that are set too low to complete the process. You should contact your hosting provider and ask them to increase your server limits to a minimum of what is recommended in our documentation.', 'krucial_theme_functionality') . '</p>';

		return $message;
	}
	// Uncomment the below
	add_filter( 'wbc_importer_description', 'krucial_importer_info', 10 );
}


if ( !function_exists( 'wbc_filter_title' ) ) {

	/**
	 * Filter for changing demo title in options panel so it's not folder name.
	 *
	 * @param [string] $title name of demo data folder
	 *
	 * @return [string] return title for demo name.
	 */

	function wbc_filter_title( $title ) {
		return trim( ucfirst( str_replace( "-", " ", $title ) ) );
	}

	// Uncomment the below
	// add_filter( 'wbc_importer_directory_title', 'wbc_filter_title', 10 );
}

if ( !function_exists( 'wbc_importer_description_text' ) ) {

	/**
	 * Filter for changing importer description info in options panel
	 * when not setting in Redux config file.
	 *
	 * @param [string] $title description above demos
	 *
	 * @return [string] return.
	 */

	function wbc_importer_description_text( $description ) {

		$message = '<p>'. esc_html__( 'Best if used on new WordPress install. Please be patient as demo can take up to 10 minutes to import.', 'krucial_theme_functionality' ) .'</p>';
		$message .= '<p>'. esc_html__( 'Images are for demo purpose only.', 'krucial_theme_functionality' ) .'</p>';

		return $message;
	}

	// Uncomment the below
	// add_filter( 'wbc_importer_description', 'wbc_importer_description_text', 10 );
}

if ( !function_exists( 'wbc_importer_label_text' ) ) {

	/**
	 * Filter for changing importer label/tab for redux section in options panel
	 * when not setting in Redux config file.
	 *
	 * @param [string] $title label above demos
	 *
	 * @return [string] return no html
	 */

	function wbc_importer_label_text( $label_text ) {

		$label_text = 'WBC Importer';

		return $label_text;
	}

	// Uncomment the below
	// add_filter( 'wbc_importer_label', 'wbc_importer_label_text', 10 );
}

if ( !function_exists( 'wbc_change_demo_directory_path' ) ) {

	/**
	 * Change the path to the directory that contains demo data folders.
	 *
	 * @param [string] $demo_directory_path
	 *
	 * @return [string]
	 */

	function wbc_change_demo_directory_path( $demo_directory_path ) {

		//$demo_directory_path = get_template_directory().'/demo-data/';

		//return $demo_directory_path;

	}

	// Uncomment the below
	//add_filter('wbc_importer_dir_path', 'wbc_change_demo_directory_path' );
}

if ( !function_exists( 'wbc_importer_before_widget' ) ) {

	/**
	 * Function/action ran before widgets get imported
	 *
	 * @param (array) $demo_active_import       Example below
	 * [wbc-import-1] => Array
	 *      (
	 *            [directory] => current demo data folder name
	 *            [content_file] => content.xml
	 *            [image] => screen-image.png
	 *            [theme_options] => theme-options.txt
	 *            [widgets] => widgets.json
	 *            [imported] => imported
	 *        )
	 * @param (string) $demo_data_directory_path path to current demo folder being imported.
	 *
	 * @return nothing
	 */

	function wbc_importer_before_widget( $demo_active_import , $demo_data_directory_path ) {

		//Do Something

	}

	// Uncomment the below
	// add_action('wbc_importer_before_widget_import', 'wbc_importer_before_widget', 10, 2 );
}

if ( !function_exists( 'wbc_after_theme_options' ) ) {

	/**
	 * Function/action ran after theme options set
	 *
	 * @param (array) $demo_active_import       Example below
	 * [wbc-import-1] => Array
	 *      (
	 *            [directory] => current demo data folder name
	 *            [content_file] => content.xml
	 *            [image] => screen-image.png
	 *            [theme_options] => theme-options.txt
	 *            [widgets] => widgets.json
	 *            [imported] => imported
	 *        )
	 * @param (string) $demo_data_directory_path path to current demo folder being imported.
	 *
	 * @return nothing
	 */

	function wbc_after_theme_options( $demo_active_import , $demo_data_directory_path ) {

		//Do Something

	}

	// Uncomment the below
	// add_action('wbc_importer_after_theme_options_import', 'wbc_after_theme_options', 10, 2 );
}


/************************************************************************
* Extended Example:
* Way to set menu, import revolution slider, and set home page.
*************************************************************************/

if ( !function_exists( 'wbc_extended_example' ) ) {
	function wbc_extended_example( $demo_active_import , $demo_directory_path ) {

		reset( $demo_active_import );
		$current_key = key( $demo_active_import );

		/************************************************************************
		* Import slider(s) for the current demo being imported
		*************************************************************************/

		//Import Sliders
		if ( class_exists( 'RevSlider' ) ) {
			$wbc_sliders_array = array(
				'demo1' => array(
					'home-slider.zip'
				)
			);

			if ( isset( $demo_active_import[$current_key]['directory'] ) && !empty( $demo_active_import[$current_key]['directory'] ) && array_key_exists( $demo_active_import[$current_key]['directory'], $wbc_sliders_array ) ) {
				$wbc_slider_import = $wbc_sliders_array[$demo_active_import[$current_key]['directory']];

				if( is_array( $wbc_slider_import ) ){
					foreach ($wbc_slider_import as $slider_zip) {
						if ( !empty($slider_zip) && file_exists( $demo_directory_path.$slider_zip ) ) {
							$slider = new RevSlider();
							$slider->importSliderFromPost( true, true, $demo_directory_path.$slider_zip );
						}
					}
				}else{
					if ( file_exists( $demo_directory_path.$wbc_slider_import ) ) {
						$slider = new RevSlider();
						$slider->importSliderFromPost( true, true, $demo_directory_path.$wbc_slider_import );
					}
				}
			}
		}


		/************************************************************************
		* Setting Menus
		*************************************************************************/

		// If it's demo1 - demo3
		$wbc_menu_array = array( 'demo1', 'demo2', 'demo3' );

		if ( isset( $demo_active_import[$current_key]['directory'] ) && !empty( $demo_active_import[$current_key]['directory'] ) && in_array( $demo_active_import[$current_key]['directory'], $wbc_menu_array ) ) {

			// Menus
			$primary_menu = get_term_by( 'name', 'Main Menu', 'nav_menu' );
			set_theme_mod( 'nav_menu_locations', array(
					'krucial-main-menu' => $primary_menu->term_id
				)
			);
		}


		/************************************************************************
		 * Remove default WP widgets
		 *************************************************************************/

		update_option( 'sidebars_widgets', null );


		/************************************************************************
		* Set HomePage
		*************************************************************************/

		// array of demos/homepages to check/select from
		$wbc_home_pages = array(
			'demo1' => 'Home',
			'demo2' => 'Home',
			'demo3' => 'Home'
		);

		if ( isset( $demo_active_import[$current_key]['directory'] ) && !empty( $demo_active_import[$current_key]['directory'] ) && array_key_exists( $demo_active_import[$current_key]['directory'], $wbc_home_pages ) ) {
			$page = get_page_by_title( $wbc_home_pages[$demo_active_import[$current_key]['directory']] );
			if ( isset( $page->ID ) ) {
				update_option( 'page_on_front', $page->ID );
				update_option( 'show_on_front', 'page' );
				update_option( 'page_for_posts', '79' );
			}
		}

	}

	add_action( 'wbc_importer_after_content_import', 'wbc_extended_example', 10, 2 );
}