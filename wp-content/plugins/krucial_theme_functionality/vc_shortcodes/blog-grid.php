<?php

function krucial_blog_grid($atts) {
	extract(shortcode_atts(array(
		'blog_grid_number' => '',
		'blog_grid_layout' => '',
	), $atts));

	ob_start();

	$query = new WP_Query(array(
			'post_type' => '',
			'posts_per_page' => $blog_grid_number,
			'ignore_sticky_posts' => 1
		)
	);

	?>

    <div class="blog-grid row">
		<?php if ( $query->have_posts() ): while( $query->have_posts() ): $query->the_post(); ?>
            <div class="<?php echo esc_attr( $blog_grid_layout ) ?>">
                <div class="blog-item">
                    <div class="blog-item-inner">
                        <?php if ( has_post_thumbnail() ) { ?>
                            <a href="<?php the_permalink(); ?>" class="blog-grid-img">
                                <?php the_post_thumbnail( 'krucial-image-700x500-cropped' ); ?>
                            </a>
                        <?php } ?>
                        <div class="blog-bottom">
                            <div class="blog-grid-categories"><?php echo krucial_post_categories(); ?></div>
                            <a href="<?php the_permalink(); ?>">
                                <div class="blog-grid-title">
                                    <span><?php the_title(); ?></span>
                                    <i class="essentials krucial-essential-light-07-arrow-right"></i>
                                </div>
                            </a>
                            <div class="blog-grid-date"><?php echo esc_html(get_the_date()); ?></div>
                        </div>
                    </div>
                </div>
            </div>
		<?php endwhile; wp_reset_postdata(); endif; ?>
    </div>
	<?php

	$result = ob_get_contents();
	ob_end_clean();
	return $result;
}
add_shortcode('krucial-blog-grid', 'krucial_blog_grid');