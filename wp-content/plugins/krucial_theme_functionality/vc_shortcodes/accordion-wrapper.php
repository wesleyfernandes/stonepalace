<?php

function krucial_accordion_wrapper($atts, $content = '') {
	extract(shortcode_atts(array(
		'accordion_wrapper_title' => '',
	), $atts));

	ob_start();

	$uniqid = uniqid( 'accordion_' );

	?>

    <div class="accordion-wrapper" id="<?php echo esc_attr( $uniqid ) ?>">
	    <?php echo do_shortcode( $content ); ?>
    </div>

	<?php

	$data = "
        jQuery(document).ready(function ($) {
            'use strict';
            
            $('#" . esc_js($uniqid) . " .accordion-item').on('click', function(){
                // slide down clicked item
                var itemId = $(this).attr('id');
                $('#' + itemId + ' .item-content').slideToggle(200);
                
                // slide up other open items
                $('#" . esc_js($uniqid) . " .accordion-item:not(#'+ itemId + ') .item-content').slideUp(200);
            });
        });
    ";

	// Add scripts to footer
	wp_add_inline_script('krucial-scripts', $data);

	$result = ob_get_contents();
	ob_end_clean();
	return $result;
}
add_shortcode('krucial_accordion_wrapper', 'krucial_accordion_wrapper');