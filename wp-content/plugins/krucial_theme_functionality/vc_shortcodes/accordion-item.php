<?php

function krucial_accordion_item($atts) {
	extract(shortcode_atts(array(
		'accordion_item_title' => '',
		'accordion_item_content' => '',
        'accordion_item_icon' => '',
	), $atts));

	ob_start();

	$uniqid = uniqid( 'accordion_item_' );

	?>

	<div class="accordion-item" id="<?php echo esc_attr( $uniqid ) ?>">
		<div class="item-head">
            <div class="item-title">
                <i class="<?php echo esc_attr($accordion_item_icon); ?>"></i>
                <span><?php echo esc_html($accordion_item_title); ?></span>
            </div>
			<div class="item-icon">
                <i class="essentials krucial-essential-light-11-plus-big"></i>
            </div>
		</div>
		<div class="item-content">
			<?php echo wp_kses_post($accordion_item_content); ?>
		</div>
	</div>

	<?php

	$result = ob_get_contents();
	ob_end_clean();
	return $result;
}
add_shortcode('krucial-accordion-item', 'krucial_accordion_item');