<?php

function krucial_number_counter($atts) {
	extract(shortcode_atts( array(
		'number_counter_title' => '',
		'number_counter_value' => '',
		'number_counter_prefix' => '',
		'number_counter_suffix' => '',
		'number_counter_timer' => '',
		'number_counter_align' => '',
        'number_counter_size' => '',
        'number_counter_color' => ''
	), $atts ) );

	ob_start();

	// Load countup
	wp_enqueue_script('countup');
	
	$counter_id = uniqid( 'counter_value_' );
	$counter_wrap_id = uniqid( 'counter_wrap_' );

	// Styles
	$data = "
        #" . esc_attr($counter_wrap_id) . " {" .
	        ((!empty($number_counter_align)) ? 'text-align:' . esc_attr($number_counter_align) . ';' : '')
        . "}
        #" . esc_attr($counter_wrap_id) . " .counter-value {" .
	        ((!empty($number_counter_color)) ? 'color:' . esc_attr($number_counter_color) . ';' : '') .
	        ((!empty($number_counter_size)) ? 'font-size:' . esc_attr($number_counter_size) . 'px;' : '') .
	        ((!empty($number_counter_size)) ? 'line-height:' . esc_attr($number_counter_size) . 'px;' : '')
        . "}
         #" . esc_attr($counter_wrap_id) . " .counter-title {" .
	        ((!empty($number_counter_color)) ? 'color:' . esc_attr($number_counter_color) . ';' : '')
        . "}";

	wp_register_style( 'krucial-footer', false );
	wp_enqueue_style( 'krucial-footer' );
	wp_add_inline_style( 'krucial-footer', $data );

	?>

	<div class="number-counter" id="<?php echo esc_attr($counter_wrap_id); ?>">
		<div class="counter-value" id="<?php echo esc_attr( $counter_id ); ?>">

		</div>
		<div class="counter-title">
			<?php echo esc_html( $number_counter_title ); ?>
		</div>
	</div>

	<?php

	$data = "
		jQuery(document).ready(function($) {
            'use strict';
            var options = {
                useEasing: true,
                useGrouping: false,
                prefix: '" . esc_js( $number_counter_prefix ) . "',
                suffix: '" . esc_js( $number_counter_suffix ) . "',
            };

            var counter = new CountUp('" . esc_js( $counter_id ) . "', 0, " . esc_js( $number_counter_value ) . ", 0, " . esc_js( $number_counter_timer ) . ", options);

            // Run when in viewport
            $(window).on('scroll', function () {
                if ($('#" . esc_js( $counter_id ) . "').isOnScreen()) {
                    counter.start();
                }
            });
		});
    ";

	// Add scripts to footer
	wp_add_inline_script('krucial-scripts', $data);

	$result = ob_get_contents();
	ob_end_clean();
	return $result;
}
add_shortcode('krucial-number-counter', 'krucial_number_counter');