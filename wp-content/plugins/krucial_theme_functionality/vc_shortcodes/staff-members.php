<?php

function krucial_staff_members($atts) {
    extract(shortcode_atts(array(
        'staff_number' => '',
        'staff_per_row' => '',
        'staff_categories' => '',
        'staff_link' => ''
    ), $atts));

    ob_start();

    // If category selected, do the tax_query
    if ( $staff_categories !== 'all' ) {
        $args = array(
            'post_type' => 'staff',
            'posts_per_page' => esc_html( $staff_number ),
            'tax_query' => array(
                array(
                    'taxonomy' => 'staff_categories',
                    'field'    => 'slug',
                    'terms'    => $staff_categories,
                )
            ),
        );
    } else {
        $args = array(
            'post_type' => 'staff',
            'posts_per_page' => esc_html( $staff_number ),
        );
    }

    $query = new WP_Query( $args );

    ?>

    <div class="staff-wrap row">
		<?php if ( $query->have_posts() ): while( $query->have_posts() ): $query->the_post(); ?>
            <div class="<?php echo esc_attr( $staff_per_row ); ?>">
                <div class="staff-member">
                    <div class="staff-img"><?php the_post_thumbnail( 'krucial-image-150x150-cropped' ); ?></div>
                    <div class="staff-inner">
                        <div class="staff-name"><?php the_title(); ?></div>
                        <div class="staff-position"><?php echo esc_html( get_field('staff_position') ); ?></div>
                        <div class="staff-excerpt"><?php the_excerpt(); ?></div>
						<?php if ( $staff_link === 'true' ) { ?>
                            <a class="staff-link" href="<?php echo get_the_permalink(); ?>"><?php echo esc_html__( 'View Profile', 'krucial_theme_functionality' ) ?><i class="icon-arrow-right"></i></a>
						<?php } ?>
                    </div>
                </div>
            </div>
		<?php endwhile; wp_reset_postdata(); endif; ?>
    </div>
	<?php

	$result = ob_get_contents();
	ob_end_clean();
	return $result;
}
add_shortcode('krucial-staff-members', 'krucial_staff_members');