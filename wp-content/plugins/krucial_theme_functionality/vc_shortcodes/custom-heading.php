<?php

function krucial_custom_heading($atts) {
	extract(shortcode_atts(array(
		'heading_title' => '',
		'heading_weight' => '',
		'heading_transform' => '',
		'heading_tag' => '',
		'heading_size' => '',
		'heading_size_medium' => '',
		'heading_size_small' => '',
		'heading_spacing' => '',
		'heading_spacing_medium' => '',
		'heading_spacing_small' => '',
		'heading_alignment' => '',
		'heading_alignment_medium' => '',
		'heading_alignment_small' => '',
		'heading_color' => '',
		'heading_mark_color' => '',
		'heading_separator_color' => '',
		'heading_margin_top' => '',
		'heading_margin_bottom' => '',
        'heading_add_separator' => ''
	), $atts));

	ob_start();
	
	$uniqid = uniqid( 'heading_' );

    $data = "
        #" . esc_attr($uniqid) . " {" .
            "text-align:" . esc_attr($heading_alignment) . ";" .
            "font-weight:" . esc_attr($heading_weight) . ";" .
            "line-height: " . ((!empty($heading_size)) ? ((int) $heading_size + 10) : '44') . "px;" .
            ((!empty($heading_color)) ? 'color:' . esc_attr($heading_color) . ';' : 'color: initial;') .
            ((!empty($heading_size)) ? 'font-size:' . esc_attr($heading_size) . 'px;' : '') .
            ((!empty($heading_spacing)) ? 'letter-spacing:' . esc_attr($heading_spacing) . 'px;' : '') .
            (($heading_transform !== 'none') ? 'text-transform:' . esc_attr($heading_transform) . ';' : '') .
            ((!empty($heading_margin_top)) ? 'margin-top:' . esc_attr($heading_margin_top) . 'px;' : '') .
            ((!empty($heading_margin_bottom)) ? 'margin-bottom:' . esc_attr($heading_margin_bottom) . 'px;' : '')
        . "}
    ";

	if (!empty($heading_mark_color)) {
		$data .= "
		    #" . esc_attr( $uniqid ) . " mark {
		        color:" . esc_attr( $heading_mark_color ) . ";
            }
        ";
	}

	if (!empty($heading_separator_color)) {
	    $data .= "
		    #" . esc_attr( $uniqid ) . ".add-separator:after {
		        background:" . esc_attr( $heading_separator_color ) . ";
            }
        ";
    }

	if ( !empty( $heading_size_medium ) || !empty( $heading_spacing_medium ) ) {
		$data .= "
	        @media screen and (max-width: 1000px) {
	            #" . esc_attr( $uniqid ) . " {" .
                    ((!empty($heading_size_medium)) ? 'font-size:' . esc_attr($heading_size_medium) . 'px;' : '') .
                    ((!empty($heading_spacing_medium)) ? 'letter-spacing:' . esc_attr($heading_spacing_medium) . 'px;' : '') .
                    ((!empty($heading_alignment_medium)) ? 'text-align:' . esc_attr($heading_alignment_medium) . ';' : '')
                . "}
	        }
	    ";
	}

	if ( !empty( $heading_size_small ) || !empty( $heading_spacing_small ) ) {
		$data .= "
	        @media screen and (max-width: 600px) {
	            #" . esc_attr( $uniqid ) . " {" .
		         ((!empty($heading_size_small)) ? 'font-size:' . esc_attr($heading_size_small) . 'px;' : '') .
		         ((!empty($heading_spacing_small)) ? 'letter-spacing:' . esc_attr($heading_spacing_small) . 'px;' : '') .
		         ((!empty($heading_alignment_small)) ? 'text-align:' . esc_attr($heading_alignment_small) . ';' : '')
		         . "}
	        }
	    ";
	}

	wp_register_style( 'krucial-footer', false );
	wp_enqueue_style( 'krucial-footer' );
	wp_add_inline_style( 'krucial-footer', $data );

	// Alignment for separator
    if ($heading_alignment === 'center') {
        $sep_align = 'center';
    } else if ($heading_alignment === 'right') {
        $sep_align = 'right';
    } else {
        $sep_align = 'left';
    }

    ?>

    <<?php echo esc_attr( $heading_tag ) ?> class="krucial-custom-heading <?php if ($heading_add_separator === 'true') echo esc_attr('add-separator ' . $sep_align); ?>" id="<?php echo esc_attr( $uniqid ) ?>">
        <?php echo wp_kses( $heading_title, array('mark' => array(), 'br' => array()) ) ?>
    </<?php echo esc_attr( $heading_tag ) ?>>

	<?php

	$result = ob_get_contents();
	ob_end_clean();
	return $result;
}
add_shortcode('krucial-custom-heading', 'krucial_custom_heading');