<?php

function krucial_services_carousel($atts) {
	extract(shortcode_atts(array(
		'services_carousel_number' => '',
		'services_carousel_per_row' => '',
		'services_carousel_show_dots' => '',
		'services_carousel_show_nav' => '',
		'services_carousel_slide_speed' => '',
		'services_carousel_autoplay' => '',
		'services_carousel_autoplay_speed' => '',
	), $atts ));

	ob_start();

	// Load slick slider
	wp_enqueue_script('slick');
	wp_enqueue_style('slick');

	$query = new WP_Query(array(
		'post_type' => 'services',
		'posts_per_page' => esc_attr( $services_carousel_number ),
	));

	$carousel_id = uniqid( 'services_carousel_' );
	$slick_prev = uniqid( 'prev_' );
	$slick_next = uniqid( 'next_' );

	?>

    <div class="services-carousel-wrapper">
        <div class="services-carousel services-grid" id="<?php echo esc_attr($carousel_id); ?>">
			<?php if( $query->have_posts() ): while( $query->have_posts() ): $query->the_post(); ?>
                <div class="services-carousel-item">
                    <div class="service-item" style="background: url(<?php the_post_thumbnail_url('krucial-image-700x900-cropped'); ?>)">
                        <div class="item-content">
                            <div class="services-grid-title"><?php the_title(); ?></div>
                            <div class="services-grid-excerpt"><?php the_excerpt(); ?></div>
                            <div class="services-grid-link">
                                <a href="<?php the_permalink(); ?>">
                                    <span><?php echo esc_html__( 'View Service', 'krucial_theme_functionality' ); ?></span>
                                    <i class="essentials krucial-essential-light-07-arrow-right"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
			<?php endwhile; wp_reset_postdata(); endif; ?>
        </div>
		<?php if ( $services_carousel_show_nav === 'true' ) { ?>
            <span id="<?php echo esc_attr( $slick_next ) ?>" class="slick-arrow-right">
                <i class="essentials krucial-essential-light-01-chevron-left"></i>
            </span>
            <span id="<?php echo esc_attr( $slick_prev ) ?>" class="slick-arrow-left">
                <i class="essentials krucial-essential-light-02-chevron-right"></i>
            </span>
		<?php } ?>
    </div>

    <?php

    $data = "
        jQuery(document).ready(function($) {
            'use strict';
            $('#" . esc_attr($carousel_id) . "').slick({
                slidesToShow: " . esc_js( $services_carousel_per_row ) . ",
                slidesToScroll: 1,
                rows: 1,
                autoplay: " . esc_js( $services_carousel_autoplay ) . ",
                autoplaySpeed: " . esc_js( $services_carousel_autoplay_speed ) . ",
                dots: " . esc_js( $services_carousel_show_dots ) . ",
                arrows: " . esc_js( $services_carousel_show_nav ) . ",
                infinite: true,
                speed: " . esc_js( $services_carousel_slide_speed ) . ",
                prevArrow: $('#" . esc_js( $slick_prev ) . "'),
                nextArrow: $('#" . esc_js( $slick_next ) . "'),
                responsive: [
                    {
                        breakpoint: 1100,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 1,
                            rows: 1,
                        }
                    },
                    {
                        breakpoint: 768,
                        settings: {
                            slidesToShow: 1,
                            slidestToScroll: 1,
                            rows: 1,
                        }
                    }
                ]
            });

        });
    ";

    // Add scripts to footer
	wp_add_inline_script('krucial-scripts', $data);

	$result = ob_get_contents();
	ob_end_clean();
	return $result;
}
add_shortcode('krucial-services-carousel', 'krucial_services_carousel');