<?php

function krucial_icon_box($atts) {
	extract(shortcode_atts(array(
		'icon_box_icon' => '',
		'icon_box_title' => '',
		'icon_box_text' => '',
		'icon_box_size' => '',
		'icon_box_icon_margin' => '',
		'icon_box_radius' => '',
		'icon_box_center' => '',
		'icon_box_icon_left' => '',
		'icon_box_background' => '',
		'icon_box_color' => '',
		'icon_box_bg' => '',
        'icon_box_active' => ''
	), $atts));

	ob_start();

	$icon_box_id = uniqid('icon_box_');

	$classes = esc_attr( $icon_box_size ) . ' ' . esc_attr( $icon_box_radius ) . ' ' . esc_attr( $icon_box_center ) . ' ' . esc_attr( $icon_box_icon_left );

	if ( $icon_box_background === 'true' ) {
		$classes .= ' bg-disabled';
	}

	if ($icon_box_active === 'true') {
		$classes .= ' is-active';
	}

	$data = "
	    #" . $icon_box_id . " .icon-box-icon {
	        background: " . esc_attr($icon_box_bg) . ";
	    }
	    #" . $icon_box_id . " i {
	        margin-top: " . esc_attr($icon_box_icon_margin) . "px;
	        color: " . esc_attr($icon_box_color) . ";
	    }
	";

	wp_register_style( 'krucial-footer', false );
	wp_enqueue_style( 'krucial-footer' );
	wp_add_inline_style( 'krucial-footer', $data );

	?>

    <div class="icon-box <?php echo esc_attr( $classes ) ?>" id="<?php echo esc_attr($icon_box_id); ?>">
        <div class="icon-box-icon">
            <i class="<?php echo esc_attr( $icon_box_icon ); ?>"></i>
        </div>
        <div class="icon-content">
            <div class="icon-box-title"><?php echo esc_html($icon_box_title); ?></div>
            <div class="icon-box-text"><?php echo esc_html($icon_box_text); ?></div>
        </div>
    </div>

	<?php

	$result = ob_get_contents();
	ob_end_clean();
	return $result;
}
add_shortcode('krucial-icon-box', 'krucial_icon_box');