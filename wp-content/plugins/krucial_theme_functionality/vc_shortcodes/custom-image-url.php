<?php

function krucial_custom_image_url($atts) { 
	extract(shortcode_atts(array(
		'custom_image_url' => '',
		'custom_image_caption' => '',
		'custom_image_link_download'=>'',
		'custom_download_image_link_text'=>'',
		'custom_download_image_url'=>'',
		'custom_image_link' => '',
		'custom_image_link_text' => '',
		'custom_image_link_url' => '',
		'custom_image_link_target' => '',
	), $atts));

	ob_start();

	$uniqid = uniqid( 'custom_image_' );

	// Image URL
	$custom_image = wp_get_attachment_image_src( $custom_image_url, 'full' )[0];
	$custom_download_image = wp_get_attachment_image_src( $custom_download_image_url, 'full' )[0];

	//se custom link for ativado, link na imagem principal
	if(esc_html($custom_image_link) == true){
	?>
		<div class="custom-image" id="<?php echo esc_attr( $uniqid ) //obter ID do bloco custom image?>">
			<a href="<?php echo esc_url($custom_image_link_url);?>" title="<?php echo esc_html($custom_image_link_text); //obter título do post e título da imagem principal ?>" target="<?php echo esc_html($custom_image_link_target); //obter o alvo para o link ?>">
				<img src="<?php echo esc_url($custom_image); //obter link da imagem principal ?>" alt="<?php echo esc_html($custom_image_caption); //obter título da imagem principal ?>">
			</a>
			<div class="img-caption"> 
				<?php //echo esc_html($custom_image_caption); //obter título da imagem principal ?>
			</div>
		</div>
	<?php
	}else{
	?>
		<div class="custom-image" id="<?php echo esc_attr( $uniqid ) //obter ID do bloco custom image ?>">
			<img src="<?php echo esc_url($custom_image); //obter link da imagem principal ?>" alt="<?php echo esc_html($custom_image_caption); //obter título da imagem principal ?>"/>
			<div class="img-caption">
				<?php //echo esc_html($custom_image_caption); //obter título da imagem principal ?>
				<?php if(esc_html($custom_image_link_download) == true){ //se o checkbox download for marcado ?>
				<div>
					<i class="fas fa-download"></i>
					<a href="<?php echo esc_url($custom_download_image); //obter link da imagem para download ?>" 
						title="<?php echo do_shortcode("[post_title]")." - ".esc_html($custom_image_caption); //obter título do post e título da imagem principal ?>" 
						target="_blank">
						<?php echo esc_html($custom_download_image_link_text); //obter texto do link para download?>
						<?php //echo esc_html__('BAIXAR IMAGEM', 'oculis'); ?>
					</a>
				</div>
				<?php }else{} ?>
			</div>
		</div>	
	<?php
	}
	
	/*
	<div class="custom-image" id="<?php echo esc_attr( $uniqid ) ?>">
		<a href="<?php echo esc_url($custom_image); ?>" title="<?php echo esc_url($custom_image_caption); ?>">
			<img src="<?php echo esc_url($custom_image); ?>">
		</a>
		<!--
			<div class="img-caption"><?php //echo esc_html($custom_image_caption); ?></div>
		!-->
	</div>
	*/

	$result = ob_get_contents();
	ob_end_clean();
	return $result;
}
add_shortcode('krucial-custom-image', 'krucial_custom_image_url');