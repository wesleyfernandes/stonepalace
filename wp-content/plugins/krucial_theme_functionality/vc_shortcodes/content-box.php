<?php

function krucial_content_box($atts, $content = '') {
	extract(shortcode_atts(array(
		'content_box_css_class' => '',
        'content_box_alignment' => '',
        'content_box_width' => ''
	), $atts ));

	ob_start();

	$uniqid = uniqid( 'content_box_' );

	if ( !empty( $content_box_width ) ) {
		$data = "
            #" . esc_attr( $uniqid ) . " {
                 max-width:" . esc_attr($content_box_width) . "px;
             }
	    ";

		wp_register_style( 'krucial-footer', false );
		wp_enqueue_style( 'krucial-footer' );
		wp_add_inline_style( 'krucial-footer', $data );
	}

	?>

	<div class="content-box <?php echo esc_attr($content_box_css_class) . ' ' . esc_attr($content_box_alignment); ?>" id="<?php echo esc_attr( $uniqid ) ?>">
		<?php echo do_shortcode( $content ); ?>
	</div>

	<?php

	$result = ob_get_contents();
	ob_end_clean();
	return $result;
}
add_shortcode('krucial_content_box', 'krucial_content_box');