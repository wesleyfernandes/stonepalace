<?php

function krucial_custom_image($atts) {
	extract(shortcode_atts(array(
		'custom_image_src' => '',
		'custom_image_caption' => '',
		'custom_image_link' => '',
		'custom_image_link_text' => '',
		'custom_image_link_url' => '',
		'custom_image_link_target' => '',
	), $atts));

	ob_start();

	$uniqid = uniqid( 'custom_image_' );

	// Image URL
	$custom_image = wp_get_attachment_image_src( $custom_image_src, 'full' )[0];

	if($custom_image_link == true){
	?>
	<div class="custom-image" id="<?php echo esc_attr( $uniqid ) ?>">
		<a href="<?php echo $custom_image_url;?>" title="<?php echo $custom_image_text;?>" target="<?php echo $custom_image_target;?>">
			<img src="<?php echo esc_url($custom_image); ?>">
		</a>
		<div class="img-caption"><?php echo esc_html($custom_image_caption); ?></div>
	</div>
	<?php
	}else{
	?>
	<div class="custom-image" id="<?php echo esc_attr( $uniqid ) ?>">
		<img src="<?php echo esc_url($custom_image); ?>">
		<div class="img-caption"><?php echo esc_html($custom_image_caption); ?></div>
	</div>	
	<?php
	}

	$result = ob_get_contents();
	ob_end_clean();
	return $result;
}
add_shortcode('krucial-custom-image', 'krucial_custom_image');