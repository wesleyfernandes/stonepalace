<?php

function krucial_custom_button($atts) {
	extract(shortcode_atts(array(
		'custom_button_text' => '',
		'custom_button_link' => '',
		'custom_button_target' => '',
		'custom_button_size' => '',
		'custom_button_corners' => '',
		'custom_button_alignment_lg' => '',
		'custom_button_alignment_med' => '',
		'custom_button_alignment_sml' => '',
		'button_background_color' => '',
		'button_text_color' => '',
		'button_background_color_hov' => '',
		'button_text_color_hov' => '',
		'button_icon' => '',
		'button_icon_color' => '',
		'button_icon_color_hov' => '',
        'button_css_class' => '',
        'button_use_div' => ''
	), $atts));

	ob_start();

	$button_classes = esc_attr( $custom_button_size ) . ' ' . esc_attr( $custom_button_corners );
	$button_id = uniqid( 'button_' );
	$button_id_css = '#' . esc_attr( $button_id );

	$data =
		esc_attr($button_id_css) . " {
            justify-content: " . esc_attr( $custom_button_alignment_lg ) . ";
        } " .
        esc_attr($button_id_css) . " .custom-button {
            background: " . esc_attr( $button_background_color ) . ";
        } " .
		esc_attr($button_id_css) . " .custom-button .btn-link {
            color: " . esc_attr( $button_text_color ) . ";
        } " .
		esc_attr($button_id_css) . " .custom-button:hover {
             background: " . esc_attr( $button_background_color_hov ) . ";
        } " .
		esc_attr($button_id_css) . " .custom-button:hover .btn-link {
            color: " . esc_attr( $button_text_color_hov ) . ";
        } " .
		esc_attr($button_id_css) . " .custom-button i {
            color: " . esc_attr( $button_icon_color ) . ";
        } " .
		esc_attr($button_id_css) . " .custom-button:hover i {
            color: " . esc_attr( $button_icon_color_hov ) . ";
        }
        
        @media screen and (max-width: 1000px) { " .
            esc_attr($button_id_css) . " {
                justify-content: " . esc_attr( $custom_button_alignment_med ) . ";
            }
        }
        
        @media screen and (max-width: 600px) { " .
		esc_attr($button_id_css) . " {
                justify-content: " . esc_attr( $custom_button_alignment_sml ) . ";
            }
        }
    ";

	wp_register_style( 'krucial-footer', false );
	wp_enqueue_style( 'krucial-footer' );
	wp_add_inline_style( 'krucial-footer', $data );

	?>

    <div class="custom-button-wrapper <?php echo esc_attr($button_css_class); ?>" id="<?php echo esc_attr( $button_id ) ?>">
        <?php if ($button_use_div === 'true') { ?>
            <div class="custom-button <?php echo esc_attr( $button_classes ) ?>">
                <div class="btn-link"><?php echo esc_html( $custom_button_text ); ?></div>
	            <?php if ( ! empty( $button_icon ) ) { ?>
                    <i class="<?php echo esc_attr( $button_icon ); ?>"></i>
	            <?php } ?>
            </div>
        <?php } else { ?>
            <a class="custom-button <?php echo esc_attr( $button_classes ) ?>" href="<?php echo esc_attr( $custom_button_link ); ?>" target="<?php echo esc_attr( $custom_button_target ); ?>">
                <span class="btn-link"><?php echo esc_html( $custom_button_text ); ?></span>
            <?php if ( ! empty( $button_icon ) ) { ?>
                <i class="<?php echo esc_attr( $button_icon ); ?>"></i>
            <?php } ?>
            </a>
        <?php } ?>
    </div>

	<?php
	$result = ob_get_contents();
	ob_end_clean();
	return $result;
}

add_shortcode('krucial-custom-button', 'krucial_custom_button');