<?php

function krucial_create_space($atts) {
	extract( shortcode_atts( array(
		'create_space_desktop' => '',
		'create_space_tablet'  => '',
		'create_space_mobile'  => '',
	), $atts ) );

	ob_start();

	$create_space_id = uniqid( 'space_' );

	?>

    <div class="create-space" id="<?php echo esc_attr( $create_space_id ); ?>"></div>

	<?php

	$data = "
        #" . esc_attr( $create_space_id ) . " {
            height: " . esc_attr( $create_space_desktop ) . "px;
        }
        @media screen and (max-width: 1000px) {
            #" . esc_attr( $create_space_id ) . " {
                height: " . esc_attr( $create_space_tablet ) . "px;
            }
        }
        @media screen and (max-width: 600px) {
            #" . esc_attr( $create_space_id ) . " {
                height: " . esc_attr( $create_space_mobile ) . "px;
            }
        }
    ";

    wp_register_style( 'krucial-footer', false );
    wp_enqueue_style( 'krucial-footer' );
    wp_add_inline_style( 'krucial-footer', $data );

	$result = ob_get_contents();
	ob_end_clean();
	return $result;
}
add_shortcode('krucial-create-space', 'krucial_create_space');