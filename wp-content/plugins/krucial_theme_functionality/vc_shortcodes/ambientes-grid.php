<?php

function krucial_ambientes_grid($atts) {
	extract(shortcode_atts(array(
		'ambientes_grid_number' => '',
		'ambientes_grid_layout' => '',
	), $atts));

	ob_start();

	$query = new WP_Query(array(
			'post_type' => '',
			'posts_per_page' => $ambientes_grid_number,
			'ignore_sticky_posts' => 1
		)
	);

	?>

	<?php 
		if ( $query->have_posts() ): 
		while( $query->have_posts() ): $query->the_post(); 
	?>
		<div class="vc_col-sm-6 <?php echo esc_attr( $ambientes_grid_layout ) ?>">
			<div class="vc_column-inner">
				<div class="wpb_wrapper">
					<div class="wpb_text_column wpb_content_element ">
						<div class="wpb_wrapper">
							<h2><strong><?php the_title(); ?></strong></h2>
						</div>
					</div>
					<div class="custom-image" id="custom_image_5d029f973400d">
						<?php if ( has_post_thumbnail() ) { ?>
							<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" target="_self">
								<?php the_post_thumbnail( 'krucial-image-530x265-cropped' ); ?>
							</a>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
	<?php 
		endwhile; 
		wp_reset_postdata(); 
		endif; 
	?>
		
    <div class="ambientes-grid row">
	<?php 
		if ( $query->have_posts() ): 
		while( $query->have_posts() ): $query->the_post(); 
	?>
			<div class="<?php echo esc_attr( $ambientes_grid_layout ) ?>">
				<div class="ambientes-item">
					<div class="ambientes-item-inner">
						<?php if ( has_post_thumbnail() ) { ?>
							<a href="<?php the_permalink(); ?>" class="ambientes-grid-img">
								<?php the_post_thumbnail( 'krucial-image-530x265-cropped' ); ?>
							</a>
						<?php } ?>
						<div class="ambientes-bottom">
							<div class="ambientes-grid-categories"><?php echo krucial_post_categories(); ?></div>
							<a href="<?php the_permalink(); ?>">
								<div class="ambientes-grid-title">
									<span><?php the_title(); ?></span>
									<i class="essentials krucial-essential-light-07-arrow-right"></i>
								</div>
							</a>
							<div class="ambientes-grid-date"><?php echo esc_html(get_the_date()); ?></div>
						</div>
					</div>
				</div>
			</div>
	<?php 
		endwhile; 
		wp_reset_postdata(); 
		endif; 
	?>
    </div>
	<?php

	$result = ob_get_contents();
	ob_end_clean();
	return $result;
}
add_shortcode('krucial-ambientes-grid', 'krucial_ambientes_grid');