<?php

if ( !class_exists( 'Krucial_GoogleMaps' ) ) {
    class Krucial_GoogleMaps {

        function __construct() {
            // Register & Print Scripts
            add_action('init', array( __CLASS__, 'krucial_register_maps_assets' ) );
            add_action('wp_head', array( __CLASS__, 'krucial_print_maps_assets' ) );
        }

        // Register Scripts
        static function krucial_register_maps_assets() {
            global $krucial_options;
            $key = (isset($krucial_options['googlemaps_api_key']) ? $krucial_options['googlemaps_api_key'] : '');
            wp_register_script('google-maps-api', 'https://maps.googleapis.com/maps/api/js?key=' .  $key, array(), 'v3');
        }

        // Print Scripts if GoogleMaps shortcode on page
        static function krucial_print_maps_assets() {
            global $post;

            if ($post) {
                if (has_shortcode($post->post_content, 'krucial_googlemaps')) {
                    wp_print_scripts('google-maps-api');
                }
            }
        }
    }
}

new Krucial_GoogleMaps();


/**
 * Google Maps
 */

add_shortcode( 'krucial_googlemaps', 'krucial_vc_google_maps');
function krucial_vc_google_maps( $atts, $content = null ) {

    global $krucial_google_maps_shortcode;
    $krucial_google_maps_shortcode['markers'] = array();

    $width = $add_transit_layer = $add_traffic_layer = $add_bicycling_layer = $height = $lat = $lng = $add_map_height = $add_map_width = $add_map_width = $add_map_styling = $add_map_pin = $add_icon = $marker_description = '';

    /**
     * Extract VC Options
     */
    extract(shortcode_atts(array(
        'fullscreen_control' => 'false',
        'streetview_control' => 'false',
        'zoom_control' => 'true',
        'map_type_control' => 'true',
        'map_type_control_options' => 'ROADMAP',
        'width' => '100',
        'width_type' => '%',
        'height' => '500',
        'height_type' => 'px',
        'lat' => '',
        'lng' => '',
        'map_name' => '',
        'styles' => '',
        'custom_map_style' => 'default',
        'zoom' => 17,
        'disable_auto_zoom' => 'true',
        'dragging_mobile' => 'true',
        'dragging_desktop' => 'true',
        'traffic_layer' => 'false',
        'transit_layer' => 'false',
        'bicycling_layer' => 'false',
        'marker_animation' => '',
        'disable_map_controls' => 'false',
    ), $atts));


    // Add Transit Layer
    if ( $transit_layer == "true" ) {
        $add_transit_layer = 'var transitLayer = new google.maps.TransitLayer();transitLayer.setMap(map);';
    }

    // Add Traffic Layer
    if ( $traffic_layer == "true" ) {
        $add_traffic_layer = 'var trafficlayer = new google.maps.TrafficLayer();trafficlayer.setMap(map);';
    }

    // Add Bicycle Layer
    if ( $bicycling_layer == "true" ) {
        $add_bicycling_layer = 'var bicyclinglayer = new google.maps.BicyclingLayer();bicyclinglayer.setMap(map);';
    }

    // Map Height
    if ( $height != '' ) {
        if( $height_type == '%' ) { $height_type = 'vh'; }
        $add_map_height = 'height: ' . $height . $height_type . '; ';
    }

    // Map Width
    if( $width != '' ) {
        $add_map_width = 'width: ' . $width . $width_type . '; ';
    }

    // Enable Dragging
    ( wp_is_mobile() ) ? $dragging = $dragging_mobile : $dragging = $dragging_desktop;

    // Add Unique ID for each map
    $id = "GoogleMap_" . uniqid();

    // Map Styles
    if ( $styles != '' ) {
        $add_map_styling = '
                var styledMap = new google.maps.StyledMapType('.rawurldecode(base64_decode(strip_tags($styles))) . ', {name: "' . $map_name . '"});
                map.mapTypes.set("map_style", styledMap);
                map.setMapTypeId("map_style");
            ';
    } else {
        $add_map_styling = '
                var styledMap = new google.maps.StyledMapType(custom_map_styles["' . $custom_map_style . '"], {name: "' . $map_name . '"});
                map.mapTypes.set("map_style", styledMap);
                map.setMapTypeId("map_style");
            ';
    }


    /**
     * Add all locations.
     */
    $markers = do_shortcode( $content );
    $all_locations = $add_places = $location_separator = '';

    foreach( $krucial_google_maps_shortcode['markers'] as $location ) {
        $pin_path = $pin_width = $pin_height = '';

        if ( $location['icon_url'] != '' ) {

            // Grab custom marker
            if ( $location['marker_type'] == 'custom' ) {
                $pin_icon = wp_get_attachment_image_src($location['icon_url'], 'full');
                if ( $pin_icon ) {
                    $pin_path = $pin_icon[0];
                    $pin_width = $pin_icon[1];
                    $pin_height = $pin_icon[2];
                }
            }
        }

        $all_locations .= $location_separator . '["' . $location['animation'] . '", ' . $location['lat'] . ', ' . $location['lng'] . ', 1, "' . $location['description'] . '", "", "' . $location['icon_url'] . '", "' . $pin_path . '", ' . $pin_width . ', ' . $pin_height . ']';
        $location_separator = ',';
    }

    $add_places = 'var places=['.$all_locations.'];';


    /**
     * Autozoom on markers
     */
    $add_disable_auto_zoom = '';
    if ($disable_auto_zoom == "false") {
        $add_disable_auto_zoom = '
            var listener = google.maps.event.addListener(map, "idle", function() { 
                if (map.getZoom() > '. $zoom .') map.setZoom('. $zoom .'); 
                google.maps.event.removeListener(listener); 
            });
        ';
    }

    /**
     * Disable map controls
     */
    if ( $disable_map_controls == "true" ) {
        $map_type_control = "false";
        $zoom_control = "false";
        $fullscreen_control = "false";
        $streetview_control = "false";
    }

    $data = '
        function initKrucialGoogleMap() {
            var custom_map_styles = {};
            
            var mapOptions = {
                scrollwheel: false,
                draggable: '.$dragging.',
                fullscreenControl: '.$fullscreen_control.',
                streetViewControl: '.$streetview_control.',
                zoomControl: '.$zoom_control.',
                mapTypeControl: '.$map_type_control.',
            }

            var map = new google.maps.Map(document.getElementById("'.$id.'"), mapOptions);
            '.$add_transit_layer.'
            '.$add_bicycling_layer.'
            '.$add_traffic_layer.'
            '.$add_map_styling.'
            '.$add_places.'

            setMarkers(map, places);

            infowindow = new google.maps.InfoWindow({
                content: "loading..."
            });
            
            // Offset center by 150px vertically
            map.panBy(0, -150);
            
            '.$add_disable_auto_zoom.'
        }
        
        function setMarkers(map, locations) {
            var bounds = new google.maps.LatLngBounds();

            for (var i = 0; i < locations.length; i++) {
                var place = locations[i], image = "";
                
                // Custom marker
                if (place[6]) {
                    image = {
                        url: place[7],
                        size: new google.maps.Size(place[8], place[9]),
                        scaledSize: new google.maps.Size(place[8] / 2, place[9] / 2),
                        origin: new google.maps.Point(0,0),
                        anchor: new google.maps.Point(place[8] / 4, place[9] / 2)
                    };
                }
                
                // Marker location
                var myLatLng = new google.maps.LatLng(place[1], place[2]);
                
                var marker = new google.maps.Marker({
                    position: myLatLng,
                    icon: image,
                    map: map,
                    zIndex: place[3],
                    html: decodeURIComponent(place[4])
                });
               
                // Marker animation
                place[0] === "DROP"
                    ? marker.setAnimation(google.maps.Animation.DROP)
                    : marker.setAnimation(google.maps.Animation.BOUNCE);
                
                bounds.extend(marker.position);
              
                // Marker tooltip
                google.maps.event.addListener(marker, "click", function () {
                    if ( decodeURIComponent( this.html ) !== "" ) {
                        infowindow.setContent(decodeURIComponent(this.html));
                        infowindow.open(map, this);
                    }
                });
            }
            map.fitBounds(bounds);   
        }                     
        google.maps.event.addDomListener(window, "load", initKrucialGoogleMap);
    ';

	// Add scripts to footer
	wp_add_inline_script('krucial-scripts', $data);

    return '<div class="krucial-google-maps"><div id="' . $id . '" style="' . $add_map_height . $add_map_width . '"></div></div>';
}


/**
 * Google Maps Marker
 */
function krucial_vc_google_maps_marker( $atts, $content = null ) {
    global $krucial_google_maps_shortcode;

    extract(shortcode_atts(array(
        'marker_icon_option' => '',
        'pin_icon' => '',
        'lat' => '',
        'lng' => '',
        'marker_description' => '',
        'marker_animation' => 'DROP',
    ), $atts));

    $marker_options = array(
        'marker_type' => $marker_icon_option,
        'icon_url' => $pin_icon,
        'lat' => $lat,
        'lng' => $lng,
        'description' => $marker_description,
        'animation' => $marker_animation,
    );

    $krucial_google_maps_shortcode['markers'][] = $marker_options;
}

add_shortcode( 'krucial_googlemaps_marker', 'krucial_vc_google_maps_marker');