<?php

function krucial_blog_carousel($atts) {
	extract( shortcode_atts( array(
		'blog_carousel_number'      => '',
		'blog_carousel_per_row'     => '',
		'blog_carousel_show_dots'   => '',
		'blog_carousel_show_nav'    => '',
		'blog_carousel_slide_speed' => '',
		'blog_carousel_autoplay' => '',
		'blog_carousel_autoplay_speed' => '',
	), $atts ) );

	ob_start();

	// Load slick slider
	wp_enqueue_script('slick');
	wp_enqueue_style('slick');

	$query = new WP_Query(array(
		'post_type'      => '',
		'posts_per_page' => esc_attr( $blog_carousel_number ),
	));

    $carousel_id = uniqid( 'blog_carousel_' );
	$slick_prev = uniqid( 'prev_' );
	$slick_next = uniqid( 'next_' );

	?>

    <div class="blog-carousel-wrapper">
        <div class="blog-carousel blog-grid" id="<?php echo esc_attr($carousel_id); ?>">
			<?php if ( $query->have_posts() ): while ( $query->have_posts() ): $query->the_post(); ?>
                <div class="blog-item">
                    <div class="blog-item-inner">
                        <?php if ( has_post_thumbnail() ) { ?>
                            <a href="<?php the_permalink(); ?>" class="blog-grid-img">
                                <?php the_post_thumbnail( 'krucial-image-700x500-cropped' ); ?>
                            </a>
                        <?php } ?>
                        <div class="blog-bottom">
                            <div class="blog-grid-categories"><?php echo krucial_post_categories(); ?></div>
                            <a href="<?php the_permalink(); ?>">
                                <div class="blog-grid-title"><?php the_title(); ?></div>
                            </a>
                            <div class="blog-grid-date"><?php echo esc_html(get_the_date()); ?></div>
                        </div>
                    </div>
                </div>
			<?php endwhile; wp_reset_postdata(); endif; ?>
        </div>
		<?php if ( $blog_carousel_show_nav === 'true' ) { ?>
            <span id="<?php echo esc_attr( $slick_next ) ?>" class="slick-arrow-right">
                <i class="essentials krucial-essential-light-01-chevron-left"></i>
            </span>
            <span id="<?php echo esc_attr( $slick_prev ) ?>" class="slick-arrow-left">
                <i class="essentials krucial-essential-light-02-chevron-right"></i>
            </span>
		<?php } ?>
    </div>

    <?php

    $data = "
        jQuery(document).ready(function ($) {
            'use strict';
            $('#" . esc_attr($carousel_id) . "').slick({
                slidesToShow: " . esc_js( $blog_carousel_per_row ) . ",
                slidesToScroll: 1,
                rows: 1,
                autoplay: " . esc_js( $blog_carousel_autoplay ) . ",
                autoplaySpeed: " . esc_js( $blog_carousel_autoplay_speed ) . ",
                dots: " . esc_js( $blog_carousel_show_dots ) . ",
                arrows: " . esc_js( $blog_carousel_show_nav ) . ",
                infinite: true,
                speed: " . esc_js( $blog_carousel_slide_speed ) . ",
                prevArrow: $('#" . esc_js( $slick_prev ) . "'),
                nextArrow: $('#" . esc_js( $slick_next ) . "'),
                responsive: [
                    {
                        breakpoint: 1000,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 1,
                            rows: 1,
                        }
                    },
                    {
                        breakpoint: 650,
                        settings: {
                            slidesToShow: 1,
                            slidestToScroll: 1,
                            rows: 1,
                        }
                    }
                ]
            });

        });
    ";

    // Add scripts to footer
	wp_add_inline_script('krucial-scripts', $data);

	$result = ob_get_contents();
	ob_end_clean();
	return $result;
}
add_shortcode('krucial-blog-carousel', 'krucial_blog_carousel');