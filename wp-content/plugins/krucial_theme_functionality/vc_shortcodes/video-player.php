<?php

function krucial_video_player($atts) {
	extract(shortcode_atts(array(
		'youtube_url' => '',
        'in_modal' => '',
        'player_icon_align' => ''
	), $atts ));

	ob_start();

	// Load plyr
	wp_enqueue_script('plyr');

	$player_id = uniqid( 'player_' );
	$video_player_id = uniqid('video_player_');
	$modal_id = uniqid('video_modal_');

	// Alignment
	if ($in_modal === 'yes') {
		$data = '#' . $player_id . ' { margin:' . $player_icon_align . '}';

		wp_register_style( 'krucial-footer', false );
		wp_enqueue_style( 'krucial-footer' );
		wp_add_inline_style( 'krucial-footer', $data );
	}


	if ($in_modal === 'yes') { ?>
        <div class="custom-video-player" id="<?php echo esc_attr($player_id); ?>">
            <i class="essentials krucial-essential-light-70-control-play"></i>
            <div class="animated-pulse">
                <div class="pulse"></div>
                <div class="pulse pulse-delay"></div>
            </div>
        </div>
        <div class="player-modal" id="<?php echo esc_attr($modal_id); ?>">
            <div class="player-content">
                <div id="<?php echo esc_attr($video_player_id); ?>" class="plyr__video-embed">
                    <iframe src="<?php echo esc_url($youtube_url); ?>?origin=https://plyr.io&amp;iv_load_policy=3&amp;modestbranding=1&amp;playsinline=1&amp;showinfo=0&amp;rel=0&amp;enablejsapi=1" allowfullscreen="allowfullscreen"></iframe>
                </div>
            </div>
            <div class="close">
                <i class="essentials krucial-essential-light-10-close-big"></i>
            </div>
        </div>
	<?php } else { ?>
        <div id="<?php echo esc_attr($video_player_id); ?>" class="plyr__video-embed">
            <iframe src="<?php echo esc_url($youtube_url); ?>?origin=https://plyr.io&amp;iv_load_policy=3&amp;modestbranding=1&amp;playsinline=1&amp;showinfo=0&amp;rel=0&amp;enablejsapi=1" allowfullscreen="allowfullscreen"></iframe>
        </div>
	<?php }

	$data = "
        jQuery(document).ready(function ($) {
            'use strict';
            
            // Initialize player
            var player = new Plyr('#" . esc_js($video_player_id) . "');
            
            // Open modal
            $('#" . esc_js($player_id) . "').on('click', function(){
                var modal = $('#" . esc_js($modal_id) . "');
                modal.css('display', 'flex');
                // Stop fixed header from showing in modal
                $('.header').addClass('fade-out-hide');
                setTimeout(function(){
                    modal.addClass('show');
                    $('body').css('overflow', 'hidden');
                }, 100);
                setTimeout(function(){
                    // Make the close button clickable (header not in way)
                    $('.header').css('z-index', '1');
                }, 500);
            });
            
            // Close modal
            $('#" . esc_js($modal_id) . " .close').on('click', function(){
                $('#" . esc_js($modal_id) . "').removeClass('show');
                // Put header back to it's original state
                $('.header').removeClass('fade-out-hide');
                $('.header').css('z-index', '');
                setTimeout(function(){
                    $('#" . esc_js($modal_id) . "').css('display', 'none');
                    $('body').css('overflow', 'initial');
                }, 300);
            });
        });
    ";

	// Add scripts to footer
	wp_add_inline_script('krucial-scripts', $data);

	$result = ob_get_contents();
	ob_end_clean();
	return $result;
}
add_shortcode('krucial-video-player', 'krucial_video_player');