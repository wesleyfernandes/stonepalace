<?php

function krucial_testimonials($atts) {
    extract(shortcode_atts(array(
        'testimonials_number' => '',
        'testimonials_per_row' => '',
        'testimonials_categories' => '',
        'testimonials_link' => '',
    ), $atts));

	ob_start();

    // If category selected, do the tax_query
    if ( $testimonials_categories !== 'all' ) {
        $args = array(
            'post_type' => 'testimonials',
            'posts_per_page' => esc_html( $testimonials_number ),
            'tax_query' => array(
                array(
                    'taxonomy' => 'testimonial_categories',
                    'field'    => 'slug',
                    'terms'    => $testimonials_categories,
                )
            ),
        );
    } else {
        $args = array(
            'post_type' => 'testimonials',
            'posts_per_page' => esc_html( $testimonials_number ),
        );
    }

    $query = new WP_Query( $args );

    ?>

    <div class="testimonials-wrap row">
        <?php if ( $query->have_posts() ): while( $query->have_posts() ): $query->the_post(); ?>
            <div class="<?php echo esc_attr( $testimonials_per_row ); ?>">
                <div class="testimonial">
                    <div class="testimonial-inner">
                        <div class="testimonial-excerpt"><?php the_excerpt(); ?></div>
                        <?php if ( $testimonials_link === 'true' ) { ?>
                            <a class="testimonial-link" href="<?php echo get_the_permalink(); ?>">
                                <?php echo esc_html__( 'Read More', 'krucial_theme_functionality' ) ?>
                                <i class="icon-arrow-right"></i>
                            </a>
                        <?php } ?>
                    </div>
                    <div class="testimonial-bottom">
                        <div class="testimonial-img"><?php the_post_thumbnail( 'krucial-image-150x150-cropped' ); ?></div>
                        <div class="left">
                            <div class="testimonial-name"><?php the_title(); ?></div>
                            <div class="testimonial-company"><?php echo esc_html( get_field('testimonial_company') ); ?></div>
                        </div>
                    </div>
                </div>
            </div>
        <?php endwhile; wp_reset_postdata(); endif; ?>
    </div>
	<?php

	$result = ob_get_contents();
	ob_end_clean();
	return $result;
}
add_shortcode('krucial-testimonials', 'krucial_testimonials');