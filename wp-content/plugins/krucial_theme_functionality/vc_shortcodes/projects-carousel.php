<?php

function krucial_projects_carousel($atts) {
	extract( shortcode_atts( array(
		'projects_carousel_number' => '',
		'projects_carousel_per_row' => '',
		'projects_carousel_show_dots' => '',
		'projects_carousel_show_nav' => '',
		'projects_carousel_slide_speed' => '',
		'projects_carousel_categories' => '',
		'projects_carousel_autoplay' => '',
		'projects_carousel_autoplay_speed' => '',
	), $atts ) );

	ob_start();

	// Load slick slider
	wp_enqueue_script('slick');
	wp_enqueue_style('slick');

    // If category selected, do the tax_query
	if ( $projects_carousel_categories !== 'all' ) {
		$args = array(
			'post_type'      => 'projects',
			'posts_per_page' => esc_attr( $projects_carousel_number ),
			'tax_query'      => array(
				array(
					'taxonomy' => 'project_categories',
					'field'    => 'slug',
					'terms'    => $projects_carousel_categories,
				)
			),
		);
	} else {
		$args = array(
			'post_type'      => 'projects',
			'posts_per_page' => esc_attr( $projects_carousel_number ),
		);
	}

	$query = new WP_Query( $args );
	$carousel_id = uniqid( 'projects_carousel_' );
	$slick_prev = uniqid( 'prev_' );
	$slick_next = uniqid( 'next_' );

	?>

    <div class="projects-carousel-wrapper">
        <div class="projects-carousel" id="<?php echo esc_attr($carousel_id); ?>">
            <?php if ( $query->have_posts() ): while ( $query->have_posts() ): $query->the_post(); ?>
                <div class="project-wrap">
                    <a href="<?php the_permalink(); ?>">
                        <figure>
                            <div class="image">
                                <?php the_post_thumbnail( 'krucial-image-600x500-cropped' ); ?>
                            </div>
                            <figcaption>
                                <div class="title"><?php the_title(); ?></div>
                                <div class="view">
                                    <span><?php echo esc_html__('View Project', 'krucial_theme_functionality'); ?></span>
                                    <i class="essentials krucial-essential-light-07-arrow-right"></i>
                                </div>
                            </figcaption>
                        </figure>
                    </a>
                </div>
            <?php endwhile; wp_reset_postdata(); endif; ?>
        </div>
	    <?php if ( $projects_carousel_show_nav === 'true' ) { ?>
            <span id="<?php echo esc_attr( $slick_next ) ?>" class="slick-arrow-right">
                <i class="essentials krucial-essential-light-01-chevron-left"></i>
            </span>
            <span id="<?php echo esc_attr( $slick_prev ) ?>" class="slick-arrow-left">
                <i class="essentials krucial-essential-light-02-chevron-right"></i>
            </span>
        <?php } ?>
    </div>
	<?php

	$data = "
        jQuery(document).ready(function($) {
            'use strict';
            $('#" . esc_attr($carousel_id) . "').slick({
                slidesToShow: " . esc_js( $projects_carousel_per_row ) . ",
                slidesToScroll: 1,
                rows: 1,
                autoplay: " . esc_js( $projects_carousel_autoplay ) . ",
                autoplaySpeed: " . esc_js( $projects_carousel_autoplay_speed ) . ",
                dots: " . esc_js( $projects_carousel_show_dots ) . ",
                arrows: " . esc_js( $projects_carousel_show_nav ) . ",
                infinite: true,
                speed: " . esc_js( $projects_carousel_slide_speed ) . ",
                prevArrow: $('#" . esc_js( $slick_prev ) . "'),
                nextArrow: $('#" . esc_js( $slick_next ) . "'),
                responsive: [
                    {
                        breakpoint: 1000,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 1,
                            rows: 1,
                        }
                    },
                    {
                        breakpoint: 600,
                        settings: {
                            slidesToShow: 1,
                            slidestToScroll: 1,
                            rows: 1,
                        }
                    }
                ]
            });

        });
    ";

	// Add scripts to footer
	wp_add_inline_script('krucial-scripts', $data);

	$result = ob_get_contents();
	ob_end_clean();
	return $result;
}
add_shortcode('krucial-projects-carousel', 'krucial_projects_carousel');