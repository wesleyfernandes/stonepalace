<?php

function krucial_animate($atts, $content = '') {
	extract( shortcode_atts( array(
		'animate_element_type'     => '',
		'animate_element_duration' => '',
		'animate_element_delay'    => '',
		'animate_element_offset'   => '',
	), $atts ) );

	ob_start();

	$unique_id = uniqid( 'animation-' );
	$duration = 'animation-duration:' . esc_attr( $animate_element_duration ) . 's;' .
	            '-moz-animation-duration:' . esc_attr( $animate_element_duration ) . 's;' .
	            '-webkit-animation-duration:' . esc_attr( $animate_element_duration ) . 's;';
	$delay = 'animation-delay:' . esc_attr( $animate_element_delay ) . 's;' .
             '-moz-animation-delay:' . esc_attr( $animate_element_delay ) . 's;' .
             '-webkit-animation-delay:' . esc_attr( $animate_element_delay ) . 's;';

    // Set default offset
	(!empty($animate_element_offset)) ? $offset = $animate_element_offset : $offset = 0;

	?>

    <div class="<?php echo esc_attr( $unique_id ) ?> animation_hidden" style="<?php echo esc_attr( $duration ) . esc_attr( $delay ) ?>">
		<?php echo do_shortcode( $content ); ?>
    </div>

    <?php

	$data = "
        jQuery(document).ready(function ($) {
            $('." . esc_attr( $unique_id ) . "').viewportChecker({
                classToAdd: 'animation_visible animated " . esc_js( $animate_element_type ) . "',
                classToRemove: 'animation_hidden',
                offset: " . esc_js( $offset ) . "
            });
        });
    ";

	// Add scripts to footer
	wp_add_inline_script('krucial-scripts', $data);

	$result = ob_get_contents();
	ob_end_clean();
	return $result;
}
add_shortcode('krucial_animate', 'krucial_animate');