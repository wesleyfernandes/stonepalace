<?php

function krucial_featured_box($atts) {
	extract(shortcode_atts(array(
        'featured_box_style' => '',
		'featured_box_image' => '',
		'featured_box_title' => '',
		'featured_box_text' => '',
		/*LINK*/
		'featured_box_enable_link' => '',
		'featured_box_link_text' => '',
		'featured_box_link_url' => '',
		'featured_box_link_target' =>'',
		/*EXTRA LINK*/
		'featured_box_extra_enable_link' => '',
		'featured_box_extra_link_text' => '',
		'featured_box_extra_link_url' => '',
		'featured_box_extra_link_target' =>'',
		/*ICO*/
		'featured_box_enable_icon' => '',
		'featured_box_icon' => '',
	), $atts));

	ob_start();
	
	$featured_box_link_target = esc_url( $featured_box_link_target );
    // Featured box image URL
	$featured_image = wp_get_attachment_image_src( $featured_box_image, 'krucial-image-700x900-cropped' )[0];

	if ($featured_box_style === 'style_1') { ?>
        <div class="featured-box style-1" style="background: url(<?php echo esc_url( $featured_image ); ?>)">
            <div class="featured-box-inner">
                <div class="featured-box-title">
		            <?php if ($featured_box_enable_icon === 'true') { ?>
                        <i class="<?php echo esc_attr($featured_box_icon); ?>"></i>
		            <?php } ?>
                    <span><?php echo esc_html( $featured_box_title ); ?></span>
                </div>
                <div class="featured-box-text"><?php echo esc_html( $featured_box_text ); ?></div>
				<?php

				if ( $featured_box_enable_link === 'true' ) { ?>
                    <div class="featured-box-link">
                        <a href="<?php echo esc_url( $featured_box_link_url ); ?>" target="<?php echo str_replace('http://', '',$featured_box_link_target ); ?>">
							<span><?php echo esc_html( $featured_box_link_text ); ?></span>
                            <i class="essentials krucial-essential-light-07-arrow-right"></i>
                        </a>
                    </div>
				<?php }
				if ( $featured_box_extra_enable_link === 'true' ) { ?>
                    <div class="featured-box-link">
                        <a href="<?php echo esc_url( $featured_box_extra_link_url ); ?>" target="<?php echo str_replace('http://', '',$featured_box_extra_link_target ); ?>">
                            <span><?php echo esc_html( $featured_box_extra_link_text ); ?></span>
                            <i class="essentials krucial-essential-light-07-arrow-right"></i>
                        </a>
                    </div>
				<?php }

				?>
            </div>
        </div>
		<?php
	} elseif($featured_box_style === 'style_2') { ?>
        <div class="featured-box style-2">
            <div class="featured-img-wrap">
                <div class="featured-img" style="background: url(<?php echo esc_url( $featured_image ); ?>)"></div>
            </div>
            <div class="featured-box-inner">
                <div class="featured-box-title">
                    <?php if ($featured_box_enable_icon === 'true') { ?>
                        <i class="<?php echo esc_attr($featured_box_icon); ?>"></i>
                    <?php } ?>
                    <span><?php echo esc_html( $featured_box_title ); ?></span>
                </div>
                <div class="featured-box-text"><?php echo esc_html( $featured_box_text ); ?></div>
				<?php

				if ( $featured_box_enable_link === 'true' ) { ?>
                    <div class="featured-box-link">
                        <a href="<?php echo esc_url( $featured_box_link_url ); ?>" target="<?php echo str_replace('http://', '',$featured_box_link_target ); ?>">
                            <span><?php echo esc_html( $featured_box_link_text ); ?></span>
                            <i class="essentials krucial-essential-light-07-arrow-right"></i>
                        </a>
                    </div>
				<?php }
				if ( $featured_box_extra_enable_link === 'true' ) { ?>
                    <div class="featured-box-link">
                        <a href="<?php echo esc_url( $featured_box_extra_link_url ); ?>" target="<?php echo str_replace('http://', '',$featured_box_extra_link_target ); ?>">
                            <span><?php echo esc_html( $featured_box_extra_link_text ); ?></span>
                            <i class="essentials krucial-essential-light-07-arrow-right"></i>
                        </a>
                    </div>
				<?php }

				?>
            </div>
        </div>
		<?php
	} elseif($featured_box_style === 'style_3') { ?>
        <div class="featured-box style-3">
            <div class="featured-img-wrap">
                <div class="featured-img" style="background: url(<?php echo esc_url( $featured_image ); ?>)"></div>
            </div>
            <div class="featured-box-inner">
                <div class="featured-box-title">
                    <?php if ($featured_box_enable_icon === 'true') { ?>
                        <i class="<?php echo esc_attr($featured_box_icon); ?>"></i>
                    <?php } ?>
                    <span><?php echo esc_html( $featured_box_title ); ?></span>
                </div>
                <div class="featured-box-text"><?php echo esc_html( $featured_box_text ); ?></div>
				<?php

				if ( $featured_box_enable_link === 'true' ) { ?>
                    <div class="featured-box-link">
                        <a href="<?php echo esc_url( $featured_box_link_url ); ?>" target="<?php echo str_replace('http://', '',$featured_box_link_target ); ?>">
                            <span><?php echo esc_html( $featured_box_link_text ); ?></span>
                            <i class="essentials krucial-essential-light-07-arrow-right"></i>
                        </a>
                    </div>
				<?php }
				if ( $featured_box_extra_enable_link === 'true' ) { ?>
                    <div class="featured-box-link">
                        <a href="<?php echo esc_url( $featured_box_extra_link_url ); ?>" target="<?php echo str_replace('http://', '',$featured_box_extra_link_target ); ?>">
                            <span><?php echo esc_html( $featured_box_extra_link_text ); ?></span>
                            <i class="essentials krucial-essential-light-07-arrow-right"></i>
                        </a>
                    </div>
				<?php }

				?>
            </div>
        </div>
    <?php 
	} else { ?>
        <div class="featured-box style-4" style="background: url(<?php echo esc_url( $featured_image ); ?>)">
            <div class="featured-box-inner">
                <div class="featured-box-title">
		            <?php if ($featured_box_enable_icon === 'true') { ?>
                        <i class="<?php echo esc_attr($featured_box_icon); ?>"></i>
		            <?php } ?>
                    <span><?php echo esc_html( $featured_box_title ); ?></span>
                </div>
                <div class="featured-box-text"><?php echo esc_html( $featured_box_text ); ?></div>
				<?php

				if ( $featured_box_enable_link === 'true' ) { ?>
                    <div class="featured-box-link">
                        <a href="<?php echo esc_url( $featured_box_link_url ); ?>" target="<?php echo str_replace('http://', '',$featured_box_link_target ); ?>">
							<span><?php echo esc_html( $featured_box_link_text ); ?></span>
                            <i class="essentials krucial-essential-light-07-arrow-right"></i>
                        </a>
                    </div>
				<?php }
				if ( $featured_box_extra_enable_link === 'true' ) { ?>
                    <div class="featured-box-link">
                        <a href="<?php echo esc_url( $featured_box_extra_link_url ); ?>" target="<?php echo str_replace('http://', '',$featured_box_extra_link_target ); ?>">
                            <span><?php echo esc_html( $featured_box_extra_link_text ); ?></span>
                            <i class="essentials krucial-essential-light-07-arrow-right"></i>
                        </a>
                    </div>
				<?php }

				?>
            </div>
        </div>
    <?php }

	$result = ob_get_contents();
	ob_end_clean();
	return $result;
}
add_shortcode('krucial-featured-box', 'krucial_featured_box');