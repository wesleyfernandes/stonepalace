<?php

function krucial_testimonials_carousel($atts) {
    extract(shortcode_atts(array(
        'testimonials_carousel_number' => '',
        'testimonials_carousel_categories' => '',
        'testimonials_carousel_per_row' => '',
        'testimonials_carousel_show_dots' => '',
        'testimonials_carousel_show_nav' => '',
        'testimonials_carousel_slide_speed' => '',
        'testimonials_carousel_link' => '',
        'testimonials_carousel_autoplay' => '',
        'testimonials_carousel_autoplay_speed' => '',
    ), $atts));

    ob_start();

	// Load slick slider
	wp_enqueue_script('slick');
	wp_enqueue_style('slick');

    // If category selected, do the tax_query
    if ( $testimonials_carousel_categories !== 'all' ) {
        $args = array(
            'post_type' => 'testimonials',
            'posts_per_page' => esc_html( $testimonials_carousel_number ),
            'tax_query' => array(
                array(
                    'taxonomy' => 'testimonial_categories',
                    'field'    => 'slug',
                    'terms'    => $testimonials_carousel_categories,
                )
            ),
        );
    } else {
        $args = array(
            'post_type' => 'testimonials',
            'posts_per_page' => esc_html( $testimonials_carousel_number ),
        );
    }

    $query = new WP_Query( $args );
	$carousel_id = uniqid( 'blog_carousel_' );
    $slick_prev = uniqid( 'prev_' );
    $slick_next = uniqid( 'next_' );

    ?>

    <div class="testimonials-carousel-wrapper testimonials-wrap">
        <div class="testimonials-carousel" id="<?php echo esc_attr($carousel_id); ?>">
            <?php if ( $query->have_posts() ): while( $query->have_posts() ): $query->the_post(); ?>
                <div class="testimonials-carousel-item">
                    <div class="testimonial">
                        <div class="testimonial-inner">
                            <div class="testimonial-excerpt"><?php the_excerpt(); ?></div>
		                    <?php if ( $testimonials_carousel_link === 'true' ) { ?>
                                <a class="testimonial-link" href="<?php echo get_the_permalink(); ?>">
                                    <?php echo esc_html__( 'Read More', 'krucial_theme_functionality' ) ?>
                                    <i class="icon-arrow-right"></i>
                                </a>
		                    <?php } ?>
                        </div>
                        <div class="testimonial-bottom">
                            <div class="testimonial-img"><?php the_post_thumbnail( 'krucial-image-150x150-cropped' ); ?></div>
                            <div class="left">
                                <div class="testimonial-name"><?php the_title(); ?></div>
                                <div class="testimonial-company"><?php echo esc_html( get_field('testimonial_company') ); ?></div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endwhile; wp_reset_postdata(); endif; ?>
        </div>
        <?php if ( $testimonials_carousel_show_nav === 'true' ) { ?>
            <span id="<?php echo esc_attr( $slick_next ) ?>" class="slick-arrow-right">
                <i class="essentials krucial-essential-light-01-chevron-left"></i>
            </span>
            <span id="<?php echo esc_attr( $slick_prev ) ?>" class="slick-arrow-left">
                <i class="essentials krucial-essential-light-02-chevron-right"></i>
            </span>
        <?php } ?>
    </div>

	<?php

	$data = "
        jQuery(document).ready(function ($) {
            'use strict';
            $('#" . esc_attr($carousel_id) . "').slick({
                slidesToShow: " . esc_js( $testimonials_carousel_per_row ) . ",
                slidesToScroll: 1,
                rows: 1,
                autoplay: " . esc_js( $testimonials_carousel_autoplay ) . ",
                autoplaySpeed: " . esc_js( $testimonials_carousel_autoplay_speed ) . ",
                dots: " . esc_js( $testimonials_carousel_show_dots ) . ",
                arrows: " . esc_js( $testimonials_carousel_show_nav ) . ",
                infinite: true,
                speed: " . esc_js( $testimonials_carousel_slide_speed ) . ",
                prevArrow: $('#" . esc_js( $slick_prev ) . "'),
                nextArrow: $('#" . esc_js( $slick_next ) . "'),
                responsive: [
                    {
                        breakpoint: 1100,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 1,
                            rows: 1,
                        }
                    },
                    {
                        breakpoint: 768,
                        settings: {
                            slidesToShow: 1,
                            slidestToScroll: 1,
                            rows: 1,
                        }
                    }
                ]
            });

        });
    ";

	// Add scripts to footer
	wp_add_inline_script('krucial-scripts', $data);

	$result = ob_get_contents();
	ob_end_clean();
	return $result;
}
add_shortcode('krucial-testimonials-carousel', 'krucial_testimonials_carousel');