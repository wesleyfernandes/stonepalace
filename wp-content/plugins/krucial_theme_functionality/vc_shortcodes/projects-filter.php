<?php

function krucial_projects_filter($atts) {
    extract(shortcode_atts(array(
        'projects_filter_per_row' => '',
    ), $atts ));

    ob_start();

	// Load dependencies
	wp_enqueue_script('isotope');

    $query = new WP_Query(array(
        'post_type' => 'projects',
    ));

    // Layout
    switch ($projects_filter_per_row) {
        case '2': {
            $style = '50%';
            break;
        }
        case '3': {
            $style = '33.333333%';
            break;
        }
        default: {
            $style = '25%';
        }
    }

    // Project width layout
    $data = '.project-wrap { width:' . $style . '}';

	wp_register_style( 'krucial-footer', false );
	wp_enqueue_style( 'krucial-footer' );
	wp_add_inline_style( 'krucial-footer', $data );

    ?>

    <!-- Filter -->
    <div class="filter-by-main">Filtrar por <span>Todos</span><i class="essentials krucial-essential-light-09-arrow-down"></i></div>
    <div class="sort-projects-overlay">
        <div class="filter-by">Filtrar por </div>
        <div class="sort-projects">
            <a href="#" data-filter="*" class="current"><?php echo esc_html__( 'Todos', 'krucial_theme_functionality') ?></a>
            <?php
            $terms = get_terms( 'project_categories' );

            foreach ( $terms as $term ) {
                echo '<a href="#" data-filter=".' . $term->slug . '">' . $term->name . ' ' . '</a>';
            }
            ?>
        </div>
        <div class="close">
            <i class="essentials krucial-essential-light-10-close-big"></i>
        </div>
    </div>

    <div class="projects-filter row">
        <?php if( $query->have_posts() ): while( $query->have_posts() ): $query->the_post();
            $cat_object = wp_get_object_terms( get_the_ID(), 'project_categories', array( 'fields' => 'slugs' ) );
            $cat_string = implode( ' ', $cat_object );

            ?>

            <div class="project-wrap <?php echo esc_attr( $cat_string ) ?>">
                <a href="<?php the_permalink(); ?>">
                    <figure>
                        <div class="image">
                            <?php the_post_thumbnail( 'krucial-image-600x500-cropped' ); ?>
                        </div>
                        <figcaption>
                            <div class="title"><?php the_title(); ?></div>
                            <div class="view">
                                <span><?php echo esc_html__('Ver projeto', 'krucial_theme_functionality'); ?></span>
                                <i class="essentials krucial-essential-light-07-arrow-right"></i>
                            </div>
                        </figcaption>
                    </figure>
                </a>
            </div>
        <?php endwhile; wp_reset_postdata(); endif; ?>
    </div>

    <?php

    $data = "
        jQuery(document).ready(function($) {
           'use_strict';
            var container = $('.projects-filter');

            // Layout Isotope after each image loads
            container.imagesLoaded().progress( function() {
                container.isotope({
                    filter: '*',
                    maxRows: 1,
                    layoutMode: 'fitRows',
                    animationOptions: {
                        duration: 750,
                        easing: 'linear',
                        queue: false
                    }
                });
            });
            
            // Open overlay
            $('.filter-by-main').on('click', function(){
                $('.sort-projects-overlay').css('display', 'flex');
                // Stop fixed header from showing in modal
                $('.header').addClass('fade-out-hide');
                setTimeout(function(){
                    $('.sort-projects-overlay').addClass('show');
                    $('body').css('overflow', 'hidden');
                }, 100);
                setTimeout(function(){
                    // Make the close button clickable (header not in way)
                    $('.header').css('z-index', '1');
                }, 500);
            });
            
            // Close overlay
            $('.sort-projects-overlay .close').on('click', function(){
                $('.sort-projects-overlay').removeClass('show');
                // Put header back to it's original state
                $('.header').removeClass('fade-out-hide');
                $('.header').css('z-index', '');
                setTimeout(function(){
                    $('.sort-projects-overlay').css('display', 'none');
                    $('body').css('overflow', 'initial');
                }, 300);
            });

            // The filter
            $('.sort-projects a').click(function(){
                // Remove overlay
                $('.sort-projects-overlay').removeClass('show');
                // Put header back to it's original state
                $('.header').removeClass('fade-out-hide');
                $('.header').css('z-index', '');
                setTimeout(function(){
                    $('.sort-projects-overlay').css('display', 'none');
                    $('body').css('overflow', 'initial');
                }, 300);
                
                // Add chosen category to .filter-by-main
                var chosenText = $(this).text();
                $('.filter-by-main span').text(chosenText);
            
                // Sort projects
                $('.sort-projects .current').removeClass('current');
                $(this).addClass('current');
                var selector = $(this).attr('data-filter');
                container.isotope({
                    filter: selector,
                    animationOptions: {
                        duration: 750,
                        easing: 'linear',
                        queue: false
                    }
                });
                return false;
            });
        });
    ";

	// Add scripts to footer
	wp_add_inline_script('krucial-scripts', $data);

	$result = ob_get_contents();
	ob_end_clean();
	return $result;
}
add_shortcode('krucial-projects-filter', 'krucial_projects_filter');