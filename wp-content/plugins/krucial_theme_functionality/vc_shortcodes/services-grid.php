<?php

function krucial_services_grid($atts) {
	extract(shortcode_atts(array(
		'services_grid_number' => '',
		'services_grid_layout' => '',
	), $atts ));

	ob_start();

	$query = new WP_Query(array(
		'post_type' => 'services',
		'posts_per_page' => esc_attr( $services_grid_number ),
	));

	?>

    <div class="services-grid row sm-gutters">
		<?php if( $query->have_posts() ): while( $query->have_posts() ): $query->the_post(); ?>
            <div class="<?php echo esc_attr( $services_grid_layout ) ?>">
                <div class="service-item" style="background: url(<?php the_post_thumbnail_url('krucial-image-700x900-cropped'); ?>)">
                    <div class="item-content">
                        <div class="services-grid-title"><?php the_title(); ?></div>
                        <div class="services-grid-excerpt"><?php the_excerpt(); ?></div>
                        <div class="services-grid-link">
                            <a href="<?php the_permalink(); ?>">
                                <span><?php echo esc_html__( 'Ver Serviço', 'krucial_theme_functionality' ); ?></span>
                                <i class="essentials krucial-essential-light-07-arrow-right"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
		<?php endwhile; wp_reset_postdata(); endif; ?>
    </div>

	<?php

	$result = ob_get_contents();
	ob_end_clean();
	return $result;
}
add_shortcode('krucial-services-grid', 'krucial_services_grid');