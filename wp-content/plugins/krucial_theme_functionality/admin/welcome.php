<?php

$theme_name = wp_get_theme()->get('Name');

?>

<div class="logo-wrapper">
    <img class="logo" src="<?php echo esc_url(get_template_directory_uri() . '/admin/logo.svg'); ?>" width="200">
</div>

<!-- First Row -->
<div class="boxes-wrap">
    <div class="box-wrapper">
        <div class="box-inner">
            <h3 class="title">Documentation</h3>
            <p>Welcome, and thank-you for purchasing <?php echo esc_html($theme_name); ?>, your support is appreciated. Please make sure to read the themes documentation which is included within the theme package you downloaded from ThemeForest. You can also read the themes documentation online using the link below.</p>
            <a href="http://oculis.krucialthemes.com/docs" target="_blank">Online Documentation</a>
        </div>
    </div>
    <div class="box-wrapper">
        <div class="box-inner">
            <h3 class="title">Server Information</h3>
            <p>You can use the Server Info tab under <?php echo esc_html($theme_name); ?> > Server Info, to find out what system status parameters your server has. This will vary by default from host to host, you can change these parameters by reading our documentation, or also calling your host and asking them to do it.</p>
            <a href="http://oculis.krucialthemes.com/docs/templates/overview/system-status-parameters.html" target="_blank">Read More On This</a>
        </div>
    </div>
    <div class="box-wrapper">
        <div class="box-inner">
            <h3 class="title">Importing Demos</h3>
            <p>Easily import anyone one of our pre-installable demos with the click of a button in <?php echo esc_html($theme_name); ?> > Theme Options > Demo Importer. Please be patient while a demo uploads, this may take up to 5+ minutes. If a demo install fails, you may need to update your server's system status parameters.</p>
            <a href="<?php echo esc_url(admin_url('admin.php?page=theme-options')); ?>">Import A Demo</a>
        </div>
    </div>
</div>

<!-- Second Row -->
<div class="boxes-wrap">
    <div class="box-wrapper">
        <div class="box-inner">
            <h3 class="title">Our Themes</h3>
            <p>As well as sending out new updates and features with our current themes, we're working hard in the background to bring you some of the best WordPress themes on the market. Stay updated and check out our other premium WordPress Themes on ThemeForest.</p>
            <a href="https://themeforest.net/user/krucialau/portfolio" target="_blank">Checkout Our Themes</a>
        </div>
    </div>
    <div class="box-wrapper">
        <div class="box-inner">
            <h3 class="title">Help & Support</h3>
            <p>Read our documentation and still need some help? No worries we provide full 5-star theme support. Before contacting support please make sure to read the documentation first, and also try disabling any third party plugins to see if this resolves the issue you are experiencing. You can get full theme support by sending us a message through the contact form on our profile page over at ThemeForest.</p>
            <a href="https://themeforest.net/user/krucialau" target="_blank">Get Theme Support</a>
        </div>
    </div>
</div>