<?php

include_once(KRUCIAL_PLUGIN_PATH . '/admin/server-info/server-info.php');

// Format Bytes Into TiB/GiB/MiB/KiB/Bytes
if (!function_exists('krucial_format_filesize')) {
    function krucial_format_filesize($rawSize) {
        if ($rawSize / 1099511627776 > 1) {
            return number_format_i18n($rawSize/1099511627776, 1) . ' TiB';
        } elseif ($rawSize / 1073741824 > 1) {
            return number_format_i18n($rawSize/1073741824, 1) . ' GiB';
        } elseif ($rawSize / 1048576 > 1) {
            return number_format_i18n($rawSize/1048576, 1) . ' MiB';
        } elseif ($rawSize / 1024 > 1) {
            return number_format_i18n($rawSize/1024, 1) . ' KiB';
        } elseif ($rawSize > 1) {
            return number_format_i18n($rawSize, 0) . ' bytes';
        } else {
            return 'unknown';
        }
    }
}

// Get PHP Max Upload Size
if (!function_exists('krucial_get_php_upload_max')) {
    function krucial_get_php_upload_max() {
        if (ini_get('upload_max_filesize')) {
            $upload_max = ini_get('upload_max_filesize');
        } else {
            $upload_max = 'N/A';
        }
        return $upload_max;
    }
}

// Get PHP Max Post Size
if (!function_exists('krucial_get_php_post_max')) {
    function krucial_get_php_post_max() {
        if (ini_get('post_max_size')) {
            $post_max = ini_get('post_max_size');
        } else {
            $post_max = 'N/A';
        }
        return $post_max;
    }
}

// PHP Maximum Execution Time
if (!function_exists('krucial_get_php_max_execution')) {
    function krucial_get_php_max_execution() {
        if (ini_get('max_execution_time')) {
            $max_execute = ini_get('max_execution_time');
        } else {
            $max_execute = 'N/A';
        }
        return $max_execute;
    }
}

// PHP Memory Limit
if (!function_exists('krucial_get_php_memory_limit')) {
    function krucial_get_php_memory_limit() {
        if (ini_get('memory_limit')) {
            $memory_limit = ini_get('memory_limit');
        } else {
            $memory_limit = 'N/A';
        }
        return $memory_limit;
    }
}

// Get MYSQL Version
if (!function_exists('krucial_get_mysql_version')) {
    function krucial_get_mysql_version() {
        global $wpdb;
        return $wpdb->get_var("SELECT VERSION() AS version");
    }
}

// Convert PHP Size Format to Localized
function krucial_format_php_size($size) {
    if (!is_numeric($size)) {
        if (strpos($size, 'M') !== false) {
            $size = intval($size)*1024*1024;
        } elseif (strpos($size, 'K') !== false) {
            $size = intval($size)*1024;
        } elseif (strpos($size, 'G') !== false) {
            $size = intval($size)*1024*1024*1024;
        }
    }
    return is_numeric($size) ? krucial_format_filesize($size) : $size;
}

?>