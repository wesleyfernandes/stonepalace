<?php

function server_info() {
global $is_IIS;

?>

<div class="server-info-top">

	<h2 class="heading"><?php echo esc_html(wp_get_theme()->get('Name')); ?></h2>
	<p class="text">Below is your server information and settings. If you are having problems importing our demo content, this is
		most likely due to your server's configuration. Please read our documentation on system status
		parameters and how to update them.</p>

	<span>Documentation: <a href="https://theme.krucialthemes.com/docs" target="_blank">https://theme.krucialthemes.com/docs</a></span><br>
	<span>Help & Support: <a href="https://themeforest.net/user/krucialau" target="_blank">https://themeforest.net/user/krucialau</a></span>

</div>

<div class="server-info-wrap">
    <h2 class="heading-medium">Server Information</h2>
	<table class="table">

		<!-- OS -->
		<tr>
			<td><h5>Operating System:</h5></td>
			<td><p><?php echo esc_html(PHP_OS); ?></p></td>
		</tr>

		<!-- Server -->
		<tr class="row-grey">
			<td><h5>Server:</h5></td>
			<td><p><?php echo esc_html($_SERVER['SERVER_SOFTWARE']); ?></p></td>
		</tr>
		<tr>

        <!-- PHP Version -->
		<tr>
			<td><h5>PHP Version:</h5></td>
			<td><p>v<?php echo esc_html(PHP_VERSION); ?></p></td>
		</tr>

		<!-- MYSQL Version -->
		<tr class="row-grey">
			<td><h5>MYSQL Version:</h5></td>
			<td><p>v<?php echo esc_html(krucial_get_mysql_version()); ?></p></td>
		</tr>
		<tr>

        <!-- Server Hostname -->
		<tr>
			<td><h5>Server Hostname:</h5></td>
			<td><p><?php echo esc_html($_SERVER['SERVER_NAME']); ?></p></td>
		</tr>

		<!-- Server IP Port -->
		<tr class="row-grey">
			<td><h5>Server IP Port:</h5></td>
			<td><p><?php echo ($is_IIS ? esc_html($_SERVER['LOCAL_ADDR']) : esc_html($_SERVER['SERVER_ADDR'])) . ' :' . esc_html($_SERVER['SERVER_PORT']); ?></p></td>
		</tr>
		<tr>

        <!-- Server Document Root -->
		<tr>
			<td><h5>Server Document Root:</h5></td>
			<td><p><?php echo esc_html($_SERVER['DOCUMENT_ROOT']); ?></p></td>
		</tr>

		<!-- Server Date/Time -->
		<tr class="row-grey">
			<td><h5>Server Date/Time:</h5></td>
			<td>
				<p><?php echo esc_html(mysql2date(sprintf('%s @ %s', get_option('date_format'), get_option('time_format')), current_time('mysql'))); ?></p>
			</td>
		</tr>
		<tr>

	</table>

    <h2 class="heading-medium">System Status Parameters</h2>
	<table class="table">

		<!-- WP Memory Limit -->
		<tr>
			<td><h5>WP Memory Limit:</h5></td>
			<td><p><?php echo esc_html(WP_MEMORY_LIMIT); ?></p></td>
		</tr>

		<!-- PHP Memory Limit -->
		<tr class="row-grey">
			<td><h5>PHP Memory Limit:</h5></td>
			<td><p><?php echo esc_html(krucial_format_php_size(krucial_get_php_memory_limit())); ?></p></td>
		</tr>

		<!-- PHP Max Upload Size -->
		<tr>
			<td><h5>PHP Max Upload Size:</h5></td>
			<td><p><?php echo esc_html(krucial_format_php_size(krucial_get_php_upload_max())); ?></p></td>
		</tr>

		<!-- PHP Max Post Size -->
		<tr class="row-grey">
			<td><h5>PHP Max Post Size:</h5></td>
			<td><p><?php echo esc_html(krucial_format_php_size(krucial_get_php_post_max())); ?></p></td>
		</tr>

		<!-- PHP Max Execution Time -->
		<tr>
			<td><h5>PHP Max Execution Time:</h5></td>
			<td><p><?php echo esc_html(krucial_get_php_max_execution()); ?>s</p></td>
		</tr>

	</table>
</div>

<?php } ?>