<?php

add_action( 'init', 'krucial_register_taxonomies', 0 );

function krucial_register_taxonomies() {
    $services_labels = array(
        'name'              => esc_html__( 'Service Categories', 'krucial_theme_functionality' ),
        'singular_name'     => esc_html__( 'Category', 'krucial_theme_functionality' ),
        'search_items'      => esc_html__( 'Search Categories', 'krucial_theme_functionality' ),
        'all_items'         => esc_html__( 'All Categories', 'krucial_theme_functionality' ),
        'parent_item'       => esc_html__( 'Parent Category', 'krucial_theme_functionality' ),
        'parent_item_colon' => esc_html__( 'Parent Category:', 'krucial_theme_functionality' ),
        'edit_item'         => esc_html__( 'Edit Category', 'krucial_theme_functionality' ),
        'update_item'       => esc_html__( 'Update Category', 'krucial_theme_functionality' ),
        'add_new_item'      => esc_html__( 'Add New Category', 'krucial_theme_functionality' ),
        'new_item_name'     => esc_html__( 'New Category Name', 'krucial_theme_functionality' ),
        'menu_name'         => esc_html__( 'Categories', 'krucial_theme_functionality' ),
    );

	// Add "Category" taxonomy to the Services post type
    $services_args = array(
        'hierarchical'      => true,
        'labels'            => $services_labels,
        'show_ui'           => true,
        'show_admin_column' => true,
        'query_var'         => true,
        'rewrite'           => array( 'slug' => 'category' ),
    );

    register_taxonomy( 'services_categories', array( 'services' ), $services_args );


	// Add "Category" taxonomy to the Projects post type
	$projects_labels = array(
		'name'              => esc_html__( 'Categorias Projetos', 'krucial_theme_functionality' ),
		'singular_name'     => esc_html__( 'Projeto', 'krucial_theme_functionality' ),
		'search_items'      => esc_html__( 'Procurar Projetos', 'krucial_theme_functionality' ),
		'all_items'         => esc_html__( 'Todas Projetos', 'krucial_theme_functionality' ),
		'parent_item'       => esc_html__( 'Família de Projeto', 'krucial_theme_functionality' ),
		'parent_item_colon' => esc_html__( 'Família de Projeto:', 'krucial_theme_functionality' ),
		'edit_item'         => esc_html__( 'Editar Projeto', 'krucial_theme_functionality' ),
		'update_item'       => esc_html__( 'Atualizar Projeto', 'krucial_theme_functionality' ),
		'add_new_item'      => esc_html__( 'Adicionar nova Projeto', 'krucial_theme_functionality' ),
		'new_item_name'     => esc_html__( 'Nova Projeto', 'krucial_theme_functionality' ),
		'menu_name'         => esc_html__( 'Projetos', 'krucial_theme_functionality' ),
	);

	$project_args = array(
		'hierarchical'      => true,
		'labels'            => $projects_labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'category' ),
	);
	register_taxonomy( 'project_categories', array( 'projects' ), $project_args );

	// Add "Category" taxonomy to the Staff Members post type
	$staff_labels = array(
		'name'              => esc_html__( 'Staff Categories', 'krucial_theme_functionality' ),
		'singular_name'     => esc_html__( 'Category', 'krucial_theme_functionality' ),
		'search_items'      => esc_html__( 'Search Categories', 'krucial_theme_functionality' ),
		'all_items'         => esc_html__( 'All Categories', 'krucial_theme_functionality' ),
		'parent_item'       => esc_html__( 'Parent Category', 'krucial_theme_functionality' ),
		'parent_item_colon' => esc_html__( 'Parent Category:', 'krucial_theme_functionality' ),
		'edit_item'         => esc_html__( 'Edit Category', 'krucial_theme_functionality' ),
		'update_item'       => esc_html__( 'Update Category', 'krucial_theme_functionality' ),
		'add_new_item'      => esc_html__( 'Add New Category', 'krucial_theme_functionality' ),
		'new_item_name'     => esc_html__( 'New Category Name', 'krucial_theme_functionality' ),
		'menu_name'         => esc_html__( 'Categories', 'krucial_theme_functionality' ),
	);

	$staff_args = array(
		'hierarchical'      => true,
		'labels'            => $staff_labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'category' ),
	);
	register_taxonomy( 'staff_categories', array( 'staff' ), $staff_args );


	// Add "Category" taxonomy to the Testimonials post type
	$testimonials_labels = array(
		'name'              => esc_html__( 'Testimonial Categories', 'krucial_theme_functionality' ),
		'singular_name'     => esc_html__( 'Category', 'krucial_theme_functionality' ),
		'search_items'      => esc_html__( 'Search Categories', 'krucial_theme_functionality' ),
		'all_items'         => esc_html__( 'All Categories', 'krucial_theme_functionality' ),
		'parent_item'       => esc_html__( 'Parent Category', 'krucial_theme_functionality' ),
		'parent_item_colon' => esc_html__( 'Parent Category:', 'krucial_theme_functionality' ),
		'edit_item'         => esc_html__( 'Edit Category', 'krucial_theme_functionality' ),
		'update_item'       => esc_html__( 'Update Category', 'krucial_theme_functionality' ),
		'add_new_item'      => esc_html__( 'Add New Category', 'krucial_theme_functionality' ),
		'new_item_name'     => esc_html__( 'New Category Name', 'krucial_theme_functionality' ),
		'menu_name'         => esc_html__( 'Categories', 'krucial_theme_functionality' ),
	);

	$testimonial_args = array(
		'hierarchical'      => true,
		'labels'            => $testimonials_labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'category' ),
	);
	register_taxonomy( 'testimonial_categories', array( 'testimonials' ), $testimonial_args );
	
	// Add "Category" taxonomy to the Ambientes post type
	$ambientes_labels = array(
		'name'              => esc_html__( 'Ambiente Categorias', 'krucial_theme_functionality' ),
		'singular_name'     => esc_html__( 'Categoria', 'krucial_theme_functionality' ),
		'search_items'      => esc_html__( 'Buscas Categorias', 'krucial_theme_functionality' ),
		'all_items'         => esc_html__( 'Todas Categorias', 'krucial_theme_functionality' ),
		'parent_item'       => esc_html__( 'Família Categoria', 'krucial_theme_functionality' ),
		'parent_item_colon' => esc_html__( 'Família Categoria:', 'krucial_theme_functionality' ),
		'edit_item'         => esc_html__( 'Editar Categoria', 'krucial_theme_functionality' ),
		'update_item'       => esc_html__( 'Atualizar Categoria', 'krucial_theme_functionality' ),
		'add_new_item'      => esc_html__( 'Add Nova Categoria', 'krucial_theme_functionality' ),
		'new_item_name'     => esc_html__( 'Novo Nome Categoria', 'krucial_theme_functionality' ),
		'menu_name'         => esc_html__( 'Categorias', 'krucial_theme_functionality' ),
	);

	$ambientes_args = array(
		'hierarchical'      => true,
		'labels'            => $ambientes_labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'category' ),
	);
	register_taxonomy( 'ambientes_categories', array( 'ambientes' ), $ambientes_args );

}