<?php

function krucial_vc_remove_fields() {
	if ( is_plugin_active( 'js_composer/js_composer.php' ) ) {

		// Remove fields from default elements
		if ( function_exists( 'vc_remove_param' ) ) {

			// Row
			vc_remove_param( 'vc_row', 'equal_height' );
			vc_remove_param( 'vc_row', 'content_placement' );
			vc_remove_param( 'vc_row', 'css_animation' );
			vc_remove_param( 'vc_row', 'disable_element' );

			// Column
			vc_remove_param( 'vc_column', 'css_animation' );

			// Text block
			vc_remove_param( 'vc_column_text', 'css_animation' );
			vc_remove_param( 'vc_column_text', 'el_id' );
			vc_remove_param( 'vc_column_text', 'el_class' );
			vc_remove_param( 'vc_column_text', 'css' );

			// Single image
			vc_remove_param( 'vc_single_image', 'title' );
			vc_remove_param( 'vc_single_image', 'add_caption' );
			vc_remove_param( 'vc_single_image', 'css_animation' );
			vc_remove_param( 'vc_single_image', 'el_id' );
			vc_remove_param( 'vc_single_image', 'el_class' );
			vc_remove_param( 'vc_single_image', 'css' );

			// Tabs
			vc_remove_param( 'vc_tta_tabs', 'style' );
			vc_remove_param( 'vc_tta_tabs', 'shape' );
			vc_remove_param( 'vc_tta_tabs', 'color' );
			vc_remove_param( 'vc_tta_tabs', 'no_fill_content_area' );
			vc_remove_param( 'vc_tta_tabs', 'spacing' );
			vc_remove_param( 'vc_tta_tabs', 'gap' );
			vc_remove_param( 'vc_tta_tabs', 'alignment' );
			vc_remove_param( 'vc_tta_tabs', 'pagination_style' );
			vc_remove_param( 'vc_tta_tabs', 'pagination_color' );
			vc_remove_param( 'vc_tta_tabs', 'css_animation' );
			vc_remove_param( 'vc_tta_tabs', 'el_id' );
			vc_remove_param( 'vc_tta_tabs', 'el_class' );
			vc_remove_param( 'vc_tta_tabs', 'css' );

			// Accordion
			vc_remove_param( 'vc_tta_accordion', 'style' );
			vc_remove_param( 'vc_tta_accordion', 'shape' );
			vc_remove_param( 'vc_tta_accordion', 'color' );
			vc_remove_param( 'vc_tta_accordion', 'no_fill' );
			vc_remove_param( 'vc_tta_accordion', 'spacing' );
			vc_remove_param( 'vc_tta_accordion', 'gap' );
			vc_remove_param( 'vc_tta_accordion', 'c_align' );
			vc_remove_param( 'vc_tta_accordion', 'css_animation' );
			vc_remove_param( 'vc_tta_accordion', 'el_id' );
			vc_remove_param( 'vc_tta_accordion', 'el_class' );
			vc_remove_param( 'vc_tta_accordion', 'css' );

			// Video player
			vc_remove_param( 'vc_video', 'css_animation' );
			vc_remove_param( 'vc_video', 'el_id' );
			vc_remove_param( 'vc_video', 'el_class' );
			vc_remove_param( 'vc_video', 'el_css' );
			vc_remove_param( 'vc_video', 'css' );

			// Google maps
			vc_remove_param( 'vc_gmaps', 'css_animation' );
			vc_remove_param( 'vc_gmaps', 'el_id' );
			vc_remove_param( 'vc_gmaps', 'el_class' );
			vc_remove_param( 'vc_gmaps', 'el_css' );
			vc_remove_param( 'vc_gmaps', 'css' );

			// Contact form 7
			vc_remove_param( 'contact-form-7', 'title' );
		}
	}
}
add_action( 'admin_init', 'krucial_vc_remove_fields' );