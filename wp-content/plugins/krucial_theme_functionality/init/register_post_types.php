<?php

function krucial_register_posttypes() {

    // Services
    $services_labels = array(
        'name'               => esc_html__( 'Services', 'krucial_theme_functionality' ),
        'singular_name'      => esc_html__( 'Service', 'krucial_theme_functionality' ),
        'menu_name'          => esc_html__( 'Services', 'krucial_theme_functionality' ),
        'name_admin_bar'     => esc_html__( 'Services', 'krucial_theme_functionality' ),
        'add_new'            => esc_html__( 'Add New', 'krucial_theme_functionality' ),
        'add_new_item'       => esc_html__( 'Add New Service', 'krucial_theme_functionality' ),
        'new_item'           => esc_html__( 'New Service', 'krucial_theme_functionality' ),
        'edit_item'          => esc_html__( 'Edit Service', 'krucial_theme_functionality' ),
        'view_item'          => esc_html__( 'View Service', 'krucial_theme_functionality' ),
        'all_items'          => esc_html__( 'All Services', 'krucial_theme_functionality' ),
        'search_items'       => esc_html__( 'Search Services', 'krucial_theme_functionality' ),
        'parent_item_colon'  => esc_html__( 'Parent Services:', 'krucial_theme_functionality' ),
        'not_found'          => esc_html__( 'No services found.', 'krucial_theme_functionality' ),
        'not_found_in_trash' => esc_html__( 'No services found in Trash.', 'krucial_theme_functionality' )
    );

    $services_args = array(
        'labels'             => $services_labels,
        'description'        => esc_html__( 'Description', 'krucial_theme_functionality' ),
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_nav_menus'  => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'service' ),
        'capability_type'    => 'post',
        'menu_icon'          => 'dashicons-schedule',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => null,
        'supports'           => array( 'title', 'editor', 'thumbnail', 'excerpt' )
    );

    register_post_type( 'services', $services_args );

	// Projects
	$projects_labels = array(
		'name'               => esc_html__( 'Projects', 'krucial_theme_functionality' ),
		'singular_name'      => esc_html__( 'Project', 'krucial_theme_functionality' ),
		'menu_name'          => esc_html__( 'Projects', 'krucial_theme_functionality' ),
		'name_admin_bar'     => esc_html__( 'Projects', 'krucial_theme_functionality' ),
		'add_new'            => esc_html__( 'Adicionar novo', 'krucial_theme_functionality' ),
		'add_new_item'       => esc_html__( 'Adicionar novo Project', 'krucial_theme_functionality' ),
		'new_item'           => esc_html__( 'Novo Project', 'krucial_theme_functionality' ),
		'edit_item'          => esc_html__( 'Editar Project', 'krucial_theme_functionality' ),
		'view_item'          => esc_html__( 'Ver MProject', 'krucial_theme_functionality' ),
		'all_items'          => esc_html__( 'Todos Project', 'krucial_theme_functionality' ),
		'categories'         => esc_html__( 'Categorias', 'krucial_theme_functionality' ),
		'search_items'       => esc_html__( 'Procurar Projects', 'krucial_theme_functionality' ),
		'parent_item_colon'  => esc_html__( 'Família de Projects:', 'krucial_theme_functionality' ),
		'not_found'          => esc_html__( 'Projects não localizados.', 'krucial_theme_functionality' ),
		'not_found_in_trash' => esc_html__( 'Projects não localizados na lixeira.', 'krucial_theme_functionality' )
	);

	$projects_args = array(
		'labels'             => $projects_labels,
		'description'        => esc_html__( 'Descrição.', 'krucial_theme_functionality' ),
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_nav_menus'  => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'project' ),
		'capability_type'    => 'post',
		'menu_icon'          => 'dashicons-images-alt',
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_position'      => null,
		'supports'           => array( 'title', 'editor', 'thumbnail', 'excerpt' )
	);

	register_post_type( 'projects', $projects_args );
	
		// Staff Members
	$staff_labels = array(
		'name'               => esc_html__( 'Staff', 'krucial_theme_functionality' ),
		'singular_name'      => esc_html__( 'Staff Member', 'krucial_theme_functionality' ),
		'menu_name'          => esc_html__( 'Staff Members', 'krucial_theme_functionality' ),
		'name_admin_bar'     => esc_html__( 'Staff', 'krucial_theme_functionality' ),
		'add_new'            => esc_html__( 'Add New', 'krucial_theme_functionality' ),
		'add_new_item'       => esc_html__( 'Add New Staff Member', 'krucial_theme_functionality' ),
		'new_item'           => esc_html__( 'New Staff Member', 'krucial_theme_functionality' ),
		'edit_item'          => esc_html__( 'Edit Staff Member', 'krucial_theme_functionality' ),
		'view_item'          => esc_html__( 'View Staff Members', 'krucial_theme_functionality' ),
		'all_items'          => esc_html__( 'All Staff', 'krucial_theme_functionality' ),
		'search_items'       => esc_html__( 'Search Staff Members', 'krucial_theme_functionality' ),
		'parent_item_colon'  => esc_html__( 'Parent Staff:', 'krucial_theme_functionality' ),
		'not_found'          => esc_html__( 'No staff members found.', 'krucial_theme_functionality' ),
		'not_found_in_trash' => esc_html__( 'No staff members found in Trash.', 'krucial_theme_functionality' )
	);

	$staff_args = array(
		'labels'             => $staff_labels,
		'description'        => esc_html__( 'Description.', 'krucial_theme_functionality' ),
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_nav_menus'  => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'members' ),
		'capability_type'    => 'post',
		'menu_icon'          => 'dashicons-groups',
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_position'      => null,
		'supports'           => array( 'title', 'editor', 'thumbnail', 'excerpt')
	);

	register_post_type( 'staff', $staff_args );


	// Testimonials
	$testimonial_labels = array(
		'name'               => esc_html__( 'Testimonials', 'krucial_theme_functionality' ),
		'singular_name'      => esc_html__( 'Testimonial', 'krucial_theme_functionality' ),
		'menu_name'          => esc_html__( 'Testimonials', 'krucial_theme_functionality' ),
		'name_admin_bar'     => esc_html__( 'Testimonials', 'krucial_theme_functionality' ),
		'add_new'            => esc_html__( 'Add New', 'krucial_theme_functionality' ),
		'add_new_item'       => esc_html__( 'Add New Testimonial', 'krucial_theme_functionality' ),
		'new_item'           => esc_html__( 'New Testimonial', 'krucial_theme_functionality' ),
		'edit_item'          => esc_html__( 'Edit Testimonial', 'krucial_theme_functionality' ),
		'view_item'          => esc_html__( 'View Testimonial', 'krucial_theme_functionality' ),
		'all_items'          => esc_html__( 'All Testimonials', 'krucial_theme_functionality' ),
		'search_items'       => esc_html__( 'Search Testimonials', 'krucial_theme_functionality' ),
		'parent_item_colon'  => esc_html__( 'Parent Testimonials:', 'krucial_theme_functionality' ),
		'not_found'          => esc_html__( 'No testimonials found.', 'krucial_theme_functionality' ),
		'not_found_in_trash' => esc_html__( 'No testimonials found in Trash.', 'krucial_theme_functionality' )
	);

	$testimonial_args = array(
		'labels'             => $testimonial_labels,
		'description'        => esc_html__( 'Description.', 'krucial_theme_functionality' ),
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_nav_menus'  => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'testimonial' ),
		'capability_type'    => 'post',
		'menu_icon'          => 'dashicons-format-chat',
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_position'      => null,
		'supports'           => array( 'title', 'editor', 'thumbnail', 'excerpt' )
	);

	register_post_type( 'testimonials', $testimonial_args );
	
	$ambientes_labels = array(
		'name'               => esc_html__( 'Ambientes', 'krucial_theme_functionality' ),
		'singular_name'      => esc_html__( 'Ambiente', 'krucial_theme_functionality' ),
		'menu_name'          => esc_html__( 'Ambientes', 'krucial_theme_functionality' ),
		'name_admin_bar'     => esc_html__( 'Ambientes', 'krucial_theme_functionality' ),
		'add_new'            => esc_html__( 'Add Ambiente', 'krucial_theme_functionality' ),
		'add_new_item'       => esc_html__( 'Add Novo Ambiente', 'krucial_theme_functionality' ),
		'new_item'           => esc_html__( 'New Ambiente', 'krucial_theme_functionality' ),
		'edit_item'          => esc_html__( 'Editar Ambiente', 'krucial_theme_functionality' ),
		'view_item'          => esc_html__( 'Ver Ambiente', 'krucial_theme_functionality' ),
		'all_items'          => esc_html__( 'Todos Ambientes', 'krucial_theme_functionality' ),
		'search_items'       => esc_html__( 'Procurar Ambientes', 'krucial_theme_functionality' ),
		'parent_item_colon'  => esc_html__( 'Família de Ambientes:', 'krucial_theme_functionality' ),
		'not_found'          => esc_html__( 'Não há ambientes.', 'krucial_theme_functionality' ),
		'not_found_in_trash' => esc_html__( 'Não há ambientes na lixeira.', 'krucial_theme_functionality' )
	);

	$ambientes_args = array(
		'labels'             => $ambientes_labels,
		'description'        => esc_html__( 'Descrição.', 'krucial_theme_functionality' ),
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_nav_menus'  => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'ambientes' ),
		'capability_type'    => 'post',
		'menu_icon'          => 'dashicons-screenoptions',
		'has_archive'        => false,
		'hierarchical'       => false,
		'menu_position'      => null,
		'supports'           => array( 'title', 'editor', 'thumbnail', 'excerpt' )
	);

	register_post_type( 'ambientes', $ambientes_args );

	
}

add_action( 'init', 'krucial_register_posttypes' );