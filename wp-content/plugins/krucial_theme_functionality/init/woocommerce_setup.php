<?php

/**
 * Remove WooCommerce Actions
 */
remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20 );
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_title', 5 );
remove_action( 'woocommerce_cart_collaterals', 'woocommerce_cross_sell_display' );


/**
 * Load on Relevant Pages Only
 */
function krucial_conditionally_load_woo(){

    if ( function_exists( 'is_woocommerce' ) ){

        // Only load CSS and JS on Woocommerce pages
        if(! is_woocommerce() && ! is_cart() && ! is_checkout() ) {

            // Dequeue scripts.
            wp_dequeue_script('woocommerce');
            wp_dequeue_script('wc-cart-fragments');

            // Deregister scripts.
            wp_deregister_script('wc-add-to-cart');

            // Dequeue styles.
            wp_dequeue_style('woocommerce-general');
            wp_dequeue_style('woocommerce-layout');
            wp_dequeue_style('woocommerce-smallscreen');

        }
    }
}
add_action( 'wp_enqueue_scripts', 'krucial_conditionally_load_woo', 200 );


/**
 * Register our own Select2
 */
function krucial_woo_select2() {
    if ( class_exists( 'WooCommerce' ) && ! is_cart() && ! is_checkout() && ! is_account_page() ) {
        wp_deregister_script('select2');
        wp_enqueue_script( 'select2', get_template_directory_uri() . '/assets/js/select2.min.js', array(), '', true );
    }
}
add_action( 'wp_enqueue_scripts', 'krucial_woo_select2', 200 );


/**
 * Change amount of related products on single product page
 */
function krucial_related_products_args( $args ) {
	$args['posts_per_page'] = 3;
	$args['columns'] = 1;
	return $args;
}
add_filter( 'woocommerce_output_related_products_args', 'krucial_related_products_args' );

?>