<?php

/**
 * Remove Redux Menu under Tools
 */
function krucial_remove_redux_menu() {
    remove_submenu_page('tools.php', 'redux-about');
}

add_action('admin_menu', 'krucial_remove_redux_menu', 12);


/**
 * Add google analytics script to head
 */
function krucial_google_analytics() {
    global $krucial_options;

    if (!empty($krucial_options['google_analytics'])) {
        echo $krucial_options['google_analytics'];
    }
}
add_action( 'wp_head', 'krucial_google_analytics' );

?>