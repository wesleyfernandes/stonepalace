<?php
/** Enable W3 Total Cache */
define('WP_CACHE', true); // Added by W3 Total Cache

/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa usar o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/pt-br:Editando_wp-config.php
 *
 * @package WordPress
 */

define ('WPLANG', 'pt_BR');
 
// ** Configurações do MySQL - Você pode pegar estas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define( 'DB_NAME', 'stonepal_bd' );

/** Usuário do banco de dados MySQL */
define( 'DB_USER', 'stonepal_user' );

/** Senha do banco de dados MySQL */
define( 'DB_PASSWORD', 'aYP$xJe1GYyv' );

/** Nome do host do MySQL */
define( 'DB_HOST', 'localhost' );

/** Charset do banco de dados a ser usado na criação das tabelas. */
define( 'DB_CHARSET', 'utf8mb4' );

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para invalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '=gdWU5OYzoa!HL~5:!>CE}gVBEPq(n`m33085ZI*p}%{>)1C#@:XX`oy5O*`psM/' );
define( 'SECURE_AUTH_KEY',  'z7^X>(Y2*nzkL:b:NJ{%b/6/}MOFFgA5>)i:,|}>gss7jv$974pz#9LMGkeh,^8s' );
define( 'LOGGED_IN_KEY',    'x RY3Y:p[6<Ok1QL.]0mdvQ_-$+.)m4}<i.UCC;j*vg)Hj59mHs-qnnXLOY3<TZ>' );
define( 'NONCE_KEY',        'Q~#(J,1_-M9bek8R_0dN^J?<_@../@A03kzk/7B`HWuJg?}Vd`-n#.[(>c~K).6p' );
define( 'AUTH_SALT',        'PZTExc&q<&o60[09wp>WFOf>E&z;dpn;H+JQe0Q}=v+m<:u(-EN(i>}R_9@Z7QiF' );
define( 'SECURE_AUTH_SALT', '3NJ`bvJI/4x4oqBh/<J;vI%ey>;0 H7ybgRxRAh2TA HEv8R#WxiI2Td$d,|PV?H' );
define( 'LOGGED_IN_SALT',   't~|@e`w5$vd%IipvJ`Hz8>;C(%Dh1YS<|QwqNP!dkB2;q=iCOXL$W`^:BE|S?3ic' );
define( 'NONCE_SALT',       'sL58`vx*7jd.RPA8*;ou3*XPcy=_Of&XT)$w&(!48#8%&y+hU@wdLp)`S-.*e9E ' );

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * um prefixo único para cada um. Somente números, letras e sublinhados!
 */
$table_prefix = 'wp_';

/**
 * Para desenvolvedores: Modo de debug do WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://codex.wordpress.org/pt-br:Depura%C3%A7%C3%A3o_no_WordPress
 */
define('WP_DEBUG', false);

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Configura as variáveis e arquivos do WordPress. */
require_once(ABSPATH . 'wp-settings.php');
define('ALLOW_UNFILTERED_UPLOADS', true);